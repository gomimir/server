module gitlab.com/gomimir/server

go 1.16

require (
	entgo.io/contrib v0.2.0
	entgo.io/ent v0.10.0
	github.com/99designs/gqlgen v0.16.0
	github.com/alicebob/miniredis/v2 v2.21.0
	github.com/bradleyjkemp/cupaloy v2.3.0+incompatible
	github.com/bradleyjkemp/cupaloy/v2 v2.6.0
	github.com/coreos/go-oidc/v3 v3.1.0
	github.com/davecgh/go-spew v1.1.1
	github.com/elastic/go-elasticsearch/v7 v7.10.0
	github.com/flynn/json5 v0.0.0-20160717195620-7620272ed633
	github.com/go-redis/redis/v8 v8.8.0
	github.com/gorilla/websocket v1.4.2
	github.com/graph-gophers/dataloader v5.0.0+incompatible
	github.com/hashicorp/go-multierror v1.1.1
	github.com/icza/gox v0.0.0-20210726201659-cd40a3f8d324
	github.com/imdario/mergo v0.3.12
	github.com/justinas/alice v1.2.0
	github.com/kylelemons/godebug v1.1.0 // indirect
	github.com/lestrrat-go/jwx v1.1.7
	github.com/minio/minio-go/v7 v7.0.63
	github.com/mitchellh/hashstructure/v2 v2.0.1
	github.com/mitchellh/mapstructure v1.4.3
	github.com/oklog/ulid/v2 v2.0.2
	github.com/opentracing/opentracing-go v1.2.0 // indirect
	github.com/pressly/goose/v3 v3.5.3
	github.com/prometheus/client_golang v1.4.0
	github.com/robertkrimen/otto v0.0.0-20200922221731-ef014fd054ac // indirect
	github.com/rs/cors v1.6.0
	github.com/rs/zerolog v1.20.0
	github.com/soheilhy/cmux v0.1.4
	github.com/spf13/cobra v1.3.0
	github.com/spf13/viper v1.10.0
	github.com/stretchr/testify v1.7.1-0.20210427113832-6241f9ab9942
	github.com/vektah/gqlparser/v2 v2.2.0
	github.com/vmihailenco/msgpack/v5 v5.3.5
	gitlab.com/gomimir/processor v0.31.0
	golang.org/x/oauth2 v0.0.0-20211104180415-d3ed0bb246c8
	golang.org/x/text v0.12.0
	google.golang.org/genproto v0.0.0-20211208223120-3a66f561d7aa
	google.golang.org/grpc v1.42.0
	google.golang.org/grpc/examples v0.0.0-20220120193159-9cb411380883 // indirect
	google.golang.org/protobuf v1.27.1
	gopkg.in/sourcemap.v1 v1.0.5 // indirect
	modernc.org/sqlite v1.14.6
)

// replace gitlab.com/gomimir/processor => ../processor
