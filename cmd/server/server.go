package main

import (
	mimirapp "gitlab.com/gomimir/processor/pkg/app"
	servercli "gitlab.com/gomimir/server/internal/cli/server"
)

// populated by build flags
var version string

func main() {
	app := mimirapp.NewApp("mimir-server", version)

	rootCmd := app.RootCommand()
	rootCmd.AddCommand(servercli.ServerCommand(app.AppInfo()))

	app.Execute()
}
