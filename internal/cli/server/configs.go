package servercli

import (
	"os"
	"regexp"

	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"gitlab.com/gomimir/server/internal/config"
)

var envRx = regexp.MustCompile(`^([^=]+)=(.*)$`)

func readConfig() *config.Config {
	// Read the environment variables
	vars := make(map[string]string)
	for _, pair := range os.Environ() {
		m := envRx.FindStringSubmatch(pair)
		if len(m) == 3 {
			vars[m[1]] = m[2]
		}
	}

	filename := viper.GetString("config")
	effectiveFilename := filename
	if filename == "" {
		effectiveFilename = "config/config.json5"
	}

	if _, err := os.Stat(effectiveFilename); err != nil {
		if filename == "" {
			config, err := config.GetDefaultConfig(vars)
			if err == nil {
				log.Info().Msg("No configuration provided, using defaults")
				return config
			}

			log.Fatal().Err(err).Msg("Unable to read default config")

		} else if os.IsNotExist(err) {
			log.Fatal().Str("filename", effectiveFilename).Msg("Config file does not exist")
		} else {
			log.Fatal().Str("filename", effectiveFilename).Err(err).Msg("Unable to read config file")
		}
	} else {
		contents, err := os.ReadFile(effectiveFilename)

		if err == nil {
			var resolved *config.Config
			resolved, err = config.ReadConfig(contents, vars)
			if err == nil {
				log.Info().Str("filename", effectiveFilename).Msg("Configuration loaded")
				return resolved
			}
		}

		log.Fatal().Str("filename", effectiveFilename).Err(err).Msg("Unable to read config file")
	}

	os.Exit(1)
	return nil
}
