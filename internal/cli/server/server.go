package servercli

import (
	"context"
	"os"

	"github.com/go-redis/redis/v8"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/processor/pkg/redistm"
	"gitlab.com/gomimir/server/internal/app"
	"gitlab.com/gomimir/server/internal/crawler"
	"gitlab.com/gomimir/server/internal/indexer/elasticsearch"
	"gitlab.com/gomimir/server/internal/requests/archivecreate"
	"gitlab.com/gomimir/server/internal/requests/archiveload"
	"gitlab.com/gomimir/server/internal/requests/cleantemp"
	"gitlab.com/gomimir/server/internal/server"
)

func ServerCommand(appInfo mimir.AppInfo) *cobra.Command {
	cmd := &cobra.Command{
		Use: "serve",
		Run: func(cmd *cobra.Command, args []string) {
			var err error
			log.Info().Str("version", appInfo.Version.String()).Msg("Starting server")
			app := app.NewApp(appInfo)

			app.InitializeWithConfig(readConfig())

			app.RedisClient = redis.NewClient(&redis.Options{
				Addr:     viper.GetString("redis-host"),
				Password: viper.GetString("redis-password"),
				DB:       viper.GetInt("redis-database"),
			})

			// Initialize queue manager
			app.TaskManager = redistm.NewRedisTaskManager(app.RedisClient)

			if err := app.TaskManager.EnsureReady(cmd.Context(), mimir.RetryOpts{}); err != nil {
				log.Fatal().Err(err).Msg("Queue manager is not ready")
				os.Exit(1)
			}

			log.Info().Str("addr", viper.GetString("redis-host")).Msg("Successfully connected to Redis")

			// Initialize indexer
			app.Indexer, err = elasticsearch.NewIndexer(app.Config.Elasticsearch)
			if err != nil {
				log.Fatal().Err(err).Msg("Unable to create Elasticsearch indexer")
			}

			if err := app.Indexer.EnsureReady(cmd.Context(), mimir.RetryOpts{}); err != nil {
				log.Fatal().Err(err).Msg("Indexer is not ready")
				os.Exit(1)
			}

			// FIXME: migrate to ent
			// err = app.Indexer.EnsureIndices(app.Config.IndexNames())
			// if err != nil {
			// 	log.Fatal().Err(err).Msg("Unable to provision Elasticsearch")
			// 	os.Exit(1)
			// }

			log.Info().Str("url", app.Config.Elasticsearch.Endpoint).Msg("Elasticsearch is app and running")

			// Register request handlers
			crawler.RegisterHandler(app)
			archiveload.RegisterHandler(app)
			archivecreate.RegisterHandler(app)
			cleantemp.RegisterHandler(app)

			doneC := app.Run(context.Background(), viper.GetString("queue"))

			startServer := func(name string, opts server.ServerOpts) {
				sublog := log.With().Str("addr", opts.Addr).Logger()
				sublog.Info().Msgf("Starting %v", name)

				err := server.Serve(app, opts)

				if err != nil {
					sublog.Fatal().Err(err).Msgf("Unable to start %v", name)
					os.Exit(1)
				}
			}

			go startServer(
				"server",
				server.ServerOpts{
					Addr:       viper.GetString("server-addr"),
					GraphQL:    server.HandlerOpts{Enabled: true},
					Playground: server.HandlerOpts{Enabled: true},
					Files:      server.FileHandlerOpts{HandlerOpts: server.HandlerOpts{Enabled: true}, SignatureKey: app.LinkSignatureKey},
				},
			)

			go startServer(
				"internal server",
				server.ServerOpts{
					Addr: viper.GetString("internal-addr"),
					GRPC: server.HandlerOpts{Enabled: true},
					Files: server.FileHandlerOpts{
						HandlerOpts:            server.HandlerOpts{Enabled: true},
						DisableSignatureChecks: true,
					},
				},
			)

			go startServer(
				"metrics server",
				server.ServerOpts{
					Addr:    viper.GetString("metrics-addr"),
					Metrics: server.HandlerOpts{Enabled: true},
				},
			)

			<-doneC
		},
	}

	cmd.Flags().String("config", "", "Path to configuration file")
	cmd.Flags().String("rules", "", "Path to rules file")
	cmd.Flags().String("redis-host", "127.0.0.1:6379", "Redis server address and port")
	cmd.Flags().Int("redis-database", 0, "Redis database")
	cmd.Flags().String("redis-password", "", "Redis database password")
	cmd.Flags().String("queue", "main", "Name of the task queue")
	cmd.Flags().String("server-addr", ":3000", "Interface on which server should listen for HTTP requests from users")
	cmd.Flags().String("internal-addr", ":3001", "Interface on which server should listen for HTTP requests from processors")
	cmd.Flags().String("metrics-addr", ":3002", "Interface on which server should listen for HTTP requests from prometheus")
	viper.BindPFlags(cmd.Flags())

	return cmd
}
