package memoryindexer

import (
	"context"
	"errors"

	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/apperror"
	"gitlab.com/gomimir/server/internal/documents"
	abstractindexer "gitlab.com/gomimir/server/internal/indexer"
)

// Indexer - dummy in-memory indexer, useful for tests
type Indexer struct {
	assets map[string]documents.IndexedAsset
}

func NewIndexer() *Indexer {
	return &Indexer{
		assets: make(map[string]documents.IndexedAsset),
	}
}

func (indexer *Indexer) EnsureReady(ctx context.Context, opts mimir.RetryOpts) error {
	return nil
}

func (indexer *Indexer) EnsureIndices(indexNames []string) error {
	return nil
}

func (indexer *Indexer) RefreshIndex(indexNames []string) error {
	return nil
}

func (indexer *Indexer) EnsureAsset(assetRef mimir.AssetRef, callback func(documents.IndexedAsset) documents.IndexedAsset) (*documents.IndexedAsset, error) {
	asset := documents.IndexedAsset{
		AssetRef: assetRef,
	}

	asset = callback(asset)

	indexer.assets[assetRef.String()] = asset

	return &asset, nil
}

func (indexer *Indexer) GetAsset(assetRef mimir.AssetRef) (*documents.IndexedAsset, error) {
	if asset, ok := indexer.assets[assetRef.String()]; ok {
		return &asset, nil
	}

	return nil, apperror.New(apperror.NotFound, "unable to find asset: %v", assetRef.String())
}

func (indexer *Indexer) UpdateAsset(assetRef mimir.AssetRef, callback func(documents.IndexedAsset) documents.IndexedAsset) (*documents.IndexedAsset, error) {
	return nil, errors.New("not implemented")
}

func (indexer *Indexer) DeleteAsset(assetRef mimir.AssetRef) error {
	return errors.New("not implemented")
}

func (indexer *Indexer) DeleteAssetsByQuery(indexNames []string, query *abstractindexer.AssetFilter) error {
	return errors.New("not implemented")
}

func (indexer *Indexer) ListAssets(opts abstractindexer.AssetListingOptions) (*abstractindexer.AssetListing, error) {
	return &abstractindexer.AssetListing{}, nil
}

func (indexer *Indexer) ScrollAssets(opts abstractindexer.ScrollAssetsOptions) <-chan abstractindexer.ScrollAssetsItem {
	c := make(chan abstractindexer.ScrollAssetsItem)
	close(c)
	return c
}

func (indexer *Indexer) UpdateAssetsByQuery(indexNames []string, query *abstractindexer.AssetFilter, callback func(documents.IndexedAsset) documents.IndexedAsset) (*abstractindexer.UpdateAssetsByQuerySummary, error) {
	return nil, errors.New("not implemented")
}

func (indexer *Indexer) GetTags(opts abstractindexer.TagListingOptions) ([]*abstractindexer.TagListingEntry, error) {
	return nil, nil
}
