package indexer

import "time"

type StatsRequest struct {
	Files   *AssetFileStatsRequest
	ByIndex *IndexStatsRequest
}

type StatsResult struct {
	Files   *AssetFileStatsResult
	ByIndex []IndexStatsResult
}

type IndexStatsRequest struct {
	Files *AssetFileStatsRequest
}

type IndexStatsResult struct {
	IndexName string
	Count     int
	Files     *AssetFileStatsResult
}

type FileStatsRequest struct {
	Count *FileCountRequest
	Size  *FileSizeRequest
}

type FileStatsResult struct {
	Count *int
	Size  *NumericStatsResult
}

type AssetFileStatsRequest struct {
	FileStatsRequest
	MainFilesOnly bool
	Sidecars      *FileStatsRequest
	PropertyStats map[string]*PropertyStatsRequest
}

type AssetFileStatsResult struct {
	FileStatsResult
	Sidecars      *FileStatsResult
	PropertyStats map[string]*PropertyStats
}

type FileCountRequest struct{}
type FileSizeRequest struct{}

type NumericStatsResult struct {
	Sum *int
}

type PropertyStatsRequest struct {
	Name      string
	TimeValue *TimeValueStatsRequest
}

type PropertyStats struct {
	TimeValue *TimeValueStats
}

type TimeValueStatsRequest struct {
	Min bool
	Max bool
}

type TimeValueStats struct {
	Min *time.Time
	Max *time.Time
}
