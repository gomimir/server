package indexer

import (
	"context"

	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/documents"
)

type TagAggsOptions struct {
	FetchThumbnails bool
	Kind            *string
}

// AssetListing - listing envelope for assets
type AssetListing struct {
	Items        []documents.IndexedAsset
	TotalCount   int
	FacetResults *FacetResults
	Stats        *StatsResult
}

// AssetListingOptions - Listing options for assets
type AssetListingOptions struct {
	IndexNames []string
	Skip       *int
	Size       *int
	Filter     *AssetFilter
	SortBy     []*SortRequest

	Facets FacetRequest
	Stats  StatsRequest
}

// TagListingOptions - Listing options for tags
type TagListingOptions struct {
	IndexNames      []string
	Kind            []string
	NameQuery       *string
	Sort            *TagSortRequest
	FetchCoverAsset bool
}

type TagListingEntry struct {
	Kind       string
	Name       string
	CoverAsset *documents.IndexedAsset
}

// UpdateAssetsByQuerySummary - result of UpdateAssetsByFilter
type UpdateAssetsByQuerySummary struct {
	NumMatched int
	NumChanged int
	NumFailed  int
}

type Index interface {
	Name() string
}

type ScrollAssetsItem struct {
	Error error
	Asset *documents.IndexedAsset
}

type ScrollAssetsOptions struct {
	IndexNames []string
	Filter     *AssetFilter
}

// Indexer - indexer abstraction
type Indexer interface {
	EnsureReady(ctx context.Context, opts mimir.RetryOpts) error

	EnsureIndices(indexNames []string) error
	RefreshIndex(indexNames []string) error

	GetAsset(assetRef mimir.AssetRef) (*documents.IndexedAsset, error)

	ListAssets(opts AssetListingOptions) (*AssetListing, error)
	ScrollAssets(opts ScrollAssetsOptions) <-chan ScrollAssetsItem

	UpdateAsset(assetRef mimir.AssetRef, callback func(documents.IndexedAsset) documents.IndexedAsset) (*documents.IndexedAsset, error)
	UpdateAssetsByQuery(indexNames []string, query *AssetFilter, callback func(documents.IndexedAsset) documents.IndexedAsset) (*UpdateAssetsByQuerySummary, error)

	DeleteAsset(assetRef mimir.AssetRef) error
	DeleteAssetsByQuery(indexNames []string, query *AssetFilter) error

	EnsureAsset(assetRef mimir.AssetRef, callback func(documents.IndexedAsset) documents.IndexedAsset) (*documents.IndexedAsset, error)

	GetTags(opts TagListingOptions) ([]*TagListingEntry, error)
}
