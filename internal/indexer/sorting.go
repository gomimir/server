package indexer

import (
	"fmt"
	"io"
	"strconv"
)

type SortMethod string

const (
	SortMethodAsc  SortMethod = "ASC"
	SortMethodDesc SortMethod = "DESC"
)

var AllSortMethod = []SortMethod{
	SortMethodAsc,
	SortMethodDesc,
}

func (e SortMethod) IsValid() bool {
	switch e {
	case SortMethodAsc, SortMethodDesc:
		return true
	}
	return false
}

func (e SortMethod) String() string {
	return string(e)
}

func (e *SortMethod) UnmarshalGQL(v interface{}) error {
	str, ok := v.(string)
	if !ok {
		return fmt.Errorf("enums must be strings")
	}

	*e = SortMethod(str)
	if !e.IsValid() {
		return fmt.Errorf("%s is not a valid SortMethod", str)
	}
	return nil
}

func (e SortMethod) MarshalGQL(w io.Writer) {
	fmt.Fprint(w, strconv.Quote(e.String()))
}

// -------------------------------------

type SortRequest struct {
	File     *FileSortRequest     `json:"file,omitempty"`
	Property *PropertySortRequest `json:"property,omitempty"`

	// Sorting method
	SortMethod *SortMethod `json:"sortMethod"`
}

// GetSortMethod - returns sort method with applied default
func (sr *SortRequest) GetSortMethod() SortMethod {
	if sr.SortMethod != nil {
		return *sr.SortMethod
	}

	return SortMethodAsc
}

// FileSortRequest - file sort request
// TODO: add file selector for configurable strategy (Elasticsearch mode + nested filter)
type FileSortRequest struct {
	Property   *PropertySortRequest
	Processing *ProcessingSort
}

// PropertySortRequest - property reference for sorting purposes
type PropertySortRequest struct {
	// Property name
	Name string `json:"name"`
	// Property type
	Type PropertyType `json:"type"`
}

// -------------------------------------
type ProcessingSort string

const (
	ProcessingSortLastFailedProcessedAt ProcessingSort = "LAST_FAILED_PROCESSED_AT"
)

// -------------------------------------

type TagSort string

const (
	TagSortLastFileDate       TagSort = "LAST_FILE_DATE"
	TagSortLastAssignedAt     TagSort = "LAST_ASSIGNED_AT"
	TagSortLastUserAssignedAt TagSort = "LAST_USER_ASSIGNED_AT"
)

func (e TagSort) String() string {
	return string(e)
}

type TagSortRequest struct {
	By     TagSort
	Method *SortMethod
}

// GetSortMethod - returns sort method with applied default
func (sr *TagSortRequest) GetSortMethod() SortMethod {
	if sr.Method != nil {
		return *sr.Method
	}

	return SortMethodAsc
}
