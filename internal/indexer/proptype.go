package indexer

type PropertyType string

const (
	PropertyTypeText    PropertyType = "TEXT"
	PropertyTypeNumber  PropertyType = "NUMBER"
	PropertyTypeTime    PropertyType = "TIME"
	PropertyTypeUnknown PropertyType = "UNKNOWN"
)
