package indexer

import mimir "gitlab.com/gomimir/processor"

// IDFilter - filter for narrowing down assets to list of IDs
type IDFilter struct {
	In    []mimir.AssetRef `json:"in"`
	NotIn []mimir.AssetRef `json:"notIn"`
}

func (f IDFilter) IsEmpty() bool {
	return f.In == nil && len(f.NotIn) == 0
}

// TextFilter - filter for text properties
type TextFilter struct {
	Eq         *string
	Matches    *string
	StartsWith *string
}

// IsEmpty - returns true if the filter has no filterable criteria
func (f TextFilter) IsEmpty() bool {
	return f.Eq == nil && f.Matches == nil && f.StartsWith == nil
}

// NumberFilter - filter for number properties
type NumberFilter struct {
	Eq  *float64 `json:"eq"`
	Gte *float64 `json:"gte"`
	Gt  *float64 `json:"gt"`
	Lte *float64 `json:"lte"`
	Lt  *float64 `json:"lt"`
}

// IsEmpty - returns true if the filter has no filterable criteria
func (f NumberFilter) IsEmpty() bool {
	// TODO: write some reflection util for this
	return f.Eq == nil && f.Gte == nil && f.Gt == nil && f.Lte == nil && f.Lt == nil
}

// TimeFilter - filter for time properties
type TimeFilter struct {
	Eq  *string `json:"eq"`
	Gte *string `json:"gte"`
	Gt  *string `json:"gt"`
	Lte *string `json:"lte"`
	Lt  *string `json:"lt"`
}

// IsEmpty - returns true if the filter has no filterable criteria
func (f TimeFilter) IsEmpty() bool {
	// TODO: write some reflection util for this
	return f.Eq == nil && f.Gte == nil && f.Gt == nil && f.Lte == nil && f.Lt == nil
}

// PropertyFilter - property filter
type PropertyFilter struct {
	Name string `json:"name"`

	TextValue    []TextFilter   `json:"textValue"`
	NumericValue []NumberFilter `json:"numericValue"`
	TimeValue    []TimeFilter   `json:"timeValue"`
}

// IsEmpty - returns true if the filter has no filterable criteria
func (f PropertyFilter) IsEmpty() bool {
	// Note: property filter without value criteria is still considered not empty
	// we interpret it as "has property"

	return f.Name == ""
}

// --------------------
type ErrorFilter struct {
	Code    []TextFilter `json:"code"`
	Message []TextFilter `json:"message"`
}
type ProcessingFilter struct {
	Name  []TextFilter `json:"name"`
	JobID *string      `json:"jobId"`
	Error *ErrorFilter
}

// AssetFileFilter - file filter
type AssetFileFilter struct {
	RepositoryName []TextFilter       `json:"repository"`
	Filename       []TextFilter       `json:"filename"`
	WithProperty   []PropertyFilter   `json:"withProperty"`
	WithProcessing []ProcessingFilter `json:"withProcessing"`
}

// IsEmpty - returns true if the filter has no filterable criteria
func (f AssetFileFilter) IsEmpty() bool {
	for _, tf := range f.RepositoryName {
		if !tf.IsEmpty() {
			return false
		}
	}

	for _, tf := range f.Filename {
		if !tf.IsEmpty() {
			return false
		}
	}

	for _, propFilter := range f.WithProperty {
		if !propFilter.IsEmpty() {
			return false
		}
	}

	return len(f.WithProcessing) == 0
}

func (f AssetFileFilter) WithoutProperty(propName string) AssetFileFilter {
	var withProperties []PropertyFilter
	for _, propFilter := range f.WithProperty {
		if propFilter.Name != propName {
			withProperties = append(withProperties, propFilter)
		}
	}

	f.WithProperty = withProperties
	return f
}

// --------------------

type TagFilter struct {
	Kind string   `json:"kind"`
	Name []string `json:"name"`
}

// --------------------

// AssetFilter - asset listing filter
type AssetFilter struct {
	Fulltext     *string          `json:"fulltext"`
	ID           *IDFilter        `json:"id"`
	WithFile     *AssetFileFilter `json:"withFile"`
	WithProperty []PropertyFilter `json:"withProperty"`
	WithTag      []TagFilter      `json:"withTag"`
}

// IsEmpty - returns true if the filter has no filterable criteria
func (f AssetFilter) IsEmpty() bool {
	if f.ID != nil && !f.ID.IsEmpty() {
		return false
	}

	if f.WithFile != nil && !f.WithFile.IsEmpty() {
		return false
	}

	if f.Fulltext != nil && *f.Fulltext != "" {
		return false
	}

	if len(f.WithTag) > 0 {
		return false
	}

	for _, propFilter := range f.WithProperty {
		if !propFilter.IsEmpty() {
			return false
		}
	}

	return true
}

func (f AssetFilter) WithoutFilePropertyFilter(propName string) AssetFilter {
	if f.WithFile != nil {
		wp := f.WithFile.WithoutProperty(propName)
		f.WithFile = &wp
	}

	return f
}

func (f AssetFilter) WithoutProperty(propName string) AssetFilter {
	var withProperties []PropertyFilter
	for _, propFilter := range f.WithProperty {
		if propFilter.Name != propName {
			withProperties = append(withProperties, propFilter)
		}
	}

	f.WithProperty = withProperties
	return f
}
