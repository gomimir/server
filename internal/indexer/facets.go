package indexer

import (
	"time"

	"gitlab.com/gomimir/server/internal/documents"
)

type Facet interface{ ID() string }
type BaseFacet struct{ id string }

func (f BaseFacet) ID() string        { return f.id }
func FacetWithID(id string) BaseFacet { return BaseFacet{id} }

// ---------------------------------------

type FacetRequest struct {
	TagFacets                   []TagFacet
	TagKindFacets               []TagKindFacet
	PropertyFacets              []PropertyFacet
	PropertyIntrospectionFacets []PropertyIntrospectionFacet
	FileFacets                  []FileFacet
}

type FacetResults struct {
	TagFacetResults                   []TagFacetResult
	TagKindFacetResults               []TagKindFacetResult
	PropertyFacetResults              []PropertyFacetResult
	PropertyIntrospectionFacetResults []PropertyIntrospectionFacetResult
	FileFacetResults                  []FileFacetResult
}

// ---------------------------------------
type FileFacet struct {
	BaseFacet
	PropertyFacets              []PropertyFacet
	PropertyIntrospectionFacets []PropertyIntrospectionFacet
}

type FileFacetResult struct {
	Facet                             FileFacet
	PropertyFacetResults              []PropertyFacetResult
	PropertyIntrospectionFacetResults []PropertyIntrospectionFacetResult
}

// ---------------------------------------

type PropertyIntrospectionFacet struct {
	BaseFacet
}

type PropertyIntrospectionFacetResult struct {
	Facet         PropertyIntrospectionFacet
	Introspection []*PropertyIntrospection
}

type PropertyIntrospection struct {
	// Property name
	Name string

	// Property type
	Type PropertyType

	// Number how many documents have this property
	Count int
}

// ---------------------------------------

type PropertyFacet struct {
	BaseFacet
	PropertyName string
	Children     []Facet
}

type PropertyFacetResult struct {
	Facet    PropertyFacet
	Children []interface{}
}

// ---------------------------------------

type TextValueFacet struct {
	BaseFacet
	SortBy     *string
	SortMethod *string
}

type TextValueFacetResult struct {
	Facet   TextValueFacet
	Buckets []*TextValueBucket
}

type TextValueBucket struct {
	Value     string
	NumAssets int
}

// --------

type TagKindFacet struct {
	BaseFacet
}

type TagKindFacetResult struct {
	Facet TagKindFacet
	Kinds []*TagKindBucket
}

type TagKindBucket struct {
	Kind    string
	NumTags int
}

// --------

type TagFacet struct {
	BaseFacet
	Kind string
}

type TagFacetResult struct {
	Facet   TagFacet
	Buckets []*TagBucket
}

type TagBucket struct {
	Kind       string
	Name       string
	NumAssets  int
	CoverAsset *documents.IndexedAsset
}

// --------

type TimeRange struct {
	Name     string  `json:"key"`
	FromExpr *string `json:"from" mapstructure:"from"`
	ToExpr   *string `json:"to" mapstructure:"to"`
}
type TimeRangeFacet struct {
	BaseFacet
	Ranges            []TimeRange
	TimeZoneOffsetStr string
}

type ResolvedTimeExpression struct {
	Expression string    `json:"expression"`
	Value      time.Time `json:"value"`
}

type ResolvedTimeRange struct {
	Name string                  `json:"name"`
	From *ResolvedTimeExpression `json:"from"`
	To   *ResolvedTimeExpression `json:"to"`
}

type TimeRangeBucket struct {
	Range     ResolvedTimeRange `json:"range"`
	NumAssets int               `json:"numAssets"`
}
type TimeRangeFacetResult struct {
	Facet   TimeRangeFacet
	Buckets []*TimeRangeBucket
}
