package elasticsearch

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"time"

	"github.com/elastic/go-elasticsearch/v7/esapi"
	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/apperror"
	"gitlab.com/gomimir/server/internal/documents"
	"gitlab.com/gomimir/server/internal/indexer/elasticsearch/searchreq"
	"gitlab.com/gomimir/server/internal/utils"
)

func (indexer *Indexer) updateAsset(indexName string, documentId string, callback func(documents.IndexedAsset) documents.IndexedAsset) (*updateResult, error) {
	sublog := log.With().Str("index", indexName).Str("document", documentId).Logger()

	for i := 1; i < 5; i++ {
		asset, err := indexer.getAsset(indexName, documentId)
		if err != nil {
			return nil, err
		}

		res, err := indexer.updateOptimisticAsset(*asset, callback)
		if err != nil {
			if esError, ok := err.(*elasticsearchError); ok {
				if esError.ErrorPayload().Type == "version_conflict_engine_exception" {
					sublog.Warn().Msg("version conflict detected while updating an asset")
				} else {
					return nil, err
				}
			} else {
				return nil, err
			}
		} else {
			return res, nil
		}
	}

	return nil, fmt.Errorf("version conflict occurred while updating asset, exceeded the maximum number of tries")
}

type updateResult struct {
	Asset   documents.IndexedAsset
	Changed bool
}

func (indexer *Indexer) updateOptimisticAsset(asset optimisticAsset, callback func(documents.IndexedAsset) documents.IndexedAsset) (*updateResult, error) {
	sublog := log.With().Str("asset", asset.AssetRef.String()).Logger()

	try := 1
	for {
		updatedAsset := callback(documents.IndexedAsset{
			Asset:    asset.Asset.Clone(),
			AssetRef: asset.AssetRef,
		})

		if updatedAsset.Equal(&asset.Asset) {
			return &updateResult{
				Asset:   asset.IndexedAsset,
				Changed: false,
			}, nil
		}

		updatePayload, err := json.Marshal(updatedAsset)
		if err != nil {
			return nil, err
		}

		// prevPayload, err := json.Marshal(asset.Asset)
		// if err != nil {
		// 	return nil, err
		// }

		// sublog.Trace().Str("body", string(updatePayload)).Str("prev", string(prevPayload)).Msg("reindexing asset")

		updateReq := esapi.IndexRequest{
			Index:         asset.AssetRef.IndexName(),
			DocumentID:    asset.AssetRef.AssetID(),
			OpType:        "index",
			IfPrimaryTerm: &asset.PrimaryTerm,
			IfSeqNo:       &asset.SeqNo,
			Body:          bytes.NewReader(updatePayload),
		}

		if err := indexer.doRequest(updateReq, nil); err != nil {
			if esError, ok := err.(*elasticsearchError); ok {
				if esError.ErrorPayload().Type == "version_conflict_engine_exception" {
					try++
					if try <= 5 {
						sublog.Debug().Msg("version conflict detected while updating an asset, trying again")
						newAsset, err := indexer.getAsset(asset.AssetRef.IndexName(), asset.AssetRef.AssetID())
						if err != nil {
							return nil, err
						}

						asset = *newAsset

						continue
					} else {
						err = fmt.Errorf("version conflict occurred while updating asset, exceeded the maximum number of tries")
					}
				}
			}

			sublog.Error().Err(err).Msg("Unable to update an asset")
			return nil, err
		} else {
			return &updateResult{
				Asset:   updatedAsset,
				Changed: true,
			}, nil
		}
	}
}

func (indexer *Indexer) getAsset(indexName string, documentId string) (*optimisticAsset, error) {
	req := esapi.GetRequest{
		Index:      indexName,
		DocumentID: documentId,
	}

	var body elasticsearchGetBody
	if err := indexer.doRequest(req, &body); err != nil {
		return nil, err
	}

	asset, err := documents.IndexedAssetFromData(indexName, documentId, body.Source)
	if err != nil {
		return nil, err
	}

	return &optimisticAsset{
		IndexedAsset: *asset,
		PrimaryTerm:  body.PrimaryTerm,
		SeqNo:        body.SeqNo,
	}, nil
}

func (indexer *Indexer) doRequest(req esapi.Request, body interface{}) error {
	for i := 0; i < 2; i++ {
		ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
		defer cancel()

		res, err := req.Do(ctx, indexer.client)
		if err != nil {
			if err == context.DeadlineExceeded {
				err = errors.New("connection timeout")
			}

			if err == io.EOF {
				log.Warn().Msg("Connection has been closed by server. Will try it again after 5s in case we hit rate limitter")
				time.Sleep(5 * time.Second)
				continue
			}

			return err
		}

		if err = getError(res); err != nil {
			return err
		}

		if body != nil {
			err = json.NewDecoder(res.Body).Decode(body)
			if err != nil {
				return err
			}
		}

		return nil
	}

	return io.EOF
}

// ensureIndices - ensures that all configured indices exist and their config is up to date
func (indexer *Indexer) EnsureIndices(indexNames []string) error {
	for _, indexName := range indexNames {
		err := indexer.ensureIndex(indexName, nil)
		if err != nil {
			return err
		}
	}

	return nil
}

// ensureIndex - creates index with given name and mappings if does not exist
func (indexer *Indexer) ensureIndex(indexName string, configFilename *string) error {
	sublog := log.Logger
	var configBytes = []byte(defaultIndexConfig)
	if configFilename != nil {
		sublog = log.With().Str("filename", *configFilename).Logger()

		if _, err := os.Stat(*configFilename); os.IsNotExist(err) {
			sublog.Error().Msg("Index configuration file was not found")
			return err
		}

		f, err := os.Open(*configFilename)
		if err != nil {
			sublog.Error().Err(err).Msg("Unable to open index configuration")
			return err
		}

		defer f.Close()

		bytes, err := ioutil.ReadAll(f)
		if err != nil {
			sublog.Error().Err(err).Msg("Unable to read index configuration")
			return err
		}

		configBytes = bytes
	}

	var data map[string]interface{}
	err := json.Unmarshal(configBytes, &data)
	if err != nil {
		sublog.Error().Err(err).Msg("Unable to parse the index configuration")
		return err
	}

	return indexer.ensureIndexWithConfig(indexName, data)
}

func (indexer *Indexer) ensureIndexWithConfig(indexName string, config map[string]interface{}) error {
	indexSublog := log.With().Str("index", indexName).Logger()

	exists, err := indexer.checkIndexExists(indexName)
	if err != nil {
		return nil
	}

	if exists {
		indexer.checkIndex(indexName, config)
	} else {
		indexSublog.Info().Msg("Creating index")

		payload, err := json.Marshal(config)
		if err != nil {
			indexSublog.Error().Err(err).Msg("Unable to serialize mappings")
			return err
		}

		req := esapi.IndicesCreateRequest{
			Index: indexName,
			Body:  bytes.NewReader(payload),
		}

		var body map[string]interface{}
		if err := indexer.doRequest(req, &body); err != nil {
			indexSublog.Error().Err(err).Msg("Unable to create index")
			return err
		}
	}

	return nil
}

func (indexer *Indexer) checkIndexExists(indexName string) (bool, error) {
	indexSublog := log.With().Str("index", indexName).Logger()

	req := esapi.IndicesExistsRequest{
		Index: []string{indexName},
	}

	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	res, err := req.Do(ctx, indexer.client)
	if err != nil {
		indexSublog.Error().Err(err).Msg("Unable check index existence")
		return false, err
	}

	return res.StatusCode == 200, nil
}

func (indexer *Indexer) validateRef(ref mimir.AssetRef) error {
	if !ref.IsValid() {
		return apperror.New(apperror.InvalidID, "invalid asset ID: %v", ref.String())
	}

	return nil
}

func (indexer *Indexer) checkIndex(indexName string, expected map[string]interface{}) error {
	indexSublog := log.With().Str("index", indexName).Logger()

	getIndexReq := esapi.IndicesGetRequest{
		Index: []string{indexName},
	}

	var getBody map[string]interface{}
	if err := indexer.doRequest(getIndexReq, &getBody); err != nil {
		indexSublog.Error().Err(err).Msg("Unable to retrieve index configuration")
		return err
	}

	if indexInfo, ok := getBody[indexName]; ok {
		if indexInfo, ok := indexInfo.(map[string]interface{}); ok {
			// Check mapping
			if expectedMappings, ok := expected["mappings"]; ok {
				if expectedMappings, ok := expectedMappings.(map[string]interface{}); ok {
					if mappings, ok := indexInfo["mappings"]; ok {
						if mappings, ok := mappings.(map[string]interface{}); ok {
							if len(utils.ObjMatch(expectedMappings, mappings)) > 0 {
								indexSublog.Warn().Msg("Index mappings are not up to date!")
							}
						}
					}
				}
			}

			// Check index settings
			if expectedIndexSettings, ok := expected["settings"]; ok {
				if expectedIndexSettings, ok := expectedIndexSettings.(map[string]interface{}); ok {
					if settings, ok := indexInfo["settings"]; ok {
						if settings, ok := settings.(map[string]interface{}); ok {
							if indexSettings, ok := settings["index"]; ok {
								if indexSettings, ok := indexSettings.(map[string]interface{}); ok {
									if len(utils.ObjMatch(expectedIndexSettings, indexSettings)) > 0 {
										indexSublog.Warn().Msg("Index settings are not up to date!")
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return nil
}

type scrollResult struct {
	Error error
	Hit   *searchreq.ElasticsearchHit
}

func (indexer *Indexer) scroll(req searchreq.SearchRequest) <-chan scrollResult {
	resultC := make(chan scrollResult, 1)

	if req.Scroll == 0 {
		resultC <- scrollResult{Error: errors.New("no scroll set")}
		close(resultC)
		return resultC
	}

	if req.Size == nil {
		size := 30
		req.Size = &size
	}

	go func() {
		processHits := func(hits []searchreq.ElasticsearchHit) {
			for _, hit := range hits {
				hitDeref := hit
				resultC <- scrollResult{Hit: &hitDeref}
			}
		}

		var initialBody searchreq.ElasticsearchSearchBody
		if err := indexer.doRequest(&req, &initialBody); err != nil {
			resultC <- scrollResult{Error: err}
			close(resultC)
			return
		}

		if initialBody.ScrollID == nil {
			resultC <- scrollResult{Error: errors.New("missing scroll id")}
			close(resultC)
			return
		}

		scrollID := *initialBody.ScrollID

		processHits(initialBody.Hits.Hits)

		if len(initialBody.Hits.Hits) <= initialBody.Hits.Total.Value {
			for {
				scrollReq := esapi.ScrollRequest{
					ScrollID: scrollID,
					Scroll:   req.Scroll,
				}

				var scrollBody searchreq.ElasticsearchScrollBody
				if err := indexer.doRequest(scrollReq, &scrollBody); err != nil {
					resultC <- scrollResult{Error: err}
					break
				}

				if scrollBody.ScrollID == nil {
					resultC <- scrollResult{Error: errors.New("missing scroll id")}
					break
				}

				scrollID = *scrollBody.ScrollID

				processHits(scrollBody.Hits.Hits)

				if len(scrollBody.Hits.Hits) < *req.Size {
					break
				}
			}
		}

		// Clear the scroll
		clearScrollReq := esapi.ClearScrollRequest{
			ScrollID: []string{scrollID},
		}

		if err := indexer.doRequest(clearScrollReq, nil); err != nil {
			resultC <- scrollResult{Error: err}
		}

		resultC <- scrollResult{Error: io.EOF}
		close(resultC)
	}()

	return resultC
}
