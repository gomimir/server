package elasticsearch

import (
	"github.com/elastic/go-elasticsearch/v7"
	"gitlab.com/gomimir/server/internal/config"
)

// Indexer - indexer object
type Indexer struct {
	client *elasticsearch.Client
}

// NewIndexer - constructor
func NewIndexer(config config.ElasticsearchIndexerConfig) (*Indexer, error) {

	es, err := elasticsearch.NewClient(elasticsearch.Config{
		Addresses: []string{
			config.Endpoint,
		},
	})

	if err != nil {
		return nil, err
	}

	return &Indexer{
		client: es,
	}, nil
}
