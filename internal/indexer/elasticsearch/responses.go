package elasticsearch

import (
	"encoding/json"
	"strings"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/gomimir/server/internal/documents"
)

type elasticsearchErrorCause struct {
	Type      string  `json:"type"`
	Reason    string  `json:"reason"`
	IndexUUID *string `json:"index_uuid,omitempty"`
	Index     *string `json:"index,omitempty"`
}

type elasticsearchErrorResponse struct {
	Body       map[string]interface{}
	StatusCode int
	StatusText string
}

type elastisearchErrorPayload struct {
	Index        *string                   `mapstructure:"index,omitempty"`
	Reason       string                    `mapstructure:"reason"`
	Type         string                    `mapstructure:"type"`
	Grouped      *bool                     `mapstructure:"grouped,omitempty"`
	RootCause    []elasticsearchErrorCause `mapstructure:"root_cause,omitempty"`
	FailedShards []map[string]interface{}  `mapstructure:"failed_shards,omitempty"`
	CausedBy     *elasticsearchErrorCause  `mapstructure:"caused_by,omitempty"`

	invalid bool
}

type elasticsearchError struct {
	Response     elasticsearchErrorResponse
	errorPayload *elastisearchErrorPayload
}

func (err *elasticsearchError) ErrorPayload() *elastisearchErrorPayload {
	if err.errorPayload == nil {
		var mapped struct {
			Error  elastisearchErrorPayload
			Status int
		}

		if mapstructure.Decode(err.Response.Body, &mapped) == nil {
			err.errorPayload = &mapped.Error
		} else {
			err.errorPayload = &elastisearchErrorPayload{invalid: true}
		}
	}

	return err.errorPayload
}

func (err *elasticsearchError) Error() string {
	payload := err.ErrorPayload()

	if !payload.invalid {
		reasons := []string{}

		if payload.Reason != "" {
			reasons = append(reasons, payload.Reason)
		}

		if len(payload.RootCause) > 0 {
			for _, cause := range payload.RootCause {
				if cause.Reason != "" && cause.Reason != payload.Reason {
					reasons = append(reasons, cause.Reason)
				}
			}
		} else if payload.CausedBy != nil {
			if payload.CausedBy.Reason != "" && payload.CausedBy.Reason != payload.Reason {
				reasons = append(reasons, payload.CausedBy.Reason)
			}
		}

		return strings.Join(reasons, ", ")
	}

	return err.Response.StatusText
}

type elasticsearchGetBody struct {
	ID          string          `json:"_id"`
	Version     int             `json:"_version"`
	PrimaryTerm int             `json:"_primary_term"`
	SeqNo       int             `json:"_seq_no"`
	Source      json.RawMessage `json:"_source"`
}

type optimisticAsset struct {
	documents.IndexedAsset

	PrimaryTerm int
	SeqNo       int
}
