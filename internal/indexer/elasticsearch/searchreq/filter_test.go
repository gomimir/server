package searchreq_test

import (
	"encoding/json"
	"testing"

	"github.com/bradleyjkemp/cupaloy/v2"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/indexer"
	"gitlab.com/gomimir/server/internal/indexer/elasticsearch/searchreq"
)

func TestFilterReq(t *testing.T) {
	textValue := "foo"
	textValue2 := "bar"

	req := searchreq.SearchRequest{
		Filter: &indexer.AssetFilter{
			WithProperty: []indexer.PropertyFilter{
				{Name: "textProp", TextValue: []indexer.TextFilter{{Eq: &textValue}}},
			},
			WithFile: &indexer.AssetFileFilter{
				WithProperty: []indexer.PropertyFilter{
					{Name: "fileTextProp", TextValue: []indexer.TextFilter{{Matches: &textValue2}}},
				},
			},
		},
	}

	body, err := req.BuildBody()
	if assert.NoError(t, err) {
		bytes, _ := json.MarshalIndent(body, "", "  ")

		snapshotter := cupaloy.New(cupaloy.SnapshotFileExtension(".json"))
		snapshotter.SnapshotT(t, string(bytes))
	}
}
