package searchreq_test

import (
	"encoding/json"
	"testing"

	"github.com/bradleyjkemp/cupaloy/v2"
	"github.com/icza/gox/gox"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/indexer"
	"gitlab.com/gomimir/server/internal/indexer/elasticsearch/searchreq"
)

func TestTagAggsAll(t *testing.T) {
	testTagAggsWithOpts(t, indexer.TagListingOptions{
		IndexNames: []string{"photos"},
	})
}

func TestTagAggsAllWithCovers(t *testing.T) {
	testTagAggsWithOpts(t, indexer.TagListingOptions{
		IndexNames:      []string{"photos"},
		FetchCoverAsset: true,
	})
}

func TestTagAggsOfKind(t *testing.T) {
	testTagAggsWithOpts(t, indexer.TagListingOptions{
		IndexNames: []string{"photos"},
		Kind:       []string{"album"},
	})
}

func TestTagAggsOfKindSortedByFileDate(t *testing.T) {
	m := indexer.SortMethodDesc

	testTagAggsWithOpts(t, indexer.TagListingOptions{
		IndexNames: []string{"photos"},
		Kind:       []string{"album"},
		Sort: &indexer.TagSortRequest{
			By:     indexer.TagSortLastFileDate,
			Method: &m,
		},
	})
}

func TestTagAggsOfKindAndName(t *testing.T) {
	testTagAggsWithOpts(t, indexer.TagListingOptions{
		IndexNames: []string{"photos"},
		Kind:       []string{"album"},
		NameQuery:  gox.NewString("2019"),
	})
}

func TestTagAggsOfName(t *testing.T) {
	testTagAggsWithOpts(t, indexer.TagListingOptions{
		IndexNames: []string{"photos"},
		NameQuery:  gox.NewString("2019"),
	})
}

func testTagAggsWithOpts(t *testing.T, opts indexer.TagListingOptions) {
	aggs := searchreq.BuildTagAggs(opts)
	payload := map[string]interface{}{
		"size": 0,
		"aggs": aggs.Aggregations(),
	}

	body, err := json.MarshalIndent(payload, "", " ")
	if assert.NoError(t, err) {
		snapshotter := cupaloy.New(cupaloy.SnapshotFileExtension(".json"))
		snapshotter.SnapshotT(t, body)
	}
}
