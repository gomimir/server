package searchreq

import (
	"fmt"
	"strings"

	"gitlab.com/gomimir/server/internal/indexer"
)

// translateSort - translates sort requests into Search parameter for Elastic
func translateSort(sortReq []*indexer.SortRequest) ([]map[string]interface{}, error) {
	var result []map[string]interface{}

	for idx, req := range sortReq {
		sortMethod := translateSortMethod(req.GetSortMethod())

		if req.File != nil {
			if req.File.Property != nil {
				propertyPath, unmappedPropertyType, err := translateProperty(req.File.Property)
				if err != nil {
					return nil, fmt.Errorf("unable to translate sort request #%v: %v", idx, err)
				}

				result = append(result, map[string]interface{}{
					fmt.Sprintf("files.properties.%v", propertyPath): map[string]interface{}{
						// TODO: make this configurable
						"mode":          "min",
						"order":         sortMethod,
						"unmapped_type": unmappedPropertyType,
						"nested": map[string]interface{}{
							"path": "files",
							"nested": map[string]interface{}{
								"path": "files.properties",
								"filter": map[string]interface{}{
									"term": map[string]interface{}{
										"files.properties.name": req.File.Property.Name,
									},
								},
							},
						},
					},
				})
			}

			if req.File.Processing != nil {
				if req.File.Property != nil {
					return nil, fmt.Errorf("unable to translate sort request #%v: contains both file property and processing sort requests", idx)
				}

				result = append(result, map[string]interface{}{
					"files.processings.processedAt": map[string]interface{}{
						"mode":          "max",
						"order":         sortMethod,
						"unmapped_type": "date",
						"nested": map[string]interface{}{
							"path": "files",
							"nested": map[string]interface{}{
								"path": "files.processings",
								"filter": map[string]interface{}{
									"exists": map[string]interface{}{
										"field": "files.processings.error",
									},
								},
							},
						},
					},
				})
			}
		}

		if req.Property != nil {
			if req.File != nil {
				return nil, fmt.Errorf("unable to translate sort request #%v: contains both file and property sort requests", idx)
			}

			propertyPath, unmappedPropertyType, err := translateProperty(req.Property)
			if err != nil {
				return nil, fmt.Errorf("unable to translate sort request #%v: %v", idx, err)
			}

			result = append(result, map[string]interface{}{
				fmt.Sprintf("properties.%v", propertyPath): map[string]interface{}{
					"order":         sortMethod,
					"unmapped_type": unmappedPropertyType,
					"nested": map[string]interface{}{
						"path": "properties",
						"filter": map[string]interface{}{
							"term": map[string]interface{}{
								"properties.name": req.Property.Name,
							},
						},
					},
				},
			})
		}
	}

	return result, nil
}

func translateSortMethod(method indexer.SortMethod) string {
	return strings.ToLower(method.String())
}

func translateProperty(prop *indexer.PropertySortRequest) (string, string, error) {
	switch prop.Type {
	case indexer.PropertyTypeText:
		return "textValue", "keyword", nil
	case indexer.PropertyTypeNumber:
		return "numericValue", "double", nil
	case indexer.PropertyTypeTime:
		return "timeValue", "date", nil
	default:
		return "", "", fmt.Errorf("unexpected sort property type %v for property %v", prop.Type, prop.Name)
	}
}
