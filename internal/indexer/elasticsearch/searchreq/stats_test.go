package searchreq_test

import (
	"encoding/json"
	"testing"

	"github.com/bradleyjkemp/cupaloy/v2"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/indexer"
	"gitlab.com/gomimir/server/internal/indexer/elasticsearch/searchreq"

	_ "embed"
)

//go:embed "fixtures/indexStatsResponse.json"
var indexStatsResponse string

func TestFilesStats(t *testing.T) {
	testStatsAggs(t, indexer.StatsRequest{
		Files: &indexer.AssetFileStatsRequest{
			FileStatsRequest: indexer.FileStatsRequest{
				Count: &indexer.FileCountRequest{},
				Size:  &indexer.FileSizeRequest{},
			},
			Sidecars: &indexer.FileStatsRequest{
				Count: &indexer.FileCountRequest{},
				Size:  &indexer.FileSizeRequest{},
			},
		},
	})
}

var indexStatsReq = indexer.StatsRequest{
	ByIndex: &indexer.IndexStatsRequest{
		Files: &indexer.AssetFileStatsRequest{
			FileStatsRequest: indexer.FileStatsRequest{
				Count: &indexer.FileCountRequest{},
				Size:  &indexer.FileSizeRequest{},
			},
			Sidecars: &indexer.FileStatsRequest{
				Count: &indexer.FileCountRequest{},
				Size:  &indexer.FileSizeRequest{},
			},
		},
	},
}

func TestIndexStats(t *testing.T) {
	testStatsAggs(t, indexStatsReq)
}

func TestIndexStatsResponse(t *testing.T) {
	var data map[string]interface{}
	if !assert.NoError(t, json.Unmarshal([]byte(indexStatsResponse), &data)) {
		return
	}

	result, err := searchreq.TranslateStatsResult(indexStatsReq, data["aggregations"].(map[string]interface{}))
	if assert.NoError(t, err) {
		cupaloy.SnapshotT(t, result)
	}
}

func testStatsAggs(t *testing.T, req indexer.StatsRequest) {
	aggs, err := searchreq.BuildStatsAggs(req)
	if assert.NoError(t, err) {

		payload := map[string]interface{}{
			"size": 0,
			"aggs": aggs.Aggregations(),
		}

		body, err := json.MarshalIndent(payload, "", " ")
		if assert.NoError(t, err) {
			snapshotter := cupaloy.New(cupaloy.SnapshotFileExtension(".json"))
			snapshotter.SnapshotT(t, body)
		}
	}
}
