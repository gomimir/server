package searchreq

import "encoding/json"

type ElasticsearchHit struct {
	ID          string          `json:"_id" mapstructure:"_id"`
	Index       string          `json:"_index" mapstructure:"_index"`
	Score       float64         `json:"_score" mapstructure:"_score"`
	Source      json.RawMessage `json:"_source" mapstructure:"_source"`
	Type        string          `json:"_type" mapstructure:"_type"`
	PrimaryTerm *int            `json:"_primary_term" mapstructure:"_primary_term,omitempty"`
	SeqNo       *int            `json:"_seq_no" mapstructure:"_seq_no,omitempty"`
}

type ElasticsearchHits struct {
	Hits     []ElasticsearchHit
	MaxScore float64            `json:"max_score"`
	Total    ElasticsearchTotal `json:"total"`
}

type ElasticsearchTotal struct {
	Relation string `json:"relation"`
	Value    int    `json:"value"`
}

type ElasticsearchSearchBody struct {
	ScrollID     *string                `json:"_scroll_id,omitempty"`
	Hits         ElasticsearchHits      `json:"hits"`
	Aggregations map[string]interface{} `json:"aggregations,omitempty"`
}

type ElasticsearchScrollBody struct {
	ScrollID *string           `json:"_scroll_id,omitempty"`
	Hits     ElasticsearchHits `json:"hits"`
}
