package searchreq

import (
	"bytes"
	"context"
	"encoding/json"
	"time"

	"github.com/elastic/go-elasticsearch/v7/esapi"
	"gitlab.com/gomimir/server/internal/indexer"
	"gitlab.com/gomimir/server/internal/utils"
)

type SearchRequest struct {
	IndexNames []string
	Filter     *indexer.AssetFilter
	SortBy     []*indexer.SortRequest

	Facets indexer.FacetRequest
	Stats  indexer.StatsRequest

	Skip *int
	Size *int

	Scroll time.Duration

	SeqNoPrimaryTerm *bool
}

func (req *SearchRequest) BuildBody() (map[string]interface{}, error) {
	body := make(map[string]interface{})

	f := indexer.AssetFilter{}
	if req.Filter != nil {
		f = *req.Filter
	}

	// Filter
	queryParam := TranslateFilters(f)
	if queryParam != nil {
		body["query"] = queryParam
	}

	// Sort
	searchParam, err := translateSort(req.SortBy)
	if err != nil {
		return nil, err
	}

	if searchParam != nil {
		body["sort"] = searchParam
	}

	// Facets
	facetAggs, err := TranslateFacets(req.Facets, f)
	if err != nil {
		return nil, err
	}

	// Stats
	statsAggs, err := BuildStatsAggs(req.Stats)
	if err != nil {
		return nil, err
	}

	aggs := utils.MergeMaps(facetAggs.Aggregations(), statsAggs.Aggregations())

	if len(aggs) > 0 {
		body["aggs"] = aggs
	}

	return body, nil
}

// Do - implementing esapi.Request
func (req *SearchRequest) Do(ctx context.Context, transport esapi.Transport) (*esapi.Response, error) {
	esapiReq := esapi.SearchRequest{
		Index:            req.IndexNames,
		From:             req.Skip,
		Size:             req.Size,
		SeqNoPrimaryTerm: req.SeqNoPrimaryTerm,
		Scroll:           req.Scroll,
	}

	body, err := req.BuildBody()
	if err != nil {
		return nil, err
	}

	if len(body) > 0 {
		payload, err := json.MarshalIndent(body, "", " ")
		if err != nil {
			return nil, err
		}

		// fmt.Println(string(payload))

		esapiReq.Body = bytes.NewReader(payload)
	}

	return esapiReq.Do(ctx, transport)
}
