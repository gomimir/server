package searchreq_test

import (
	"encoding/json"
	"testing"

	"github.com/bradleyjkemp/cupaloy/v2"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/indexer"
	"gitlab.com/gomimir/server/internal/indexer/elasticsearch/searchreq"
)

func TestSortPropReq(t *testing.T) {
	m := indexer.SortMethodDesc

	req := searchreq.SearchRequest{
		SortBy: []*indexer.SortRequest{
			// Asset property sort
			{Property: &indexer.PropertySortRequest{Name: "someTextProp", Type: indexer.PropertyTypeText}},

			// File property sort
			{
				File: &indexer.FileSortRequest{
					Property: &indexer.PropertySortRequest{Name: "fileTextProp", Type: indexer.PropertyTypeText},
				},
				SortMethod: &m,
			},
		},
	}

	testSearchReq(t, req)
}

func TestSortFileProcessingReq(t *testing.T) {
	m := indexer.SortMethodDesc
	p := indexer.ProcessingSortLastFailedProcessedAt

	req := searchreq.SearchRequest{
		SortBy: []*indexer.SortRequest{
			{
				File: &indexer.FileSortRequest{
					Processing: &p,
				},
				SortMethod: &m,
			},
		},
	}

	testSearchReq(t, req)
}

func testSearchReq(t *testing.T, req searchreq.SearchRequest) {
	body, err := req.BuildBody()
	if assert.NoError(t, err) {
		bytes, _ := json.MarshalIndent(body, "", "  ")

		snapshotter := cupaloy.New(cupaloy.SnapshotFileExtension(".json"))
		snapshotter.SnapshotT(t, string(bytes))
	}
}
