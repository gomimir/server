package searchreq_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/indexer/elasticsearch/searchreq"
)

func TestReverseNestedAggToJSON(t *testing.T) {
	nestedAgg := searchreq.NestedAgg{
		Nested: searchreq.NestedAggOpts{
			Path: "files",
		},
	}

	nestedAgg.AddChild("fileToAsset", searchreq.ReverseNestedAgg{})

	bytes, err := json.Marshal(nestedAgg)
	if assert.NoError(t, err) {
		assert.Equal(t, `{"aggs":{"fileToAsset":{"reverse_nested":{}}},"nested":{"path":"files"}}`, string(bytes))
	}
}
