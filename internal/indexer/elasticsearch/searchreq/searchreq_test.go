package searchreq_test

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/indexer"
	"gitlab.com/gomimir/server/internal/indexer/elasticsearch/searchreq"
)

func TestBuildBody(t *testing.T) {
	req := searchreq.SearchRequest{
		Facets: indexer.FacetRequest{
			TagFacets: []indexer.TagFacet{
				{
					BaseFacet: indexer.FacetWithID("allTags"),
				},
			},
		},
	}

	body, err := req.BuildBody()

	if assert.NoError(t, err) {
		jsonBytes, err := json.MarshalIndent(body, "", "  ")
		if assert.NoError(t, err) {
			fmt.Println(string(jsonBytes))
		}
	}
}
