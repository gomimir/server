package searchreq

import (
	"fmt"
	"time"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/gomimir/server/internal/indexer"
)

func BuildStatsAggs(req indexer.StatsRequest) (AggContainer, error) {
	rootAgg := BaseAgg{}

	if req.Files != nil {
		appendFileStatsAggs(&rootAgg, *req.Files)
	}

	if req.ByIndex != nil {
		indexAgg := rootAgg.AddTerms("indexStats", TermAggOpts{
			Field: "_index",
		})

		if req.ByIndex.Files != nil {
			appendFileStatsAggs(indexAgg, *req.ByIndex.Files)
		}
	}

	return rootAgg, nil
}

func appendFileStatsAggs(rootAgg ChainableAgg, req indexer.AssetFileStatsRequest) {
	fileFilter := map[string]interface{}{}

	if req.MainFilesOnly {
		fileFilter["term"] = map[string]interface{}{
			"files.main": true,
		}
	} else {
		fileFilter["match_all"] = map[string]interface{}{}
	}

	filesAgg := rootAgg.AddNested("fileStats", NestedAggOpts{Path: "files"}).AddFilter("fileFilter", fileFilter)

	// File size sum
	if req.Size != nil {
		filesAgg.AddSum("sumFileSize", "files.fileInfo.size")
	}

	// File property statistics
	if len(req.PropertyStats) > 0 {
		filesAgg.AddChild("filePropertyStats", propertyStats(req.PropertyStats, "files.properties"))
	}

	if req.Sidecars != nil {
		sidecarsAgg := filesAgg.AddNested("sidecarStats", NestedAggOpts{Path: "files.sidecars"})
		if req.Sidecars.Size != nil {
			sidecarsAgg.AddSum("sumFileSize", "files.sidecars.file.fileInfo.size")
		}
	}
}

func propertyStats(propStatsReq map[string]*indexer.PropertyStatsRequest, propertiesPath string) NestedAgg {
	fileProperties := NestedAgg{Nested: NestedAggOpts{Path: propertiesPath}}

	for alias, propStat := range propStatsReq {
		propFilter := fileProperties.AddFilter(alias, TermFilter{
			Name:  fmt.Sprintf("%v.name", propertiesPath),
			Value: propStat.Name,
		})

		if propStat.TimeValue != nil {
			if propStat.TimeValue.Min {
				propFilter.AddMin("minTimeValue", fmt.Sprintf("%v.timeValue", propertiesPath))
			}

			if propStat.TimeValue.Max {
				propFilter.AddMax("maxTimeValue", fmt.Sprintf("%v.timeValue", propertiesPath))
			}
		}
	}

	return fileProperties
}

type fileStatsResponse struct {
	Count        int `mapstructure:"doc_count"`
	SidecarStats *struct {
		Count       int `mapstructure:"doc_count"`
		SumFileSize *struct {
			Value int
		}
	}
	SumFileSize *struct {
		Value int
	}
	FilePropertyStats map[string]interface{}
}

func TranslateStatsResult(req indexer.StatsRequest, aggData map[string]interface{}) (*indexer.StatsResult, error) {
	result := &indexer.StatsResult{}

	var parsed struct {
		FileStats *struct {
			FileFilter *fileStatsResponse
		}
		IndexStats *struct {
			Buckets []struct {
				Key       string
				Count     int `mapstructure:"doc_count"`
				FileStats *struct {
					FileFilter *fileStatsResponse
				}
			}
		}
	}

	err := mapstructure.Decode(aggData, &parsed)
	if err != nil {
		return nil, err
	}

	if parsed.FileStats != nil {
		result.Files = translateFileStatsResult(req.Files, parsed.FileStats.FileFilter)
	}

	if parsed.IndexStats != nil {
		result.ByIndex = make([]indexer.IndexStatsResult, len(parsed.IndexStats.Buckets))
		for i, bucket := range parsed.IndexStats.Buckets {
			result.ByIndex[i] = indexer.IndexStatsResult{
				IndexName: bucket.Key,
				Count:     bucket.Count,
			}

			if bucket.FileStats != nil {
				result.ByIndex[i].Files = translateFileStatsResult(req.ByIndex.Files, bucket.FileStats.FileFilter)
			}
		}
	}

	return result, nil
}

func translateFileStatsResult(req *indexer.AssetFileStatsRequest, response *fileStatsResponse) *indexer.AssetFileStatsResult {
	if response != nil {
		result := &indexer.AssetFileStatsResult{
			FileStatsResult: indexer.FileStatsResult{
				Count: &response.Count,
			},
		}

		if len(req.PropertyStats) > 0 && response.FilePropertyStats != nil {
			propRes, err := translatePropStatsResults(req.PropertyStats, response.FilePropertyStats)
			if err != nil {
				return nil
			}

			result.PropertyStats = propRes
		}

		if response.SumFileSize != nil {
			result.FileStatsResult.Size = &indexer.NumericStatsResult{
				Sum: &response.SumFileSize.Value,
			}
		}

		if response.SidecarStats != nil {
			result.Sidecars = &indexer.FileStatsResult{
				Count: &response.SidecarStats.Count,
			}

			if response.SidecarStats.SumFileSize != nil {
				result.Sidecars.Size = &indexer.NumericStatsResult{
					Sum: &response.SidecarStats.SumFileSize.Value,
				}
			}
		}

		return result
	}

	return nil
}

func translatePropStatsResults(req map[string]*indexer.PropertyStatsRequest, data map[string]interface{}) (map[string]*indexer.PropertyStats, error) {
	res := make(map[string]*indexer.PropertyStats)

	for alias, propReq := range req {
		propData := data[alias]
		if propData != nil {
			res[alias] = &indexer.PropertyStats{}

			if propReq.TimeValue != nil {
				var parsed struct {
					MinTimeValue struct {
						Value string `mapstructure:"value_as_string"`
					}
					MaxTimeValue struct {
						Value string `mapstructure:"value_as_string"`
					}
				}

				err := mapstructure.Decode(propData, &parsed)
				if err != nil {
					return nil, err
				}

				res[alias].TimeValue = &indexer.TimeValueStats{}

				min, err := time.Parse(time.RFC3339Nano, parsed.MinTimeValue.Value)
				if err == nil {
					res[alias].TimeValue.Min = &min
				}

				max, err := time.Parse(time.RFC3339Nano, parsed.MaxTimeValue.Value)
				if err == nil {
					res[alias].TimeValue.Max = &max
				}
			}
		}
	}

	return res, nil
}
