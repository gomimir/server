package searchreq

import (
	"strings"

	"github.com/icza/gox/gox"
	"gitlab.com/gomimir/server/internal/indexer"
)

func BuildTagAggs(req indexer.TagListingOptions) AggContainer {
	rootAgg := BaseAgg{}
	var chainableAgg ChainableAgg = rootAgg.AddNested("assetToTags", NestedAggOpts{
		Path: "tags",
	})

	// Apply tag kind filter
	if req.Kind != nil {
		chainableAgg = chainableAgg.AddFilter("kindFilter", map[string]interface{}{
			"terms": map[string]interface{}{
				"tags.kind": req.Kind,
			},
		})
	}

	// Apply name query matching
	if req.NameQuery != nil {
		chainableAgg = chainableAgg.AddFilter("matchTagName", MultiMatchFilter{
			Fields: []string{"tags.name.prefix", "tags.name.prefix._2gram", "tags.name.prefix._3gram", "tags.name.prefix._index_prefix"},
			Query:  *req.NameQuery,
			Type:   "bool_prefix",
		})
	}

	// For each tag name
	multiTermsAggOpts := MultiTermsAggOpts{
		Terms: []MultiTermsTerm{
			{Field: "tags.name.raw"},
			{Field: "tags.kind"},
		},
		Size: gox.NewInt(10000),
	}

	if req.Sort != nil {
		if req.Sort.By == indexer.TagSortLastFileDate {
			multiTermsAggOpts.Order = map[string]string{
				"tagToAsset>assetToFiles>mainFile>mainFileProperties>fileDateProperty>lastFileDate": strings.ToLower(req.Sort.GetSortMethod().String()),
			}
		} else if req.Sort.By == indexer.TagSortLastAssignedAt {
			multiTermsAggOpts.Order = map[string]string{
				"tagToReason>lastAssignmentTime": strings.ToLower(req.Sort.GetSortMethod().String()),
			}
		} else if req.Sort.By == indexer.TagSortLastUserAssignedAt {
			multiTermsAggOpts.Order = map[string]string{
				"tagToReason>userTagAssignment>lastAssignmentTime": strings.ToLower(req.Sort.GetSortMethod().String()),
			}
		}
	}

	chainableAgg = chainableAgg.AddMultiTerms("byNameAndKind", multiTermsAggOpts)

	tagToAsset := chainableAgg.AddReverseNested("tagToAsset")

	if req.FetchCoverAsset {
		tagToAsset.AddFilter("assetsWithThumbnail", NestedFilter{
			Path: "files.sidecars",
			Query: TermFilter{
				Name:  "files.sidecars.kind",
				Value: "thumbnail",
			},
		}).AddTopHits("coverAsset", map[string]interface{}{
			"sort": []map[string]interface{}{
				{
					"properties.numericValue": map[string]interface{}{
						"order":         "desc",
						"unmapped_type": "double",
						"nested": map[string]interface{}{
							"path": "properties",
							"filter": map[string]interface{}{
								"term": map[string]interface{}{
									"properties.name": "priority",
								},
							},
						},
					},
				},
			},
			"size": 1,
			// "_source": []string{"key"},
		})
	}

	if req.Sort != nil {
		if req.Sort.By == indexer.TagSortLastFileDate {
			tagToAsset.AddNested("assetToFiles", NestedAggOpts{
				Path: "files",
			}).AddFilter("mainFile", map[string]interface{}{
				"term": map[string]interface{}{
					"files.main": true,
				},
			}).AddNested("mainFileProperties", NestedAggOpts{
				Path: "files.properties",
			}).AddFilter("fileDateProperty", map[string]interface{}{
				"term": map[string]interface{}{
					"files.properties.name": "date",
				},
			}).AddChild("lastFileDate", map[string]interface{}{
				"max": map[string]interface{}{
					"field": "files.properties.timeValue",
				},
			})
		} else {
			var reasonAgg ChainableAgg = chainableAgg.AddNested("tagToReason", NestedAggOpts{
				Path: "tags.reasons",
			})

			if req.Sort.By == indexer.TagSortLastUserAssignedAt {
				reasonAgg = reasonAgg.AddFilter("userTagAssignment", map[string]interface{}{
					"term": map[string]interface{}{
						"tags.reasons.kind": "user",
					},
				})
			}

			reasonAgg.AddChild("lastAssignmentTime", map[string]interface{}{
				"max": map[string]interface{}{
					"field": "tags.reasons.createdAt",
				},
			})
		}
	}

	return rootAgg
}
