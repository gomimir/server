package searchreq

import (
	"fmt"

	"gitlab.com/gomimir/server/internal/indexer"
)

type AggContainer interface {
	Aggregations() map[string]interface{}
}

type ChainableAgg interface {
	AddChild(name string, child interface{})
	AddTerms(name string, opts TermAggOpts) *TermsAgg
	AddMultiTerms(name string, opts MultiTermsAggOpts) *MultiTermsAgg
	AddNested(name string, opts NestedAggOpts) *NestedAgg
	AddReverseNested(name string) *ReverseNestedAgg
	AddFilter(name string, filter interface{}) *FilterAgg
	AddTopHits(name string, opts map[string]interface{}) *TopHitsAgg
	AddDateRange(name string, opts DateRangeAggOpts) *DateRangeAgg
	AddValueCount(name string, field string) *ValueCountAgg
	AddCardinality(name string, field string) *CardinalityAgg
	Aggregations() map[string]interface{}
}

type BaseAgg struct {
	Children map[string]interface{} `json:"aggs,omitempty"`
}

func (agg *BaseAgg) AddChild(name string, child interface{}) {
	if agg.Children == nil {
		agg.Children = make(map[string]interface{})
	}

	agg.Children[name] = child
}

func (agg *BaseAgg) AddGlobal(name string) *GlobalAgg {
	child := &GlobalAgg{}
	agg.AddChild(name, child)
	return child
}

func (agg *BaseAgg) AddTerms(name string, opts TermAggOpts) *TermsAgg {
	child := &TermsAgg{Terms: opts}
	agg.AddChild(name, child)
	return child
}

func (agg *BaseAgg) AddMultiTerms(name string, opts MultiTermsAggOpts) *MultiTermsAgg {
	child := &MultiTermsAgg{MultiTerms: opts}
	agg.AddChild(name, child)
	return child
}

func (agg *BaseAgg) AddNested(name string, opts NestedAggOpts) *NestedAgg {
	child := &NestedAgg{Nested: opts}
	agg.AddChild(name, child)
	return child
}

func (agg *BaseAgg) AddReverseNested(name string) *ReverseNestedAgg {
	child := &ReverseNestedAgg{}
	agg.AddChild(name, child)
	return child
}

func (agg *BaseAgg) AddFilter(name string, filter interface{}) *FilterAgg {
	child := &FilterAgg{Filter: filter}
	agg.AddChild(name, child)
	return child
}

func (agg *BaseAgg) AddTopHits(name string, opts map[string]interface{}) *TopHitsAgg {
	child := &TopHitsAgg{TopHits: opts}
	agg.AddChild(name, child)
	return child
}

func (agg *BaseAgg) AddDateRange(name string, opts DateRangeAggOpts) *DateRangeAgg {
	child := &DateRangeAgg{DateRange: opts}
	agg.AddChild(name, child)
	return child
}

func (agg *BaseAgg) AddValueCount(name string, field string) *ValueCountAgg {
	child := &ValueCountAgg{ValueCount: ValueCountOpts{Field: field}}
	agg.AddChild(name, child)
	return child
}

func (agg *BaseAgg) AddSum(name string, field string) *SumAgg {
	child := &SumAgg{Sum: SumOpts{Field: field}}
	agg.AddChild(name, child)
	return child
}

func (agg *BaseAgg) AddMin(name string, field string) *MinAgg {
	child := &MinAgg{Min: MinOpts{Field: field}}
	agg.AddChild(name, child)
	return child
}

func (agg *BaseAgg) AddMax(name string, field string) *MaxAgg {
	child := &MaxAgg{Max: MaxOpts{Field: field}}
	agg.AddChild(name, child)
	return child
}

func (agg *BaseAgg) AddCardinality(name string, field string) *CardinalityAgg {
	child := &CardinalityAgg{Cardinality: CardinalityOpts{Field: field}}
	agg.AddChild(name, child)
	return child
}

func (agg BaseAgg) Aggregations() map[string]interface{} {
	return agg.Children
}

type NestedAggOpts struct {
	Path string `json:"path"`
}

// Nested aggregation
// https://www.elastic.co/guide/en/elasticsearch/reference/7.x/search-aggregations-bucket-nested-aggregation.html
type NestedAgg struct {
	BaseAgg
	Nested NestedAggOpts `json:"nested"`
}

// Reverse nested aggegation
// https://www.elastic.co/guide/en/elasticsearch/reference/7.x/search-aggregations-bucket-reverse-nested-aggregation.html
type ReverseNestedAgg struct {
	BaseAgg
	ReverseNestedAgg struct{} `json:"reverse_nested"`
}

type GlobalAgg struct {
	BaseAgg
	GlobalAgg struct{} `json:"global"`
}

type TermAggOpts struct {
	Field string            `json:"field"`
	Order map[string]string `json:"order,omitempty"`
	Size  *int              `json:"size,omitempty"`
}

// Terms aggregation
// https://www.elastic.co/guide/en/elasticsearch/reference/7.x/search-aggregations-bucket-terms-aggregation.html
type TermsAgg struct {
	BaseAgg
	Terms TermAggOpts `json:"terms"`
}

type MultiTermsTerm struct {
	Field string `json:"field"`
}
type MultiTermsAggOpts struct {
	Terms []MultiTermsTerm  `json:"terms"`
	Order map[string]string `json:"order,omitempty"`
	Size  *int              `json:"size,omitempty"`
}

// Multi terms aggregation
// https://www.elastic.co/guide/en/elasticsearch/reference/7.x/search-aggregations-bucket-multi-terms-aggregation.html
type MultiTermsAgg struct {
	BaseAgg
	MultiTerms MultiTermsAggOpts `json:"multi_terms"`
}

// Filter aggregation
// https://www.elastic.co/guide/en/elasticsearch/reference/7.x/search-aggregations-bucket-filter-aggregation.html
type FilterAgg struct {
	BaseAgg
	Filter interface{} `json:"filter"`
}

// Top hits aggregation
// https://www.elastic.co/guide/en/elasticsearch/reference/7.x/search-aggregations-metrics-top-hits-aggregation.html
type TopHitsAgg struct {
	BaseAgg
	TopHits map[string]interface{} `json:"top_hits"`
}

// Date range aggregation
// https://www.elastic.co/guide/en/elasticsearch/reference/7.x/search-aggregations-bucket-daterange-aggregation.html
type DateRangeAgg struct {
	BaseAgg
	DateRange DateRangeAggOpts `json:"date_range"`
}

type DateRangeAggOpts struct {
	Field    string              `json:"field"`
	Ranges   []indexer.TimeRange `json:"ranges"`
	TimeZone string              `json:"time_zone"`
}

// Value count aggregation
// https://www.elastic.co/guide/en/elasticsearch/reference/7.x/search-aggregations-metrics-valuecount-aggregation.html
type ValueCountAgg struct {
	BaseAgg
	ValueCount ValueCountOpts `json:"value_count"`
}

type ValueCountOpts struct {
	Field string `json:"field"`
}

// Sum aggregation
// https://www.elastic.co/guide/en/elasticsearch/reference/7.x/search-aggregations-metrics-sum-aggregation.html
type SumAgg struct {
	BaseAgg
	Sum SumOpts `json:"sum"`
}

type SumOpts struct {
	Field string `json:"field"`
}

// Min aggregation
// https://www.elastic.co/guide/en/elasticsearch/reference/7.x/search-aggregations-metrics-min-aggregation.html
type MinAgg struct {
	BaseAgg
	Min MinOpts `json:"min"`
}

type MinOpts struct {
	Field string `json:"field"`
}

// Max aggregation
// https://www.elastic.co/guide/en/elasticsearch/reference/7.x/search-aggregations-metrics-max-aggregation.html
type MaxAgg struct {
	BaseAgg
	Max MaxOpts `json:"max"`
}

type MaxOpts struct {
	Field string `json:"field"`
}

// Cardinality aggregation
// https://www.elastic.co/guide/en/elasticsearch/reference/7.x/search-aggregations-metrics-cardinality-aggregation.html
type CardinalityAgg struct {
	BaseAgg
	Cardinality CardinalityOpts `json:"cardinality"`
}

type CardinalityOpts struct {
	Field string `json:"field"`
}

// Composite aggregation
// https://www.elastic.co/guide/en/elasticsearch/reference/7.x/search-aggregations-bucket-composite-aggregation.html
type CompositeAgg struct {
	BaseAgg
	Composite CompositeAggOpts `json:"composite"`
}

type CompositeAggOpts struct {
	Sources []map[string]interface{} `json:"sources"`
}

type AggVisitorItem struct {
	Name        string
	Aggregation interface{}
	Data        map[string]interface{}
	Args        map[string]interface{}
}

func forEachTermResultIntermid(rootAgg AggContainer, data map[string]interface{}, callback func(map[string]interface{}), forEachAgg func(AggVisitorItem) (map[string]interface{}, error), args map[string]interface{}, pathStr string) error {
	for k, child := range rootAgg.Aggregations() {
		var childPath string
		if pathStr != "" {
			childPath = pathStr + ">" + k
		} else {
			childPath = k
		}

		if childData, ok := data[k]; ok {
			if childDataMap, ok := childData.(map[string]interface{}); ok {
				additionalArgs, err := forEachAgg(AggVisitorItem{
					Name:        k,
					Aggregation: child,
					Data:        childDataMap,
					Args:        args,
				})

				if err != nil {
					return err
				}

				childArgs := args
				if additionalArgs != nil {
					childArgs := make(map[string]interface{})
					for k, v := range args {
						childArgs[k] = v
					}

					for k, v := range childArgs {
						childArgs[k] = v
					}
				}

				_, isTermsAgg := child.(*TermsAgg)
				_, isMultiTermsAgg := child.(*MultiTermsAgg)

				if isTermsAgg || isMultiTermsAgg {
					if bucketsData, ok := childDataMap["buckets"]; ok {
						if bucketsArray, ok := bucketsData.([]interface{}); ok {
							for i, bucketData := range bucketsArray {
								if bucketDataMap, ok := bucketData.(map[string]interface{}); ok {
									if bucketKey, ok := bucketDataMap["key"]; ok {
										bucketArg := make(map[string]interface{})
										for k, v := range childArgs {
											bucketArg[k] = v
										}

										bucketArg[k] = bucketKey

										callback(bucketArg)

										if containerAgg, ok := child.(AggContainer); ok {
											forEachTermResultIntermid(containerAgg, bucketDataMap, callback, forEachAgg, bucketArg, pathStr)
										}

										continue
									}
								}

								return fmt.Errorf("malformed aggregation data for %v (bucket %v)", pathStr, i)
							}
						}
					}
				} else if containerAgg, ok := child.(AggContainer); ok {
					forEachTermResultIntermid(containerAgg, childDataMap, callback, forEachAgg, childArgs, childPath)
					continue
				}
			}
		}

		return fmt.Errorf("malformed aggregation data for %v", pathStr)
	}

	return nil
}

func ForEachTermResult(rootAgg AggContainer, data map[string]interface{}, forEachBucket func(map[string]interface{}), forEachAgg func(AggVisitorItem) (map[string]interface{}, error)) error {
	return forEachTermResultIntermid(rootAgg, data, forEachBucket, forEachAgg, nil, "")
}
