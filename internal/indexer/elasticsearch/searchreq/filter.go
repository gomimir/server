package searchreq

import (
	"fmt"

	"gitlab.com/gomimir/server/internal/indexer"
)

// TranslateFilters - translates asset filter into query parameter for Search request
func TranslateFilters(filter indexer.AssetFilter) *BoolFilter {
	if filter.IsEmpty() {
		return nil
	}

	result := &BoolFilter{}

	if filter.ID != nil && !filter.IsEmpty() {
		if filter.ID.In != nil {
			ids := make([]string, len(filter.ID.In))
			for i, assetRef := range filter.ID.In {
				ids[i] = assetRef.AssetID()
			}

			result.AppendMust(map[string]interface{}{
				"ids": map[string]interface{}{
					"values": ids,
				},
			})
		}

		if len(filter.ID.NotIn) > 0 {
			ids := make([]string, len(filter.ID.NotIn))
			for i, assetRef := range filter.ID.NotIn {
				ids[i] = assetRef.AssetID()
			}

			result.AppendMustNot(map[string]interface{}{
				"ids": map[string]interface{}{
					"values": ids,
				},
			})
		}
	}

	if filter.Fulltext != nil && *filter.Fulltext != "" {
		fulltextBool := BoolFilter{}

		fulltextBool.AppendShould(NestedFilter{
			Path: "properties",
			Query: MultiMatchFilter{
				Fields: []string{"properties.textValue", "properties.textValue.folding", "properties.textValue.hunspell_folding"},
				Query:  *filter.Fulltext,
			},
		})

		fulltextBool.AppendShould(NestedFilter{
			Path: "files",
			Query: MultiMatchFilter{
				Fields: []string{"files.filename", "files.filename.folding", "files.filename.hunspell_folding"},
				Query:  *filter.Fulltext,
			},
		})

		fulltextBool.AppendShould(NestedFilter{
			Path: "files.properties",
			Query: MultiMatchFilter{
				Fields: []string{"files.properties.textValue", "files.properties.textValue.folding", "files.properties.textValue.hunspell_folding"},
				Query:  *filter.Fulltext,
			},
		})

		result.AppendMust(fulltextBool)
	}

	if filter.WithFile != nil && !filter.WithFile.IsEmpty() {
		fileBool := BoolFilter{}

		if len(filter.WithFile.RepositoryName) > 0 {
			fileBool.AppendMust(translateTextFilter("files.repository", filter.WithFile.RepositoryName))
		}

		if len(filter.WithFile.Filename) > 0 {
			fileBool.AppendMust(translateTextFilter("files.filename", filter.WithFile.Filename))
		}

		for _, propFilter := range filter.WithFile.WithProperty {
			if !propFilter.IsEmpty() {
				fileBool.AppendMust(translatePropertyFilter("files.properties", propFilter))
			}
		}

		for _, processingFilter := range filter.WithFile.WithProcessing {
			processingBool := BoolFilter{}

			if len(processingFilter.Name) > 0 {
				processingBool.AppendMust(translateTextFilter("files.processings.name", processingFilter.Name))
			}

			if processingFilter.JobID != nil {
				processingBool.AppendMust(TermFilter{
					Name:  "files.processings.jobId",
					Value: *processingFilter.JobID,
				})
			}

			if processingFilter.Error != nil {
				if len(processingFilter.Error.Code) > 0 {
					processingBool.AppendMust(translateTextFilter("files.processings.error.code", processingFilter.Error.Code))
				}

				if len(processingFilter.Error.Message) > 0 {
					processingBool.AppendMust(translateTextFilter("files.processings.error.message", processingFilter.Error.Message))
				}

				if len(processingFilter.Error.Code) == 0 && len(processingFilter.Error.Message) == 0 {
					processingBool.AppendMust(ExistsFilter{Name: "files.processings.error.message"})
				}
			}

			if len(processingBool.Must) == 0 {
				processingBool.AppendMust(ExistsFilter{Name: "files.processings.name"})
			}

			fileBool.AppendMust(NestedFilter{
				Path:  "files.processings",
				Query: processingBool,
			})
		}

		// TODO: make this configurable
		fileBool.AppendMust(TermFilter{
			Name:  "files.main",
			Value: true,
		})

		result.AppendMust(NestedFilter{
			Path:  "files",
			Query: fileBool,
		})
	}

	for _, propFilter := range filter.WithProperty {
		if !propFilter.IsEmpty() {
			result.AppendMust(translatePropertyFilter("properties", propFilter))
		}
	}

	for _, tagFilter := range filter.WithTag {
		tagBool := BoolFilter{}
		tagBool.AppendMust(TermFilter{Name: "tags.kind", Value: tagFilter.Kind})

		if len(tagFilter.Name) > 0 {
			tagBool.SetMinimumShouldMatch(1)
			for _, name := range tagFilter.Name {
				tagBool.AppendShould(TermFilter{Name: "tags.name.raw", Value: name})
			}
		}

		result.AppendMust(NestedFilter{
			Path:  "tags",
			Query: tagBool,
		})
	}

	return result
}

func trPropFilter(propPath string, propName string, conditions ...interface{}) NestedFilter {
	query := BoolFilter{}
	query.AppendMust(TermFilter{Name: fmt.Sprintf("%v.name", propPath), Value: propName})
	query.AppendMust(conditions...)

	return NestedFilter{
		Path:  propPath,
		Query: query,
	}
}

// translatePropertyFilter - translates single property filter into list of AND conditions
func translatePropertyFilter(propPath string, propFilter indexer.PropertyFilter) BoolFilter {
	result := BoolFilter{}

	if len(propFilter.TextValue) > 0 {
		result.AppendMust(trPropFilter(propPath, propFilter.Name, translateTextFilter(
			fmt.Sprintf("%v.textValue", propPath),
			propFilter.TextValue,
		)))
	}

	if len(propFilter.NumericValue) > 0 {
		result.AppendMust(trPropFilter(propPath, propFilter.Name, translateNumberFilter(
			fmt.Sprintf("%v.numericValue", propPath),
			propFilter.NumericValue,
		)))
	}

	if len(propFilter.TimeValue) > 0 {
		result.AppendMust(trPropFilter(propPath, propFilter.Name, translateTimeFilter(
			fmt.Sprintf("%v.timeValue", propPath),
			propFilter.TimeValue,
		)))
	}

	if len(result.Must) == 0 {
		result.AppendMust(trPropFilter(propPath, propFilter.Name))
	}

	return result
}

func translateTextFilter(fieldPrefix string, textFilter []indexer.TextFilter) interface{} {
	or := BoolFilter{}
	or.SetMinimumShouldMatch(1)

	for _, f := range textFilter {
		and := BoolFilter{}

		if f.Eq != nil {
			and.AppendMust(TermFilter{
				Name:  fmt.Sprintf("%v.raw", fieldPrefix),
				Value: *f.Eq,
			})
		}

		if f.Matches != nil {
			and.AppendMust(MultiMatchFilter{
				Fields: []string{fieldPrefix, fmt.Sprintf("%v.folding", fieldPrefix), fmt.Sprintf("%v.hunspell_folding", fieldPrefix)},
				Query:  *f.Matches,
			})
		}

		if f.StartsWith != nil {
			and.AppendMust(PrefixFilter{
				Name:   fmt.Sprintf("%v.raw", fieldPrefix),
				Prefix: *f.StartsWith,
			})
		}

		or.AppendShould(and.Simplified())
	}

	return or.Simplified()
}

func translateNumberFilter(fieldPrefix string, numberFilter []indexer.NumberFilter) interface{} {
	or := BoolFilter{}
	or.SetMinimumShouldMatch(1)

	for _, f := range numberFilter {
		and := BoolFilter{}

		if f.Eq != nil {
			and.AppendMust(TermFilter{
				Name:  fieldPrefix,
				Value: *f.Eq,
			})
		}

		rangeProps := make(map[string]interface{})

		if f.Gt != nil {
			rangeProps["gt"] = *f.Gt
		}

		if f.Gte != nil {
			rangeProps["gte"] = *f.Gte
		}

		if f.Lt != nil {
			rangeProps["lt"] = *f.Lt
		}

		if f.Lte != nil {
			rangeProps["lte"] = *f.Lte
		}

		if len(rangeProps) > 0 {
			and.AppendMust(map[string]interface{}{
				"range": map[string]interface{}{
					fieldPrefix: rangeProps,
				},
			})
		}

		or.AppendShould(and.Simplified())
	}

	return or.Simplified()
}

func translateTimeFilter(fieldPrefix string, timeFilter []indexer.TimeFilter) interface{} {
	or := BoolFilter{}
	or.SetMinimumShouldMatch(1)

	for _, f := range timeFilter {
		and := BoolFilter{}

		if f.Eq != nil {
			and.AppendMust(TermFilter{
				Name:  fieldPrefix,
				Value: *f.Eq,
			})
		}

		rangeProps := make(map[string]interface{})

		if f.Gt != nil {
			rangeProps["gt"] = *f.Gt
		}

		if f.Gte != nil {
			rangeProps["gte"] = *f.Gte
		}

		if f.Lt != nil {
			rangeProps["lt"] = *f.Lt
		}

		if f.Lte != nil {
			rangeProps["lte"] = *f.Lte
		}

		if len(rangeProps) > 0 {
			and.AppendMust(map[string]interface{}{
				"range": map[string]interface{}{
					fieldPrefix: rangeProps,
				},
			})
		}

		or.AppendShould(and.Simplified())
	}

	return or.Simplified()
}
