package searchreq_test

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/bradleyjkemp/cupaloy/v2"
	"github.com/icza/gox/gox"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/indexer"
	"gitlab.com/gomimir/server/internal/indexer/elasticsearch/searchreq"

	_ "embed"
)

//go:embed "fixtures/response.json"
var responseFixture string

func TestTranslateFacets(t *testing.T) {
	facets := indexer.FacetRequest{
		TagFacets: []indexer.TagFacet{
			{
				BaseFacet: indexer.FacetWithID("albums"),
				// FetchCover: gox.NewBool(true),
				Kind: "album",
			},
		},
		FileFacets: []indexer.FileFacet{
			{
				BaseFacet: indexer.FacetWithID("fileProperties"),
				PropertyFacets: []indexer.PropertyFacet{
					{
						BaseFacet:    indexer.FacetWithID("authoredAt"),
						PropertyName: "file_authoredAt",
						Children: []indexer.Facet{
							indexer.TimeRangeFacet{
								BaseFacet: indexer.FacetWithID("authoredAtTimeRanges"),
								Ranges: []indexer.TimeRange{
									{
										Name:     "today",
										FromExpr: gox.NewString("now/d"),
									},
								},
								TimeZoneOffsetStr: "UTC",
							},
						},
					},
				},
			},
		},
	}

	translated, err := searchreq.TranslateFacets(facets, indexer.AssetFilter{})
	if assert.NoError(t, err) {
		data, err := json.MarshalIndent(translated, "", "  ")
		if assert.NoError(t, err) {
			snapshotter := cupaloy.New(cupaloy.SnapshotFileExtension(".json"))
			snapshotter.SnapshotT(t, data)
		}
	}
}

func TestTranslateFacetResults(t *testing.T) {
	var data map[string]interface{}
	if !assert.NoError(t, json.Unmarshal([]byte(responseFixture), &data)) {
		return
	}

	facets := indexer.FacetRequest{
		TagFacets: []indexer.TagFacet{
			{
				BaseFacet: indexer.FacetWithID("allTags"),
			},
		},
	}

	results, err := searchreq.TranslateFacetResults(facets, data["aggregations"].(map[string]interface{}))
	if assert.NoError(t, err) {
		assert.Equal(t, len(facets.TagFacets), len(results.TagFacetResults))
		fmt.Printf("%#v\n", results)
	}
}
