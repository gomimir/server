package searchreq

import "encoding/json"

type BoolFilter struct {
	Must               []interface{}
	MustNot            []interface{}
	Should             []interface{}
	MinimumShouldMatch *int
}

func (f *BoolFilter) AppendMust(must ...interface{}) *BoolFilter {
	f.Must = append(f.Must, must...)
	return f
}

func (f *BoolFilter) AppendMustNot(mustNot ...interface{}) *BoolFilter {
	f.MustNot = append(f.MustNot, mustNot...)
	return f
}

func (f *BoolFilter) AppendShould(should ...interface{}) *BoolFilter {
	f.Should = append(f.Should, should...)
	return f
}

func (f *BoolFilter) SetMinimumShouldMatch(min int) *BoolFilter {
	f.MinimumShouldMatch = &min
	return f
}

func (f *BoolFilter) Simplified() interface{} {
	if len(f.Must) == 1 && len(f.Should) == 0 && len(f.MustNot) == 0 {
		return f.Must[0]
	}

	if len(f.Should) == 1 && len(f.Must) == 0 && len(f.MustNot) == 0 && f.MinimumShouldMatch != nil && *f.MinimumShouldMatch == 1 {
		return f.Should[0]
	}

	return f
}

func (f BoolFilter) MarshalJSON() ([]byte, error) {
	m := make(map[string]interface{})

	if len(f.Must) > 0 {
		m["must"] = f.Must
	}

	if len(f.MustNot) > 0 {
		m["must_not"] = f.MustNot
	}

	if len(f.Should) > 0 {
		m["should"] = f.Should
	}

	if f.MinimumShouldMatch != nil {
		m["minimum_should_match"] = *f.MinimumShouldMatch
	}

	return json.Marshal(map[string]interface{}{
		"bool": m,
	})
}

type NestedFilter struct {
	Path  string
	Query interface{}
}

func (f NestedFilter) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]interface{}{
		"nested": map[string]interface{}{
			"path":  f.Path,
			"query": f.Query,
		},
	})
}

type TermFilter struct {
	Name  string
	Value interface{}
}

func (f TermFilter) MarshalJSON() ([]byte, error) {
	m := make(map[string]interface{})
	m[f.Name] = f.Value

	return json.Marshal(map[string]interface{}{
		"term": m,
	})
}

type MatchFilter struct {
	Name  string
	Query string
}

func (f MatchFilter) MarshalJSON() ([]byte, error) {
	m := make(map[string]interface{})
	m[f.Name] = f.Query

	return json.Marshal(map[string]interface{}{
		"match": m,
	})
}

type MultiMatchFilter struct {
	Fields []string
	Query  string
	Type   string
}

func (f MultiMatchFilter) MarshalJSON() ([]byte, error) {
	t := "most_fields"
	if f.Type != "" {
		t = f.Type
	}

	return json.Marshal(map[string]interface{}{
		"multi_match": map[string]interface{}{
			"query":  f.Query,
			"fields": f.Fields,
			"type":   t,
		},
	})
}

type PrefixFilter struct {
	Name   string
	Prefix string
}

func (f PrefixFilter) MarshalJSON() ([]byte, error) {
	m := make(map[string]interface{})
	m[f.Name] = f.Prefix

	return json.Marshal(map[string]interface{}{
		"prefix": m,
	})
}

type ExistsFilter struct {
	Name string
}

func (f ExistsFilter) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]interface{}{
		"exists": map[string]interface{}{
			"field": f.Name,
		},
	})
}
