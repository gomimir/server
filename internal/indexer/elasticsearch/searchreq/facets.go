package searchreq

import (
	"fmt"
	"reflect"
	"strings"
	"time"

	"github.com/icza/gox/gox"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/gomimir/server/internal/documents"
	"gitlab.com/gomimir/server/internal/indexer"
	"gitlab.com/gomimir/server/internal/utils"
)

func TranslateFacets(facets indexer.FacetRequest, filter indexer.AssetFilter) (AggContainer, error) {
	facettedFilter, nonFacettedFilter := splitFilter(filter)

	globalFilter := FilterAgg{
		Filter: translateFilterOrMatchAll(nonFacettedFilter),
	}

	// Tags
	if len(facets.TagFacets) > 0 {
		tagAgg := globalFilter.AddFilter("filterForTags", translateFilterOrMatchAll(globalTagFilter(facettedFilter)))
		for _, facet := range facets.TagFacets {
			child, err := translateTagFacet(facet)
			if err != nil {
				return nil, fmt.Errorf("unable to translate facet %v: %v", facet.ID(), err.Error())
			}

			tagAgg.AddFilter(
				facet.ID(), translateFilterOrMatchAll(tagKindFilter(facettedFilter, facet.Kind)),
			).AddNested(
				"assetToTag", NestedAggOpts{Path: "tags"},
			).AddChild(
				"tags", child,
			)
		}
	}

	// Tag kinds
	for _, facet := range facets.TagKindFacets {
		globalFilter.AddNested(
			facet.ID(), NestedAggOpts{Path: "tags"},
		).AddTerms("byKind", TermAggOpts{
			Field: "tags.kind",
		}).AddCardinality("countDistinctTags", "tags.name.raw")
	}

	// Property introspection
	for _, facet := range facets.PropertyIntrospectionFacets {
		globalFilter.AddChild(facet.ID(), translatePropertyIntrospectionFacet("properties", facet))
	}

	// Property values
	for _, propFacet := range facets.PropertyFacets {
		propAgg := globalFilter.AddFilter(
			composeAggName("assetProperty", propFacet.ID()), translateFilterOrMatchAll(facettedFilter.WithoutProperty(propFacet.PropertyName)),
		).AddNested(
			"properties", NestedAggOpts{Path: "properties"},
		)

		err := translatePropertyFacet("properties", propFacet, propAgg)
		if err != nil {
			return nil, err
		}
	}

	// Files
	for _, fileFacet := range facets.FileFacets {
		// File property introspection
		if len(fileFacet.PropertyIntrospectionFacets) > 0 {
			fileNestedFilter := globalFilter.AddFilter(fileFacet.ID(), translateFilterOrMatchAll(facettedFilter)).AddNested("files", NestedAggOpts{
				Path: "files",
			}).AddFilter("mainFile", map[string]interface{}{
				"term": map[string]interface{}{
					"files.main": true,
				},
			})

			for _, facet := range fileFacet.PropertyIntrospectionFacets {
				fileNestedFilter.AddChild(composeAggName(fileFacet.ID(), facet.ID()), translatePropertyIntrospectionFacet("files.properties", facet))
			}
		}

		// File property values
		for _, propFacet := range fileFacet.PropertyFacets {
			propAgg := globalFilter.AddFilter(
				composeAggName(fileFacet.ID(), propFacet.ID()), translateFilterOrMatchAll(facettedFilter.WithoutFilePropertyFilter(propFacet.PropertyName)),
			).AddNested("files", NestedAggOpts{
				Path: "files",
			}).AddFilter("mainFile", map[string]interface{}{
				"term": map[string]interface{}{
					"files.main": true,
				},
			}).AddNested(
				"properties", NestedAggOpts{Path: "files.properties"},
			)

			err := translatePropertyFacet("files.properties", propFacet, propAgg)
			if err != nil {
				return nil, err
			}
		}
	}

	root := BaseAgg{}
	if len(globalFilter.Aggregations()) > 0 {
		root.AddGlobal("global").AddChild("globalFilter", globalFilter)
	}

	return root, nil
}

func composeAggName(name ...string) string {
	return strings.Join(name, "_")
}

// Returns a filter without facetted conditions
func splitFilter(filter indexer.AssetFilter) (indexer.AssetFilter, indexer.AssetFilter) {
	facetted := indexer.AssetFilter{}
	nonFacetted := filter

	// Tag and property conditions belongs to facets
	nonFacetted.WithTag = nil
	facetted.WithTag = filter.WithTag

	nonFacetted.WithProperty = nil
	facetted.WithProperty = filter.WithProperty

	if filter.WithFile != nil {
		withFile := *filter.WithFile

		withFile.WithProperty = nil

		nonFacetted.WithFile = &withFile

		if len(filter.WithFile.WithProperty) > 0 {
			facetted.WithFile = &indexer.AssetFileFilter{
				WithProperty: filter.WithFile.WithProperty,
			}
		}
	}

	return facetted, nonFacetted
}

// Returns filter for tag facets (only property conditions)
func globalTagFilter(facettedFilter indexer.AssetFilter) indexer.AssetFilter {
	facettedFilter.WithTag = nil
	return facettedFilter
}

func tagKindFilter(filter indexer.AssetFilter, kind string) indexer.AssetFilter {
	facetFilter := indexer.AssetFilter{}

	for _, tagFilter := range filter.WithTag {
		if tagFilter.Kind != kind {
			facetFilter.WithTag = append(facetFilter.WithTag, tagFilter)
		}
	}

	return facetFilter
}

func translateFilterOrMatchAll(filter indexer.AssetFilter) interface{} {
	translatedFilter := TranslateFilters(filter)
	if translatedFilter != nil {
		return translatedFilter
	}

	return map[string]interface{}{
		"match_all": struct{}{},
	}
}

func TranslateFacetResults(facets indexer.FacetRequest, data map[string]interface{}) (*indexer.FacetResults, error) {
	var err error
	result := indexer.FacetResults{}

	// Tags
	if len(facets.TagFacets) > 0 {
		result.TagFacetResults = make([]indexer.TagFacetResult, len(facets.TagFacets))
		for i, facet := range facets.TagFacets {
			if facetData, ok := utils.DeeplyGet(data, "global", "globalFilter", "filterForTags", facet.ID(), "assetToTag", "tags").(map[string]interface{}); ok {
				valueResults, err := translateTagFacetResults(facet, facetData)
				if err != nil {
					return nil, fmt.Errorf("unable to process facet results %v: %v", facet.ID(), err.Error())
				}

				result.TagFacetResults[i] = *valueResults
			} else {
				return nil, fmt.Errorf("unable to process facet results %v: malformed data", facet.ID())
			}
		}
	}

	// Tag kinds
	if len(facets.TagKindFacets) > 0 {
		result.TagKindFacetResults = make([]indexer.TagKindFacetResult, len(facets.TagKindFacets))
		for i, facet := range facets.TagKindFacets {
			if facetData, ok := utils.DeeplyGet(data, "global", "globalFilter", facet.ID()).(map[string]interface{}); ok {
				valueResults, err := translateTagKindFacetResults(facet, facetData)
				if err != nil {
					return nil, fmt.Errorf("unable to process facet results %v: %v", facet.ID(), err.Error())
				}

				result.TagKindFacetResults[i] = *valueResults
			}
		}
	}

	// Property introspection
	result.PropertyIntrospectionFacetResults, err = translatePropertyIntrospectionFacetResults(
		facets.PropertyIntrospectionFacets,
		utils.DeeplyGet(data, "global", "globalFilter"),
		[]string{},
	)

	if err != nil {
		return nil, err
	}

	// Property values
	result.PropertyFacetResults, err = translatePropertyFacetResults(facets.PropertyFacets, func(propFacet indexer.PropertyFacet, propValueFacet indexer.Facet) interface{} {
		return utils.DeeplyGet(data, "global", "globalFilter", composeAggName("assetProperty", propFacet.ID()), "properties", propValueFacet.ID())
	})

	if err != nil {
		return nil, err
	}

	// Files
	if len(facets.FileFacets) > 0 {
		result.FileFacetResults = make([]indexer.FileFacetResult, len(facets.FileFacets))
		for i, fileFacet := range facets.FileFacets {
			result.FileFacetResults[i].Facet = fileFacet

			result.FileFacetResults[i].PropertyFacetResults, err = translatePropertyFacetResults(fileFacet.PropertyFacets, func(propFacet indexer.PropertyFacet, propValueFacet indexer.Facet) interface{} {
				return utils.DeeplyGet(data, "global", "globalFilter", composeAggName(fileFacet.ID(), propFacet.ID()), "files", "mainFile", "properties", propValueFacet.ID())
			})

			if err != nil {
				return nil, err
			}

			result.FileFacetResults[i].PropertyIntrospectionFacetResults, err = translatePropertyIntrospectionFacetResults(
				fileFacet.PropertyIntrospectionFacets,
				utils.DeeplyGet(data, "global", "globalFilter", fileFacet.ID(), "files", "mainFile"),
				[]string{fileFacet.ID()},
			)

			if err != nil {
				return nil, err
			}
		}
	}

	return &result, nil
}

func translatePropertyFacetResults(facets []indexer.PropertyFacet, getData func(propFacet indexer.PropertyFacet, propValueFacet indexer.Facet) interface{}) ([]indexer.PropertyFacetResult, error) {
	var results []indexer.PropertyFacetResult

	if len(facets) > 0 {
		results = make([]indexer.PropertyFacetResult, len(facets))
		for propFacetIdx, propFacet := range facets {
			propFacetResult := indexer.PropertyFacetResult{
				Facet:    propFacet,
				Children: make([]interface{}, len(propFacet.Children)),
			}

			for propValueFacetIdx, propValueFacet := range propFacet.Children {
				if facetData, ok := getData(propFacet, propValueFacet).(map[string]interface{}); ok {
					switch propValueFacet := propValueFacet.(type) {
					case indexer.TextValueFacet:
						valueResults, err := translateTextPropertyFacetResults(propValueFacet, facetData)
						if err != nil {
							return nil, fmt.Errorf("unable to process property facet results %v: %v", propValueFacet.ID(), err.Error())
						}

						propFacetResult.Children[propValueFacetIdx] = *valueResults
					case indexer.TimeRangeFacet:
						valueResults, err := translateTimeRangeFacetResults(propValueFacet, facetData)
						if err != nil {
							return nil, fmt.Errorf("unable to process property facet results %v: %v", propValueFacet.ID(), err.Error())
						}

						propFacetResult.Children[propValueFacetIdx] = *valueResults
					default:
						return nil, fmt.Errorf("unable to process property facet results: unsupported type %v", reflect.TypeOf(propValueFacet))
					}
				} else {
					return nil, fmt.Errorf("unable to process property facet results %v: malformed data", propValueFacet.ID())
				}
			}

			results[propFacetIdx] = propFacetResult
		}
	}

	return results, nil
}

func translatePropertyIntrospectionFacet(prefix string, facet indexer.PropertyIntrospectionFacet) NestedAgg {
	assetToProperty := NestedAgg{
		Nested: NestedAggOpts{Path: prefix},
	}

	byPropertyName := assetToProperty.AddTerms("byPropertyName", TermAggOpts{
		Field: fmt.Sprintf("%v.name", prefix),
		// Note: we might want to replace this with composite aggregation for proper pagination
		Size: gox.NewInt(50),
	})

	byPropertyName.AddValueCount("numTextValues", fmt.Sprintf("%v.textValue.raw", prefix))
	byPropertyName.AddValueCount("numNumericValues", fmt.Sprintf("%v.numericValue", prefix))
	byPropertyName.AddValueCount("numTimeValues", fmt.Sprintf("%v.timeValue", prefix))

	return assetToProperty
}

func translatePropertyIntrospectionFacetResults(facets []indexer.PropertyIntrospectionFacet, data interface{}, idPrefix []string) ([]indexer.PropertyIntrospectionFacetResult, error) {
	var results []indexer.PropertyIntrospectionFacetResult

	if len(facets) > 0 {
		if data, ok := data.(map[string]interface{}); ok {
			results = make([]indexer.PropertyIntrospectionFacetResult, len(facets))
			for i, facet := range facets {
				key := make([]string, len(idPrefix)+1)
				copy(key, idPrefix)
				key[len(idPrefix)] = facet.ID()

				facetResult, err := translatePropertyIntrospectionFacetResult(
					facet,
					data[composeAggName(key...)],
				)

				if err != nil {
					return nil, fmt.Errorf("unable to process facet results %v: %v", facet.ID(), err.Error())
				}

				results[i] = *facetResult
			}
		} else {
			return nil, fmt.Errorf("unable to process property introspection results: malformed data")
		}
	}

	return results, nil
}

func translatePropertyIntrospectionFacetResult(facet indexer.PropertyIntrospectionFacet, data interface{}) (*indexer.PropertyIntrospectionFacetResult, error) {
	result := indexer.PropertyIntrospectionFacetResult{
		Facet: facet,
	}

	var mapped struct {
		ByPropertyName struct {
			Buckets []struct {
				Key           string
				Count         int `mapstructure:"doc_count"`
				NumTextValues struct {
					Value int
				}
				NumNumericValues struct {
					Value int
				}
				NumTimeValues struct {
					Value int
				}
			}
		}
	}

	err := mapstructure.Decode(data, &mapped)
	if err != nil {
		return nil, err
	}

	result.Introspection = make([]*indexer.PropertyIntrospection, len(mapped.ByPropertyName.Buckets))
	for i, bucket := range mapped.ByPropertyName.Buckets {
		result.Introspection[i] = &indexer.PropertyIntrospection{
			Name:  bucket.Key,
			Count: bucket.Count,
		}

		if bucket.Count == bucket.NumTextValues.Value {
			result.Introspection[i].Type = indexer.PropertyTypeText
		} else if bucket.Count == bucket.NumNumericValues.Value {
			result.Introspection[i].Type = indexer.PropertyTypeNumber
		} else if bucket.Count == bucket.NumTimeValues.Value {
			result.Introspection[i].Type = indexer.PropertyTypeTime
		} else {
			result.Introspection[i].Type = indexer.PropertyTypeUnknown
		}
	}

	return &result, nil
}

func translatePropertyFacet(prefix string, propFacet indexer.PropertyFacet, propAgg ChainableAgg) error {
	for _, propertyValueFacet := range propFacet.Children {
		switch propertyValueFacet := propertyValueFacet.(type) {
		case indexer.TextValueFacet:
			child, err := translateTextPropertyFacet(prefix, propFacet.PropertyName, propertyValueFacet)
			if err != nil {
				return fmt.Errorf("unable to translate facet %v: %v", propertyValueFacet.ID(), err.Error())
			}

			propAgg.AddChild(propertyValueFacet.ID(), child)

		case indexer.TimeRangeFacet:
			child, err := translateTimeRangeFacet(prefix, propFacet.PropertyName, propertyValueFacet)
			if err != nil {
				return fmt.Errorf("unable to translate facet %v: %v", propertyValueFacet.ID(), err.Error())
			}

			propAgg.AddChild(propertyValueFacet.ID(), child)

		default:
			return fmt.Errorf("unable to translate property facet: unsupported type %v", reflect.TypeOf(propertyValueFacet))
		}
	}

	return nil
}

func translateTextPropertyFacet(prefix, propertyName string, facet indexer.TextValueFacet) (*FilterAgg, error) {
	filterAgg := &FilterAgg{
		Filter: map[string]interface{}{
			"term": map[string]interface{}{
				fmt.Sprintf("%v.name", prefix): propertyName,
			},
		},
	}

	filterAgg.AddTerms("values",
		TermAggOpts{
			Field: fmt.Sprintf("%v.textValue.raw", prefix),
			Order: map[string]string{
				"fileToAsset": "desc",
			},
		}).AddReverseNested("fileToAsset")

	return filterAgg, nil
}

func translateTextPropertyFacetResults(facet indexer.TextValueFacet, data map[string]interface{}) (*indexer.TextValueFacetResult, error) {
	result := &indexer.TextValueFacetResult{
		Facet: facet,
	}

	var mapped struct {
		Values struct {
			Buckets []struct {
				Key         string
				FileToAsset struct {
					Count int `mapstructure:"doc_count"`
				}
			}
		}
	}

	err := mapstructure.Decode(data, &mapped)
	if err != nil {
		return nil, err
	}

	for _, bucketData := range mapped.Values.Buckets {
		result.Buckets = append(result.Buckets, &indexer.TextValueBucket{
			Value:     bucketData.Key,
			NumAssets: bucketData.FileToAsset.Count,
		})
	}

	return result, nil
}

type tagKindFacetResults struct {
	Buckets []struct {
		Key        string
		Count      int `mapstructure:"doc_count"`
		TagToAsset *struct {
			AssetsWithThumbnails struct {
				ThumbnailAsset struct {
					Hits ElasticsearchHits
				}
			}
		}
	}
}

func translateTimeRangeFacet(prefix, propertyName string, facet indexer.TimeRangeFacet) (*FilterAgg, error) {
	filterAgg := &FilterAgg{
		Filter: map[string]interface{}{
			"term": map[string]interface{}{
				fmt.Sprintf("%v.name", prefix): propertyName,
			},
		},
	}

	filterAgg.AddDateRange("timeValueRanges", DateRangeAggOpts{
		Field:    fmt.Sprintf("%v.timeValue", prefix),
		Ranges:   facet.Ranges,
		TimeZone: facet.TimeZoneOffsetStr,
	}).AddReverseNested("fileToAsset")

	return filterAgg, nil
}

func translateTimeRangeFacetResults(facet indexer.TimeRangeFacet, data map[string]interface{}) (*indexer.TimeRangeFacetResult, error) {
	var mapped struct {
		TimeValueRanges struct {
			Buckets []struct {
				Key         string
				From        *time.Time `mapstructure:"from_as_string"`
				To          *time.Time `mapstructure:"to_as_string"`
				FileToAsset struct {
					Count int `mapstructure:"doc_count"`
				}
			}
		}
	}

	decoder, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		DecodeHook: mapstructure.StringToTimeHookFunc(time.RFC3339),
		Result:     &mapped,
	})

	if err != nil {
		return nil, err
	}

	err = decoder.Decode(data)
	if err != nil {
		return nil, err
	}

	result := &indexer.TimeRangeFacetResult{
		Facet:   facet,
		Buckets: make([]*indexer.TimeRangeBucket, len(mapped.TimeValueRanges.Buckets)),
	}

	for i, bucket := range mapped.TimeValueRanges.Buckets {
		resolvedRange := indexer.ResolvedTimeRange{
			Name: bucket.Key,
		}

		var unresolvedRange *indexer.TimeRange
		for _, r := range facet.Ranges {
			if r.Name == bucket.Key {
				unresolvedRange = &r
			}
		}

		if unresolvedRange == nil {
			continue
		}

		if bucket.From != nil && unresolvedRange.FromExpr != nil {
			resolvedRange.From = &indexer.ResolvedTimeExpression{
				Expression: *unresolvedRange.FromExpr,
				Value:      *bucket.From,
			}
		}

		if bucket.To != nil && unresolvedRange.ToExpr != nil {
			resolvedRange.To = &indexer.ResolvedTimeExpression{
				Expression: *unresolvedRange.ToExpr,
				Value:      *bucket.To,
			}
		}

		result.Buckets[i] = &indexer.TimeRangeBucket{
			Range:     resolvedRange,
			NumAssets: bucket.FileToAsset.Count,
		}
	}

	return result, nil
}

func translateTagBuckets(tagKind string, data tagKindFacetResults) ([]*indexer.TagBucket, error) {
	var result []*indexer.TagBucket

	for _, bucketData := range data.Buckets {
		tagBucket := &indexer.TagBucket{
			Kind:      tagKind,
			Name:      bucketData.Key,
			NumAssets: bucketData.Count,
		}

		if bucketData.TagToAsset != nil && len(bucketData.TagToAsset.AssetsWithThumbnails.ThumbnailAsset.Hits.Hits) > 0 {
			hit := bucketData.TagToAsset.AssetsWithThumbnails.ThumbnailAsset.Hits.Hits[0]
			asset, err := documents.IndexedAssetFromData(hit.Index, hit.ID, hit.Source)
			if err != nil {
				return nil, err
			}

			tagBucket.CoverAsset = asset
		}

		result = append(result, tagBucket)
	}

	return result, nil
}

func translateTagFacetResults(facet indexer.TagFacet, data map[string]interface{}) (*indexer.TagFacetResult, error) {
	result := &indexer.TagFacetResult{
		Facet: facet,
	}

	var mapped struct {
		ByName tagKindFacetResults
	}

	err := mapstructure.Decode(data, &mapped)
	if err != nil {
		return nil, err
	}

	result.Buckets, err = translateTagBuckets(facet.Kind, mapped.ByName)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func translateTagFacet(facet indexer.TagFacet) (interface{}, error) {
	byName := TermsAgg{
		Terms: TermAggOpts{
			Field: "tags.name.raw",

			// Sort by the tag name (default sorting is by count)
			// Order: map[string]string{
			// 	"_term": "asc"
			// },
		},
	}

	withKind := &FilterAgg{
		Filter: map[string]interface{}{
			"term": map[string]interface{}{
				"tags.kind": facet.Kind,
			},
		},
	}

	withKind.AddChild("byName", byName)
	return withKind, nil
}

func translateTagKindFacetResults(facet indexer.TagKindFacet, data map[string]interface{}) (*indexer.TagKindFacetResult, error) {
	result := &indexer.TagKindFacetResult{
		Facet: facet,
	}

	var mapped struct {
		ByKind struct {
			Buckets []struct {
				Key               string
				Count             int `mapstructure:"doc_count"`
				CountDistinctTags struct {
					Value int
				}
			}
		}
	}

	err := mapstructure.Decode(data, &mapped)
	if err != nil {
		return nil, err
	}

	for _, bucket := range mapped.ByKind.Buckets {
		result.Kinds = append(result.Kinds, &indexer.TagKindBucket{
			Kind:    bucket.Key,
			NumTags: bucket.CountDistinctTags.Value,
		})
	}

	return result, nil
}
