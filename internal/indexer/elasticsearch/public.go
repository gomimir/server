package elasticsearch

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"time"

	"github.com/elastic/go-elasticsearch/v7/esapi"
	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/apperror"
	"gitlab.com/gomimir/server/internal/documents"
	abstractindexer "gitlab.com/gomimir/server/internal/indexer"
	"gitlab.com/gomimir/server/internal/indexer/elasticsearch/searchreq"

	// for go:embed
	_ "embed"
)

//go:embed index.json
var defaultIndexConfig string

// EnsureReady - runs healthcheck loop to wait until Elasticsearch is alive
func (indexer *Indexer) EnsureReady(ctx context.Context, opts mimir.RetryOpts) error {
	err := mimir.Retry(ctx, func(ctx context.Context, ri mimir.RetryInfo) error {
		req := esapi.PingRequest{}

		err := indexer.doRequest(req, nil)
		if err == nil {
			return nil
		}

		if ri.WillTryAgain {
			log.Warn().Int("try", ri.Try).Err(err).Msgf("Unable to establish connection to Elasticsearch. Will try again in %v.", ri.NextCooldown)
		}

		return err
	}, opts.ApplyDefaults())

	if err != nil {
		return err
	}

	return nil
}

// RefreshIndex - ensures that all the indexed items are available for search
func (indexer *Indexer) RefreshIndex(indexNames []string) error {
	req := esapi.IndicesRefreshRequest{
		Index: indexNames,
	}

	var body elasticsearchGetBody
	if err := indexer.doRequest(req, &body); err != nil {
		log.Error().Err(err).Msg("Unable to refresh indices")
		return err
	}

	return nil
}

// GetAsset - get asset document by ID
func (indexer *Indexer) GetAsset(assetRef mimir.AssetRef) (*documents.IndexedAsset, error) {
	err := indexer.validateRef(assetRef)
	if err != nil {
		return nil, err
	}

	sublog := log.With().Str("asset", assetRef.String()).Logger()
	res, err := indexer.getAsset(assetRef.IndexName(), assetRef.AssetID())
	if err != nil {
		if esError, ok := err.(*elasticsearchError); ok {
			if esError.Response.StatusCode == 404 {
				return nil, apperror.New(apperror.NotFound, "unable to find asset: %v", assetRef.String())
			}
		}

		sublog.Error().Err(err).Msg("Unable to get asset")
		return nil, err
	}

	return &res.IndexedAsset, nil
}

// ListAssets - list asset documents
func (indexer *Indexer) ListAssets(opts abstractindexer.AssetListingOptions) (*abstractindexer.AssetListing, error) {
	req := &searchreq.SearchRequest{
		IndexNames: opts.IndexNames,
		Skip:       opts.Skip,
		Size:       opts.Size,
		Filter:     opts.Filter,
		SortBy:     opts.SortBy,
		Facets:     opts.Facets,
		Stats:      opts.Stats,
	}

	var body searchreq.ElasticsearchSearchBody
	if err := indexer.doRequest(req, &body); err != nil {
		log.Error().Err(err).Msg("Unable to list assets")
		return nil, err
	}

	listing := &abstractindexer.AssetListing{
		TotalCount: body.Hits.Total.Value,
	}

	for _, hit := range body.Hits.Hits {
		asset, err := documents.IndexedAssetFromData(hit.Index, hit.ID, hit.Source)
		if err != nil {
			log.Error().Err(err).Str("index", hit.Index).Str("document", hit.ID).Msg("Unable to parse asset document")
			continue
		}

		listing.Items = append(listing.Items, *asset)
	}

	var err error
	listing.FacetResults, err = searchreq.TranslateFacetResults(opts.Facets, body.Aggregations)
	if err != nil {
		return nil, err
	}

	listing.Stats, err = searchreq.TranslateStatsResult(opts.Stats, body.Aggregations)
	if err != nil {
		return nil, err
	}

	return listing, nil
}

func (indexer *Indexer) UpdateAsset(assetRef mimir.AssetRef, callback func(documents.IndexedAsset) documents.IndexedAsset) (*documents.IndexedAsset, error) {
	err := indexer.validateRef(assetRef)
	if err != nil {
		return nil, err
	}

	res, err := indexer.updateAsset(assetRef.IndexName(), assetRef.AssetID(), callback)
	if err != nil {
		if esError, ok := err.(*elasticsearchError); ok {
			if esError.Response.StatusCode == 404 {
				return nil, apperror.New(apperror.NotFound, "unable to asset, asset not found: %v", assetRef.String())
			}
		}

		log.Error().Str("asset", assetRef.String()).Err(err).Msg("Unable to update asset")
		return nil, err
	}

	return &res.Asset, nil
}

func (indexer *Indexer) DeleteAsset(assetRef mimir.AssetRef) error {
	err := indexer.validateRef(assetRef)
	if err != nil {
		return err
	}

	req := esapi.DeleteRequest{
		Index:      assetRef.IndexName(),
		DocumentID: assetRef.AssetID(),
	}

	var body elasticsearchGetBody
	if err := indexer.doRequest(req, &body); err != nil {
		if esError, ok := err.(*elasticsearchError); ok {
			if esError.Response.StatusCode == 404 {
				return nil
			}
		}

		log.Error().Str("asset", assetRef.String()).Err(err).Msg("Unable to delete asset")
		return err
	}

	return nil
}

func (indexer *Indexer) DeleteAssetsByQuery(indexNames []string, query *abstractindexer.AssetFilter) error {
	body := make(map[string]interface{})

	// Filter
	if query != nil {
		queryParam := searchreq.TranslateFilters(*query)
		if queryParam != nil {
			body["query"] = queryParam
		}
	}

	if body["query"] == nil {
		body["query"] = map[string]interface{}{
			"match_all": map[string]interface{}{},
		}
	}

	payload, err := json.Marshal(body)
	if err != nil {
		return err
	}

	req := esapi.DeleteByQueryRequest{
		Index: indexNames,
		Body:  bytes.NewReader(payload),
	}

	return indexer.doRequest(req, nil)
}

// EnsureAsset - ensures that file is indexed
func (indexer *Indexer) EnsureAsset(assetRef mimir.AssetRef, callback func(documents.IndexedAsset) documents.IndexedAsset) (*documents.IndexedAsset, error) {
	err := indexer.validateRef(assetRef)
	if err != nil {
		return nil, err
	}

	sublog := log.With().Str("asset", assetRef.String()).Logger()

	res, err := indexer.getAsset(assetRef.IndexName(), assetRef.AssetID())
	if err != nil {
		if esError, ok := err.(*elasticsearchError); ok {
			if esError.Response.StatusCode == 404 {
				asset := callback(documents.IndexedAsset{
					AssetRef: assetRef,
				})

				payload, err := json.Marshal(asset)
				if err != nil {
					return nil, err
				}

				req := esapi.IndexRequest{
					Index:      assetRef.IndexName(),
					DocumentID: assetRef.AssetID(),
					OpType:     "create",
					Body:       bytes.NewReader(payload),
				}

				err = indexer.doRequest(req, nil)
				if err != nil {
					sublog.Error().Err(err).Msg("Unable to create asset")
					return nil, err
				}

				return &asset, nil
			}
		}

		sublog.Error().Err(err).Msg("Unable to get asset")
		return nil, err
	}

	updRes, err := indexer.updateOptimisticAsset(*res, callback)
	if err != nil {
		sublog.Error().Err(err).Msg("Unable to update asset")
		return nil, err
	}

	return &updRes.Asset, nil

}

func (indexer *Indexer) ScrollAssets(opts abstractindexer.ScrollAssetsOptions) <-chan abstractindexer.ScrollAssetsItem {
	pageSize := 50
	scrollDuration := 2 * time.Minute
	seqNoPrimaryTerm := true

	req := searchreq.SearchRequest{
		IndexNames: opts.IndexNames,

		SeqNoPrimaryTerm: &seqNoPrimaryTerm,
		Size:             &pageSize,

		Filter: opts.Filter,
		Scroll: scrollDuration,
	}

	resultC := make(chan abstractindexer.ScrollAssetsItem, 1)

	go func() {
		for item := range indexer.scroll(req) {
			if item.Error != nil {
				resultC <- abstractindexer.ScrollAssetsItem{
					Error: item.Error,
				}
			} else {
				asset, err := documents.IndexedAssetFromData(item.Hit.Index, item.Hit.ID, item.Hit.Source)
				if err != nil {
					resultC <- abstractindexer.ScrollAssetsItem{
						Error: err,
					}
				} else {
					resultC <- abstractindexer.ScrollAssetsItem{
						Asset: asset,
					}
				}
			}
		}

		close(resultC)
	}()

	return resultC
}

// UpdateAssetsByQuery - update asset documents by filter query, returns number of items updated
func (indexer *Indexer) UpdateAssetsByQuery(indexNames []string, filter *abstractindexer.AssetFilter, callback func(documents.IndexedAsset) documents.IndexedAsset) (*abstractindexer.UpdateAssetsByQuerySummary, error) {
	pageSize := 50
	scrollDuration := 2 * time.Minute
	seqNoPrimaryTerm := true

	req := searchreq.SearchRequest{
		IndexNames: indexNames,

		SeqNoPrimaryTerm: &seqNoPrimaryTerm,
		Size:             &pageSize,

		Filter: filter,
		Scroll: scrollDuration,
	}

	summary := &abstractindexer.UpdateAssetsByQuerySummary{
		NumMatched: 0,
		NumChanged: 0,
		NumFailed:  0,
	}

	var err error
	var assets []optimisticAsset
	for item := range indexer.scroll(req) {
		if item.Error != nil {
			if item.Error != io.EOF {
				err = item.Error
				log.Error().Err(item.Error).Msg("Error while scrolling the search results")
			}
		} else {
			summary.NumMatched++
			asset, err := documents.IndexedAssetFromData(item.Hit.Index, item.Hit.ID, item.Hit.Source)
			if err != nil {
				summary.NumFailed++
				log.Error().Err(err).Str("index", item.Hit.Index).Str("document", item.Hit.ID).Msg("Unable to parse asset document")
			} else {
				assets = append(assets, optimisticAsset{
					IndexedAsset: *asset,

					PrimaryTerm: *item.Hit.PrimaryTerm,
					SeqNo:       *item.Hit.SeqNo,
				})
			}
		}
	}

	for _, optimisticAsset := range assets {
		res, updateErr := indexer.updateOptimisticAsset(optimisticAsset, callback)

		if updateErr != nil {
			log.Error().Err(updateErr).Msg("Unable to update asset")
			summary.NumFailed++
			continue
		}

		if res.Changed {
			summary.NumChanged++
		}
	}

	return summary, err
}

func (indexer *Indexer) GetTags(opts abstractindexer.TagListingOptions) ([]*abstractindexer.TagListingEntry, error) {
	aggs := searchreq.BuildTagAggs(opts)
	payload := map[string]interface{}{
		"size": 0,
		"aggs": aggs.Aggregations(),
	}

	body, err := json.MarshalIndent(payload, "", " ")
	if err != nil {
		return nil, err
	}

	req := esapi.SearchRequest{
		Index: opts.IndexNames,
		Body:  bytes.NewReader(body),
	}

	var resBody searchreq.ElasticsearchSearchBody
	if err := indexer.doRequest(req, &resBody); err != nil {
		log.Error().Err(err).Msg("Unable to list tags")
		return nil, err
	}

	var tags []*abstractindexer.TagListingEntry
	err = searchreq.ForEachTermResult(aggs, resBody.Aggregations, func(args map[string]interface{}) {
		if nameAndKind, ok := args["byNameAndKind"].([]interface{}); ok {
			tags = append(tags, &abstractindexer.TagListingEntry{
				Kind: nameAndKind[1].(string),
				Name: nameAndKind[0].(string),
			})
		}
	}, func(item searchreq.AggVisitorItem) (map[string]interface{}, error) {
		if item.Name == "coverAsset" {
			var topHitsBody searchreq.ElasticsearchSearchBody

			// FIXME: nasty workaround for parsing the asset document
			// (it relies on JSON unmarshalling and it is not easy to refactor all the aggregation handling to use json.RawMessage)
			bytes, err := json.Marshal(item.Data)
			if err != nil {
				return nil, err
			}

			err = json.Unmarshal(bytes, &topHitsBody)
			if err != nil {
				return nil, err
			}

			if len(topHitsBody.Hits.Hits) > 0 {
				hit := topHitsBody.Hits.Hits[0]

				asset, err := documents.IndexedAssetFromData(hit.Index, hit.ID, hit.Source)
				if err != nil {
					return nil, err
				}

				tags[len(tags)-1].CoverAsset = asset
			}
		}

		return nil, nil
	})

	if err != nil {
		return nil, err
	}

	return tags, nil
}
