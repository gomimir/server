package elasticsearch

import (
	"encoding/json"
	"errors"

	"github.com/elastic/go-elasticsearch/v7/esapi"
	"github.com/rs/zerolog/log"
)

func getError(res *esapi.Response) error {
	if res.IsError() {
		var body map[string]interface{}
		err := json.NewDecoder(res.Body).Decode(&body)
		if err == nil {
			return &elasticsearchError{
				Response: elasticsearchErrorResponse{
					Body:       body,
					StatusCode: res.StatusCode,
					StatusText: res.Status(),
				},
			}
		} else {
			log.Warn().Err(err).Msg("Unable to parse error response")
		}

		return errors.New(res.Status())
	}

	return nil
}
