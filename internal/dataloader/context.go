package dataloader

import (
	"context"
	"errors"
)

type contextKey struct {
	name string
}

var ctxKey = &contextKey{"dataloader"}

func CacheContext(parent context.Context) context.Context {
	return context.WithValue(parent, ctxKey, newCache())
}

func cacheFromContext(ctx context.Context) (*cache, error) {
	if session, ok := ctx.Value(ctxKey).(*cache); ok {
		return session, nil
	}

	return nil, errors.New("unable to retrieve data loader cache from context")
}
