package dataloader

import (
	"context"
	"fmt"

	"github.com/graph-gophers/dataloader"
	"gitlab.com/gomimir/server/internal/apperror"
	"gitlab.com/gomimir/server/internal/ent"
	entrepository "gitlab.com/gomimir/server/internal/ent/repository"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/repository"
	"gitlab.com/gomimir/server/internal/repository/local"
	"gitlab.com/gomimir/server/internal/repository/s3"
	"gitlab.com/gomimir/server/internal/secret"
)

func RepositoryByID(ctx context.Context, id pulid.ID) (repository.Repository, error) {
	entClient := ent.FromContext(ctx)

	repoEnt, err := entClient.Repository.Get(ctx, id)
	if err != nil {
		if ent.IsNotFound(err) {
			// Translating the error to be consistent with dataloader behaviour
			return nil, repoNotFoundError(id)
		}

		return nil, err
	}

	keeper, err := secret.KeeperForContext(ctx)
	if err != nil {
		return nil, err
	}

	return RepositoryInitializer(repoEnt, keeper)
}

func RepositoryByIDCached(ctx context.Context, id pulid.ID) (repository.Repository, error) {
	cache, err := cacheFromContext(ctx)
	if err != nil {
		return nil, err
	}

	thunk := cache.repositoryLoader.Load(ctx, dataloader.StringKey(string(id)))

	res, err := thunk()
	if err != nil {
		return nil, err
	}

	return res.(repository.Repository), nil
}

func newRepositoryLoader() *dataloader.Loader {
	return dataloader.NewBatchedLoader(loadRepositories)
}

func loadRepositories(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
	ids := make([]pulid.ID, len(keys))
	byID := make(map[pulid.ID]*dataloader.Result)
	results := make([]*dataloader.Result, len(keys))

	for i, key := range keys {
		ids[i] = pulid.ID(key.String())
		results[i] = &dataloader.Result{}
		byID[ids[i]] = results[i]
	}

	entClient := ent.FromContext(ctx)
	ents, err := entClient.Repository.Query().Where(entrepository.IDIn(ids...)).All(ctx)
	if err == nil {
		keeper, err := secret.KeeperForContext(ctx)
		if err == nil {

			for _, ent := range ents {
				repo, err := RepositoryInitializer(ent, keeper)
				if err != nil {
					byID[ent.ID].Error = err
				} else {
					byID[ent.ID].Data = repo
				}

				delete(byID, ent.ID)
			}

			for id, res := range byID {
				res.Error = repoNotFoundError(id)
			}

			return results
		}
	}

	for _, res := range results {
		res.Error = err
	}

	return results
}

var RepositoryInitializer = loadRepository

func loadRepository(repoEnt *ent.Repository, secretKeeper secret.Keeper) (repository.Repository, error) {
	if repoEnt.Config != nil {
		if repoEnt.Config.S3 != nil {
			secretKey, err := secretKeeper.DecryptString(repoEnt.Config.S3.SecretKey)
			if err != nil {
				return nil, err
			}

			return s3.NewRepository(&s3.RepositoryConfig{
				Endpoint:  repoEnt.Config.S3.Endpoint,
				UseSSL:    repoEnt.Config.S3.UseSSL,
				AccessKey: repoEnt.Config.S3.AccessKey,
				SecretKey: secretKey,
				CACert:    repoEnt.Config.S3.CACert,
			})
		} else if repoEnt.Config.Local != nil {
			return local.NewRepository(&local.RepositoryConfig{
				BasePath: repoEnt.Config.Local.Path,
			})
		}
	}

	return nil, fmt.Errorf("not supported repository")
}

func repoNotFoundError(id pulid.ID) error {
	return apperror.New(apperror.NotFound, "repository %v not found", id)
}
