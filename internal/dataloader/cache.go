package dataloader

import "github.com/graph-gophers/dataloader"

type cache struct {
	repositoryLoader *dataloader.Loader
}

func newCache() *cache {
	return &cache{
		repositoryLoader: newRepositoryLoader(),
	}
}
