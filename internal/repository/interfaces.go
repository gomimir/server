package repository

import (
	"context"
	"io"
	"time"
)

type Entry interface {
	GetFilename() string
	GetLastModified() time.Time
	GetSize() int64
}

type GetFile struct {
	// File reader
	Reader io.ReadSeekCloser

	// File conent hash usable for ETag header purpose
	ETag *string

	// Date of last modifcation
	LastModified *time.Time

	// File size in bytes
	Size int64
}

type Repository interface {
	EnsureTemp(ctx context.Context, prefix string) error
	ListFiles(ctx context.Context, prefix string) (chan Entry, error)
	GetFile(ctx context.Context, filename string) (*GetFile, error)
	UploadFile(ctx context.Context, filename string, fileSize int64, reader io.Reader) error
	DeleteFile(ctx context.Context, filename string) error
}
