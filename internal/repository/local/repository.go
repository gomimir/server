package local

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"

	"gitlab.com/gomimir/server/internal/apperror"
	"gitlab.com/gomimir/server/internal/repository"
	"gitlab.com/gomimir/server/internal/utils"
)

type Repository struct {
	basePath string
}

var basePathRx = regexp.MustCompile(`[^./]`)

func NewRepository(config *RepositoryConfig) (repository.Repository, error) {
	basePath := filepath.Clean(config.BasePath)

	if !basePathRx.MatchString(basePath) {
		return nil, fmt.Errorf("invalid base path: %v", config.BasePath)
	}

	return &Repository{
		basePath: basePath,
	}, nil
}

// Note: local filesystem has no native way of cleaning the files, it needs to be accompanied with scheduled job
func (repo *Repository) EnsureTemp(ctx context.Context, prefix string) error {
	absFilename, err := repo.absFilename(prefix)
	if err != nil {
		return err
	}

	dir := path.Dir(absFilename)
	return os.MkdirAll(dir, 0o0750)
}

func (repo *Repository) ListFiles(ctx context.Context, prefix string) (chan repository.Entry, error) {
	absFilename, err := repo.absFilename(prefix)
	if err != nil {
		return nil, err
	}

	fi, err := os.Stat(absFilename)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			if strings.HasSuffix(prefix, "/") {
				entryC := make(chan repository.Entry)
				close(entryC)
				return entryC, nil
			} else {
				// Emulating prefix search
				entryC := make(chan repository.Entry)
				go func() {
					// Note: We are swallowing error here
					_ = filepath.Walk(path.Dir(absFilename), func(path string, fi fs.FileInfo, err error) error {
						if err == nil {
							if !fi.IsDir() && strings.HasPrefix(path, absFilename) {
								if rel, err := filepath.Rel(repo.basePath, path); err == nil {
									entryC <- Entry{filename: rel, fi: fi}
								}
							}
						}

						return err
					})

					close(entryC)
				}()

				return entryC, nil
			}
		}

		return nil, err
	}

	if fi.IsDir() {
		entryC := make(chan repository.Entry)
		go func() {
			filepath.Walk(absFilename, func(path string, fi fs.FileInfo, err error) error {
				if !fi.IsDir() {
					if rel, err := filepath.Rel(repo.basePath, path); err == nil {
						entryC <- Entry{filename: rel, fi: fi}
					}
				}

				return nil
			})

			close(entryC)
		}()

		return entryC, nil
	} else {
		entryC := make(chan repository.Entry, 1)
		entryC <- Entry{filename: prefix, fi: fi}
		close(entryC)
		return entryC, nil
	}
}

func (repo *Repository) GetFile(ctx context.Context, filename string) (*repository.GetFile, error) {
	absFilename, err := repo.absFilename(filename)
	if err != nil {
		return nil, err
	}

	fi, err := os.Stat(absFilename)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return nil, apperror.New(apperror.FileNotFound, "file %v was not found", filename)
		}

		return nil, err
	}

	lastModified := fi.ModTime()

	file, err := os.Open(absFilename)
	if err != nil {
		return nil, err
	}

	res := &repository.GetFile{
		Reader:       file,
		Size:         fi.Size(),
		LastModified: &lastModified,
	}

	return res, nil
}

func (repo *Repository) UploadFile(ctx context.Context, filename string, fileSize int64, reader io.Reader) error {
	absFilename, err := repo.absFilename(filename)
	if err != nil {
		return err
	}

	dir := path.Dir(absFilename)
	err = os.MkdirAll(dir, 0o0750)
	if err != nil {
		return err
	}

	file, err := os.OpenFile(absFilename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0o0640)
	if err != nil {
		return err
	}

	_, err = io.Copy(file, reader)
	if err != nil {
		return err
	}

	err = file.Close()
	if err != nil {
		return err
	}

	return nil
}

func (repo *Repository) DeleteFile(ctx context.Context, filename string) error {
	absFilename, err := repo.absFilename(filename)
	if err != nil {
		return err
	}

	err = os.Remove(absFilename)
	if errors.Is(err, fs.ErrNotExist) {
		return nil
	}

	return err
}

func (repo *Repository) absFilename(filename string) (string, error) {
	sanitized := utils.SanitizeRelPath(filename)
	if sanitized == "" {
		return "", fmt.Errorf("filename cannot be empty")
	}

	return utils.JoinPath(repo.basePath, sanitized), nil
}
