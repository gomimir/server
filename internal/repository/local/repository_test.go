package local_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gomimir/server/internal/repository/local"
)

func TestNewRepository(t *testing.T) {
	_, err := local.NewRepository(&local.RepositoryConfig{})
	require.Error(t, err)

	_, err = local.NewRepository(&local.RepositoryConfig{BasePath: "."})
	require.Error(t, err)

	_, err = local.NewRepository(&local.RepositoryConfig{BasePath: "/"})
	require.Error(t, err)

	_, err = local.NewRepository(&local.RepositoryConfig{BasePath: ".."})
	require.Error(t, err)

	_, err = local.NewRepository(&local.RepositoryConfig{BasePath: "../.."})
	require.Error(t, err)

	_, err = local.NewRepository(&local.RepositoryConfig{BasePath: "../ok"})
	require.NoError(t, err)

	_, err = local.NewRepository(&local.RepositoryConfig{BasePath: "ok"})
	require.NoError(t, err)

	_, err = local.NewRepository(&local.RepositoryConfig{BasePath: "/ok"})
	require.NoError(t, err)
}
