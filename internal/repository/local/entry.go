package local

import (
	"os"
	"time"
)

type Entry struct {
	filename string
	fi       os.FileInfo
}

func (e Entry) GetFilename() string {
	return e.filename
}

func (e Entry) GetLastModified() time.Time {
	return e.fi.ModTime()
}

func (e Entry) GetSize() int64 {
	return e.fi.Size()
}
