package s3

import (
	"fmt"
	"time"

	"github.com/minio/minio-go/v7"
	"gitlab.com/gomimir/server/internal/repository"
)

// Entry - S3 implementation of Entry interface
type Entry struct {
	bucketName  string
	minioObject minio.ObjectInfo
}

// NewEntry - constructor
func NewEntry(bucketName string, minioObject minio.ObjectInfo) repository.Entry {
	return &Entry{
		bucketName:  bucketName,
		minioObject: minioObject,
	}
}

// GetFilename - returns absolute file path including the bucket
func (entry *Entry) GetFilename() string {
	return fmt.Sprintf("%v/%v", entry.bucketName, entry.minioObject.Key)
}

// GetLastModified - returns time of files last modification
func (entry *Entry) GetLastModified() time.Time {
	return entry.minioObject.LastModified
}

// GetSize - return file size in bytes
func (entry *Entry) GetSize() int64 {
	return entry.minioObject.Size
}
