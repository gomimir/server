package s3

import "github.com/mitchellh/mapstructure"

// RepositoryConfig - config options for the S3 repository
type RepositoryConfig struct {
	Endpoint  string `mapstructure:"endpoint"`
	UseSSL    bool   `mapstructure:"useSsl"`
	CACert    string `mapstructure:"caCert"`
	AccessKey string `mapstructure:"accessKey"`
	SecretKey string `mapstructure:"secretKey"`
}

// RepositoryConfigFromMap - parses repository config struct from raw map
func RepositoryConfigFromMap(raw map[string]interface{}) (*RepositoryConfig, error) {
	config := &RepositoryConfig{
		UseSSL: true,
	}

	d, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		Result:           config,
		ErrorUnused:      true,
		WeaklyTypedInput: true,
	})

	if err != nil {
		return nil, err
	}

	err = d.Decode(raw)
	if err != nil {
		return nil, err
	}

	return config, nil
}
