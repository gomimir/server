package s3

import (
	"context"
	"crypto/x509"
	"errors"
	"io"
	"strings"

	"regexp"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/minio/minio-go/v7/pkg/lifecycle"
	"github.com/rs/zerolog/log"
	"gitlab.com/gomimir/server/internal/apperror"
	"gitlab.com/gomimir/server/internal/repository"
	"gitlab.com/gomimir/server/internal/utils"
)

// Repository - S3 implementation of Repository interface
type Repository struct {
	minioClient *minio.Core
}

// NewRepository - constructor
func NewRepository(config *RepositoryConfig) (repository.Repository, error) {
	transport, err := minio.DefaultTransport(config.UseSSL)
	if err != nil {
		return nil, err
	}

	if config.CACert != "" {
		roots := x509.NewCertPool()
		if ok := roots.AppendCertsFromPEM([]byte(config.CACert)); !ok {
			return nil, errors.New("unable to append CA certificate")
		}

		transport.TLSClientConfig.RootCAs = roots
	}

	minioClient, err := minio.NewCore(config.Endpoint, &minio.Options{
		Creds:     credentials.NewStaticV4(config.AccessKey, config.SecretKey, ""),
		Secure:    config.UseSSL,
		Transport: transport,
	})

	if err != nil {
		return nil, err
	}

	return &Repository{
		minioClient: minioClient,
	}, nil
}

var bucketNameRx = regexp.MustCompile(`^(^[^/]+)\/?`)

func (repo *Repository) EnsureTemp(ctx context.Context, prefix string) error {
	bucketName, objectPrefix, err := parsePrefix(prefix)
	if err != nil {
		return nil
	}

	exists, err := repo.minioClient.BucketExists(ctx, bucketName)
	if err != nil {
		return err
	}

	if exists {
		// TODO: We should also check the existence of the Lifecycle rule, especially if objectPrefix is involved
		return nil
	}

	err = repo.minioClient.MakeBucket(ctx, bucketName, minio.MakeBucketOptions{})
	if err != nil {
		return err
	}

	lifecycleRule := lifecycle.Rule{
		ID: "Temp",
		Expiration: lifecycle.Expiration{
			Days: 2,
		},
		Status: "Enabled",
	}

	if objectPrefix != "" {
		lifecycleRule.RuleFilter = lifecycle.Filter{
			Prefix: utils.EnsureTrailingSlash(objectPrefix),
		}
	}

	err = repo.minioClient.SetBucketLifecycle(ctx, bucketName, &lifecycle.Configuration{
		Rules: []lifecycle.Rule{
			lifecycleRule,
		},
	})

	if err != nil {
		return err
	}

	return nil
}

// ListFiles - outputs repository entries to the channel
func (repo *Repository) ListFiles(ctx context.Context, prefix string) (chan repository.Entry, error) {

	buckets, err := repo.minioClient.ListBuckets(ctx)
	if err != nil {
		return nil, err
	}

	channel := make(chan repository.Entry)

	go func(resultC chan repository.Entry) {
		m := bucketNameRx.FindStringSubmatch(prefix)
		if len(m) > 0 && m[0][len(m[0])-1] == '/' {
			bucketName := m[1]
			objectName := prefix[len(m[0]):]
			repo.listFilesInBucket(ctx, bucketName, objectName, resultC)
		} else {
			for _, bucket := range buckets {
				if len(m) == 0 || strings.HasPrefix(bucket.Name, m[1]) {
					repo.listFilesInBucket(ctx, bucket.Name, "", resultC)
				}
			}
		}

		close(resultC)
	}(channel)

	return channel, nil
}

// GetFile - retrieves single file
func (repo *Repository) GetFile(ctx context.Context, filename string) (*repository.GetFile, error) {
	bucketName, objectName, err := parseFilename(filename)
	if err != nil {
		return nil, err
	}

	obj, err := repo.minioClient.Client.GetObject(ctx, bucketName, objectName, minio.GetObjectOptions{})
	if err != nil {
		if errResp, ok := err.(minio.ErrorResponse); ok {
			if errResp.Code == "NoSuchKey" {
				return nil, apperror.New(apperror.FileNotFound, "file %v was not found", filename)
			}
		}

		return nil, err
	}

	info, err := obj.Stat()
	if err != nil {
		return nil, err
	}

	file := &repository.GetFile{
		Reader:       obj,
		Size:         info.Size,
		LastModified: &info.LastModified,
	}

	if info.ETag != "" {
		file.ETag = &info.ETag
	}

	return file, nil
}

// UploadFile - uploads file to the repository
func (repo *Repository) UploadFile(ctx context.Context, filename string, fileSize int64, reader io.Reader) error {
	bucketName, objectName, err := parseFilename(filename)
	if err != nil {
		return err
	}

	sublog := log.With().Str("bucket", bucketName).Logger()

	// Check if upload bucket exists
	bucketExists, err := repo.minioClient.Client.BucketExists(context.TODO(), bucketName)
	if err != nil {
		sublog.Error().Err(err).Msg("Error occurred while checking if bucket exists")
		return err
	}

	// Create upload bucket if it does not exist
	if !bucketExists {
		sublog.Info().Msg("Upload bucket does not exist. Trying to create it.")

		err = repo.minioClient.Client.MakeBucket(context.TODO(), bucketName, minio.MakeBucketOptions{})
		if err != nil {
			sublog.Info().Err(err).Msg("Unable to create upload bucket")
			return err
		}

		sublog.Info().Msg("Upload bucket has been created")
	}

	_, err = repo.minioClient.Client.PutObject(context.TODO(), bucketName, objectName, reader, fileSize, minio.PutObjectOptions{})
	if err != nil {
		sublog.Error().Str("object", objectName).Err(err).Msg("Unable to put object on S3")
		return err
	}

	return nil
}

func (repo *Repository) DeleteFile(ctx context.Context, filename string) error {
	bucketName, objectName, err := parseFilename(filename)
	if err != nil {
		return err
	}

	return repo.minioClient.RemoveObject(ctx, bucketName, objectName, minio.RemoveObjectOptions{})
}

func (repo *Repository) listFilesInBucket(ctx context.Context, bucketName string, prefix string, entryC chan repository.Entry) {
	for obj := range repo.minioClient.Client.ListObjects(ctx, bucketName, minio.ListObjectsOptions{
		Prefix:    prefix,
		Recursive: true,
	}) {
		if obj.Err != nil {
			log.Error().Err(obj.Err).Msg("Error occurred while listing objects")
			return
		} else {
			entryC <- NewEntry(bucketName, obj)
		}
	}
}

func parseFilename(filename string) (string, string, error) {
	m := bucketNameRx.FindStringSubmatch(filename)
	if len(m) > 0 && m[0][len(m[0])-1] == '/' {
		bucketName := m[1]
		objectName := filename[len(m[0]):]

		return bucketName, objectName, nil
	}

	return "", "", apperror.New(apperror.InvalidFilename, "invalid filename, unable to parse bucket name from %v", filename)
}

func parsePrefix(prefix string) (string, string, error) {
	m := bucketNameRx.FindStringSubmatch(prefix)
	if len(m) > 0 {
		bucketName := m[1]
		objectPrefix := prefix[len(m[0]):]

		return bucketName, objectPrefix, nil
	}

	return "", "", apperror.New(apperror.InvalidFilename, "invalid prefix, unable to parse bucket name from %v", prefix)
}
