package filedeletequeue_test

import (
	"context"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/dataloader"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/testdata"
	"gitlab.com/gomimir/server/internal/filedeletequeue"
	"gitlab.com/gomimir/server/internal/idprovider"
	"gitlab.com/gomimir/server/internal/secret"
	"gitlab.com/gomimir/server/internal/testutils"
)

func TestFileDeleteQueue(t *testing.T) {
	client := testutils.EntClient(t)
	defer client.Close()

	ctx := context.Background()
	ctx = ent.NewContext(ctx, client)
	ctx = idprovider.ProviderContext(ctx, idprovider.NewTestProvider())
	ctx = secret.KeeperContext(ctx, secret.NewTestKeeper())
	ctx = dataloader.CacheContext(ctx)

	var repoEnt *ent.Repository
	var indexEnt *ent.Index
	databuilder := testdata.NewTestDataBuilder(ctx)
	databuilder.Require(testdata.IndexPhotos().Ref(&indexEnt))
	databuilder.Require(testdata.RepositoryMinio().Ref(&repoEnt))

	var deletedFilenames []string
	repoMock := testutils.MockRepository(t, repoEnt.ID)
	repoMock.MockDeleteFile(func(ctx context.Context, filename string) error {
		deletedFilenames = append(deletedFilenames, filename)
		return nil
	})

	assetRef := mimir.AssetRefForID(strings.ToLower(string(indexEnt.ID)), "XXXXXXXXXXX")

	fdq := filedeletequeue.NewFileDeleteQueue()

	err := fdq.Enqueue(ctx, assetRef, mimir.FileRef{
		Repository: string(repoEnt.ID),
		Filename:   "photos/skip_this.jpg",
	}, mimir.FileRef{
		Repository: string(repoEnt.ID),
		Filename:   "photos-sidecars/delete_this.jpg",
	}, mimir.FileRef{
		Repository: string(repoEnt.ID),
		Filename:   "non_existing_location/skip_this.jpg",
	}, mimir.FileRef{
		Repository: "non_existing_repo",
		Filename:   "photos-sidecars/skip_this.jpg",
	})

	require.NoError(t, err)

	stats, err := fdq.Execute(ctx)
	require.NoError(t, err)
	require.Equal(t, 4, stats.Total)
	require.Equal(t, 1, stats.Queued)
	require.Equal(t, 1, stats.Deleted)
	require.Equal(t, 3, stats.Skipped)

	require.Equal(t, []string{"photos-sidecars/delete_this.jpg"}, deletedFilenames)
}
