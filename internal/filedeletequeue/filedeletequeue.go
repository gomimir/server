package filedeletequeue

import (
	"context"
	"strings"
	"time"

	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/apperror"
	"gitlab.com/gomimir/server/internal/dataloader"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/filelocation"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/repository"
)

// In these location types we allow file deletion
var AllowedLocationTypes = map[filelocation.Type]struct{}{
	filelocation.TypeSidecar:   {},
	filelocation.TypeTemporary: {},
	filelocation.TypeUpload:    {},
}

type FileDeleteRequest struct {
	Repository repository.Repository
	FileRef    mimir.FileRef
}

type FileDeleteStats struct {
	Total   int
	Skipped int
	Queued  int
	Deleted int
	Took    time.Duration
}

type FileDeleteQueue struct {
	// Loaded file locations per index for faster lookup
	fileLocations map[pulid.ID][]*ent.FileLocation

	queue []FileDeleteRequest
	stats *FileDeleteStats
}

func NewFileDeleteQueue() *FileDeleteQueue {
	return &FileDeleteQueue{
		fileLocations: make(map[pulid.ID][]*ent.FileLocation),
		stats:         &FileDeleteStats{},
	}
}

func (fdq *FileDeleteQueue) Enqueue(ctx context.Context, assetRef mimir.AssetRef, fileRef ...mimir.FileRef) error {
	fdq.stats.Total += len(fileRef)

	indexFileLocations, err := fdq.fileLocationsForIndex(ctx, pulid.ID(strings.ToUpper(assetRef.IndexName())))
	if err != nil {
		return err
	}

	for _, currFileRef := range fileRef {
		repoID := pulid.ID(currFileRef.Repository)

		// Check if file is in location which allows us to delete files
		isInAllowedLocation := false
		for _, fl := range indexFileLocations {
			if fl.RepositoryID == repoID && strings.HasPrefix(currFileRef.Filename, fl.Prefix) {
				if _, ok := AllowedLocationTypes[fl.Type]; ok {
					isInAllowedLocation = true
				} else {
					// Sanity check in case of overlap
					isInAllowedLocation = false
					break
				}
			}
		}

		if isInAllowedLocation {
			repo, err := dataloader.RepositoryByIDCached(ctx, repoID)
			if err != nil {
				if apperror.MatchesCode(err, apperror.NotFound) {
					// We treat invalid repo reference as non-existing file => Skipping
					// This should generally not happen because there should not exist such
					// file location pointing to repository which does not exist.
					fdq.stats.Skipped++
					continue
				}

				return err
			}

			fdq.queue = append(fdq.queue, FileDeleteRequest{
				Repository: repo,
				FileRef:    currFileRef,
			})
		} else {
			fdq.stats.Skipped++
		}
	}

	return nil
}

func (fdq *FileDeleteQueue) Execute(ctx context.Context) (*FileDeleteStats, error) {
	var err error
	start := time.Now()
	fdq.stats.Queued = len(fdq.queue)

	for i, req := range fdq.queue {
		err = req.Repository.DeleteFile(ctx, req.FileRef.Filename)
		if err != nil {
			fdq.stats.Deleted = i
			return fdq.stats, err
		}
	}

	fdq.stats.Deleted = len(fdq.queue)
	fdq.stats.Took = time.Since(start)
	return fdq.stats, nil
}

func (fdq *FileDeleteQueue) fileLocationsForIndex(ctx context.Context, indexID pulid.ID) ([]*ent.FileLocation, error) {
	fl := fdq.fileLocations[indexID]
	if fl == nil {
		entClient := ent.FromContext(ctx)

		var err error
		fl, err = entClient.FileLocation.Query().Where(filelocation.IndexID(indexID)).All(ctx)
		if err != nil {
			return nil, err
		}

		fdq.fileLocations[indexID] = fl
	}

	return fl, nil
}
