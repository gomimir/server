package idprovider

import (
	"context"
	"errors"
)

type contextKey struct {
	name string
}

var ctxKey = &contextKey{"idProvider"}

func ProviderContext(parent context.Context, provider Provider) context.Context {
	return context.WithValue(parent, ctxKey, provider)
}

func ProviderForContext(ctx context.Context) (Provider, error) {
	if provider, ok := ctx.Value(ctxKey).(Provider); ok {
		return provider, nil
	}

	return nil, errors.New("unable to retrieve ID provider from context")
}
