package idprovider

import "fmt"

type testIDProvider struct {
	takenIDs map[string]int
}

func NewTestProvider() Provider {
	return &testIDProvider{
		takenIDs: make(map[string]int),
	}
}

func (p *testIDProvider) NextIDFor(prefix string) string {
	id := p.takenIDs[prefix] + 1
	p.takenIDs[prefix] = id
	return fmt.Sprintf("%v%026d", prefix, id)
}
