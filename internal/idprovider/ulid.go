package idprovider

import (
	"crypto/rand"
	"fmt"
	"time"

	"github.com/oklog/ulid/v2"
)

type defaultProvider struct {
	entropySource *ulid.MonotonicEntropy
}

func NewDefaultProvider() Provider {
	return &defaultProvider{
		entropySource: ulid.Monotonic(rand.Reader, 0),
	}
}

func (p *defaultProvider) NextIDFor(prefix string) string {
	id := ulid.MustNew(ulid.Timestamp(time.Now()), p.entropySource)
	return prefix + fmt.Sprint(id)
}
