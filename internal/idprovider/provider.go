package idprovider

type Provider interface {
	NextIDFor(prefix string) string
}
