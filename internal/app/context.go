package app

import (
	"context"

	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/idprovider"
	"gitlab.com/gomimir/server/internal/secret"
)

func (app *App) DefaultContext(parent context.Context) context.Context {
	newCtx := ent.NewContext(parent, app.EntClient)
	newCtx = idprovider.ProviderContext(newCtx, app.IDProvider)
	newCtx = secret.KeeperContext(newCtx, app.SecretKeeper)
	return newCtx
}
