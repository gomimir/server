package app

import (
	"fmt"

	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/auth/rolemapper"
	"gitlab.com/gomimir/server/internal/auth/usersync"
	"gitlab.com/gomimir/server/internal/config"
)

func authProvider(config *config.AuthConfig) (auth.Provider, error) {
	if config.Provider == nil {
		return auth.NewDummyProvider(rolemapper.NewDictionaryRoleMapper(rolemapper.DictionaryRoleMapperConfig{
			GuestRoles: []string{"guest", "user", "admin"},
		}))
	}

	rm := rolemapper.NewDictionaryRoleMapper(rolemapper.DictionaryRoleMapperConfig{
		GuestRoles: []string{"guest"},
		UserRoles:  []string{"user"},
	})

	switch config := config.Provider.(type) {
	case auth.ProxyConfig:
		return auth.NewProxyProvider(config, usersync.NewSynchronizer(), rm)
	case auth.OIDCConfig:
		return auth.NewOIDCProvider(config, usersync.NewSynchronizer(), rm)

	default:
		return nil, fmt.Errorf("no auth provider for config: %v", config.Type())
	}
}
