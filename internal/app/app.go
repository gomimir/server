package app

import (
	"context"
	"encoding/base64"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/icza/gox/gox"
	"github.com/mitchellh/hashstructure/v2"
	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/config"
	"gitlab.com/gomimir/server/internal/documents"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/idprovider"
	"gitlab.com/gomimir/server/internal/indexer"
	"gitlab.com/gomimir/server/internal/scheduler"
	"gitlab.com/gomimir/server/internal/scheduler/redisscheduler"
	"gitlab.com/gomimir/server/internal/secret"
)

// App - application context
type App struct {
	Config       *config.Config
	AuthProvider auth.Provider
	EntClient    *ent.Client
	RedisClient  *redis.Client
	IDProvider   idprovider.Provider
	SecretKeeper secret.Keeper

	LinkSignatureKey []byte

	Indexer indexer.Indexer

	TaskManager  mimir.TaskManager
	TaskHandlers map[string]mimir.TaskHandler
	JobMonitor   mimir.JobMonitor

	Rand              *rand.Rand
	AppInfo           mimir.AppInfo
	TimeZoneOffsetStr string
}

// NewApp - constructor
func NewApp(appInfo mimir.AppInfo) *App {
	return &App{
		TaskHandlers:      make(map[string]mimir.TaskHandler),
		Rand:              rand.New(rand.NewSource(time.Now().UnixNano())),
		AppInfo:           appInfo,
		TimeZoneOffsetStr: time.Now().Format("-07:00"),
	}
}

// Initializes application instance with configuration
func (app *App) InitializeWithConfig(config *config.Config) {
	var err error

	// Application config
	app.Config = config

	// ID provider
	app.IDProvider = idprovider.NewDefaultProvider()

	// Configurable payload crypter
	if app.Config.Server.PayloadSecret == "" {
		app.SecretKeeper, err = secret.NewAESKeeperWithRandKey()
		log.Warn().Msg("Using random payload secret. You won't be able to resume upload sessions after restart. This can cause additional problems in HA mode.")
	} else {
		app.SecretKeeper, err = secret.NewAESKeeperFromBase64Str(app.Config.Server.PayloadSecret)
	}

	if err != nil {
		log.Fatal().Err(err).Msg("Unable to initialize secret keeper")
		os.Exit(1)
	}

	// Database client
	app.EntClient = initDB(idprovider.ProviderContext(context.Background(), app.IDProvider), config, app.SecretKeeper)

	// Configurable secret
	if app.Config.Server.LinkSecret == "" {
		app.LinkSignatureKey = make([]byte, 16)
		_, err = rand.Read(app.LinkSignatureKey)
		log.Warn().Msg("Using random link signing secret. You won't be able to properly cache your files. This can cause additional problems in HA mode.")
	} else {
		app.LinkSignatureKey, err = base64.StdEncoding.DecodeString(app.Config.Server.LinkSecret)
	}

	if err != nil {
		log.Fatal().Err(err).Msg("Unable to initialize link signing secret")
		os.Exit(1)
	}

	app.AuthProvider, err = authProvider(&config.Server.Auth)
	if err != nil {
		log.Fatal().Err(err).Msg("Unable to initialize auth. provider")
		os.Exit(1)
	}
}

// RegisterTaskHandler - registers task handler
func (app *App) RegisterTaskHandler(name string, handler mimir.TaskHandler) {
	app.TaskHandlers[name] = handler
}

func (app *App) startJobMonitoring(ctx context.Context) {
	var err error
	app.JobMonitor, err = app.TaskManager.MonitorJobs(ctx)
	if err != nil {
		log.Fatal().Err(err).Msg("Unable to start job monitoring")
		return
	}

	go func() {
		err := <-app.JobMonitor.Done()
		log.Fatal().Err(err).Msg("Job monitor stopped")
	}()
}

func (app *App) startTaskReportProcessing(ctx context.Context) {
	err := app.TaskManager.ProcessTaskReports(ctx, mimir.ProcessTaskReportsOpts{
		Handler: func(report mimir.TaskReport) error {
			// Do not log reports from ourselves and dummy wait queue
			if (report.Processor() == nil || report.Processor().Name != app.AppInfo.Name) && report.Task().Name() != "waitForAllFiles" {

				sublog := log.With().Str("task", report.Task().Name()).Str("task_id", report.Task().ID()).Logger()

				// Update file processing info on the assets
				var taskParams struct {
					Asset  *mimir.AssetRef        `json:"asset"`
					File   mimir.FileRef          `json:"file"`
					Config map[string]interface{} `json:"config"`
				}

				err := report.Task().Parameters().LoadTo(&taskParams)
				if err != nil {
					sublog.Error().Err(err).Msg("Unable to parse task report parameters")
					return err
				} else {
					if taskParams.File.Filename != "" && taskParams.File.Repository != "" && taskParams.Asset != nil {
						sublog = sublog.With().Str("filename", taskParams.File.Filename).Str("repository", taskParams.File.Repository).Logger()

						query := &indexer.AssetFilter{
							WithFile: &indexer.AssetFileFilter{
								RepositoryName: []indexer.TextFilter{{Eq: gox.NewString(taskParams.File.Repository)}},
								Filename:       []indexer.TextFilter{{Eq: gox.NewString(taskParams.File.Filename)}},
							},
						}

						hash, err := hashstructure.Hash(taskParams.Config, hashstructure.FormatV2, nil)
						if err != nil {
							sublog.Error().Err(err).Msg("Unable to hash processing config")
							return err
						}

						configHash := strconv.FormatUint(hash, 16)

						_, err = app.Indexer.UpdateAssetsByQuery([]string{taskParams.Asset.IndexName()}, query, func(asset documents.IndexedAsset) documents.IndexedAsset {
							fileIndex := asset.Files.IndexByRef(taskParams.File)
							if fileIndex > -1 {
								fp := documents.FileProcessing{
									JobID:       report.Job().ID(),
									Name:        report.Task().Name(),
									ProcessedAt: report.Task().QueuedAt(),
									ConfigHash:  configHash,
								}

								if report.Error() != nil {
									code := report.Error().Code()
									if code == "" {
										code = "UNKNOWN"
									}

									fp.Error = &documents.FileProcessingError{
										Code:    code,
										Message: report.Error().Error(),
									}
								} else {
									fp.Took = gox.NewInt(int(report.Took().Milliseconds()))
								}

								file := &asset.Files[fileIndex]
								file.EnsureProcessing(fp)
							}

							return asset
						})

						if err != nil {
							sublog.Error().Err(err).Msg("Unable to update assets with processing results")
							return err
						} else {
							sublog.Debug().Msg("Processing results updated")
						}
					}
				}

				// Log it
				if report.Processor() != nil {
					sublog = sublog.With().Str("processor", report.Processor().Name).Str("processor_version", report.Processor().Version.String()).Logger()
				}

				if report.Took() != 0 {
					sublog = sublog.With().Str("took", fmt.Sprintf("%v", report.Took())).Logger()
				}

				if report.Error() != nil {
					sublog.Warn().Err(report.Error()).Msg("Task reported as failed")
				} else {
					sublog.Debug().Msg("Task reported as finished")
				}
			}

			return nil
		},
	})

	if err != nil {
		log.Fatal().Err(err).Msg("Task report processor stopped")
	}
}

func (app *App) startMainQueueProcessing(ctx context.Context) {
	queue := "main"

	sublog := log.With().Str("queue", queue).Logger()
	sublog.Info().Msg("Ready for tasks")

	err := app.TaskManager.ProcessQueue(ctx, mimir.ProcessQueueOpts{
		QueueName: queue,
		Processor: &app.AppInfo,
		Handler: func(req mimir.HandleRequest) error {
			if req.Error() != nil {
				sublog.Error().Err(req.Error()).Msg("Failed to process queued task")
				return req.Error()
			}

			if handler, ok := app.TaskHandlers[req.Task().Name()]; ok {
				return handler(req)
			}

			err := fmt.Errorf("no handler for task: %v", req.Task().Name())
			sublog.Error().Err(err).Str("task", req.Task().Name()).Str("task_id", req.Task().ID()).Msg("Failed to process queued task")

			return err
		},
		IdleTimeout: 5 * time.Second,
		IdleHandler: func(summary mimir.ProcessingSummary) {
			sublog.Info().Msgf("Idle. Processed %v tasks (%v failed) in %v since last idle state.",
				summary.NumProcessed,
				summary.NumProcessed-summary.NumSucceeded,
				summary.TimeSpent,
			)
		},
	})

	if err != nil {
		log.Fatal().Err(err).Msg("Main queue processor stopped")
	}
}

// Run - handles the queued requests
func (app *App) Run(ctx context.Context, queueName string) <-chan struct{} {
	app.startJobMonitoring(ctx)
	go app.startTaskReportProcessing(ctx)
	go app.startMainQueueProcessing(ctx)

	sched := redisscheduler.NewScheduler(app.RedisClient, app.TaskManager)
	err := sched.SetupSchedule(ctx, scheduler.TaskSchedule{
		TaskName: "cleanTemp",
		NextTime: scheduler.RepeatEvery(24 * time.Hour),
	})

	if err != nil {
		log.Error().Err(err).Msg("Failed to setup schedule")
		os.Exit(1)
	}

	go func() {
		for {
			err := sched.Run(ctx)
			if err != nil {
				log.Error().Err(err).Msg("Scheduler finished with error, restarting")
			}
		}
	}()

	return ctx.Done()
}
