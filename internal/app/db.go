package app

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"

	"database/sql"

	entsql "entgo.io/ent/dialect/sql"

	"github.com/rs/zerolog/log"
	"gitlab.com/gomimir/server/internal/config"
	"gitlab.com/gomimir/server/internal/ent"
	_ "gitlab.com/gomimir/server/internal/ent/runtime"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/ent/schema/schematype"
	"gitlab.com/gomimir/server/internal/goose"
	"gitlab.com/gomimir/server/internal/secret"
	_ "modernc.org/sqlite"
)

func initDB(ctx context.Context, cfg *config.Config, secretKeeper secret.Keeper) *ent.Client {

	sqlDb, err := sql.Open("sqlite", cfg.Database.ConnectionString)
	if err != nil {
		log.Fatal().Err(err).Msg("Unable to initialize the database")
		os.Exit(1)
	}

	sqlDb.SetMaxOpenConns(100)
	drv := entsql.OpenDB("sqlite3", sqlDb)

	// TODO: https://github.com/go-redsync/redsync
	goose.Migrate(drv.DB())

	client := ent.NewClient(ent.Driver(drv))

	for i, repo := range cfg.Provision.Repositories {
		err := provisionRepository(ctx, client, repo, secretKeeper)
		if err != nil {
			log.Fatal().Err(err).Msgf("Failed to provision repository #%v: %v", i, err)
			os.Exit(1)
		}
	}

	return client
}

func provisionRepository(ctx context.Context, entClient *ent.Client, data config.RepositoryProvisioning, secretKeeper secret.Keeper) error {
	cfg := &schematype.RepositoryConfig{}

	if data.S3 != nil {
		s3Cfg := &schematype.S3RepositoryConfig{}
		cfg.S3 = s3Cfg

		if data.S3.Endpoint != nil {
			s3Cfg.Endpoint = *data.S3.Endpoint
		}

		if data.S3.UseSSL != nil {
			s3Cfg.UseSSL = *data.S3.UseSSL
		}

		if data.S3.CACertFile != nil && *data.S3.CACertFile != "" {
			caCertBytes, err := ioutil.ReadFile(*data.S3.CACertFile)
			if err != nil {
				return fmt.Errorf("unable to read CA certificate: %v", err)
			}

			s3Cfg.CACert = string(caCertBytes)
		}

		if data.S3.AccessKey != nil {
			s3Cfg.AccessKey = *data.S3.AccessKey
		}

		if data.S3.SecretKey != nil {
			secret, err := secretKeeper.EncryptString(*data.S3.SecretKey)
			if err != nil {
				return err
			}

			s3Cfg.SecretKey = secret
		}
	}

	if data.Local != nil {
		if data.S3 != nil {
			return fmt.Errorf("local and s3 repository configuration options are mutually exclusive")
		}

		localCfg := &schematype.LocalRepositoryConfig{}
		cfg.Local = localCfg
		cfg.Local.Path = data.Local.Path
	}

	name := "Auto-provisioned repository"
	if data.Name != nil {
		name = *data.Name
	}

	return entClient.Repository.Create().
		SetID(pulid.ID(data.ID)).
		SetName(name).
		SetConfig(cfg).
		OnConflict().UpdateNewValues().
		Exec(ctx)
}
