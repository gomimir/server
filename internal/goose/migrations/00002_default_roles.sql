-- +goose Up
INSERT INTO user_roles (id, name, display_name) VALUES
('UR01FVQBXR9GND2G7B58MRHAZ3E3', 'admin', 'Administrator'),
('UR01FVQBXR9GND2G7B58MRN8K6GX', 'user', 'User'),
('UR01FVQBXR9GND2G7B58MV5MQYVK', 'guest', 'Guest');
