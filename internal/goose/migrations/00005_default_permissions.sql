-- +goose Up

-- Admin role permissions
INSERT INTO global_permissions (id, subject_id, resource, collection_operations, item_operations) VALUES
('GP01FVQDZE5YFVVMAYBCEKDA7AED', 'UR01FVQBXR9GND2G7B58MRHAZ3E3', 'USER_ROLE', '["LIST"]', '["READ"]');