package goose

import (
	"database/sql"
	"fmt"
	"os"
	"strings"

	"github.com/rs/zerolog/log"

	"embed"

	"github.com/pressly/goose/v3"
)

//go:embed migrations/*.sql
var embedMigrations embed.FS

type customLogger struct{}

func (*customLogger) Fatal(v ...interface{})                 { log.Fatal().Msg(fmt.Sprint(v...)) }
func (*customLogger) Fatalf(format string, v ...interface{}) { log.Fatal().Msgf(format, v...) }
func (*customLogger) Print(v ...interface{})                 { log.Info().Msg(fmt.Sprint(v...)) }
func (*customLogger) Println(v ...interface{})               { log.Info().Msg(fmt.Sprint(v...)) }
func (*customLogger) Printf(format string, v ...interface{}) {
	log.Info().Msgf(strings.TrimRight(format, "\n"), v...)
}

func Migrate(db *sql.DB) {

	// provider := idprovider.NewDefaultProvider()
	// for i := 0; i < 10; i++ {
	// 	fmt.Println(provider.NextIDFor("GP"))
	// }
	// os.Exit(1)

	goose.SetDialect("sqlite3")
	goose.SetBaseFS(embedMigrations)
	goose.SetLogger(&customLogger{})

	log.Info().Msg("Running database migrations")

	err := goose.Up(db, "migrations")
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to migrate the database")
		os.Exit(1)
	} else {
		log.Info().Msg("Database migration finished")
	}
}
