package server

import (
	"context"
	"fmt"
	"math/rand"
	"net/http"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/gomimir/server/internal/apperror"
	"gitlab.com/gomimir/server/internal/dataloader"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/utils"
)

func NewFileHandler(opts FileHandlerOpts) func(http.ResponseWriter, *http.Request) {
	rx := regexp.MustCompile(`^/file/([^/]+)/(.+)$`)
	rxSuffix := regexp.MustCompile(`[^.]+$`)

	return func(rw http.ResponseWriter, r *http.Request) {
		if r.Method != "GET" && r.Method != "HEAD" {
			rw.WriteHeader(405)
			return
		}

		if m := rx.FindStringSubmatch(r.URL.Path); len(m) > 0 {
			repositoryName := m[1]
			filename := m[2]

			sublog := log.With().Str("repository", repositoryName).Logger()
			repo, err := dataloader.RepositoryByID(r.Context(), pulid.ID(repositoryName))
			if err == nil {
				sublog = sublog.With().Str("filename", filename).Logger()

				if !opts.DisableSignatureChecks {
					values := r.URL.Query()
					signature := values.Get("s")

					if signature == "" {
						rw.WriteHeader(401)
						return
					}

					err := utils.CheckFileSignature(repositoryName, filename, signature, opts.SignatureKey)
					if err != nil {
						sublog.Warn().Str("remote_addr", r.RemoteAddr).Msg("Invalid file link signature")
						time.Sleep(time.Duration((20 + rand.Intn(1000)) * int(time.Millisecond)))
						rw.WriteHeader(403)
						return
					}
				}

				res, err := repo.GetFile(context.Background(), filename)

				if err != nil {
					if apperror.MatchesCode(err, apperror.FileNotFound) {
						sublog.Info().Err(err).Msg("File does not exist")
						rw.WriteHeader(404)
						return
					}

					sublog.Info().Err(err).Msg("Unable to get file")
					rw.WriteHeader(500)
					return
				}

				defer res.Reader.Close()

				suffix := strings.ToLower(rxSuffix.FindString(filename))
				switch suffix {
				// Images
				case "jpg", "jpeg":
					rw.Header().Add("Content-Type", "image/jpeg")
				case "webp":
					rw.Header().Add("Content-Type", "image/webp")

				// Video files
				// http://help.encoding.com/knowledge-base/article/correct-mime-types-for-serving-video-files/
				case "mp4":
					rw.Header().Add("Content-Type", "video/mp4")
				case "m4v":
					rw.Header().Add("Content-Type", "video/mp4")
				case "mov":
					rw.Header().Add("Content-Type", "video/quicktime")

				// PDF documents
				case "pdf":
					rw.Header().Add("Content-Type", "application/pdf")
				}

				rw.Header().Add("Content-Length", fmt.Sprintf("%v", res.Size))

				// Long-term caching for requests with signature
				// (it has last modified date in it => it will change and generate different hit)
				if !opts.DisableSignatureChecks {
					rw.Header().Add("Cache-Control", "max-age=31536000")
				}

				q := r.URL.Query()
				if _, ok := q["download"]; ok {
					rw.Header().Add("Content-Disposition", fmt.Sprintf("attachment; filename=%v", filepath.Base(filename)))
				}

				var lastmod time.Time
				if res.LastModified != nil {
					lastmod = *res.LastModified
				}

				if res.ETag != nil {
					rw.Header().Add("Etag", fmt.Sprintf("\"%v\"", *res.ETag))
				}

				http.ServeContent(rw, r, filepath.Base(filename), lastmod, res.Reader)
				res.Reader.Close()

				return
			}

			sublog.Error().Err(err).Msg("Unable to resolve repository")
		}

		rw.WriteHeader(404)
	}
}
