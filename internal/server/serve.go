package server

import (
	"net"
	"net/http"

	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/justinas/alice"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/cors"
	"github.com/soheilhy/cmux"
	"gitlab.com/gomimir/server/internal/app"
	"gitlab.com/gomimir/server/internal/graphql"
	"gitlab.com/gomimir/server/internal/grpc"
)

type HandlerOpts struct {
	Enabled bool
}

type FileHandlerOpts struct {
	HandlerOpts
	DisableSignatureChecks bool
	SignatureKey           []byte
}

type ServerOpts struct {
	Addr string

	GRPC       HandlerOpts
	GraphQL    HandlerOpts
	Metrics    HandlerOpts
	Playground HandlerOpts
	Files      FileHandlerOpts
}

// Serve - starts web server
// The solution is based on: https://github.com/grpc/grpc-go/issues/555#issuecomment-506865430
func Serve(app *app.App, opts ServerOpts) error {
	l, err := net.Listen("tcp", opts.Addr)
	if err != nil {
		return err
	}

	m := cmux.New(l)

	httpL := m.Match(cmux.HTTP1Fast())

	errC := make(chan error, 3)

	if opts.GRPC.Enabled {
		grpcL := m.MatchWithWriters(cmux.HTTP2MatchHeaderFieldSendSettings("content-type", "application/grpc"))
		go func() { errC <- grpc.Listen(grpcL, app) }()
	}

	go func() { errC <- http.Serve(httpL, httpHandler(app, opts)) }()
	go func() { errC <- m.Serve() }()

	return <-errC
}

func httpHandler(app *app.App, opts ServerOpts) http.Handler {
	mux := http.NewServeMux()

	if opts.Metrics.Enabled {
		mux.Handle("/metrics", promhttp.Handler())
	}

	if opts.Playground.Enabled {
		mux.Handle("/playground", playground.Handler("GraphQL playground", "/graphql"))
	}

	if opts.GraphQL.Enabled {
		mux.Handle("/graphql", graphql.HTTPHandler(graphql.HTTPHandlerOpts{
			AppVersionInfo:    app.AppInfo.Version,
			Indexer:           app.Indexer,
			AuthProvider:      app.AuthProvider,
			TimeZoneOffsetStr: app.TimeZoneOffsetStr,
			TaskManager:       app.TaskManager,
			JobMonitor:        app.JobMonitor,
			LinkSignatureKey:  app.LinkSignatureKey,
			Rand:              app.Rand,
		}))
	}

	if opts.Files.Enabled {
		mux.HandleFunc("/file/", NewFileHandler(opts.Files))
	}

	corsMiddle := cors.New(cors.Options{
		AllowedOrigins: app.Config.Server.CORS.AllowedOrigins,
		AllowedHeaders: []string{"*"},
	}).Handler

	return alice.New(corsMiddle, func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			newCtx := app.DefaultContext(r.Context())
			next.ServeHTTP(w, r.WithContext(newCtx))
		})
	}).Then(mux)
}
