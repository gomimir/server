package server_test

import (
	"bytes"
	"context"
	"io/ioutil"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/testdata"
	"gitlab.com/gomimir/server/internal/idprovider"
	"gitlab.com/gomimir/server/internal/repository"
	"gitlab.com/gomimir/server/internal/secret"
	"gitlab.com/gomimir/server/internal/server"
	"gitlab.com/gomimir/server/internal/testutils"
	"gitlab.com/gomimir/server/internal/utils"
)

type MockReadSeekCloser struct {
	*bytes.Reader
}

func (mrsc *MockReadSeekCloser) Close() error { return nil }

func TestFileHandler(t *testing.T) {
	client := testutils.EntClient(t)
	defer client.Close()

	ctx := context.Background()
	ctx = ent.NewContext(ctx, client)
	ctx = idprovider.ProviderContext(ctx, idprovider.NewTestProvider())
	ctx = secret.KeeperContext(ctx, secret.NewTestKeeper())

	var repoEnt *ent.Repository
	databuilder := testdata.NewTestDataBuilder(ctx)
	databuilder.Require(testdata.RepositoryMinio().Ref(&repoEnt))

	signatureKey := []byte("abcdefghijklmnop")
	url, err := utils.FileUrl(string(repoEnt.ID), "photos/someAlbum/image12.jpg", 1646845689255959000, signatureKey)
	require.NoError(t, err)

	fileContent := []byte("hello world")

	repoMock := testutils.MockRepository(t, repoEnt.ID)
	repoMock.MockGetFile(func(ctx context.Context, filename string) (*repository.GetFile, error) {
		return &repository.GetFile{
			Reader: &MockReadSeekCloser{Reader: bytes.NewReader(fileContent)},
			Size:   int64(len(fileContent)),
		}, nil
	})

	r := httptest.NewRequest("GET", url, nil).WithContext(ctx)
	w := httptest.NewRecorder()

	handler := server.NewFileHandler(server.FileHandlerOpts{
		HandlerOpts:            server.HandlerOpts{Enabled: true},
		DisableSignatureChecks: false,
		SignatureKey:           signatureKey,
	})

	handler(w, r)

	require.Equal(t, 200, w.Code)

	bytes, err := ioutil.ReadAll(w.Result().Body)
	require.NoError(t, err)
	require.Equal(t, fileContent, bytes)
}
