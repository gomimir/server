package server

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var repoTTFB = promauto.NewHistogramVec(prometheus.HistogramOpts{
	Name:    "repo_time_to_first_byte",
	Help:    "Time to get a first byte of file content in the repository",
	Buckets: []float64{0.005, 0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1},
}, []string{"repository"})

func init() {
	prometheus.Register(repoTTFB)
}
