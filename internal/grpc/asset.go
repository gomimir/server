package grpc

import (
	"fmt"
	"reflect"
	"strings"
	"time"

	mimir "gitlab.com/gomimir/processor"
	mimirpb "gitlab.com/gomimir/processor/protobuf"
	mimirpbconv "gitlab.com/gomimir/processor/protobufconv"
	"gitlab.com/gomimir/server/internal/documents"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func buildAssetUpdater(req *mimirpb.UpdateAssetsRequest) (func(asset documents.IndexedAsset) documents.IndexedAsset, error) {
	matchAsset, err := buildAssetMatcher(req.Selector)
	if err != nil {
		return nil, err
	}

	setProperties, err := processPBProps(req.SetProperties)
	if err != nil {
		return nil, err
	}

	reqsWithMatchers := make([]struct {
		Match         func(documents.AssetFile) bool
		Req           *mimirpb.UpdateAssetFileRequest
		SetProperties *mimir.Properties
	}, len(req.UpdateFiles))

	for i, fileUpdReq := range req.UpdateFiles {
		reqsWithMatchers[i].Req = fileUpdReq
		reqsWithMatchers[i].Match, err = buildAssetFileMatcher(fileUpdReq.Selector)
		if err != nil {
			return nil, err
		}

		reqsWithMatchers[i].SetProperties, err = processPBProps(fileUpdReq.SetProperties)
		if err != nil {
			return nil, err
		}
	}

	return func(asset documents.IndexedAsset) documents.IndexedAsset {
		if !matchAsset(asset) {
			return asset
		}

		if setProperties != nil {
			for _, propPair := range setProperties.Pairs() {
				asset.Properties.Set(propPair.Name, propPair.Value)
			}
		}

		for _, propName := range req.UnsetProperties {
			asset.Properties.Unset(propName)
		}

		for _, tag := range req.AddTags {
			asset.Tags.AssignTag(documents.Tag{Kind: tag.Kind, Name: tag.Name}, documents.UserTagAssignment{
				UserID:    "SYSTEM",
				CreatedAt: time.Now(),
			})
		}

		for _, tag := range req.RemoveTags {
			asset.Tags.RemoveTag(documents.Tag{Kind: tag.Kind, Name: tag.Name})
		}

		for assetFileIdx, assetFile := range asset.Files {
			for _, fileUpdReq := range reqsWithMatchers {
				if !fileUpdReq.Match(assetFile) {
					continue
				}

				// Setting properties
				if fileUpdReq.SetProperties != nil {
					for _, propPair := range fileUpdReq.SetProperties.Pairs() {
						asset.Files[assetFileIdx].Properties.Set(propPair.Name, propPair.Value)
					}
				}

				// Unsetting properties
				for _, propName := range fileUpdReq.Req.UnsetProperties {
					asset.Files[assetFileIdx].Properties.Unset(propName)
				}
			}
		}

		return asset
	}, nil
}

func buildAssetMatcher(sel interface{}) (func(asset documents.IndexedAsset) bool, error) {
	switch sel := sel.(type) {
	case *mimirpb.UpdateAssetsRequest_Ref:
		return func(asset documents.IndexedAsset) bool {
			return asset.AssetRef.AssetID() == sel.Ref.AssetId && asset.AssetRef.IndexName() == sel.Ref.Index
		}, nil

	case *mimirpb.UpdateAssetFileRequest_Simple:
		switch sel.Simple {
		case mimirpb.SimpleSelector_NONE:
			return func(asset documents.IndexedAsset) bool {
				return false
			}, nil
		case mimirpb.SimpleSelector_ALL:
			return func(asset documents.IndexedAsset) bool {
				return true
			}, nil
		default:
			return nil, status.Errorf(codes.Unimplemented, "simple asset selector %v not implemented", sel.Simple.String())
		}

	default:
		return nil, status.Errorf(codes.Unimplemented, "asset selector %v not implemented", reflect.TypeOf(sel))
	}
}

func buildAssetFileMatcher(sel interface{}) (func(assetFile documents.AssetFile) bool, error) {
	switch sel := sel.(type) {
	case *mimirpb.UpdateAssetFileRequest_Ref:
		return func(assetFile documents.AssetFile) bool {
			return assetFile.FileRef.Filename == sel.Ref.Filename && assetFile.FileRef.Repository == sel.Ref.Repository
		}, nil

	case *mimirpb.UpdateAssetFileRequest_Simple:
		switch sel.Simple {
		case mimirpb.SimpleSelector_NONE:
			return func(assetFile documents.AssetFile) bool {
				return false
			}, nil
		case mimirpb.SimpleSelector_ALL:
			return func(assetFile documents.AssetFile) bool {
				return true
			}, nil
		default:
			return nil, status.Errorf(codes.Unimplemented, "simple asset file selector %v not implemented", sel.Simple.String())
		}

	default:
		return nil, status.Errorf(codes.Unimplemented, "asset file selector %v not implemented", reflect.TypeOf(sel))
	}
}

func processPBProps(pairs []*mimirpb.PropertyPair) (*mimir.Properties, error) {
	if len(pairs) > 0 {
		if err := validatePropAssignments(pairs); err != nil {
			return nil, err
		}

		setProperties, err := mimirpbconv.PropertiesFromProtoBuf(pairs)
		if err != nil {
			return nil, err
		}

		return setProperties, nil
	}

	return nil, nil
}

func validatePropAssignments(pairs []*mimirpb.PropertyPair) error {
	for propIdx, propPair := range pairs {
		if propPair.Name == "" {
			return fmt.Errorf("invalid property name at %v: cannot be empty", propIdx)
		} else if strings.Contains(propPair.Name, ".") {
			return fmt.Errorf("invalid property name at %v: cannot contain dots", propIdx)
		}
	}

	return nil
}
