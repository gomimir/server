package grpc

import (
	"bytes"
	"context"
	"errors"
	"io"
	"net"
	"reflect"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
	mimirpb "gitlab.com/gomimir/processor/protobuf"
	mimirpbconv "gitlab.com/gomimir/processor/protobufconv"
	"gitlab.com/gomimir/server/internal/app"
	"gitlab.com/gomimir/server/internal/apperror"
	"gitlab.com/gomimir/server/internal/dataloader"
	"gitlab.com/gomimir/server/internal/documents"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/filelocation"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/utils"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type server struct {
	mimirpb.UnimplementedMimirServer
	App *app.App
}

func (s *server) ServerVersion(context.Context, *emptypb.Empty) (*mimirpb.ServerVersionReply, error) {
	return &mimirpb.ServerVersionReply{
		VersionString: s.App.AppInfo.Version.String(),
	}, nil
}

func (s *server) UpdateAssets(ctx context.Context, req *mimirpb.UpdateAssetsRequest) (*emptypb.Empty, error) {
	updater, err := buildAssetUpdater(req)
	if err != nil {
		return nil, err
	}

	switch sel := req.Selector.(type) {
	case *mimirpb.UpdateAssetsRequest_Ref:
		_, err := s.App.Indexer.UpdateAsset(mimir.AssetRefForID(sel.Ref.Index, sel.Ref.AssetId), updater)
		if err != nil {
			return nil, err
		}
	default:
		return nil, status.Errorf(codes.Unimplemented, "asset selector %v not implemented", reflect.TypeOf(sel))
	}

	return &emptypb.Empty{}, nil
}

func (s *server) UploadSidecars(ctx context.Context, req *mimirpb.UploadSidecarsRequest) (*emptypb.Empty, error) {
	fl, err := s.App.EntClient.FileLocation.Query().Where(
		filelocation.IndexID(pulid.ID(strings.ToUpper(req.Asset.Index))),
		filelocation.TypeEQ(filelocation.TypeSidecar),
	).First(ctx)

	if err != nil {
		if ent.IsNotFound(err) {
			log.Warn().Msg("Sidecars repository is not configured")
			return nil, errors.New("sidecar file repository is not configured")
		} else {
			return nil, err
		}
	}

	repo, err := dataloader.RepositoryByID(ctx, fl.RepositoryID)
	if err != nil {
		log.Warn().Str("repository", string(fl.RepositoryID)).Err(err).Msg("Unable to get sidecar file repository")
		return nil, err
	}

	now := time.Now().UTC()
	filenameGen := utils.NewUploadFilenameGenerator(fl.Prefix, s.App.Rand)
	fileReqs := make([]struct {
		AssetFileRef mimir.FileRef
		Filename     string
		Size         int64
		Reader       io.Reader
		Sidecar      documents.Sidecar
	}, len(req.Sidecars))

	// Preprocess request to fail early
	for i, sidecarReq := range req.Sidecars {
		filename := utils.SanitizeRelPath(sidecarReq.Filename)
		if filename == "" {
			return nil, apperror.New(apperror.InvalidUploadFilename, "invalid upload filename (#%v) %v", i, sidecarReq.Filename)
		}

		fileReqs[i].Filename = filenameGen.Filename(filename)
		fileReqs[i].Size = int64(len(sidecarReq.Bytes))
		fileReqs[i].Reader = bytes.NewReader(sidecarReq.Bytes)

		fileReqs[i].AssetFileRef = mimir.FileRef{
			Repository: sidecarReq.SourceFile.Repository,
			Filename:   sidecarReq.SourceFile.Filename,
		}

		sidecarFile := documents.File{
			FileRef: mimir.FileRef{
				Repository: string(fl.RepositoryID),
				Filename:   fileReqs[i].Filename,
			},
			FileInfo: documents.FileInfo{
				LastModified: now,
				Size:         fileReqs[i].Size,
			},
		}

		switch sidecarDesc := sidecarReq.SidecarDescriptor.(type) {
		case *mimirpb.UploadAssetSidecarRequest_ThumbnailDescriptor:
			thumbDesc, err := mimirpbconv.ThumbnailDescFromProtoBuf(sidecarDesc.ThumbnailDescriptor)
			if err != nil {
				return nil, status.Errorf(codes.InvalidArgument, "invalid thumbnail descriptor")
			}

			fileReqs[i].Sidecar = documents.Thumbnail{
				SidecarFile:         sidecarFile,
				ThumbnailDescriptor: *thumbDesc,
			}
		case *mimirpb.UploadAssetSidecarRequest_VideoDescriptor:
			videoDesc, err := mimirpbconv.VideoDescFromProtoBuf(sidecarDesc.VideoDescriptor)
			if err != nil {
				return nil, status.Errorf(codes.InvalidArgument, "invalid video descriptor")
			}

			fileReqs[i].Sidecar = documents.Video{
				SidecarFile:     sidecarFile,
				VideoDescriptor: *videoDesc,
			}
		default:
			log.Warn().Msgf("sidecar descriptor type %v not implemented", reflect.TypeOf(sidecarDesc))
			return nil, status.Errorf(codes.Unimplemented, "sidecar descriptor type %v not implemented", reflect.TypeOf(sidecarDesc))
		}
	}

	// Register clean up for uploaded files in case of failure
	var filesToClean []string
	defer func(filenames *[]string) {
		if filenames != nil {
			for _, filename := range *filenames {
				log.Info().Str("filename", filename).Msg("Cleaning up uploaded file due failure")
				err := repo.DeleteFile(context.Background(), filename)
				if err != nil {
					log.Error().Str("filename", filename).Msg("Failed to clean up uploaded file")
				}
			}
		}
	}(&filesToClean)

	// Upload files
	for _, req := range fileReqs {
		log.Info().Str("filename", req.Filename).Msg("Uploading file")
		err := repo.UploadFile(ctx, req.Filename, req.Size, req.Reader)
		if err != nil {
			log.Error().Err(err).Msg("File upload failed")
			return nil, status.Error(codes.Internal, "file upload failed")
		}

		filesToClean = append(filesToClean, req.Filename)
	}

	updater := func(asset documents.IndexedAsset) documents.IndexedAsset {
		for _, fileReq := range fileReqs {
			assetFile := asset.Files.FindByRef(fileReq.AssetFileRef)
			if assetFile != nil {
				existing := false
				for i, sidecar := range assetFile.Sidecars {
					if sidecar.Match(fileReq.Sidecar) {
						assetFile.Sidecars[i] = fileReq.Sidecar
						existing = true
						break
					}
				}

				if !existing {
					assetFile.Sidecars = append(assetFile.Sidecars, fileReq.Sidecar)
				}
			} else {
				log.Warn().Str("index", req.Asset.Index).Str("asset", req.Asset.AssetId).Msgf("Trying to upload sidecar to non-existing file %v/%v", fileReq.AssetFileRef.Repository, fileReq.AssetFileRef.Filename)
			}
		}

		return asset
	}

	_, err = s.App.Indexer.UpdateAsset(mimir.AssetRefForID(req.Asset.Index, req.Asset.AssetId), updater)
	if err != nil {
		return nil, err
	}

	// We finished successfully
	filesToClean = nil

	return &emptypb.Empty{}, nil
}

func defaultContext(app *app.App) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		newCtx := app.DefaultContext(ctx)
		newCtx = dataloader.CacheContext(newCtx)

		return handler(newCtx, req)
	}
}

func Listen(lis net.Listener, app *app.App) error {
	s := grpc.NewServer(
		grpc.MaxRecvMsgSize(64*1024*1024 /* 64 MB */),
		grpc.UnaryInterceptor(
			defaultContext(app),
		),
	)

	mimirpb.RegisterMimirServer(s, &server{
		App: app,
	})

	return s.Serve(lis)
}
