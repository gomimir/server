package config_test

import (
	"testing"

	_ "embed"

	"github.com/bradleyjkemp/cupaloy/v2"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/config"
)

//go:embed fixture/config.json5
var sampleConfig []byte

//go:embed fixture/docker.json5
var dockerConfig []byte

func TestDefaultConfig(t *testing.T) {
	config, err := config.GetDefaultConfig(map[string]string{})
	if assert.NoError(t, err) {
		cupaloy.SnapshotT(t, config)
	}
}

func TestDockerConfig(t *testing.T) {
	config, err := config.ReadConfig(dockerConfig, map[string]string{
		"MIMIR_PROVISION_REPO_NAME":       "minio",
		"MIMIR_PROVISION_REPO_ENDPOINT":   "127.0.0.1:9000",
		"MIMIR_PROVISION_REPO_ACCESS_KEY": "minio",
		"MIMIR_PROVISION_REPO_SECRET_KEY": "minio123",
	})
	if assert.NoError(t, err) {
		cupaloy.SnapshotT(t, config)
	}
}

func TestSampleConfig(t *testing.T) {
	config, err := config.ReadConfig(sampleConfig, map[string]string{})
	if assert.NoError(t, err) {
		cupaloy.SnapshotT(t, config)
	}
}
