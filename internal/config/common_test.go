package config_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/config"
)

type nestedObj struct {
	SomeString string
}

type testObj struct {
	SomeString string
	SomeNumber int32
	NestedObj  nestedObj
	NestedArr  []nestedObj
	NestedMap  map[string]nestedObj
}

func TestResolveValue(t *testing.T) {
	data := testObj{
		SomeString: "${var1}",
		SomeNumber: 13,
		NestedObj: nestedObj{
			SomeString: "${var2}",
		},
		NestedArr: []nestedObj{
			{SomeString: "${var1}"},
			{SomeString: "${var2}"},
			{SomeString: "${var3-default}"},
			{SomeString: "${var4-}"},
			{SomeString: "${var5}"},
		},
		NestedMap: make(map[string]nestedObj),
	}

	data.NestedMap["Hello"] = nestedObj{SomeString: "${var2}"}

	vars := make(map[string]string)
	vars["var1"] = "Hello"
	vars["var2"] = "World"

	resolved := config.ResolveValue(data, vars).(testObj)

	assert.Equal(t, "Hello", resolved.SomeString)
	assert.Equal(t, int32(13), resolved.SomeNumber)
	assert.Equal(t, "World", resolved.NestedObj.SomeString)
	assert.Equal(t, "Hello", resolved.NestedArr[0].SomeString)
	assert.Equal(t, "World", resolved.NestedArr[1].SomeString)
	assert.Equal(t, "default", resolved.NestedArr[2].SomeString)
	assert.Equal(t, "", resolved.NestedArr[3].SomeString)
	assert.Equal(t, "${var5}", resolved.NestedArr[4].SomeString)
	assert.Equal(t, "World", resolved.NestedMap["Hello"].SomeString)
}
