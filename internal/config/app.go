package config

import (
	_ "embed"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/gomimir/server/internal/auth"
)

// Config - Application configuration
type Config struct {
	Elasticsearch ElasticsearchIndexerConfig
	Database      DatabaseConfig
	Server        ServerConfig
	Provision     ProvisioningConfig
}

type AuthConfig struct {
	Provider auth.ProviderConfig `mapstructure:"provider"`
}

type ServerConfig struct {
	Auth AuthConfig `mapstructure:"auth"`
	CORS CORSConfig `mapstructure:"cors"`

	PayloadSecret string `mapstructure:"payloadSecret"`
	LinkSecret    string `mapstructure:"linkSecret"`
}

type CORSConfig struct {
	AllowedOrigins []string `mapstructure:"allowedOrigins"`
}

// ElasticsearchIndexerConfig - Configuration for Elasticsearch indexer
type ElasticsearchIndexerConfig struct {
	Endpoint string
}

type DatabaseConfig struct {
	ConnectionString string
}

type ProvisioningConfig struct {
	Repositories []RepositoryProvisioning
}

type RepositoryProvisioning struct {
	ID   string `mapstructure:"id"`
	Name *string

	S3    *S3RepositoryConfig
	Local *LocalRepositoryConfig
}

type LocalRepositoryConfig struct {
	Path string
}

type S3RepositoryConfig struct {
	Endpoint   *string
	UseSSL     *bool   `mapstructure:"useSsl"`
	CACertFile *string `mapstructure:"caCertFile"`
	AccessKey  *string
	SecretKey  *string
}

//go:embed default/config.json5
var defaultConfig []byte

func GetDefaultConfig(vars map[string]string) (*Config, error) {
	return ReadConfig(defaultConfig, vars)
}

func ReadConfig(contents []byte, vars map[string]string) (*Config, error) {
	var data map[string]interface{}
	err := unmarshalJSON5(contents, &data)
	if err != nil {
		return nil, err
	}

	return resolveConfigMap(data, vars)
}

func resolveConfigMap(data map[string]interface{}, vars map[string]string) (*Config, error) {
	resolved := ResolveValue(data, vars).(map[string]interface{})

	var config Config

	d, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		Result:           &config,
		ErrorUnused:      true,
		WeaklyTypedInput: true,
		DecodeHook:       mapstructure.ComposeDecodeHookFunc(auth.ProviderConfigHook),
	})

	if err != nil {
		return nil, err
	}

	err = d.Decode(resolved)
	if err != nil {
		return nil, err
	}

	return &config, nil
}
