package config

import (
	"errors"
	"reflect"
	"regexp"

	"github.com/flynn/json5"
	"golang.org/x/text/unicode/norm"
)

// ResolveValue - Resolve any referenced variables
func ResolveValue(obj interface{}, vars map[string]string) interface{} {
	src := reflect.ValueOf(obj)

	if src.Type().Kind() == reflect.Struct {
		out := reflect.New(src.Type()).Elem()
		for i := 0; i < src.NumField(); i++ {
			srcField := src.Field(i)
			outField := out.Field(i)

			resolved := ResolveValue(srcField.Interface(), vars)
			if resolved != nil {
				outField.Set(reflect.ValueOf(resolved))
			}
		}

		return out.Interface()

	} else if src.Type().Kind() == reflect.Slice {
		var out = reflect.MakeSlice(src.Type(), src.Len(), src.Cap())

		for i := 0; i < src.Len(); i++ {
			resolved := ResolveValue(src.Index(i).Interface(), vars)
			out.Index(i).Set(reflect.ValueOf(resolved))
		}

		return out.Interface()

	} else if src.Type().Kind() == reflect.Map {
		var out = reflect.MakeMap(src.Type())

		iter := src.MapRange()
		for iter.Next() {
			resolved := ResolveValue(iter.Value().Interface(), vars)
			out.SetMapIndex(iter.Key(), reflect.ValueOf(resolved))
		}

		return out.Interface()

	} else if src.Type().Kind() == reflect.String {
		return resolveString(src.String(), vars)
	} else if src.Type().Kind() == reflect.Ptr {
		if src.IsNil() {
			return nil
		}

		resolved := ResolveValue(src.Elem().Interface(), vars)

		ptr := reflect.New(src.Type().Elem())
		ptr.Elem().Set(reflect.ValueOf(resolved))
		return ptr.Interface()
	} else {
		// fmt.Printf("type = %v\n", src.Type())
		return obj
	}
}

var varRx = regexp.MustCompile(`\$\{([^}]+)\}`)
var defaultVarRx = regexp.MustCompile(`^([^-]+)(-(.*))?$`)

// Resolved ${vars} to variable values according to given dictionary
// If referenced variable is not defined, the string is left as it is.
// You can define default value like ${var-default}. In that case it will replace the value.
func resolveString(str string, vars map[string]string) string {
	return varRx.ReplaceAllStringFunc(str, func(match string) string {
		m := defaultVarRx.FindStringSubmatch(match[2 : len(match)-1])
		if len(m) > 0 {
			if value, ok := vars[m[1]]; ok {
				return value
			} else if m[2] != "" {
				return m[3]
			}
		}

		return str
	})
}

func unmarshalJSON5(data []byte, out interface{}) error {
	// Note: this is a sanity check because we have been dealing with a log of unicode issues (maybe we should just always convert it?)
	if !norm.NFC.IsNormal(data) {
		return errors.New("expected config data in NFC form")
	}

	return json5.Unmarshal(data, &out)
}
