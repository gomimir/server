package config

import (
	"context"
	"reflect"
	"regexp"

	"github.com/icza/gox/gox"
	"gitlab.com/gomimir/server/internal/ent"
	entrule "gitlab.com/gomimir/server/internal/ent/rule"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

// Rule - Rule descriptor
type Rule struct {
	Repositories []string       `json:"repositories,omitempty"`
	Pattern      *regexp.Regexp `json:"match,omitempty" mapstructure:"match"`
	Inverse      *bool
	Actions      []RuleAction `json:"actions,omitempty"`
}

// MatchesRepository - returns true if the rule is applicable for files from given repository
func (rule *Rule) MatchesRepository(repositoryName string) bool {
	if len(rule.Repositories) == 0 {
		return true
	}

	effectiveInverse := false
	if rule.Inverse != nil {
		effectiveInverse = *rule.Inverse
	}

	for _, repo := range rule.Repositories {
		if repo == repositoryName {
			return !effectiveInverse
		}
	}

	return effectiveInverse
}

// Tag - Tag descriptor
type Tag struct {
	Name string
	Kind string
}

// ProcessAction - process action descriptor
type ProcessAction struct {
	TaskName    *string                 `json:"name,omitempty" mapstructure:"name,omitempty"`
	RequestKind *string                 `json:"request,omitempty" mapstructure:"request,omitempty"`
	QueueName   *string                 `json:"queue,omitempty" mapstructure:"queue,omitempty"`
	Config      *map[string]interface{} `json:"config,omitempty" mapstructure:"config,omitempty"`
}

// GetName - returns name of the process action which can be used to merged multiple definitions together
func (a *ProcessAction) GetName() string {
	if a.TaskName != nil && *a.TaskName != "" {
		return *a.TaskName
	}

	if a.RequestKind != nil {
		return *a.RequestKind
	}

	return ""
}

// RuleAction - Rule action
type RuleAction struct {
	Ignore  *bool                   `json:"ignore,omitempty" mapstructure:",omitempty"`
	Key     *string                 `json:"groupingKey,omitempty" mapstructure:"groupingKey,omitempty"`
	Set     *map[string]interface{} `json:"set,omitempty" mapstructure:",omitempty"`
	AddTag  *Tag                    `json:"addTag,omitempty" mapstructure:",omitempty"`
	Process *ProcessAction          `json:"process,omitempty" mapstructure:",omitempty"`
}

func regexpDecodeHook(from reflect.Type, to reflect.Type, data interface{}) (interface{}, error) {
	if from.Kind() != reflect.String {
		return data, nil
	}

	if to != reflect.TypeOf(&regexp.Regexp{}) {
		return data, nil
	}

	return regexp.Compile(data.(string))
}

// TODO: remove the conversion once we support entities fully in the crawler
func IndexRules(ctx context.Context, indexID pulid.ID) ([]Rule, error) {
	entClient := ent.FromContext(ctx)
	indexRules, err := entClient.Rule.Query().WithActions().Where(entrule.IndexID(indexID)).All(ctx)
	if err != nil {
		return nil, err
	}

	var rules []Rule
	for _, ruleEnt := range indexRules {
		if !ruleEnt.Enabled {
			continue
		}

		rule := Rule{
			Pattern: regexp.MustCompile(ruleEnt.Match),
			Inverse: gox.NewBool(ruleEnt.Inverse),
		}

		for _, actionEnt := range ruleEnt.Edges.Actions {
			if !actionEnt.Enabled {
				continue
			}

			action := RuleAction{
				Ignore: actionEnt.Config.Ignore,
				Key:    actionEnt.Config.SetKey,
				Set:    &actionEnt.Config.SetParams,
				AddTag: (*Tag)(actionEnt.Config.AddTag),
			}

			if actionEnt.Config.Process != nil {
				action.Process = &ProcessAction{
					RequestKind: &actionEnt.Config.Process.Request,
					TaskName:    &actionEnt.Config.Process.Request,
					QueueName:   actionEnt.Config.Process.Queue,
					Config:      &actionEnt.Config.Process.Parameters,
				}
			}

			rule.Actions = append(rule.Actions, action)
		}

		rules = append(rules, rule)
	}

	return rules, nil
}
