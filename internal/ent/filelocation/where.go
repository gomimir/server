// Code generated by entc, DO NOT EDIT.

package filelocation

import (
	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"gitlab.com/gomimir/server/internal/ent/predicate"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

// ID filters vertices based on their ID field.
func ID(id pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldID), id))
	})
}

// IDEQ applies the EQ predicate on the ID field.
func IDEQ(id pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldID), id))
	})
}

// IDNEQ applies the NEQ predicate on the ID field.
func IDNEQ(id pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldID), id))
	})
}

// IDIn applies the In predicate on the ID field.
func IDIn(ids ...pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(ids) == 0 {
			s.Where(sql.False())
			return
		}
		v := make([]interface{}, len(ids))
		for i := range v {
			v[i] = ids[i]
		}
		s.Where(sql.In(s.C(FieldID), v...))
	})
}

// IDNotIn applies the NotIn predicate on the ID field.
func IDNotIn(ids ...pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(ids) == 0 {
			s.Where(sql.False())
			return
		}
		v := make([]interface{}, len(ids))
		for i := range v {
			v[i] = ids[i]
		}
		s.Where(sql.NotIn(s.C(FieldID), v...))
	})
}

// IDGT applies the GT predicate on the ID field.
func IDGT(id pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldID), id))
	})
}

// IDGTE applies the GTE predicate on the ID field.
func IDGTE(id pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldID), id))
	})
}

// IDLT applies the LT predicate on the ID field.
func IDLT(id pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldID), id))
	})
}

// IDLTE applies the LTE predicate on the ID field.
func IDLTE(id pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldID), id))
	})
}

// IndexID applies equality check predicate on the "index_id" field. It's identical to IndexIDEQ.
func IndexID(v pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldIndexID), v))
	})
}

// RepositoryID applies equality check predicate on the "repository_id" field. It's identical to RepositoryIDEQ.
func RepositoryID(v pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldRepositoryID), v))
	})
}

// Prefix applies equality check predicate on the "prefix" field. It's identical to PrefixEQ.
func Prefix(v string) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldPrefix), v))
	})
}

// IndexIDEQ applies the EQ predicate on the "index_id" field.
func IndexIDEQ(v pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldIndexID), v))
	})
}

// IndexIDNEQ applies the NEQ predicate on the "index_id" field.
func IndexIDNEQ(v pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldIndexID), v))
	})
}

// IndexIDIn applies the In predicate on the "index_id" field.
func IndexIDIn(vs ...pulid.ID) predicate.FileLocation {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.FileLocation(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.In(s.C(FieldIndexID), v...))
	})
}

// IndexIDNotIn applies the NotIn predicate on the "index_id" field.
func IndexIDNotIn(vs ...pulid.ID) predicate.FileLocation {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.FileLocation(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.NotIn(s.C(FieldIndexID), v...))
	})
}

// IndexIDGT applies the GT predicate on the "index_id" field.
func IndexIDGT(v pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldIndexID), v))
	})
}

// IndexIDGTE applies the GTE predicate on the "index_id" field.
func IndexIDGTE(v pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldIndexID), v))
	})
}

// IndexIDLT applies the LT predicate on the "index_id" field.
func IndexIDLT(v pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldIndexID), v))
	})
}

// IndexIDLTE applies the LTE predicate on the "index_id" field.
func IndexIDLTE(v pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldIndexID), v))
	})
}

// IndexIDContains applies the Contains predicate on the "index_id" field.
func IndexIDContains(v pulid.ID) predicate.FileLocation {
	vc := string(v)
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldIndexID), vc))
	})
}

// IndexIDHasPrefix applies the HasPrefix predicate on the "index_id" field.
func IndexIDHasPrefix(v pulid.ID) predicate.FileLocation {
	vc := string(v)
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldIndexID), vc))
	})
}

// IndexIDHasSuffix applies the HasSuffix predicate on the "index_id" field.
func IndexIDHasSuffix(v pulid.ID) predicate.FileLocation {
	vc := string(v)
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldIndexID), vc))
	})
}

// IndexIDEqualFold applies the EqualFold predicate on the "index_id" field.
func IndexIDEqualFold(v pulid.ID) predicate.FileLocation {
	vc := string(v)
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldIndexID), vc))
	})
}

// IndexIDContainsFold applies the ContainsFold predicate on the "index_id" field.
func IndexIDContainsFold(v pulid.ID) predicate.FileLocation {
	vc := string(v)
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldIndexID), vc))
	})
}

// TypeEQ applies the EQ predicate on the "type" field.
func TypeEQ(v Type) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldType), v))
	})
}

// TypeNEQ applies the NEQ predicate on the "type" field.
func TypeNEQ(v Type) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldType), v))
	})
}

// TypeIn applies the In predicate on the "type" field.
func TypeIn(vs ...Type) predicate.FileLocation {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.FileLocation(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.In(s.C(FieldType), v...))
	})
}

// TypeNotIn applies the NotIn predicate on the "type" field.
func TypeNotIn(vs ...Type) predicate.FileLocation {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.FileLocation(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.NotIn(s.C(FieldType), v...))
	})
}

// RepositoryIDEQ applies the EQ predicate on the "repository_id" field.
func RepositoryIDEQ(v pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldRepositoryID), v))
	})
}

// RepositoryIDNEQ applies the NEQ predicate on the "repository_id" field.
func RepositoryIDNEQ(v pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldRepositoryID), v))
	})
}

// RepositoryIDIn applies the In predicate on the "repository_id" field.
func RepositoryIDIn(vs ...pulid.ID) predicate.FileLocation {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.FileLocation(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.In(s.C(FieldRepositoryID), v...))
	})
}

// RepositoryIDNotIn applies the NotIn predicate on the "repository_id" field.
func RepositoryIDNotIn(vs ...pulid.ID) predicate.FileLocation {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.FileLocation(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.NotIn(s.C(FieldRepositoryID), v...))
	})
}

// RepositoryIDGT applies the GT predicate on the "repository_id" field.
func RepositoryIDGT(v pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldRepositoryID), v))
	})
}

// RepositoryIDGTE applies the GTE predicate on the "repository_id" field.
func RepositoryIDGTE(v pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldRepositoryID), v))
	})
}

// RepositoryIDLT applies the LT predicate on the "repository_id" field.
func RepositoryIDLT(v pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldRepositoryID), v))
	})
}

// RepositoryIDLTE applies the LTE predicate on the "repository_id" field.
func RepositoryIDLTE(v pulid.ID) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldRepositoryID), v))
	})
}

// RepositoryIDContains applies the Contains predicate on the "repository_id" field.
func RepositoryIDContains(v pulid.ID) predicate.FileLocation {
	vc := string(v)
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldRepositoryID), vc))
	})
}

// RepositoryIDHasPrefix applies the HasPrefix predicate on the "repository_id" field.
func RepositoryIDHasPrefix(v pulid.ID) predicate.FileLocation {
	vc := string(v)
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldRepositoryID), vc))
	})
}

// RepositoryIDHasSuffix applies the HasSuffix predicate on the "repository_id" field.
func RepositoryIDHasSuffix(v pulid.ID) predicate.FileLocation {
	vc := string(v)
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldRepositoryID), vc))
	})
}

// RepositoryIDEqualFold applies the EqualFold predicate on the "repository_id" field.
func RepositoryIDEqualFold(v pulid.ID) predicate.FileLocation {
	vc := string(v)
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldRepositoryID), vc))
	})
}

// RepositoryIDContainsFold applies the ContainsFold predicate on the "repository_id" field.
func RepositoryIDContainsFold(v pulid.ID) predicate.FileLocation {
	vc := string(v)
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldRepositoryID), vc))
	})
}

// PrefixEQ applies the EQ predicate on the "prefix" field.
func PrefixEQ(v string) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldPrefix), v))
	})
}

// PrefixNEQ applies the NEQ predicate on the "prefix" field.
func PrefixNEQ(v string) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldPrefix), v))
	})
}

// PrefixIn applies the In predicate on the "prefix" field.
func PrefixIn(vs ...string) predicate.FileLocation {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.FileLocation(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.In(s.C(FieldPrefix), v...))
	})
}

// PrefixNotIn applies the NotIn predicate on the "prefix" field.
func PrefixNotIn(vs ...string) predicate.FileLocation {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.FileLocation(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.NotIn(s.C(FieldPrefix), v...))
	})
}

// PrefixGT applies the GT predicate on the "prefix" field.
func PrefixGT(v string) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldPrefix), v))
	})
}

// PrefixGTE applies the GTE predicate on the "prefix" field.
func PrefixGTE(v string) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldPrefix), v))
	})
}

// PrefixLT applies the LT predicate on the "prefix" field.
func PrefixLT(v string) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldPrefix), v))
	})
}

// PrefixLTE applies the LTE predicate on the "prefix" field.
func PrefixLTE(v string) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldPrefix), v))
	})
}

// PrefixContains applies the Contains predicate on the "prefix" field.
func PrefixContains(v string) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldPrefix), v))
	})
}

// PrefixHasPrefix applies the HasPrefix predicate on the "prefix" field.
func PrefixHasPrefix(v string) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldPrefix), v))
	})
}

// PrefixHasSuffix applies the HasSuffix predicate on the "prefix" field.
func PrefixHasSuffix(v string) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldPrefix), v))
	})
}

// PrefixEqualFold applies the EqualFold predicate on the "prefix" field.
func PrefixEqualFold(v string) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldPrefix), v))
	})
}

// PrefixContainsFold applies the ContainsFold predicate on the "prefix" field.
func PrefixContainsFold(v string) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldPrefix), v))
	})
}

// HasRepository applies the HasEdge predicate on the "repository" edge.
func HasRepository() predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		step := sqlgraph.NewStep(
			sqlgraph.From(Table, FieldID),
			sqlgraph.To(RepositoryTable, FieldID),
			sqlgraph.Edge(sqlgraph.M2O, false, RepositoryTable, RepositoryColumn),
		)
		sqlgraph.HasNeighbors(s, step)
	})
}

// HasRepositoryWith applies the HasEdge predicate on the "repository" edge with a given conditions (other predicates).
func HasRepositoryWith(preds ...predicate.Repository) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		step := sqlgraph.NewStep(
			sqlgraph.From(Table, FieldID),
			sqlgraph.To(RepositoryInverseTable, FieldID),
			sqlgraph.Edge(sqlgraph.M2O, false, RepositoryTable, RepositoryColumn),
		)
		sqlgraph.HasNeighborsWith(s, step, func(s *sql.Selector) {
			for _, p := range preds {
				p(s)
			}
		})
	})
}

// HasIndex applies the HasEdge predicate on the "index" edge.
func HasIndex() predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		step := sqlgraph.NewStep(
			sqlgraph.From(Table, FieldID),
			sqlgraph.To(IndexTable, FieldID),
			sqlgraph.Edge(sqlgraph.M2O, true, IndexTable, IndexColumn),
		)
		sqlgraph.HasNeighbors(s, step)
	})
}

// HasIndexWith applies the HasEdge predicate on the "index" edge with a given conditions (other predicates).
func HasIndexWith(preds ...predicate.Index) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		step := sqlgraph.NewStep(
			sqlgraph.From(Table, FieldID),
			sqlgraph.To(IndexInverseTable, FieldID),
			sqlgraph.Edge(sqlgraph.M2O, true, IndexTable, IndexColumn),
		)
		sqlgraph.HasNeighborsWith(s, step, func(s *sql.Selector) {
			for _, p := range preds {
				p(s)
			}
		})
	})
}

// And groups predicates with the AND operator between them.
func And(predicates ...predicate.FileLocation) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s1 := s.Clone().SetP(nil)
		for _, p := range predicates {
			p(s1)
		}
		s.Where(s1.P())
	})
}

// Or groups predicates with the OR operator between them.
func Or(predicates ...predicate.FileLocation) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		s1 := s.Clone().SetP(nil)
		for i, p := range predicates {
			if i > 0 {
				s1.Or()
			}
			p(s1)
		}
		s.Where(s1.P())
	})
}

// Not applies the not operator on the given predicate.
func Not(p predicate.FileLocation) predicate.FileLocation {
	return predicate.FileLocation(func(s *sql.Selector) {
		p(s.Not())
	})
}
