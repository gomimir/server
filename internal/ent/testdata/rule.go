package testdata

import (
	"gitlab.com/gomimir/server/internal/ent"
)

type TestRuleRequest struct {
	baseTestData
	actions   []*TestActionRequest
	callbacks []func(*ent.RuleCreate)
}

func (r *TestRuleRequest) Ref(out **ent.Rule) *TestRuleRequest {
	r.ref(out)
	return r
}

func (r *TestRuleRequest) SetActions(actions ...*TestActionRequest) *TestRuleRequest {
	r.actions = actions
	return r
}

func (r *TestRuleRequest) SetByCallback(callback func(*ent.RuleCreate)) *TestRuleRequest {
	r.callbacks = append(r.callbacks, callback)
	return r
}

func (r *TestRuleRequest) Compose() *composedTestDataRequest {
	return r.compose(func(builder *TestDataBuilder) interface{} {
		create := builder.entClient.Rule.Create()

		for _, callback := range r.callbacks {
			callback(create)
		}

		rule := create.SaveX(builder.ctx)

		for _, actionReq := range r.actions {
			builder.Require(actionReq.SetByCallback(func(rac *ent.RuleActionCreate) {
				rac.SetRuleID(rule.ID)
			}))
		}

		return rule
	})
}

func Rule() *TestRuleRequest {
	return &TestRuleRequest{
		baseTestData: baseTestData{key: &testDataKey{key: "rule"}},
	}
}
