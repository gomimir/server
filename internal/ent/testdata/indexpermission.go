package testdata

import (
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

type TestIndexPermissionRequest struct {
	baseTestData

	indexID pulid.ID
	subject TestDataRequest

	indexOperations           []auth.Operation
	assetOperations           []auth.Operation
	assetCollectionOperations []auth.Operation
}

func (r *TestIndexPermissionRequest) Ref(out **ent.IndexPermission) *TestIndexPermissionRequest {
	r.ref(out)
	return r
}

func (r *TestIndexPermissionRequest) SetIndexID(id pulid.ID) *TestIndexPermissionRequest {
	r.indexID = id
	return r
}

func (r *TestIndexPermissionRequest) SetSubject(req TestDataRequest) *TestIndexPermissionRequest {
	r.subject = req
	return r
}

func (r *TestIndexPermissionRequest) SetIndexOperations(ops ...auth.Operation) *TestIndexPermissionRequest {
	r.indexOperations = ops
	return r
}

func (r *TestIndexPermissionRequest) SetAssetOperations(ops ...auth.Operation) *TestIndexPermissionRequest {
	r.assetOperations = ops
	return r
}

func (r *TestIndexPermissionRequest) SetAssetCollectionOperations(ops ...auth.Operation) *TestIndexPermissionRequest {
	r.assetCollectionOperations = ops
	return r
}

func (r *TestIndexPermissionRequest) Compose() *composedTestDataRequest {
	return r.compose(func(builder *TestDataBuilder) interface{} {
		create := builder.entClient.IndexPermission.Create().
			SetIndexID(r.indexID).
			SetSubjectID(resolvePermissionsSubject(builder, r.subject)).
			SetIndexOperations(r.indexOperations).
			SetAssetOperations(r.assetOperations).
			SetAssetCollectionOperations(r.assetCollectionOperations)

		return create.SaveX(builder.ctx)
	})
}

func IndexPermission() *TestIndexPermissionRequest {
	return &TestIndexPermissionRequest{
		baseTestData: baseTestData{
			key: &testDataKey{"index_permission"},
		},
	}
}
