package testdata

import (
	"fmt"

	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

type TestGlobalPermissionRequest struct {
	baseTestData

	resource             auth.ResourceType
	subjectReq           TestDataRequest
	subjectID            pulid.ID
	collectionOperations []auth.Operation
	itemOperations       []auth.Operation
}

func (r *TestGlobalPermissionRequest) Ref(out **ent.GlobalPermission) *TestGlobalPermissionRequest {
	r.ref(out)
	return r
}

func (r *TestGlobalPermissionRequest) SetResource(resourceType auth.ResourceType) *TestGlobalPermissionRequest {
	r.resource = resourceType
	return r
}

func (r *TestGlobalPermissionRequest) SetSubject(req TestDataRequest) *TestGlobalPermissionRequest {
	r.subjectReq = req
	return r
}

func (r *TestGlobalPermissionRequest) SetSubjectID(id pulid.ID) *TestGlobalPermissionRequest {
	r.subjectID = id
	return r
}

func (r *TestGlobalPermissionRequest) SetCollectionOperations(ops ...auth.Operation) *TestGlobalPermissionRequest {
	r.collectionOperations = ops
	return r
}

func (r *TestGlobalPermissionRequest) SetItemOperations(ops ...auth.Operation) *TestGlobalPermissionRequest {
	r.itemOperations = ops
	return r
}

func (r *TestGlobalPermissionRequest) Compose() *composedTestDataRequest {
	return r.compose(func(builder *TestDataBuilder) interface{} {
		create := builder.entClient.GlobalPermission.Create().
			SetResource(r.resource).
			SetCollectionOperations(r.collectionOperations).
			SetItemOperations(r.itemOperations)

		if r.subjectID != "" {
			create.SetSubjectID(r.subjectID)
		} else {
			create.SetSubjectID(resolvePermissionsSubject(builder, r.subjectReq))
		}

		return create.SaveX(builder.ctx)
	})
}

func GlobalPermission() *TestGlobalPermissionRequest {
	return &TestGlobalPermissionRequest{
		baseTestData: baseTestData{
			key: &testDataKey{"global_permission"},
		},
	}
}

func resolvePermissionsSubject(builder *TestDataBuilder, subject TestDataRequest) pulid.ID {
	switch subjectReq := subject.(type) {
	case *TestUserRequest:
		var user *ent.User
		builder.Require(subjectReq.Ref(&user))
		return user.ID
	case *TestUserRoleRequest:
		var role *ent.UserRole
		builder.Require(subjectReq.Ref(&role))
		return role.ID
	default:
		panic(fmt.Errorf("invalid subject for index permission"))
	}
}
