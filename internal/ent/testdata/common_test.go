package testdata_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/enttest"
	"gitlab.com/gomimir/server/internal/ent/testdata"
	"gitlab.com/gomimir/server/internal/idprovider"
	_ "modernc.org/sqlite"
)

func TestMakeTestData(t *testing.T) {
	client := enttest.Open(t, "sqlite3", "file:ent?mode=memory&cache=shared&_fk=1")
	ctx := idprovider.ProviderContext(context.Background(), idprovider.NewTestProvider())

	var bob *ent.User
	var admin *ent.UserRole
	testdata.NewTestDataBuilder(ctx).Require(
		testdata.UserBob().Ref(&bob),
		testdata.UserRoleAdmin().Ref(&admin),
	)

	require.Equal(t, "bob", bob.Username)

	roles, err := bob.Roles(ctx)
	require.NoError(t, err)
	require.Equal(t, 1, len(roles))
	require.Equal(t, admin.ID, roles[0].ID)
}
