package testdata

import (
	"context"
	"fmt"
	"reflect"

	"gitlab.com/gomimir/server/internal/ent"
)

type testDataKey struct {
	key string
}

type composedTestDataRequest struct {
	btd   *baseTestData
	build func(builder *TestDataBuilder) interface{}
}

type TestDataRequest interface {
	Compose() *composedTestDataRequest
}

// ----------
type baseTestData struct {
	key    *testDataKey
	entity interface{}
	refs   []reflect.Value
}

func (r *baseTestData) ref(out interface{}) {
	outVal := reflect.ValueOf(out).Elem()

	if r.entity != nil {
		outVal.Set(reflect.ValueOf(r.entity))
	} else {
		r.refs = append(r.refs, outVal)
	}
}

func (r *baseTestData) propagate(entity interface{}) {
	r.entity = entity
	for _, ref := range r.refs {
		ref.Set(reflect.ValueOf(r.entity))
	}
}

func (r *baseTestData) compose(callback func(builder *TestDataBuilder) interface{}) *composedTestDataRequest {
	return &composedTestDataRequest{
		btd: r,
		build: func(builder *TestDataBuilder) interface{} {
			r.entity = callback(builder)
			r.propagate(r.entity)
			return r.entity
		},
	}
}

// ----------

type TestDataBuilder struct {
	ctx       context.Context
	entClient *ent.Client
	done      map[*testDataKey]interface{}
}

func (b *TestDataBuilder) Require(reqs ...TestDataRequest) {
	for _, req := range reqs {
		composed := req.Compose()
		key := composed.btd.key

		existing, ok := b.done[key]
		if ok {
			if existing != nil {
				composed.btd.propagate(existing)
			} else {
				panic(fmt.Errorf("cycle detected when trying to resolve: %v", key.key))
			}
		} else {
			b.done[key] = nil
			b.done[key] = composed.build(b)
		}
	}
}

// ----------

func NewTestDataBuilder(ctx context.Context) *TestDataBuilder {
	return &TestDataBuilder{
		ctx:       ctx,
		entClient: ent.FromContext(ctx),
		done:      make(map[*testDataKey]interface{}),
	}
}
