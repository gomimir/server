package testdata

import (
	"context"
	"fmt"

	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/schema/schematype"
	"gitlab.com/gomimir/server/internal/secret"
)

type TestRepositoryRequest struct {
	baseTestData

	name      string
	callbacks []func(context.Context, *ent.RepositoryCreate)
}

func (r *TestRepositoryRequest) Ref(out **ent.Repository) *TestRepositoryRequest {
	r.ref(out)
	return r
}

func (r *TestRepositoryRequest) SetByCallback(callback func(context.Context, *ent.RepositoryCreate)) *TestRepositoryRequest {
	r.callbacks = append(r.callbacks, callback)
	return r
}

func (r *TestRepositoryRequest) Compose() *composedTestDataRequest {
	return r.compose(func(builder *TestDataBuilder) interface{} {
		create := builder.entClient.Repository.Create().
			SetName(r.name)

		for _, callback := range r.callbacks {
			callback(builder.ctx, create)
		}

		return create.SaveX(builder.ctx)
	})
}

func repoWithKey(name string, key *testDataKey) *TestRepositoryRequest {
	return &TestRepositoryRequest{
		baseTestData: baseTestData{key: key},
		name:         name,
	}
}

func Repository(name string) *TestRepositoryRequest {
	return repoWithKey(name, &testDataKey{fmt.Sprintf("repository:%v", name)})
}

var minioKey = &testDataKey{"repository:minio"}

func RepositoryMinio() *TestRepositoryRequest {
	return repoWithKey("minio", minioKey).SetByCallback(func(ctx context.Context, uc *ent.RepositoryCreate) {
		keeper, err := secret.KeeperForContext(ctx)
		if err != nil {
			panic(err)
		}

		secret, err := keeper.EncryptString("minio123")
		if err != nil {
			panic(err)
		}

		uc.SetConfig(&schematype.RepositoryConfig{
			S3: &schematype.S3RepositoryConfig{
				Endpoint:  "127.0.0.1:9000",
				UseSSL:    false,
				AccessKey: "minio",
				SecretKey: secret,
			},
		})
	})
}
