package testdata

import (
	"fmt"

	"gitlab.com/gomimir/server/internal/ent"
)

type TestUserRequest struct {
	baseTestData

	username  string
	roles     []*TestUserRoleRequest
	callbacks []func(*ent.UserCreate)
}

func (r *TestUserRequest) Ref(out **ent.User) *TestUserRequest {
	r.ref(out)
	return r
}

func (r *TestUserRequest) SetRoles(roles ...*TestUserRoleRequest) *TestUserRequest {
	r.roles = roles
	return r
}

func (r *TestUserRequest) SetByCallback(callback func(*ent.UserCreate)) *TestUserRequest {
	r.callbacks = append(r.callbacks, callback)
	return r
}

func (r *TestUserRequest) Compose() *composedTestDataRequest {
	return r.compose(func(builder *TestDataBuilder) interface{} {
		roles := make([]*ent.UserRole, len(r.roles))
		for i, roleReq := range r.roles {
			builder.Require(roleReq.Ref(&roles[i]))
		}

		create := builder.entClient.User.Create().
			SetUsername(r.username).
			AddRoles(roles...)

		for _, callback := range r.callbacks {
			callback(create)
		}

		return create.SaveX(builder.ctx)
	})
}

func userWithKey(username string, key *testDataKey) *TestUserRequest {
	return &TestUserRequest{
		baseTestData: baseTestData{key: key},
		username:     username,
	}
}

func User(username string) *TestUserRequest {
	return userWithKey(username, &testDataKey{fmt.Sprintf("user:%v", username)})
}

var adminKey = &testDataKey{"user:admin"}

func UserAdmin() *TestUserRequest {
	return userWithKey("admin", adminKey).SetRoles(UserRoleAdmin(), UserRoleUser()).SetByCallback(func(uc *ent.UserCreate) {
		uc.SetDisplayName("Admin")
	})
}

var bobKey = &testDataKey{"user:bob"}

func UserBob() *TestUserRequest {
	return userWithKey("bob", bobKey).SetRoles(UserRoleUser()).SetByCallback(func(uc *ent.UserCreate) {
		uc.SetDisplayName("Bob")
	})
}

var aliceKey = &testDataKey{"user:alice"}

func UserAlice() *TestUserRequest {
	return userWithKey("alice", aliceKey).SetRoles(UserRoleUser()).SetByCallback(func(uc *ent.UserCreate) {
		uc.SetDisplayName("Alice")
	})
}
