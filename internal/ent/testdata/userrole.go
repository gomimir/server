package testdata

import (
	"fmt"

	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/ent"
)

type TestUserRoleRequest struct {
	baseTestData

	name        string
	callbacks   []func(*ent.UserRoleCreate)
	permissions []*TestGlobalPermissionRequest
}

func (r *TestUserRoleRequest) Ref(out **ent.UserRole) *TestUserRoleRequest {
	r.ref(out)
	return r
}

func (r *TestUserRoleRequest) SetByCallback(callback func(*ent.UserRoleCreate)) *TestUserRoleRequest {
	r.callbacks = append(r.callbacks, callback)
	return r
}

func (r *TestUserRoleRequest) SetGlobalPermissions(reqs ...*TestGlobalPermissionRequest) *TestUserRoleRequest {
	r.permissions = reqs
	return r
}

func (r *TestUserRoleRequest) Compose() *composedTestDataRequest {
	return r.compose(func(builder *TestDataBuilder) interface{} {
		create := builder.entClient.UserRole.Create().
			SetName(r.name)

		for _, callback := range r.callbacks {
			callback(create)
		}

		userRole := create.SaveX(builder.ctx)

		for _, permissionReq := range r.permissions {
			builder.Require(permissionReq.SetSubjectID(userRole.ID))
		}

		return userRole
	})
}

func userRoleWithKey(name string, key *testDataKey) *TestUserRoleRequest {
	return &TestUserRoleRequest{
		baseTestData: baseTestData{key: key},
		name:         name,
	}
}

func UserRole(name string) *TestUserRoleRequest {
	return userRoleWithKey(name, &testDataKey{fmt.Sprintf("user_role:%v", name)})
}

// ---------------------

var adminRoleKey = &testDataKey{"user_role:admin"}

func UserRoleAdmin() *TestUserRoleRequest {
	return userRoleWithKey("admin", adminRoleKey).SetByCallback(func(urc *ent.UserRoleCreate) {
		urc.SetDisplayName("Administrator")
	}).SetGlobalPermissions(
		GlobalPermission().
			SetResource(auth.RT_Index).
			SetCollectionOperations(auth.OP_List, auth.OP_Create).
			SetItemOperations(auth.OP_Read, auth.OP_Update, auth.OP_Delete, auth.OP_Crawl),
		GlobalPermission().
			SetResource(auth.RT_Asset).
			SetCollectionOperations(auth.OP_List, auth.OP_Upload).
			SetItemOperations(auth.OP_Read, auth.OP_Update, auth.OP_Delete, auth.OP_Process),
		GlobalPermission().
			SetResource(auth.RT_Repository).
			SetCollectionOperations(auth.OP_List, auth.OP_Create).
			SetItemOperations(auth.OP_Read, auth.OP_Update, auth.OP_Delete),
	)
}

var userRoleKey = &testDataKey{"user_role:user"}

func UserRoleUser() *TestUserRoleRequest {
	return userRoleWithKey("user", userRoleKey).SetByCallback(func(urc *ent.UserRoleCreate) {
		urc.SetDisplayName("User")
	}).SetGlobalPermissions(
		GlobalPermission().
			SetResource(auth.RT_Index).
			SetCollectionOperations(auth.OP_List),
		GlobalPermission().
			SetResource(auth.RT_User).
			SetCollectionOperations(auth.OP_List).
			SetItemOperations(auth.OP_Read),
		GlobalPermission().
			SetResource(auth.RT_Repository).
			SetCollectionOperations(auth.OP_List).
			SetItemOperations(auth.OP_Read),
		GlobalPermission().
			SetResource(auth.RT_Job).
			SetCollectionOperations(auth.OP_List).
			SetItemOperations(auth.OP_Read),
	)
}

var guestRoleKey = &testDataKey{"user_role:guest"}

func UserRoleGuest() *TestUserRoleRequest {
	return userRoleWithKey("guest", guestRoleKey).SetByCallback(func(urc *ent.UserRoleCreate) {
		urc.SetDisplayName("Guest")
	})
}
