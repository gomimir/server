package testdata

import (
	"gitlab.com/gomimir/server/internal/ent"
)

type TestActionRequest struct {
	baseTestData
	callbacks []func(*ent.RuleActionCreate)
}

func (r *TestActionRequest) Ref(out **ent.RuleAction) *TestActionRequest {
	r.ref(out)
	return r
}

func (r *TestActionRequest) SetByCallback(callback func(*ent.RuleActionCreate)) *TestActionRequest {
	r.callbacks = append(r.callbacks, callback)
	return r
}

func (r *TestActionRequest) Compose() *composedTestDataRequest {
	return r.compose(func(builder *TestDataBuilder) interface{} {
		create := builder.entClient.RuleAction.Create()

		for _, callback := range r.callbacks {
			callback(create)
		}

		return create.SaveX(builder.ctx)
	})
}

func RuleAction() *TestActionRequest {
	return &TestActionRequest{
		baseTestData: baseTestData{key: &testDataKey{key: "rule_action"}},
	}
}
