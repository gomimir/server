package testdata

import (
	"fmt"

	"github.com/icza/gox/gox"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/filelocation"
	"gitlab.com/gomimir/server/internal/ent/index"
	entindex "gitlab.com/gomimir/server/internal/ent/index"
	"gitlab.com/gomimir/server/internal/ent/schema/schematype"
)

type TestIndexRequest struct {
	baseTestData
	name        string
	assetType   *index.AssetType
	rules       []*TestRuleRequest
	permissions []*TestIndexPermissionRequest
}

func (r *TestIndexRequest) Ref(out **ent.Index) *TestIndexRequest {
	r.ref(out)
	return r
}

func (r *TestIndexRequest) SetRules(rules ...*TestRuleRequest) *TestIndexRequest {
	r.rules = rules
	return r
}

func (r *TestIndexRequest) SetPermissions(permissions ...*TestIndexPermissionRequest) *TestIndexRequest {
	r.permissions = permissions
	return r
}

func (r *TestIndexRequest) SetAssetType(assetType entindex.AssetType) *TestIndexRequest {
	r.assetType = &assetType
	return r
}

func (r *TestIndexRequest) Compose() *composedTestDataRequest {
	return r.compose(func(builder *TestDataBuilder) interface{} {
		index := builder.entClient.Index.Create().
			SetName(r.name).
			SetNillableAssetType(r.assetType).
			SaveX(builder.ctx)

		for i, ruleReq := range r.rules {
			builder.Require(ruleReq.SetByCallback(func(rc *ent.RuleCreate) {
				rc.SetIndexID(index.ID)
				if _, exists := rc.Mutation().Order(); !exists {
					rc.SetOrder(i + 1)
				}
			}))
		}

		for _, permissionReq := range r.permissions {
			builder.Require(permissionReq.SetIndexID(index.ID))
		}

		return index
	})
}

func indexWithKey(name string, key *testDataKey) *TestIndexRequest {
	return &TestIndexRequest{
		baseTestData: baseTestData{key: key},
		name:         name,
	}
}

func Index(name string) *TestIndexRequest {
	return indexWithKey(name, &testDataKey{fmt.Sprintf("index:%v", name)})
}

// ------------------------------------------------
type DocumentsIndexRequest struct {
	baseTestData
}

func (r *DocumentsIndexRequest) Ref(out **ent.Index) *DocumentsIndexRequest {
	r.ref(out)
	return r
}

func (r *DocumentsIndexRequest) Compose() *composedTestDataRequest {
	return r.compose(func(builder *TestDataBuilder) interface{} {
		client := builder.entClient
		ctx := builder.ctx

		var index *ent.Index
		builder.Require(
			Index("documents").Ref(&index).
				SetAssetType(entindex.AssetTypeDocument).
				SetRules(
					Rule().
						SetByCallback(func(rc *ent.RuleCreate) {
							rc.SetDescription("Match all documents")
							rc.SetMatch("(?i)\\.(doc|docx|pdf)$")
						}).
						SetActions(
							RuleAction().SetByCallback(func(rac *ent.RuleActionCreate) {
								rac.SetDescription("Generate thumbnails")
								rac.SetConfig(&schematype.ActionConfig{
									Process: &schematype.ProcessActionConfig{
										Request: "generateThumbnails",
										Queue:   gox.NewString("thumbnails"),
										Parameters: map[string]interface{}{
											"thumbnails": []interface{}{
												map[string]interface{}{
													"height": 450,
													"format": "jpeg",
												},
												map[string]interface{}{},
											},
										},
									},
								})
							}),
						),
				).
				SetPermissions(
					IndexPermission().
						SetSubject(UserBob()).
						SetIndexOperations(auth.OP_Read, auth.OP_Update, auth.OP_Delete, auth.OP_Crawl).
						SetAssetOperations(auth.OP_Read, auth.OP_Update, auth.OP_Delete, auth.OP_Process).
						SetAssetCollectionOperations(auth.OP_List, auth.OP_Upload),
				),
		)

		var minio *ent.Repository
		builder.Require(RepositoryMinio().Ref(&minio))

		client.FileLocation.Create().
			SetIndexID(index.ID).
			SetType(filelocation.TypeCrawl).
			SetRepositoryID(minio.ID).
			SetPrefix("documents/").
			SaveX(ctx)

		return index
	})
}

var documentsIndexKey = &testDataKey{"index:documents"}

func IndexDocuments() *DocumentsIndexRequest {
	return &DocumentsIndexRequest{
		baseTestData: baseTestData{key: documentsIndexKey},
	}
}

// ------------------------------------------------
type PhotosIndexRequest struct {
	baseTestData
}

func (r *PhotosIndexRequest) Ref(out **ent.Index) *PhotosIndexRequest {
	r.ref(out)
	return r
}

func (r *PhotosIndexRequest) Compose() *composedTestDataRequest {
	return r.compose(func(builder *TestDataBuilder) interface{} {
		client := builder.entClient
		ctx := builder.ctx

		var index *ent.Index
		builder.Require(
			Index("photos").Ref(&index).
				SetAssetType(entindex.AssetTypePhoto).
				SetRules(
					// Note: intentionally saved in the reverse order to test `Order` field
					Rule().
						SetByCallback(func(rc *ent.RuleCreate) {
							rc.SetOrder(2)
							rc.SetDescription("Match all images")
							rc.SetMatch("\\.(jpg|jpeg|heic)$")
						}).
						SetActions(
							RuleAction().SetByCallback(func(rac *ent.RuleActionCreate) {
								rac.SetDescription("Generate thumbnails")
								rac.SetConfig(&schematype.ActionConfig{
									Process: &schematype.ProcessActionConfig{
										Request: "generateThumbnails",
										Queue:   gox.NewString("thumbnails"),
										Parameters: map[string]interface{}{
											"thumbnails": []interface{}{
												map[string]interface{}{
													"height": 450,
													"format": "webp",
												},
												map[string]interface{}{
													"height": 450,
													"format": "jpeg",
												},
												map[string]interface{}{
													"format": "webp",
												},
												map[string]interface{}{},
											},
										},
									},
								})
							}),
							RuleAction().SetByCallback(func(rac *ent.RuleActionCreate) {
								rac.SetDescription("Extract EXIF metadata")
								rac.SetConfig(&schematype.ActionConfig{
									Process: &schematype.ProcessActionConfig{
										Request: "extractExif",
									},
								})
							}),
						),
					Rule().
						SetByCallback(func(rc *ent.RuleCreate) {
							rc.SetOrder(1)
							rc.SetDescription("Match all files which are not images")
							rc.SetMatch("(?i)\\.(jpg|jpeg|heic)$")
							rc.SetInverse(true)
						}).
						SetActions(
							RuleAction().SetByCallback(func(rac *ent.RuleActionCreate) {
								rac.SetDescription("Ignore during crawl")
								rac.SetConfig(&schematype.ActionConfig{
									Ignore: gox.NewBool(true),
								})
							}),
						),
				).
				SetPermissions(
					IndexPermission().
						SetSubject(UserRoleUser()).
						SetIndexOperations(auth.OP_Read).
						SetAssetOperations(auth.OP_Read).
						SetAssetCollectionOperations(auth.OP_List),
					IndexPermission().
						SetSubject(UserAlice()).
						SetIndexOperations(auth.OP_Read, auth.OP_Upload, auth.OP_Crawl).
						SetAssetOperations(auth.OP_Read, auth.OP_Update, auth.OP_Delete, auth.OP_Process).
						SetAssetCollectionOperations(auth.OP_List, auth.OP_Upload),
				),
		)

		var minio *ent.Repository
		builder.Require(RepositoryMinio().Ref(&minio))

		client.FileLocation.Create().
			SetIndexID(index.ID).
			SetType(filelocation.TypeCrawl).
			SetRepositoryID(minio.ID).
			SetPrefix("photos/").
			SaveX(ctx)

		client.FileLocation.Create().
			SetIndexID(index.ID).
			SetType(filelocation.TypeSidecar).
			SetRepositoryID(minio.ID).
			SetPrefix("photos-sidecars/").
			SaveX(ctx)

		client.TagKind.CreateBulk(
			client.TagKind.Create().SetIndexID(index.ID).SetName("album").SetUserAssignable(false),
			client.TagKind.Create().SetIndexID(index.ID).SetName("pick"),
		).ExecX(ctx)

		return index
	})
}

var photosIndexKey = &testDataKey{"index:photos"}

func IndexPhotos() *PhotosIndexRequest {
	return &PhotosIndexRequest{
		baseTestData: baseTestData{key: photosIndexKey},
	}
}
