// Code generated by entc, DO NOT EDIT.

package hook

import (
	"context"
	"fmt"

	"gitlab.com/gomimir/server/internal/ent"
)

// The FileLocationFunc type is an adapter to allow the use of ordinary
// function as FileLocation mutator.
type FileLocationFunc func(context.Context, *ent.FileLocationMutation) (ent.Value, error)

// Mutate calls f(ctx, m).
func (f FileLocationFunc) Mutate(ctx context.Context, m ent.Mutation) (ent.Value, error) {
	mv, ok := m.(*ent.FileLocationMutation)
	if !ok {
		return nil, fmt.Errorf("unexpected mutation type %T. expect *ent.FileLocationMutation", m)
	}
	return f(ctx, mv)
}

// The GlobalPermissionFunc type is an adapter to allow the use of ordinary
// function as GlobalPermission mutator.
type GlobalPermissionFunc func(context.Context, *ent.GlobalPermissionMutation) (ent.Value, error)

// Mutate calls f(ctx, m).
func (f GlobalPermissionFunc) Mutate(ctx context.Context, m ent.Mutation) (ent.Value, error) {
	mv, ok := m.(*ent.GlobalPermissionMutation)
	if !ok {
		return nil, fmt.Errorf("unexpected mutation type %T. expect *ent.GlobalPermissionMutation", m)
	}
	return f(ctx, mv)
}

// The IndexFunc type is an adapter to allow the use of ordinary
// function as Index mutator.
type IndexFunc func(context.Context, *ent.IndexMutation) (ent.Value, error)

// Mutate calls f(ctx, m).
func (f IndexFunc) Mutate(ctx context.Context, m ent.Mutation) (ent.Value, error) {
	mv, ok := m.(*ent.IndexMutation)
	if !ok {
		return nil, fmt.Errorf("unexpected mutation type %T. expect *ent.IndexMutation", m)
	}
	return f(ctx, mv)
}

// The IndexPermissionFunc type is an adapter to allow the use of ordinary
// function as IndexPermission mutator.
type IndexPermissionFunc func(context.Context, *ent.IndexPermissionMutation) (ent.Value, error)

// Mutate calls f(ctx, m).
func (f IndexPermissionFunc) Mutate(ctx context.Context, m ent.Mutation) (ent.Value, error) {
	mv, ok := m.(*ent.IndexPermissionMutation)
	if !ok {
		return nil, fmt.Errorf("unexpected mutation type %T. expect *ent.IndexPermissionMutation", m)
	}
	return f(ctx, mv)
}

// The RepositoryFunc type is an adapter to allow the use of ordinary
// function as Repository mutator.
type RepositoryFunc func(context.Context, *ent.RepositoryMutation) (ent.Value, error)

// Mutate calls f(ctx, m).
func (f RepositoryFunc) Mutate(ctx context.Context, m ent.Mutation) (ent.Value, error) {
	mv, ok := m.(*ent.RepositoryMutation)
	if !ok {
		return nil, fmt.Errorf("unexpected mutation type %T. expect *ent.RepositoryMutation", m)
	}
	return f(ctx, mv)
}

// The RuleFunc type is an adapter to allow the use of ordinary
// function as Rule mutator.
type RuleFunc func(context.Context, *ent.RuleMutation) (ent.Value, error)

// Mutate calls f(ctx, m).
func (f RuleFunc) Mutate(ctx context.Context, m ent.Mutation) (ent.Value, error) {
	mv, ok := m.(*ent.RuleMutation)
	if !ok {
		return nil, fmt.Errorf("unexpected mutation type %T. expect *ent.RuleMutation", m)
	}
	return f(ctx, mv)
}

// The RuleActionFunc type is an adapter to allow the use of ordinary
// function as RuleAction mutator.
type RuleActionFunc func(context.Context, *ent.RuleActionMutation) (ent.Value, error)

// Mutate calls f(ctx, m).
func (f RuleActionFunc) Mutate(ctx context.Context, m ent.Mutation) (ent.Value, error) {
	mv, ok := m.(*ent.RuleActionMutation)
	if !ok {
		return nil, fmt.Errorf("unexpected mutation type %T. expect *ent.RuleActionMutation", m)
	}
	return f(ctx, mv)
}

// The TagKindFunc type is an adapter to allow the use of ordinary
// function as TagKind mutator.
type TagKindFunc func(context.Context, *ent.TagKindMutation) (ent.Value, error)

// Mutate calls f(ctx, m).
func (f TagKindFunc) Mutate(ctx context.Context, m ent.Mutation) (ent.Value, error) {
	mv, ok := m.(*ent.TagKindMutation)
	if !ok {
		return nil, fmt.Errorf("unexpected mutation type %T. expect *ent.TagKindMutation", m)
	}
	return f(ctx, mv)
}

// The UserFunc type is an adapter to allow the use of ordinary
// function as User mutator.
type UserFunc func(context.Context, *ent.UserMutation) (ent.Value, error)

// Mutate calls f(ctx, m).
func (f UserFunc) Mutate(ctx context.Context, m ent.Mutation) (ent.Value, error) {
	mv, ok := m.(*ent.UserMutation)
	if !ok {
		return nil, fmt.Errorf("unexpected mutation type %T. expect *ent.UserMutation", m)
	}
	return f(ctx, mv)
}

// The UserRoleFunc type is an adapter to allow the use of ordinary
// function as UserRole mutator.
type UserRoleFunc func(context.Context, *ent.UserRoleMutation) (ent.Value, error)

// Mutate calls f(ctx, m).
func (f UserRoleFunc) Mutate(ctx context.Context, m ent.Mutation) (ent.Value, error) {
	mv, ok := m.(*ent.UserRoleMutation)
	if !ok {
		return nil, fmt.Errorf("unexpected mutation type %T. expect *ent.UserRoleMutation", m)
	}
	return f(ctx, mv)
}

// Condition is a hook condition function.
type Condition func(context.Context, ent.Mutation) bool

// And groups conditions with the AND operator.
func And(first, second Condition, rest ...Condition) Condition {
	return func(ctx context.Context, m ent.Mutation) bool {
		if !first(ctx, m) || !second(ctx, m) {
			return false
		}
		for _, cond := range rest {
			if !cond(ctx, m) {
				return false
			}
		}
		return true
	}
}

// Or groups conditions with the OR operator.
func Or(first, second Condition, rest ...Condition) Condition {
	return func(ctx context.Context, m ent.Mutation) bool {
		if first(ctx, m) || second(ctx, m) {
			return true
		}
		for _, cond := range rest {
			if cond(ctx, m) {
				return true
			}
		}
		return false
	}
}

// Not negates a given condition.
func Not(cond Condition) Condition {
	return func(ctx context.Context, m ent.Mutation) bool {
		return !cond(ctx, m)
	}
}

// HasOp is a condition testing mutation operation.
func HasOp(op ent.Op) Condition {
	return func(_ context.Context, m ent.Mutation) bool {
		return m.Op().Is(op)
	}
}

// HasAddedFields is a condition validating `.AddedField` on fields.
func HasAddedFields(field string, fields ...string) Condition {
	return func(_ context.Context, m ent.Mutation) bool {
		if _, exists := m.AddedField(field); !exists {
			return false
		}
		for _, field := range fields {
			if _, exists := m.AddedField(field); !exists {
				return false
			}
		}
		return true
	}
}

// HasClearedFields is a condition validating `.FieldCleared` on fields.
func HasClearedFields(field string, fields ...string) Condition {
	return func(_ context.Context, m ent.Mutation) bool {
		if exists := m.FieldCleared(field); !exists {
			return false
		}
		for _, field := range fields {
			if exists := m.FieldCleared(field); !exists {
				return false
			}
		}
		return true
	}
}

// HasFields is a condition validating `.Field` on fields.
func HasFields(field string, fields ...string) Condition {
	return func(_ context.Context, m ent.Mutation) bool {
		if _, exists := m.Field(field); !exists {
			return false
		}
		for _, field := range fields {
			if _, exists := m.Field(field); !exists {
				return false
			}
		}
		return true
	}
}

// If executes the given hook under condition.
//
//	hook.If(ComputeAverage, And(HasFields(...), HasAddedFields(...)))
//
func If(hk ent.Hook, cond Condition) ent.Hook {
	return func(next ent.Mutator) ent.Mutator {
		return ent.MutateFunc(func(ctx context.Context, m ent.Mutation) (ent.Value, error) {
			if cond(ctx, m) {
				return hk(next).Mutate(ctx, m)
			}
			return next.Mutate(ctx, m)
		})
	}
}

// On executes the given hook only for the given operation.
//
//	hook.On(Log, ent.Delete|ent.Create)
//
func On(hk ent.Hook, op ent.Op) ent.Hook {
	return If(hk, HasOp(op))
}

// Unless skips the given hook only for the given operation.
//
//	hook.Unless(Log, ent.Update|ent.UpdateOne)
//
func Unless(hk ent.Hook, op ent.Op) ent.Hook {
	return If(hk, Not(HasOp(op)))
}

// FixedError is a hook returning a fixed error.
func FixedError(err error) ent.Hook {
	return func(ent.Mutator) ent.Mutator {
		return ent.MutateFunc(func(context.Context, ent.Mutation) (ent.Value, error) {
			return nil, err
		})
	}
}

// Reject returns a hook that rejects all operations that match op.
//
//	func (T) Hooks() []ent.Hook {
//		return []ent.Hook{
//			Reject(ent.Delete|ent.Update),
//		}
//	}
//
func Reject(op ent.Op) ent.Hook {
	hk := FixedError(fmt.Errorf("%s operation is not allowed", op))
	return On(hk, op)
}

// Chain acts as a list of hooks and is effectively immutable.
// Once created, it will always hold the same set of hooks in the same order.
type Chain struct {
	hooks []ent.Hook
}

// NewChain creates a new chain of hooks.
func NewChain(hooks ...ent.Hook) Chain {
	return Chain{append([]ent.Hook(nil), hooks...)}
}

// Hook chains the list of hooks and returns the final hook.
func (c Chain) Hook() ent.Hook {
	return func(mutator ent.Mutator) ent.Mutator {
		for i := len(c.hooks) - 1; i >= 0; i-- {
			mutator = c.hooks[i](mutator)
		}
		return mutator
	}
}

// Append extends a chain, adding the specified hook
// as the last ones in the mutation flow.
func (c Chain) Append(hooks ...ent.Hook) Chain {
	newHooks := make([]ent.Hook, 0, len(c.hooks)+len(hooks))
	newHooks = append(newHooks, c.hooks...)
	newHooks = append(newHooks, hooks...)
	return Chain{newHooks}
}

// Extend extends a chain, adding the specified chain
// as the last ones in the mutation flow.
func (c Chain) Extend(chain Chain) Chain {
	return c.Append(chain.hooks...)
}
