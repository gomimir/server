// Code generated by entc, DO NOT EDIT.

package repository

import (
	"entgo.io/ent"
)

const (
	// Label holds the string label denoting the repository type in the database.
	Label = "repository"
	// FieldID holds the string denoting the id field in the database.
	FieldID = "id"
	// FieldName holds the string denoting the name field in the database.
	FieldName = "name"
	// FieldConfig holds the string denoting the config field in the database.
	FieldConfig = "config"
	// Table holds the table name of the repository in the database.
	Table = "repositories"
)

// Columns holds all SQL columns for repository fields.
var Columns = []string{
	FieldID,
	FieldName,
	FieldConfig,
}

// ValidColumn reports if the column name is valid (part of the table columns).
func ValidColumn(column string) bool {
	for i := range Columns {
		if column == Columns[i] {
			return true
		}
	}
	return false
}

// Note that the variables below are initialized by the runtime
// package on the initialization of the application. Therefore,
// it should be imported in the main as follows:
//
//	import _ "gitlab.com/gomimir/server/internal/ent/runtime"
//
var (
	Hooks [1]ent.Hook
	// NameValidator is a validator for the "name" field. It is called by the builders before save.
	NameValidator func(string) error
)
