// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"
	"math"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"gitlab.com/gomimir/server/internal/ent/index"
	"gitlab.com/gomimir/server/internal/ent/predicate"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/ent/tagkind"
)

// TagKindQuery is the builder for querying TagKind entities.
type TagKindQuery struct {
	config
	limit      *int
	offset     *int
	unique     *bool
	order      []OrderFunc
	fields     []string
	predicates []predicate.TagKind
	// eager-loading edges.
	withIndex *IndexQuery
	// intermediate query (i.e. traversal path).
	sql  *sql.Selector
	path func(context.Context) (*sql.Selector, error)
}

// Where adds a new predicate for the TagKindQuery builder.
func (tkq *TagKindQuery) Where(ps ...predicate.TagKind) *TagKindQuery {
	tkq.predicates = append(tkq.predicates, ps...)
	return tkq
}

// Limit adds a limit step to the query.
func (tkq *TagKindQuery) Limit(limit int) *TagKindQuery {
	tkq.limit = &limit
	return tkq
}

// Offset adds an offset step to the query.
func (tkq *TagKindQuery) Offset(offset int) *TagKindQuery {
	tkq.offset = &offset
	return tkq
}

// Unique configures the query builder to filter duplicate records on query.
// By default, unique is set to true, and can be disabled using this method.
func (tkq *TagKindQuery) Unique(unique bool) *TagKindQuery {
	tkq.unique = &unique
	return tkq
}

// Order adds an order step to the query.
func (tkq *TagKindQuery) Order(o ...OrderFunc) *TagKindQuery {
	tkq.order = append(tkq.order, o...)
	return tkq
}

// QueryIndex chains the current query on the "index" edge.
func (tkq *TagKindQuery) QueryIndex() *IndexQuery {
	query := &IndexQuery{config: tkq.config}
	query.path = func(ctx context.Context) (fromU *sql.Selector, err error) {
		if err := tkq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		selector := tkq.sqlQuery(ctx)
		if err := selector.Err(); err != nil {
			return nil, err
		}
		step := sqlgraph.NewStep(
			sqlgraph.From(tagkind.Table, tagkind.FieldID, selector),
			sqlgraph.To(index.Table, index.FieldID),
			sqlgraph.Edge(sqlgraph.M2O, true, tagkind.IndexTable, tagkind.IndexColumn),
		)
		fromU = sqlgraph.SetNeighbors(tkq.driver.Dialect(), step)
		return fromU, nil
	}
	return query
}

// First returns the first TagKind entity from the query.
// Returns a *NotFoundError when no TagKind was found.
func (tkq *TagKindQuery) First(ctx context.Context) (*TagKind, error) {
	nodes, err := tkq.Limit(1).All(ctx)
	if err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nil, &NotFoundError{tagkind.Label}
	}
	return nodes[0], nil
}

// FirstX is like First, but panics if an error occurs.
func (tkq *TagKindQuery) FirstX(ctx context.Context) *TagKind {
	node, err := tkq.First(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return node
}

// FirstID returns the first TagKind ID from the query.
// Returns a *NotFoundError when no TagKind ID was found.
func (tkq *TagKindQuery) FirstID(ctx context.Context) (id pulid.ID, err error) {
	var ids []pulid.ID
	if ids, err = tkq.Limit(1).IDs(ctx); err != nil {
		return
	}
	if len(ids) == 0 {
		err = &NotFoundError{tagkind.Label}
		return
	}
	return ids[0], nil
}

// FirstIDX is like FirstID, but panics if an error occurs.
func (tkq *TagKindQuery) FirstIDX(ctx context.Context) pulid.ID {
	id, err := tkq.FirstID(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return id
}

// Only returns a single TagKind entity found by the query, ensuring it only returns one.
// Returns a *NotSingularError when exactly one TagKind entity is not found.
// Returns a *NotFoundError when no TagKind entities are found.
func (tkq *TagKindQuery) Only(ctx context.Context) (*TagKind, error) {
	nodes, err := tkq.Limit(2).All(ctx)
	if err != nil {
		return nil, err
	}
	switch len(nodes) {
	case 1:
		return nodes[0], nil
	case 0:
		return nil, &NotFoundError{tagkind.Label}
	default:
		return nil, &NotSingularError{tagkind.Label}
	}
}

// OnlyX is like Only, but panics if an error occurs.
func (tkq *TagKindQuery) OnlyX(ctx context.Context) *TagKind {
	node, err := tkq.Only(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// OnlyID is like Only, but returns the only TagKind ID in the query.
// Returns a *NotSingularError when exactly one TagKind ID is not found.
// Returns a *NotFoundError when no entities are found.
func (tkq *TagKindQuery) OnlyID(ctx context.Context) (id pulid.ID, err error) {
	var ids []pulid.ID
	if ids, err = tkq.Limit(2).IDs(ctx); err != nil {
		return
	}
	switch len(ids) {
	case 1:
		id = ids[0]
	case 0:
		err = &NotFoundError{tagkind.Label}
	default:
		err = &NotSingularError{tagkind.Label}
	}
	return
}

// OnlyIDX is like OnlyID, but panics if an error occurs.
func (tkq *TagKindQuery) OnlyIDX(ctx context.Context) pulid.ID {
	id, err := tkq.OnlyID(ctx)
	if err != nil {
		panic(err)
	}
	return id
}

// All executes the query and returns a list of TagKinds.
func (tkq *TagKindQuery) All(ctx context.Context) ([]*TagKind, error) {
	if err := tkq.prepareQuery(ctx); err != nil {
		return nil, err
	}
	return tkq.sqlAll(ctx)
}

// AllX is like All, but panics if an error occurs.
func (tkq *TagKindQuery) AllX(ctx context.Context) []*TagKind {
	nodes, err := tkq.All(ctx)
	if err != nil {
		panic(err)
	}
	return nodes
}

// IDs executes the query and returns a list of TagKind IDs.
func (tkq *TagKindQuery) IDs(ctx context.Context) ([]pulid.ID, error) {
	var ids []pulid.ID
	if err := tkq.Select(tagkind.FieldID).Scan(ctx, &ids); err != nil {
		return nil, err
	}
	return ids, nil
}

// IDsX is like IDs, but panics if an error occurs.
func (tkq *TagKindQuery) IDsX(ctx context.Context) []pulid.ID {
	ids, err := tkq.IDs(ctx)
	if err != nil {
		panic(err)
	}
	return ids
}

// Count returns the count of the given query.
func (tkq *TagKindQuery) Count(ctx context.Context) (int, error) {
	if err := tkq.prepareQuery(ctx); err != nil {
		return 0, err
	}
	return tkq.sqlCount(ctx)
}

// CountX is like Count, but panics if an error occurs.
func (tkq *TagKindQuery) CountX(ctx context.Context) int {
	count, err := tkq.Count(ctx)
	if err != nil {
		panic(err)
	}
	return count
}

// Exist returns true if the query has elements in the graph.
func (tkq *TagKindQuery) Exist(ctx context.Context) (bool, error) {
	if err := tkq.prepareQuery(ctx); err != nil {
		return false, err
	}
	return tkq.sqlExist(ctx)
}

// ExistX is like Exist, but panics if an error occurs.
func (tkq *TagKindQuery) ExistX(ctx context.Context) bool {
	exist, err := tkq.Exist(ctx)
	if err != nil {
		panic(err)
	}
	return exist
}

// Clone returns a duplicate of the TagKindQuery builder, including all associated steps. It can be
// used to prepare common query builders and use them differently after the clone is made.
func (tkq *TagKindQuery) Clone() *TagKindQuery {
	if tkq == nil {
		return nil
	}
	return &TagKindQuery{
		config:     tkq.config,
		limit:      tkq.limit,
		offset:     tkq.offset,
		order:      append([]OrderFunc{}, tkq.order...),
		predicates: append([]predicate.TagKind{}, tkq.predicates...),
		withIndex:  tkq.withIndex.Clone(),
		// clone intermediate query.
		sql:  tkq.sql.Clone(),
		path: tkq.path,
	}
}

// WithIndex tells the query-builder to eager-load the nodes that are connected to
// the "index" edge. The optional arguments are used to configure the query builder of the edge.
func (tkq *TagKindQuery) WithIndex(opts ...func(*IndexQuery)) *TagKindQuery {
	query := &IndexQuery{config: tkq.config}
	for _, opt := range opts {
		opt(query)
	}
	tkq.withIndex = query
	return tkq
}

// GroupBy is used to group vertices by one or more fields/columns.
// It is often used with aggregate functions, like: count, max, mean, min, sum.
//
// Example:
//
//	var v []struct {
//		IndexID pulid.ID `json:"index_id,omitempty"`
//		Count int `json:"count,omitempty"`
//	}
//
//	client.TagKind.Query().
//		GroupBy(tagkind.FieldIndexID).
//		Aggregate(ent.Count()).
//		Scan(ctx, &v)
//
func (tkq *TagKindQuery) GroupBy(field string, fields ...string) *TagKindGroupBy {
	group := &TagKindGroupBy{config: tkq.config}
	group.fields = append([]string{field}, fields...)
	group.path = func(ctx context.Context) (prev *sql.Selector, err error) {
		if err := tkq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		return tkq.sqlQuery(ctx), nil
	}
	return group
}

// Select allows the selection one or more fields/columns for the given query,
// instead of selecting all fields in the entity.
//
// Example:
//
//	var v []struct {
//		IndexID pulid.ID `json:"index_id,omitempty"`
//	}
//
//	client.TagKind.Query().
//		Select(tagkind.FieldIndexID).
//		Scan(ctx, &v)
//
func (tkq *TagKindQuery) Select(fields ...string) *TagKindSelect {
	tkq.fields = append(tkq.fields, fields...)
	return &TagKindSelect{TagKindQuery: tkq}
}

func (tkq *TagKindQuery) prepareQuery(ctx context.Context) error {
	for _, f := range tkq.fields {
		if !tagkind.ValidColumn(f) {
			return &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
		}
	}
	if tkq.path != nil {
		prev, err := tkq.path(ctx)
		if err != nil {
			return err
		}
		tkq.sql = prev
	}
	return nil
}

func (tkq *TagKindQuery) sqlAll(ctx context.Context) ([]*TagKind, error) {
	var (
		nodes       = []*TagKind{}
		_spec       = tkq.querySpec()
		loadedTypes = [1]bool{
			tkq.withIndex != nil,
		}
	)
	_spec.ScanValues = func(columns []string) ([]interface{}, error) {
		node := &TagKind{config: tkq.config}
		nodes = append(nodes, node)
		return node.scanValues(columns)
	}
	_spec.Assign = func(columns []string, values []interface{}) error {
		if len(nodes) == 0 {
			return fmt.Errorf("ent: Assign called without calling ScanValues")
		}
		node := nodes[len(nodes)-1]
		node.Edges.loadedTypes = loadedTypes
		return node.assignValues(columns, values)
	}
	if err := sqlgraph.QueryNodes(ctx, tkq.driver, _spec); err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nodes, nil
	}

	if query := tkq.withIndex; query != nil {
		ids := make([]pulid.ID, 0, len(nodes))
		nodeids := make(map[pulid.ID][]*TagKind)
		for i := range nodes {
			fk := nodes[i].IndexID
			if _, ok := nodeids[fk]; !ok {
				ids = append(ids, fk)
			}
			nodeids[fk] = append(nodeids[fk], nodes[i])
		}
		query.Where(index.IDIn(ids...))
		neighbors, err := query.All(ctx)
		if err != nil {
			return nil, err
		}
		for _, n := range neighbors {
			nodes, ok := nodeids[n.ID]
			if !ok {
				return nil, fmt.Errorf(`unexpected foreign-key "index_id" returned %v`, n.ID)
			}
			for i := range nodes {
				nodes[i].Edges.Index = n
			}
		}
	}

	return nodes, nil
}

func (tkq *TagKindQuery) sqlCount(ctx context.Context) (int, error) {
	_spec := tkq.querySpec()
	_spec.Node.Columns = tkq.fields
	if len(tkq.fields) > 0 {
		_spec.Unique = tkq.unique != nil && *tkq.unique
	}
	return sqlgraph.CountNodes(ctx, tkq.driver, _spec)
}

func (tkq *TagKindQuery) sqlExist(ctx context.Context) (bool, error) {
	n, err := tkq.sqlCount(ctx)
	if err != nil {
		return false, fmt.Errorf("ent: check existence: %w", err)
	}
	return n > 0, nil
}

func (tkq *TagKindQuery) querySpec() *sqlgraph.QuerySpec {
	_spec := &sqlgraph.QuerySpec{
		Node: &sqlgraph.NodeSpec{
			Table:   tagkind.Table,
			Columns: tagkind.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeString,
				Column: tagkind.FieldID,
			},
		},
		From:   tkq.sql,
		Unique: true,
	}
	if unique := tkq.unique; unique != nil {
		_spec.Unique = *unique
	}
	if fields := tkq.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, tagkind.FieldID)
		for i := range fields {
			if fields[i] != tagkind.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, fields[i])
			}
		}
	}
	if ps := tkq.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if limit := tkq.limit; limit != nil {
		_spec.Limit = *limit
	}
	if offset := tkq.offset; offset != nil {
		_spec.Offset = *offset
	}
	if ps := tkq.order; len(ps) > 0 {
		_spec.Order = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	return _spec
}

func (tkq *TagKindQuery) sqlQuery(ctx context.Context) *sql.Selector {
	builder := sql.Dialect(tkq.driver.Dialect())
	t1 := builder.Table(tagkind.Table)
	columns := tkq.fields
	if len(columns) == 0 {
		columns = tagkind.Columns
	}
	selector := builder.Select(t1.Columns(columns...)...).From(t1)
	if tkq.sql != nil {
		selector = tkq.sql
		selector.Select(selector.Columns(columns...)...)
	}
	if tkq.unique != nil && *tkq.unique {
		selector.Distinct()
	}
	for _, p := range tkq.predicates {
		p(selector)
	}
	for _, p := range tkq.order {
		p(selector)
	}
	if offset := tkq.offset; offset != nil {
		// limit is mandatory for offset clause. We start
		// with default value, and override it below if needed.
		selector.Offset(*offset).Limit(math.MaxInt32)
	}
	if limit := tkq.limit; limit != nil {
		selector.Limit(*limit)
	}
	return selector
}

// TagKindGroupBy is the group-by builder for TagKind entities.
type TagKindGroupBy struct {
	config
	fields []string
	fns    []AggregateFunc
	// intermediate query (i.e. traversal path).
	sql  *sql.Selector
	path func(context.Context) (*sql.Selector, error)
}

// Aggregate adds the given aggregation functions to the group-by query.
func (tkgb *TagKindGroupBy) Aggregate(fns ...AggregateFunc) *TagKindGroupBy {
	tkgb.fns = append(tkgb.fns, fns...)
	return tkgb
}

// Scan applies the group-by query and scans the result into the given value.
func (tkgb *TagKindGroupBy) Scan(ctx context.Context, v interface{}) error {
	query, err := tkgb.path(ctx)
	if err != nil {
		return err
	}
	tkgb.sql = query
	return tkgb.sqlScan(ctx, v)
}

// ScanX is like Scan, but panics if an error occurs.
func (tkgb *TagKindGroupBy) ScanX(ctx context.Context, v interface{}) {
	if err := tkgb.Scan(ctx, v); err != nil {
		panic(err)
	}
}

// Strings returns list of strings from group-by.
// It is only allowed when executing a group-by query with one field.
func (tkgb *TagKindGroupBy) Strings(ctx context.Context) ([]string, error) {
	if len(tkgb.fields) > 1 {
		return nil, errors.New("ent: TagKindGroupBy.Strings is not achievable when grouping more than 1 field")
	}
	var v []string
	if err := tkgb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// StringsX is like Strings, but panics if an error occurs.
func (tkgb *TagKindGroupBy) StringsX(ctx context.Context) []string {
	v, err := tkgb.Strings(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// String returns a single string from a group-by query.
// It is only allowed when executing a group-by query with one field.
func (tkgb *TagKindGroupBy) String(ctx context.Context) (_ string, err error) {
	var v []string
	if v, err = tkgb.Strings(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{tagkind.Label}
	default:
		err = fmt.Errorf("ent: TagKindGroupBy.Strings returned %d results when one was expected", len(v))
	}
	return
}

// StringX is like String, but panics if an error occurs.
func (tkgb *TagKindGroupBy) StringX(ctx context.Context) string {
	v, err := tkgb.String(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Ints returns list of ints from group-by.
// It is only allowed when executing a group-by query with one field.
func (tkgb *TagKindGroupBy) Ints(ctx context.Context) ([]int, error) {
	if len(tkgb.fields) > 1 {
		return nil, errors.New("ent: TagKindGroupBy.Ints is not achievable when grouping more than 1 field")
	}
	var v []int
	if err := tkgb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// IntsX is like Ints, but panics if an error occurs.
func (tkgb *TagKindGroupBy) IntsX(ctx context.Context) []int {
	v, err := tkgb.Ints(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Int returns a single int from a group-by query.
// It is only allowed when executing a group-by query with one field.
func (tkgb *TagKindGroupBy) Int(ctx context.Context) (_ int, err error) {
	var v []int
	if v, err = tkgb.Ints(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{tagkind.Label}
	default:
		err = fmt.Errorf("ent: TagKindGroupBy.Ints returned %d results when one was expected", len(v))
	}
	return
}

// IntX is like Int, but panics if an error occurs.
func (tkgb *TagKindGroupBy) IntX(ctx context.Context) int {
	v, err := tkgb.Int(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64s returns list of float64s from group-by.
// It is only allowed when executing a group-by query with one field.
func (tkgb *TagKindGroupBy) Float64s(ctx context.Context) ([]float64, error) {
	if len(tkgb.fields) > 1 {
		return nil, errors.New("ent: TagKindGroupBy.Float64s is not achievable when grouping more than 1 field")
	}
	var v []float64
	if err := tkgb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// Float64sX is like Float64s, but panics if an error occurs.
func (tkgb *TagKindGroupBy) Float64sX(ctx context.Context) []float64 {
	v, err := tkgb.Float64s(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64 returns a single float64 from a group-by query.
// It is only allowed when executing a group-by query with one field.
func (tkgb *TagKindGroupBy) Float64(ctx context.Context) (_ float64, err error) {
	var v []float64
	if v, err = tkgb.Float64s(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{tagkind.Label}
	default:
		err = fmt.Errorf("ent: TagKindGroupBy.Float64s returned %d results when one was expected", len(v))
	}
	return
}

// Float64X is like Float64, but panics if an error occurs.
func (tkgb *TagKindGroupBy) Float64X(ctx context.Context) float64 {
	v, err := tkgb.Float64(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bools returns list of bools from group-by.
// It is only allowed when executing a group-by query with one field.
func (tkgb *TagKindGroupBy) Bools(ctx context.Context) ([]bool, error) {
	if len(tkgb.fields) > 1 {
		return nil, errors.New("ent: TagKindGroupBy.Bools is not achievable when grouping more than 1 field")
	}
	var v []bool
	if err := tkgb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// BoolsX is like Bools, but panics if an error occurs.
func (tkgb *TagKindGroupBy) BoolsX(ctx context.Context) []bool {
	v, err := tkgb.Bools(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bool returns a single bool from a group-by query.
// It is only allowed when executing a group-by query with one field.
func (tkgb *TagKindGroupBy) Bool(ctx context.Context) (_ bool, err error) {
	var v []bool
	if v, err = tkgb.Bools(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{tagkind.Label}
	default:
		err = fmt.Errorf("ent: TagKindGroupBy.Bools returned %d results when one was expected", len(v))
	}
	return
}

// BoolX is like Bool, but panics if an error occurs.
func (tkgb *TagKindGroupBy) BoolX(ctx context.Context) bool {
	v, err := tkgb.Bool(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

func (tkgb *TagKindGroupBy) sqlScan(ctx context.Context, v interface{}) error {
	for _, f := range tkgb.fields {
		if !tagkind.ValidColumn(f) {
			return &ValidationError{Name: f, err: fmt.Errorf("invalid field %q for group-by", f)}
		}
	}
	selector := tkgb.sqlQuery()
	if err := selector.Err(); err != nil {
		return err
	}
	rows := &sql.Rows{}
	query, args := selector.Query()
	if err := tkgb.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}

func (tkgb *TagKindGroupBy) sqlQuery() *sql.Selector {
	selector := tkgb.sql.Select()
	aggregation := make([]string, 0, len(tkgb.fns))
	for _, fn := range tkgb.fns {
		aggregation = append(aggregation, fn(selector))
	}
	// If no columns were selected in a custom aggregation function, the default
	// selection is the fields used for "group-by", and the aggregation functions.
	if len(selector.SelectedColumns()) == 0 {
		columns := make([]string, 0, len(tkgb.fields)+len(tkgb.fns))
		for _, f := range tkgb.fields {
			columns = append(columns, selector.C(f))
		}
		columns = append(columns, aggregation...)
		selector.Select(columns...)
	}
	return selector.GroupBy(selector.Columns(tkgb.fields...)...)
}

// TagKindSelect is the builder for selecting fields of TagKind entities.
type TagKindSelect struct {
	*TagKindQuery
	// intermediate query (i.e. traversal path).
	sql *sql.Selector
}

// Scan applies the selector query and scans the result into the given value.
func (tks *TagKindSelect) Scan(ctx context.Context, v interface{}) error {
	if err := tks.prepareQuery(ctx); err != nil {
		return err
	}
	tks.sql = tks.TagKindQuery.sqlQuery(ctx)
	return tks.sqlScan(ctx, v)
}

// ScanX is like Scan, but panics if an error occurs.
func (tks *TagKindSelect) ScanX(ctx context.Context, v interface{}) {
	if err := tks.Scan(ctx, v); err != nil {
		panic(err)
	}
}

// Strings returns list of strings from a selector. It is only allowed when selecting one field.
func (tks *TagKindSelect) Strings(ctx context.Context) ([]string, error) {
	if len(tks.fields) > 1 {
		return nil, errors.New("ent: TagKindSelect.Strings is not achievable when selecting more than 1 field")
	}
	var v []string
	if err := tks.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// StringsX is like Strings, but panics if an error occurs.
func (tks *TagKindSelect) StringsX(ctx context.Context) []string {
	v, err := tks.Strings(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// String returns a single string from a selector. It is only allowed when selecting one field.
func (tks *TagKindSelect) String(ctx context.Context) (_ string, err error) {
	var v []string
	if v, err = tks.Strings(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{tagkind.Label}
	default:
		err = fmt.Errorf("ent: TagKindSelect.Strings returned %d results when one was expected", len(v))
	}
	return
}

// StringX is like String, but panics if an error occurs.
func (tks *TagKindSelect) StringX(ctx context.Context) string {
	v, err := tks.String(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Ints returns list of ints from a selector. It is only allowed when selecting one field.
func (tks *TagKindSelect) Ints(ctx context.Context) ([]int, error) {
	if len(tks.fields) > 1 {
		return nil, errors.New("ent: TagKindSelect.Ints is not achievable when selecting more than 1 field")
	}
	var v []int
	if err := tks.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// IntsX is like Ints, but panics if an error occurs.
func (tks *TagKindSelect) IntsX(ctx context.Context) []int {
	v, err := tks.Ints(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Int returns a single int from a selector. It is only allowed when selecting one field.
func (tks *TagKindSelect) Int(ctx context.Context) (_ int, err error) {
	var v []int
	if v, err = tks.Ints(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{tagkind.Label}
	default:
		err = fmt.Errorf("ent: TagKindSelect.Ints returned %d results when one was expected", len(v))
	}
	return
}

// IntX is like Int, but panics if an error occurs.
func (tks *TagKindSelect) IntX(ctx context.Context) int {
	v, err := tks.Int(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64s returns list of float64s from a selector. It is only allowed when selecting one field.
func (tks *TagKindSelect) Float64s(ctx context.Context) ([]float64, error) {
	if len(tks.fields) > 1 {
		return nil, errors.New("ent: TagKindSelect.Float64s is not achievable when selecting more than 1 field")
	}
	var v []float64
	if err := tks.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// Float64sX is like Float64s, but panics if an error occurs.
func (tks *TagKindSelect) Float64sX(ctx context.Context) []float64 {
	v, err := tks.Float64s(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64 returns a single float64 from a selector. It is only allowed when selecting one field.
func (tks *TagKindSelect) Float64(ctx context.Context) (_ float64, err error) {
	var v []float64
	if v, err = tks.Float64s(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{tagkind.Label}
	default:
		err = fmt.Errorf("ent: TagKindSelect.Float64s returned %d results when one was expected", len(v))
	}
	return
}

// Float64X is like Float64, but panics if an error occurs.
func (tks *TagKindSelect) Float64X(ctx context.Context) float64 {
	v, err := tks.Float64(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bools returns list of bools from a selector. It is only allowed when selecting one field.
func (tks *TagKindSelect) Bools(ctx context.Context) ([]bool, error) {
	if len(tks.fields) > 1 {
		return nil, errors.New("ent: TagKindSelect.Bools is not achievable when selecting more than 1 field")
	}
	var v []bool
	if err := tks.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// BoolsX is like Bools, but panics if an error occurs.
func (tks *TagKindSelect) BoolsX(ctx context.Context) []bool {
	v, err := tks.Bools(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bool returns a single bool from a selector. It is only allowed when selecting one field.
func (tks *TagKindSelect) Bool(ctx context.Context) (_ bool, err error) {
	var v []bool
	if v, err = tks.Bools(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{tagkind.Label}
	default:
		err = fmt.Errorf("ent: TagKindSelect.Bools returned %d results when one was expected", len(v))
	}
	return
}

// BoolX is like Bool, but panics if an error occurs.
func (tks *TagKindSelect) BoolX(ctx context.Context) bool {
	v, err := tks.Bool(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

func (tks *TagKindSelect) sqlScan(ctx context.Context, v interface{}) error {
	rows := &sql.Rows{}
	query, args := tks.sql.Query()
	if err := tks.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}
