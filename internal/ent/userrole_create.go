// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"

	"entgo.io/ent/dialect"
	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/ent/user"
	"gitlab.com/gomimir/server/internal/ent/userrole"
)

// UserRoleCreate is the builder for creating a UserRole entity.
type UserRoleCreate struct {
	config
	mutation *UserRoleMutation
	hooks    []Hook
	conflict []sql.ConflictOption
}

// SetName sets the "name" field.
func (urc *UserRoleCreate) SetName(s string) *UserRoleCreate {
	urc.mutation.SetName(s)
	return urc
}

// SetDisplayName sets the "displayName" field.
func (urc *UserRoleCreate) SetDisplayName(s string) *UserRoleCreate {
	urc.mutation.SetDisplayName(s)
	return urc
}

// SetNillableDisplayName sets the "displayName" field if the given value is not nil.
func (urc *UserRoleCreate) SetNillableDisplayName(s *string) *UserRoleCreate {
	if s != nil {
		urc.SetDisplayName(*s)
	}
	return urc
}

// SetID sets the "id" field.
func (urc *UserRoleCreate) SetID(pu pulid.ID) *UserRoleCreate {
	urc.mutation.SetID(pu)
	return urc
}

// AddUserIDs adds the "users" edge to the User entity by IDs.
func (urc *UserRoleCreate) AddUserIDs(ids ...pulid.ID) *UserRoleCreate {
	urc.mutation.AddUserIDs(ids...)
	return urc
}

// AddUsers adds the "users" edges to the User entity.
func (urc *UserRoleCreate) AddUsers(u ...*User) *UserRoleCreate {
	ids := make([]pulid.ID, len(u))
	for i := range u {
		ids[i] = u[i].ID
	}
	return urc.AddUserIDs(ids...)
}

// Mutation returns the UserRoleMutation object of the builder.
func (urc *UserRoleCreate) Mutation() *UserRoleMutation {
	return urc.mutation
}

// Save creates the UserRole in the database.
func (urc *UserRoleCreate) Save(ctx context.Context) (*UserRole, error) {
	var (
		err  error
		node *UserRole
	)
	if err := urc.defaults(); err != nil {
		return nil, err
	}
	if len(urc.hooks) == 0 {
		if err = urc.check(); err != nil {
			return nil, err
		}
		node, err = urc.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*UserRoleMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			if err = urc.check(); err != nil {
				return nil, err
			}
			urc.mutation = mutation
			if node, err = urc.sqlSave(ctx); err != nil {
				return nil, err
			}
			mutation.id = &node.ID
			mutation.done = true
			return node, err
		})
		for i := len(urc.hooks) - 1; i >= 0; i-- {
			if urc.hooks[i] == nil {
				return nil, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = urc.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, urc.mutation); err != nil {
			return nil, err
		}
	}
	return node, err
}

// SaveX calls Save and panics if Save returns an error.
func (urc *UserRoleCreate) SaveX(ctx context.Context) *UserRole {
	v, err := urc.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (urc *UserRoleCreate) Exec(ctx context.Context) error {
	_, err := urc.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (urc *UserRoleCreate) ExecX(ctx context.Context) {
	if err := urc.Exec(ctx); err != nil {
		panic(err)
	}
}

// defaults sets the default values of the builder before save.
func (urc *UserRoleCreate) defaults() error {
	if _, ok := urc.mutation.DisplayName(); !ok {
		v := userrole.DefaultDisplayName
		urc.mutation.SetDisplayName(v)
	}
	return nil
}

// check runs all checks and user-defined validators on the builder.
func (urc *UserRoleCreate) check() error {
	if _, ok := urc.mutation.Name(); !ok {
		return &ValidationError{Name: "name", err: errors.New(`ent: missing required field "UserRole.name"`)}
	}
	if v, ok := urc.mutation.Name(); ok {
		if err := userrole.NameValidator(v); err != nil {
			return &ValidationError{Name: "name", err: fmt.Errorf(`ent: validator failed for field "UserRole.name": %w`, err)}
		}
	}
	if _, ok := urc.mutation.DisplayName(); !ok {
		return &ValidationError{Name: "displayName", err: errors.New(`ent: missing required field "UserRole.displayName"`)}
	}
	return nil
}

func (urc *UserRoleCreate) sqlSave(ctx context.Context) (*UserRole, error) {
	_node, _spec := urc.createSpec()
	if err := sqlgraph.CreateNode(ctx, urc.driver, _spec); err != nil {
		if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{err.Error(), err}
		}
		return nil, err
	}
	if _spec.ID.Value != nil {
		if id, ok := _spec.ID.Value.(*pulid.ID); ok {
			_node.ID = *id
		} else if err := _node.ID.Scan(_spec.ID.Value); err != nil {
			return nil, err
		}
	}
	return _node, nil
}

func (urc *UserRoleCreate) createSpec() (*UserRole, *sqlgraph.CreateSpec) {
	var (
		_node = &UserRole{config: urc.config}
		_spec = &sqlgraph.CreateSpec{
			Table: userrole.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeString,
				Column: userrole.FieldID,
			},
		}
	)
	_spec.OnConflict = urc.conflict
	if id, ok := urc.mutation.ID(); ok {
		_node.ID = id
		_spec.ID.Value = &id
	}
	if value, ok := urc.mutation.Name(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: userrole.FieldName,
		})
		_node.Name = value
	}
	if value, ok := urc.mutation.DisplayName(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: userrole.FieldDisplayName,
		})
		_node.DisplayName = value
	}
	if nodes := urc.mutation.UsersIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2M,
			Inverse: true,
			Table:   userrole.UsersTable,
			Columns: userrole.UsersPrimaryKey,
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeString,
					Column: user.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges = append(_spec.Edges, edge)
	}
	return _node, _spec
}

// OnConflict allows configuring the `ON CONFLICT` / `ON DUPLICATE KEY` clause
// of the `INSERT` statement. For example:
//
//	client.UserRole.Create().
//		SetName(v).
//		OnConflict(
//			// Update the row with the new values
//			// the was proposed for insertion.
//			sql.ResolveWithNewValues(),
//		).
//		// Override some of the fields with custom
//		// update values.
//		Update(func(u *ent.UserRoleUpsert) {
//			SetName(v+v).
//		}).
//		Exec(ctx)
//
func (urc *UserRoleCreate) OnConflict(opts ...sql.ConflictOption) *UserRoleUpsertOne {
	urc.conflict = opts
	return &UserRoleUpsertOne{
		create: urc,
	}
}

// OnConflictColumns calls `OnConflict` and configures the columns
// as conflict target. Using this option is equivalent to using:
//
//	client.UserRole.Create().
//		OnConflict(sql.ConflictColumns(columns...)).
//		Exec(ctx)
//
func (urc *UserRoleCreate) OnConflictColumns(columns ...string) *UserRoleUpsertOne {
	urc.conflict = append(urc.conflict, sql.ConflictColumns(columns...))
	return &UserRoleUpsertOne{
		create: urc,
	}
}

type (
	// UserRoleUpsertOne is the builder for "upsert"-ing
	//  one UserRole node.
	UserRoleUpsertOne struct {
		create *UserRoleCreate
	}

	// UserRoleUpsert is the "OnConflict" setter.
	UserRoleUpsert struct {
		*sql.UpdateSet
	}
)

// SetName sets the "name" field.
func (u *UserRoleUpsert) SetName(v string) *UserRoleUpsert {
	u.Set(userrole.FieldName, v)
	return u
}

// UpdateName sets the "name" field to the value that was provided on create.
func (u *UserRoleUpsert) UpdateName() *UserRoleUpsert {
	u.SetExcluded(userrole.FieldName)
	return u
}

// SetDisplayName sets the "displayName" field.
func (u *UserRoleUpsert) SetDisplayName(v string) *UserRoleUpsert {
	u.Set(userrole.FieldDisplayName, v)
	return u
}

// UpdateDisplayName sets the "displayName" field to the value that was provided on create.
func (u *UserRoleUpsert) UpdateDisplayName() *UserRoleUpsert {
	u.SetExcluded(userrole.FieldDisplayName)
	return u
}

// UpdateNewValues updates the mutable fields using the new values that were set on create except the ID field.
// Using this option is equivalent to using:
//
//	client.UserRole.Create().
//		OnConflict(
//			sql.ResolveWithNewValues(),
//			sql.ResolveWith(func(u *sql.UpdateSet) {
//				u.SetIgnore(userrole.FieldID)
//			}),
//		).
//		Exec(ctx)
//
func (u *UserRoleUpsertOne) UpdateNewValues() *UserRoleUpsertOne {
	u.create.conflict = append(u.create.conflict, sql.ResolveWithNewValues())
	u.create.conflict = append(u.create.conflict, sql.ResolveWith(func(s *sql.UpdateSet) {
		if _, exists := u.create.mutation.ID(); exists {
			s.SetIgnore(userrole.FieldID)
		}
	}))
	return u
}

// Ignore sets each column to itself in case of conflict.
// Using this option is equivalent to using:
//
//  client.UserRole.Create().
//      OnConflict(sql.ResolveWithIgnore()).
//      Exec(ctx)
//
func (u *UserRoleUpsertOne) Ignore() *UserRoleUpsertOne {
	u.create.conflict = append(u.create.conflict, sql.ResolveWithIgnore())
	return u
}

// DoNothing configures the conflict_action to `DO NOTHING`.
// Supported only by SQLite and PostgreSQL.
func (u *UserRoleUpsertOne) DoNothing() *UserRoleUpsertOne {
	u.create.conflict = append(u.create.conflict, sql.DoNothing())
	return u
}

// Update allows overriding fields `UPDATE` values. See the UserRoleCreate.OnConflict
// documentation for more info.
func (u *UserRoleUpsertOne) Update(set func(*UserRoleUpsert)) *UserRoleUpsertOne {
	u.create.conflict = append(u.create.conflict, sql.ResolveWith(func(update *sql.UpdateSet) {
		set(&UserRoleUpsert{UpdateSet: update})
	}))
	return u
}

// SetName sets the "name" field.
func (u *UserRoleUpsertOne) SetName(v string) *UserRoleUpsertOne {
	return u.Update(func(s *UserRoleUpsert) {
		s.SetName(v)
	})
}

// UpdateName sets the "name" field to the value that was provided on create.
func (u *UserRoleUpsertOne) UpdateName() *UserRoleUpsertOne {
	return u.Update(func(s *UserRoleUpsert) {
		s.UpdateName()
	})
}

// SetDisplayName sets the "displayName" field.
func (u *UserRoleUpsertOne) SetDisplayName(v string) *UserRoleUpsertOne {
	return u.Update(func(s *UserRoleUpsert) {
		s.SetDisplayName(v)
	})
}

// UpdateDisplayName sets the "displayName" field to the value that was provided on create.
func (u *UserRoleUpsertOne) UpdateDisplayName() *UserRoleUpsertOne {
	return u.Update(func(s *UserRoleUpsert) {
		s.UpdateDisplayName()
	})
}

// Exec executes the query.
func (u *UserRoleUpsertOne) Exec(ctx context.Context) error {
	if len(u.create.conflict) == 0 {
		return errors.New("ent: missing options for UserRoleCreate.OnConflict")
	}
	return u.create.Exec(ctx)
}

// ExecX is like Exec, but panics if an error occurs.
func (u *UserRoleUpsertOne) ExecX(ctx context.Context) {
	if err := u.create.Exec(ctx); err != nil {
		panic(err)
	}
}

// Exec executes the UPSERT query and returns the inserted/updated ID.
func (u *UserRoleUpsertOne) ID(ctx context.Context) (id pulid.ID, err error) {
	if u.create.driver.Dialect() == dialect.MySQL {
		// In case of "ON CONFLICT", there is no way to get back non-numeric ID
		// fields from the database since MySQL does not support the RETURNING clause.
		return id, errors.New("ent: UserRoleUpsertOne.ID is not supported by MySQL driver. Use UserRoleUpsertOne.Exec instead")
	}
	node, err := u.create.Save(ctx)
	if err != nil {
		return id, err
	}
	return node.ID, nil
}

// IDX is like ID, but panics if an error occurs.
func (u *UserRoleUpsertOne) IDX(ctx context.Context) pulid.ID {
	id, err := u.ID(ctx)
	if err != nil {
		panic(err)
	}
	return id
}

// UserRoleCreateBulk is the builder for creating many UserRole entities in bulk.
type UserRoleCreateBulk struct {
	config
	builders []*UserRoleCreate
	conflict []sql.ConflictOption
}

// Save creates the UserRole entities in the database.
func (urcb *UserRoleCreateBulk) Save(ctx context.Context) ([]*UserRole, error) {
	specs := make([]*sqlgraph.CreateSpec, len(urcb.builders))
	nodes := make([]*UserRole, len(urcb.builders))
	mutators := make([]Mutator, len(urcb.builders))
	for i := range urcb.builders {
		func(i int, root context.Context) {
			builder := urcb.builders[i]
			builder.defaults()
			var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
				mutation, ok := m.(*UserRoleMutation)
				if !ok {
					return nil, fmt.Errorf("unexpected mutation type %T", m)
				}
				if err := builder.check(); err != nil {
					return nil, err
				}
				builder.mutation = mutation
				nodes[i], specs[i] = builder.createSpec()
				var err error
				if i < len(mutators)-1 {
					_, err = mutators[i+1].Mutate(root, urcb.builders[i+1].mutation)
				} else {
					spec := &sqlgraph.BatchCreateSpec{Nodes: specs}
					spec.OnConflict = urcb.conflict
					// Invoke the actual operation on the latest mutation in the chain.
					if err = sqlgraph.BatchCreate(ctx, urcb.driver, spec); err != nil {
						if sqlgraph.IsConstraintError(err) {
							err = &ConstraintError{err.Error(), err}
						}
					}
				}
				if err != nil {
					return nil, err
				}
				mutation.id = &nodes[i].ID
				mutation.done = true
				return nodes[i], nil
			})
			for i := len(builder.hooks) - 1; i >= 0; i-- {
				mut = builder.hooks[i](mut)
			}
			mutators[i] = mut
		}(i, ctx)
	}
	if len(mutators) > 0 {
		if _, err := mutators[0].Mutate(ctx, urcb.builders[0].mutation); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

// SaveX is like Save, but panics if an error occurs.
func (urcb *UserRoleCreateBulk) SaveX(ctx context.Context) []*UserRole {
	v, err := urcb.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (urcb *UserRoleCreateBulk) Exec(ctx context.Context) error {
	_, err := urcb.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (urcb *UserRoleCreateBulk) ExecX(ctx context.Context) {
	if err := urcb.Exec(ctx); err != nil {
		panic(err)
	}
}

// OnConflict allows configuring the `ON CONFLICT` / `ON DUPLICATE KEY` clause
// of the `INSERT` statement. For example:
//
//	client.UserRole.CreateBulk(builders...).
//		OnConflict(
//			// Update the row with the new values
//			// the was proposed for insertion.
//			sql.ResolveWithNewValues(),
//		).
//		// Override some of the fields with custom
//		// update values.
//		Update(func(u *ent.UserRoleUpsert) {
//			SetName(v+v).
//		}).
//		Exec(ctx)
//
func (urcb *UserRoleCreateBulk) OnConflict(opts ...sql.ConflictOption) *UserRoleUpsertBulk {
	urcb.conflict = opts
	return &UserRoleUpsertBulk{
		create: urcb,
	}
}

// OnConflictColumns calls `OnConflict` and configures the columns
// as conflict target. Using this option is equivalent to using:
//
//	client.UserRole.Create().
//		OnConflict(sql.ConflictColumns(columns...)).
//		Exec(ctx)
//
func (urcb *UserRoleCreateBulk) OnConflictColumns(columns ...string) *UserRoleUpsertBulk {
	urcb.conflict = append(urcb.conflict, sql.ConflictColumns(columns...))
	return &UserRoleUpsertBulk{
		create: urcb,
	}
}

// UserRoleUpsertBulk is the builder for "upsert"-ing
// a bulk of UserRole nodes.
type UserRoleUpsertBulk struct {
	create *UserRoleCreateBulk
}

// UpdateNewValues updates the mutable fields using the new values that
// were set on create. Using this option is equivalent to using:
//
//	client.UserRole.Create().
//		OnConflict(
//			sql.ResolveWithNewValues(),
//			sql.ResolveWith(func(u *sql.UpdateSet) {
//				u.SetIgnore(userrole.FieldID)
//			}),
//		).
//		Exec(ctx)
//
func (u *UserRoleUpsertBulk) UpdateNewValues() *UserRoleUpsertBulk {
	u.create.conflict = append(u.create.conflict, sql.ResolveWithNewValues())
	u.create.conflict = append(u.create.conflict, sql.ResolveWith(func(s *sql.UpdateSet) {
		for _, b := range u.create.builders {
			if _, exists := b.mutation.ID(); exists {
				s.SetIgnore(userrole.FieldID)
				return
			}
		}
	}))
	return u
}

// Ignore sets each column to itself in case of conflict.
// Using this option is equivalent to using:
//
//	client.UserRole.Create().
//		OnConflict(sql.ResolveWithIgnore()).
//		Exec(ctx)
//
func (u *UserRoleUpsertBulk) Ignore() *UserRoleUpsertBulk {
	u.create.conflict = append(u.create.conflict, sql.ResolveWithIgnore())
	return u
}

// DoNothing configures the conflict_action to `DO NOTHING`.
// Supported only by SQLite and PostgreSQL.
func (u *UserRoleUpsertBulk) DoNothing() *UserRoleUpsertBulk {
	u.create.conflict = append(u.create.conflict, sql.DoNothing())
	return u
}

// Update allows overriding fields `UPDATE` values. See the UserRoleCreateBulk.OnConflict
// documentation for more info.
func (u *UserRoleUpsertBulk) Update(set func(*UserRoleUpsert)) *UserRoleUpsertBulk {
	u.create.conflict = append(u.create.conflict, sql.ResolveWith(func(update *sql.UpdateSet) {
		set(&UserRoleUpsert{UpdateSet: update})
	}))
	return u
}

// SetName sets the "name" field.
func (u *UserRoleUpsertBulk) SetName(v string) *UserRoleUpsertBulk {
	return u.Update(func(s *UserRoleUpsert) {
		s.SetName(v)
	})
}

// UpdateName sets the "name" field to the value that was provided on create.
func (u *UserRoleUpsertBulk) UpdateName() *UserRoleUpsertBulk {
	return u.Update(func(s *UserRoleUpsert) {
		s.UpdateName()
	})
}

// SetDisplayName sets the "displayName" field.
func (u *UserRoleUpsertBulk) SetDisplayName(v string) *UserRoleUpsertBulk {
	return u.Update(func(s *UserRoleUpsert) {
		s.SetDisplayName(v)
	})
}

// UpdateDisplayName sets the "displayName" field to the value that was provided on create.
func (u *UserRoleUpsertBulk) UpdateDisplayName() *UserRoleUpsertBulk {
	return u.Update(func(s *UserRoleUpsert) {
		s.UpdateDisplayName()
	})
}

// Exec executes the query.
func (u *UserRoleUpsertBulk) Exec(ctx context.Context) error {
	for i, b := range u.create.builders {
		if len(b.conflict) != 0 {
			return fmt.Errorf("ent: OnConflict was set for builder %d. Set it on the UserRoleCreateBulk instead", i)
		}
	}
	if len(u.create.conflict) == 0 {
		return errors.New("ent: missing options for UserRoleCreateBulk.OnConflict")
	}
	return u.create.Exec(ctx)
}

// ExecX is like Exec, but panics if an error occurs.
func (u *UserRoleUpsertBulk) ExecX(ctx context.Context) {
	if err := u.create.Exec(ctx); err != nil {
		panic(err)
	}
}
