package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

// GlobalPermission holds the schema definition for the GlobalPermission entity.
type GlobalPermission struct {
	ent.Schema
}

// Mixins of the GlobalPermission.
func (GlobalPermission) Mixin() []ent.Mixin {
	return []ent.Mixin{
		pulid.MixinWithPrefix("GP"),
	}
}

// Fields of the GlobalPermission.
func (GlobalPermission) Fields() []ent.Field {
	return []ent.Field{
		field.String("subject_id").GoType(pulid.ID("")),
		field.String("resource").GoType(auth.ResourceType("")),
		field.JSON("collection_operations", []auth.Operation{}),
		field.JSON("item_operations", []auth.Operation{}),
	}
}

// Edges of the GlobalPermission.
func (GlobalPermission) Edges() []ent.Edge {
	return nil
}
