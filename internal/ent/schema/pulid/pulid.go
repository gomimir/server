// Package pulid implements the pulid type.
// A pulid is an identifier that is a two-byte prefixed ULIDs, with the first two bytes encoding the type of the entity.
// Inspired by: https://github.com/ent/contrib/blob/master/entgql/internal/todopulid/ent/schema/pulid/pulid.go
package pulid

import (
	"database/sql/driver"
	"fmt"
	"io"
	"strconv"
)

// ID implements a PULID - a prefixed ULID.
type ID string

// UnmarshalGQL implements the graphql.Unmarshaler interface
func (u *ID) UnmarshalGQL(v interface{}) error {
	return u.Scan(v)
}

// MarshalGQL implements the graphql.Marshaler interface
func (u ID) MarshalGQL(w io.Writer) {
	_, _ = io.WriteString(w, strconv.Quote(string(u)))
}

// Scan implements the Scanner interface.
func (u *ID) Scan(src interface{}) error {
	if src == nil {
		return fmt.Errorf("pulid: expected a value")
	}
	s, ok := src.(string)
	if !ok {
		return fmt.Errorf("pulid: expected a string")
	}
	*u = ID(s)
	return nil
}

// Value implements the driver Valuer interface.
func (u ID) Value() (driver.Value, error) {
	return string(u), nil
}
