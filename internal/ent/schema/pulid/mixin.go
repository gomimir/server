// Inspired by: https://github.com/ent/contrib/blob/master/entgql/internal/todopulid/ent/schema/pulid/mixin.go

package pulid

import (
	"context"
	"fmt"

	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/mixin"
	"gitlab.com/gomimir/server/internal/idprovider"
)

// MixinWithPrefix creates a Mixin that encodes the provided prefix.
func MixinWithPrefix(prefix string) *Mixin {
	return &Mixin{prefix: prefix}
}

// Mixin defines an ent Mixin that captures the PULID prefix for a type.
type Mixin struct {
	mixin.Schema
	prefix string
}

// Fields provides the id field.
func (m Mixin) Fields() []ent.Field {
	return []ent.Field{
		field.String("id").GoType(ID("")),
	}
}

func (m Mixin) Hooks() []ent.Hook {
	type IDSetter interface {
		ID() (ID, bool)
		SetID(id ID)
	}

	return []ent.Hook{
		func(next ent.Mutator) ent.Mutator {
			return ent.MutateFunc(func(ctx context.Context, mutation ent.Mutation) (ent.Value, error) {
				if mutation.Op().Is(ent.OpCreate) {
					if setter, ok := mutation.(IDSetter); ok {
						if _, exists := setter.ID(); !exists {
							idProvider, err := idprovider.ProviderForContext(ctx)
							if err != nil {
								return nil, err
							}

							setter.SetID(ID(idProvider.NextIDFor(m.prefix)))
						}
					} else {
						return nil, fmt.Errorf("unable to set id, unexpected mutation interface")
					}
				}

				v, err := next.Mutate(ctx, mutation)
				return v, err
			})
		},
	}
}
