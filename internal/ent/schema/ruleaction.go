package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/ent/schema/schematype"
)

// RuleAction holds the schema definition for the RuleAction entity.
type RuleAction struct {
	ent.Schema
}

// Mixins of the RuleAction.
func (RuleAction) Mixin() []ent.Mixin {
	return []ent.Mixin{
		pulid.MixinWithPrefix("RA"),
	}
}

// Fields of the RuleAction.
func (RuleAction) Fields() []ent.Field {
	return []ent.Field{
		field.String("rule_id").GoType(pulid.ID("")),
		field.String("description").Default(""),
		field.Bool("enabled").Default(true),
		field.Other("config", &schematype.ActionConfig{}).
			SchemaType(map[string]string{
				dialect.SQLite: "json",
			}).
			Optional(),
	}
}

// Edges of the RuleAction.
func (RuleAction) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("rule", Rule.Type).Ref("actions").Field("rule_id").Required().Unique(),
	}
}
