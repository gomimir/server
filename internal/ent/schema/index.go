package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

// Index holds the schema definition for the Index entity.
type Index struct {
	ent.Schema
}

// Annotations of the Index.
func (Index) Annotations() []schema.Annotation {
	return []schema.Annotation{
		entsql.Annotation{Table: "indices"},
	}
}

// Mixins of the Index.
func (Index) Mixin() []ent.Mixin {
	return []ent.Mixin{
		pulid.MixinWithPrefix("IX"),
	}
}

// Fields of the Index.
func (Index) Fields() []ent.Field {
	return []ent.Field{
		field.String("name").NotEmpty(),
		field.Enum("assetType").NamedValues(
			"Document", "DOCUMENT",
			"Photo", "PHOTO",
			"Unknown", "UNKNOWN",
		).Default("UNKNOWN"),
	}
}

// Edges of the Index.
func (Index) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("rules", Rule.Type).Annotations(entsql.Annotation{
			OnDelete: entsql.Cascade,
		}),
		edge.To("file_locations", FileLocation.Type).Annotations(entsql.Annotation{
			OnDelete: entsql.Cascade,
		}),
		edge.To("tag_kinds", TagKind.Type).Annotations(entsql.Annotation{
			OnDelete: entsql.Cascade,
		}),
		edge.To("permissions", IndexPermission.Type).Annotations(entsql.Annotation{
			OnDelete: entsql.Cascade,
		}),
	}
}

// Indexes of the Index.
func (Index) Indexes() []ent.Index {
	return nil
}
