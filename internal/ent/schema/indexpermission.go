package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

// IndexPermission holds the schema definition for the IndexPermission entity.
type IndexPermission struct {
	ent.Schema
}

// Mixins of the IndexPermission.
func (IndexPermission) Mixin() []ent.Mixin {
	return []ent.Mixin{
		pulid.MixinWithPrefix("IP"),
	}
}

// Fields of the IndexPermission.
func (IndexPermission) Fields() []ent.Field {
	return []ent.Field{
		field.String("index_id").GoType(pulid.ID("")),
		field.String("subject_id").GoType(pulid.ID("")),
		field.JSON("index_operations", []auth.Operation{}).Default([]auth.Operation{}),
		field.JSON("asset_operations", []auth.Operation{}).Default([]auth.Operation{}),
		field.JSON("asset_collection_operations", []auth.Operation{}).Default([]auth.Operation{}),
	}
}

// Edges of the IndexPermission.
func (IndexPermission) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("index", Index.Type).Ref("permissions").Field("index_id").Required().Unique(),
	}
}
