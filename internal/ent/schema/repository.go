package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect"
	"entgo.io/ent/schema/field"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/ent/schema/schematype"
)

// Repository holds the schema definition for the Repository entity.
type Repository struct {
	ent.Schema
}

// Mixins of the Repository.
func (Repository) Mixin() []ent.Mixin {
	return []ent.Mixin{
		pulid.MixinWithPrefix("RE"),
	}
}

// Fields of the Repository.
func (Repository) Fields() []ent.Field {
	return []ent.Field{
		field.String("name").NotEmpty(),
		field.Other("config", &schematype.RepositoryConfig{}).
			SchemaType(map[string]string{
				dialect.SQLite: "json",
			}).
			Optional(),
	}
}

// Edges of the Repository.
func (Repository) Edges() []ent.Edge {
	return nil
}
