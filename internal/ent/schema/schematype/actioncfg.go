package schematype

import (
	"database/sql/driver"
	"encoding/json"
)

// ActionConfig implements the field.ValueScanner interface.
type ActionConfig struct {
	Ignore    *bool                  `json:"ignore,omitempty"`
	SetKey    *string                `json:"setKey,omitempty"`
	SetParams map[string]interface{} `json:"setParams,omitempty"`
	AddTag    *Tag                   `json:"addTag,omitempty"`
	Process   *ProcessActionConfig   `json:"process,omitempty"`
}

// Note: we intentionally treat empty SetParams dictionary as non-empty config (it holds information about type of the action)
// Same is true for ProcessActionConfig
func (t ActionConfig) IsEmpty() bool {
	return t.Ignore == nil &&
		t.SetKey == nil &&
		t.SetParams == nil &&
		t.AddTag == nil &&
		t.Process == nil
}

func (t *ActionConfig) Scan(v interface{}) (err error) {
	switch v := v.(type) {
	case string:
		err = json.Unmarshal([]byte(v), t)
	case []byte:
		err = json.Unmarshal(v, t)
	}
	return
}

func (t ActionConfig) Value() (driver.Value, error) {
	return json.Marshal(t)
}

type ProcessActionConfig struct {
	Request    string                 `json:"request,omitempty"`
	Queue      *string                `json:"queue,omitempty"`
	Parameters map[string]interface{} `json:"params,omitempty"`
}
