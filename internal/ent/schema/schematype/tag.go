package schematype

type Tag struct {
	Name string `json:"name"`
	Kind string `json:"kind"`
}
