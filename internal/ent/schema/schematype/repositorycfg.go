package schematype

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
)

type RepositoryConfig struct {
	S3    *S3RepositoryConfig
	Local *LocalRepositoryConfig
}

func (t RepositoryConfig) Clone() *RepositoryConfig {
	copy := &RepositoryConfig{}

	if t.S3 != nil {
		copy.S3 = t.S3.Clone()
	}

	if t.Local != nil {
		copy.Local = t.Local.Clone()
	}

	return copy
}

func (t *RepositoryConfig) Scan(v interface{}) (err error) {
	switch v := v.(type) {
	case string:
		err = json.Unmarshal([]byte(v), t)
	case []byte:
		err = json.Unmarshal(v, t)
	}
	return
}

func (t RepositoryConfig) Value() (driver.Value, error) {
	if t.S3 != nil && t.Local != nil {
		return nil, fmt.Errorf("local and s3 repository configuration options are mutually exclusive")
	}

	return json.Marshal(t)
}

type S3RepositoryConfig struct {
	Endpoint  string `json:"endpoint"`
	UseSSL    bool   `json:"useSsl"`
	CACert    string `json:"caCert,omitempty"`
	AccessKey string `json:"accessKey,omitempty"`
	SecretKey string `json:"secretKey,omitempty"`
}

func (cfg S3RepositoryConfig) Clone() *S3RepositoryConfig {
	return &cfg
}

type LocalRepositoryConfig struct {
	Path string `json:"path"`
}

func (cfg LocalRepositoryConfig) Clone() *LocalRepositoryConfig {
	return &cfg
}
