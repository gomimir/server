package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

// UserRole holds the schema definition for the UserRole entity.
type UserRole struct {
	ent.Schema
}

// Mixins of the UserRole.
func (UserRole) Mixin() []ent.Mixin {
	return []ent.Mixin{
		pulid.MixinWithPrefix("UR"),
	}
}

// Fields of the UserRole.
func (UserRole) Fields() []ent.Field {
	return []ent.Field{
		field.String("name").NotEmpty(),
		field.String("displayName").Default(""),
	}
}

// Edges of the UserRole.
func (UserRole) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("users", User.Type).Ref("roles").Annotations(entsql.Annotation{
			OnDelete: entsql.Cascade,
		}),
	}
}

// Indexes of the UserRole.
func (UserRole) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("name").Unique(),
	}
}
