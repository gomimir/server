package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

// TagKind holds the schema definition for the TagKind entity.
type TagKind struct {
	ent.Schema
}

// Mixins of the TagKind.
func (TagKind) Mixin() []ent.Mixin {
	return []ent.Mixin{
		pulid.MixinWithPrefix("TK"),
	}
}

// Fields of the TagKind.
func (TagKind) Fields() []ent.Field {
	return []ent.Field{
		field.String("index_id").GoType(pulid.ID("")),
		field.String("name").NotEmpty(),
		field.String("displayName").Optional(),
		field.String("displayNamePlural").Optional(),
		field.Bool("userAssignable").Default(true),
	}
}

// Edges of the TagKind.
func (TagKind) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("index", Index.Type).Ref("tag_kinds").Field("index_id").Required().Unique(),
	}
}
