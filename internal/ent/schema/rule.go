package schema

import (
	"regexp"

	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

// Rule holds the schema definition for the Rule entity.
type Rule struct {
	ent.Schema
}

// Mixins of the Rule.
func (Rule) Mixin() []ent.Mixin {
	return []ent.Mixin{
		pulid.MixinWithPrefix("IR"),
	}
}

// Fields of the Rule.
func (Rule) Fields() []ent.Field {
	return []ent.Field{
		field.String("index_id").GoType(pulid.ID("")),
		field.String("description").Default(""),
		field.String("match").Default("").Validate(func(s string) error {
			_, err := regexp.Compile(s)
			return err
		}),
		field.Bool("enabled").Default(true),
		field.Bool("inverse").Default(false),
		field.Int("order"),
	}
}

// Edges of the Rule.
func (Rule) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("actions", RuleAction.Type).Annotations(entsql.Annotation{
			OnDelete: entsql.Cascade,
		}),
		edge.From("index", Index.Type).Ref("rules").Field("index_id").Required().Unique(),
	}
}
