package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

// FileLocation holds the schema definition for the FileLocation entity.
type FileLocation struct {
	ent.Schema
}

// Mixins of the FileLocation.
func (FileLocation) Mixin() []ent.Mixin {
	return []ent.Mixin{
		pulid.MixinWithPrefix("FL"),
	}
}

// Fields of the FileLocation.
func (FileLocation) Fields() []ent.Field {
	return []ent.Field{
		field.String("index_id").GoType(pulid.ID("")),
		field.Enum("type").NamedValues(
			"Crawl", "CRAWL",
			"Upload", "UPLOAD",
			"Sidecar", "SIDECAR",
			"Temporary", "TEMP",
		),
		field.String("repository_id").GoType(pulid.ID("")),
		field.String("prefix").NotEmpty(),
	}
}

// Edges of the FileLocation.
func (FileLocation) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("repository", Repository.Type).Annotations(entsql.Annotation{
			OnDelete: entsql.Cascade,
		}).Field("repository_id").Required().Unique(),
		edge.From("index", Index.Type).Ref("file_locations").Field("index_id").Required().Unique(),
	}
}
