//go:build ignore
// +build ignore

package main

import (
	"log"

	"entgo.io/contrib/entgql"
	"entgo.io/ent/entc"
	"entgo.io/ent/entc/gen"
)

func main() {
	ex, err := entgql.NewExtension(
	// entgql.WithSchemaPath("../graphql/ent.graphqls"),
	// entgql.WithConfigPath("../gqlgen.yml"),
	)

	if err != nil {
		log.Fatalf("creating entgql extension: %v", err)
	}

	cfg := &gen.Config{
		Features: []gen.Feature{
			gen.FeatureUpsert,
		},
	}

	if err := entc.Generate("./schema", cfg, entc.Extensions(ex)); err != nil {
		log.Fatal("running ent codegen:", err)
	}
}
