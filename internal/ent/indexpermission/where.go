// Code generated by entc, DO NOT EDIT.

package indexpermission

import (
	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"gitlab.com/gomimir/server/internal/ent/predicate"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

// ID filters vertices based on their ID field.
func ID(id pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldID), id))
	})
}

// IDEQ applies the EQ predicate on the ID field.
func IDEQ(id pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldID), id))
	})
}

// IDNEQ applies the NEQ predicate on the ID field.
func IDNEQ(id pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldID), id))
	})
}

// IDIn applies the In predicate on the ID field.
func IDIn(ids ...pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(ids) == 0 {
			s.Where(sql.False())
			return
		}
		v := make([]interface{}, len(ids))
		for i := range v {
			v[i] = ids[i]
		}
		s.Where(sql.In(s.C(FieldID), v...))
	})
}

// IDNotIn applies the NotIn predicate on the ID field.
func IDNotIn(ids ...pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(ids) == 0 {
			s.Where(sql.False())
			return
		}
		v := make([]interface{}, len(ids))
		for i := range v {
			v[i] = ids[i]
		}
		s.Where(sql.NotIn(s.C(FieldID), v...))
	})
}

// IDGT applies the GT predicate on the ID field.
func IDGT(id pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldID), id))
	})
}

// IDGTE applies the GTE predicate on the ID field.
func IDGTE(id pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldID), id))
	})
}

// IDLT applies the LT predicate on the ID field.
func IDLT(id pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldID), id))
	})
}

// IDLTE applies the LTE predicate on the ID field.
func IDLTE(id pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldID), id))
	})
}

// IndexID applies equality check predicate on the "index_id" field. It's identical to IndexIDEQ.
func IndexID(v pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldIndexID), v))
	})
}

// SubjectID applies equality check predicate on the "subject_id" field. It's identical to SubjectIDEQ.
func SubjectID(v pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldSubjectID), v))
	})
}

// IndexIDEQ applies the EQ predicate on the "index_id" field.
func IndexIDEQ(v pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldIndexID), v))
	})
}

// IndexIDNEQ applies the NEQ predicate on the "index_id" field.
func IndexIDNEQ(v pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldIndexID), v))
	})
}

// IndexIDIn applies the In predicate on the "index_id" field.
func IndexIDIn(vs ...pulid.ID) predicate.IndexPermission {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.IndexPermission(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.In(s.C(FieldIndexID), v...))
	})
}

// IndexIDNotIn applies the NotIn predicate on the "index_id" field.
func IndexIDNotIn(vs ...pulid.ID) predicate.IndexPermission {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.IndexPermission(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.NotIn(s.C(FieldIndexID), v...))
	})
}

// IndexIDGT applies the GT predicate on the "index_id" field.
func IndexIDGT(v pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldIndexID), v))
	})
}

// IndexIDGTE applies the GTE predicate on the "index_id" field.
func IndexIDGTE(v pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldIndexID), v))
	})
}

// IndexIDLT applies the LT predicate on the "index_id" field.
func IndexIDLT(v pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldIndexID), v))
	})
}

// IndexIDLTE applies the LTE predicate on the "index_id" field.
func IndexIDLTE(v pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldIndexID), v))
	})
}

// IndexIDContains applies the Contains predicate on the "index_id" field.
func IndexIDContains(v pulid.ID) predicate.IndexPermission {
	vc := string(v)
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldIndexID), vc))
	})
}

// IndexIDHasPrefix applies the HasPrefix predicate on the "index_id" field.
func IndexIDHasPrefix(v pulid.ID) predicate.IndexPermission {
	vc := string(v)
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldIndexID), vc))
	})
}

// IndexIDHasSuffix applies the HasSuffix predicate on the "index_id" field.
func IndexIDHasSuffix(v pulid.ID) predicate.IndexPermission {
	vc := string(v)
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldIndexID), vc))
	})
}

// IndexIDEqualFold applies the EqualFold predicate on the "index_id" field.
func IndexIDEqualFold(v pulid.ID) predicate.IndexPermission {
	vc := string(v)
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldIndexID), vc))
	})
}

// IndexIDContainsFold applies the ContainsFold predicate on the "index_id" field.
func IndexIDContainsFold(v pulid.ID) predicate.IndexPermission {
	vc := string(v)
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldIndexID), vc))
	})
}

// SubjectIDEQ applies the EQ predicate on the "subject_id" field.
func SubjectIDEQ(v pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldSubjectID), v))
	})
}

// SubjectIDNEQ applies the NEQ predicate on the "subject_id" field.
func SubjectIDNEQ(v pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldSubjectID), v))
	})
}

// SubjectIDIn applies the In predicate on the "subject_id" field.
func SubjectIDIn(vs ...pulid.ID) predicate.IndexPermission {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.IndexPermission(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.In(s.C(FieldSubjectID), v...))
	})
}

// SubjectIDNotIn applies the NotIn predicate on the "subject_id" field.
func SubjectIDNotIn(vs ...pulid.ID) predicate.IndexPermission {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.IndexPermission(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.NotIn(s.C(FieldSubjectID), v...))
	})
}

// SubjectIDGT applies the GT predicate on the "subject_id" field.
func SubjectIDGT(v pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldSubjectID), v))
	})
}

// SubjectIDGTE applies the GTE predicate on the "subject_id" field.
func SubjectIDGTE(v pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldSubjectID), v))
	})
}

// SubjectIDLT applies the LT predicate on the "subject_id" field.
func SubjectIDLT(v pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldSubjectID), v))
	})
}

// SubjectIDLTE applies the LTE predicate on the "subject_id" field.
func SubjectIDLTE(v pulid.ID) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldSubjectID), v))
	})
}

// SubjectIDContains applies the Contains predicate on the "subject_id" field.
func SubjectIDContains(v pulid.ID) predicate.IndexPermission {
	vc := string(v)
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldSubjectID), vc))
	})
}

// SubjectIDHasPrefix applies the HasPrefix predicate on the "subject_id" field.
func SubjectIDHasPrefix(v pulid.ID) predicate.IndexPermission {
	vc := string(v)
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldSubjectID), vc))
	})
}

// SubjectIDHasSuffix applies the HasSuffix predicate on the "subject_id" field.
func SubjectIDHasSuffix(v pulid.ID) predicate.IndexPermission {
	vc := string(v)
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldSubjectID), vc))
	})
}

// SubjectIDEqualFold applies the EqualFold predicate on the "subject_id" field.
func SubjectIDEqualFold(v pulid.ID) predicate.IndexPermission {
	vc := string(v)
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldSubjectID), vc))
	})
}

// SubjectIDContainsFold applies the ContainsFold predicate on the "subject_id" field.
func SubjectIDContainsFold(v pulid.ID) predicate.IndexPermission {
	vc := string(v)
	return predicate.IndexPermission(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldSubjectID), vc))
	})
}

// HasIndex applies the HasEdge predicate on the "index" edge.
func HasIndex() predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		step := sqlgraph.NewStep(
			sqlgraph.From(Table, FieldID),
			sqlgraph.To(IndexTable, FieldID),
			sqlgraph.Edge(sqlgraph.M2O, true, IndexTable, IndexColumn),
		)
		sqlgraph.HasNeighbors(s, step)
	})
}

// HasIndexWith applies the HasEdge predicate on the "index" edge with a given conditions (other predicates).
func HasIndexWith(preds ...predicate.Index) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		step := sqlgraph.NewStep(
			sqlgraph.From(Table, FieldID),
			sqlgraph.To(IndexInverseTable, FieldID),
			sqlgraph.Edge(sqlgraph.M2O, true, IndexTable, IndexColumn),
		)
		sqlgraph.HasNeighborsWith(s, step, func(s *sql.Selector) {
			for _, p := range preds {
				p(s)
			}
		})
	})
}

// And groups predicates with the AND operator between them.
func And(predicates ...predicate.IndexPermission) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s1 := s.Clone().SetP(nil)
		for _, p := range predicates {
			p(s1)
		}
		s.Where(s1.P())
	})
}

// Or groups predicates with the OR operator between them.
func Or(predicates ...predicate.IndexPermission) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		s1 := s.Clone().SetP(nil)
		for i, p := range predicates {
			if i > 0 {
				s1.Or()
			}
			p(s1)
		}
		s.Where(s1.P())
	})
}

// Not applies the not operator on the given predicate.
func Not(p predicate.IndexPermission) predicate.IndexPermission {
	return predicate.IndexPermission(func(s *sql.Selector) {
		p(s.Not())
	})
}
