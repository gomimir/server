// Code generated by entc, DO NOT EDIT.

package ent

import (
	"fmt"
	"strings"

	"entgo.io/ent/dialect/sql"
	"gitlab.com/gomimir/server/internal/ent/index"
	"gitlab.com/gomimir/server/internal/ent/rule"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

// Rule is the model entity for the Rule schema.
type Rule struct {
	config `json:"-"`
	// ID of the ent.
	ID pulid.ID `json:"id,omitempty"`
	// IndexID holds the value of the "index_id" field.
	IndexID pulid.ID `json:"index_id,omitempty"`
	// Description holds the value of the "description" field.
	Description string `json:"description,omitempty"`
	// Match holds the value of the "match" field.
	Match string `json:"match,omitempty"`
	// Enabled holds the value of the "enabled" field.
	Enabled bool `json:"enabled,omitempty"`
	// Inverse holds the value of the "inverse" field.
	Inverse bool `json:"inverse,omitempty"`
	// Order holds the value of the "order" field.
	Order int `json:"order,omitempty"`
	// Edges holds the relations/edges for other nodes in the graph.
	// The values are being populated by the RuleQuery when eager-loading is set.
	Edges RuleEdges `json:"edges"`
}

// RuleEdges holds the relations/edges for other nodes in the graph.
type RuleEdges struct {
	// Actions holds the value of the actions edge.
	Actions []*RuleAction `json:"actions,omitempty"`
	// Index holds the value of the index edge.
	Index *Index `json:"index,omitempty"`
	// loadedTypes holds the information for reporting if a
	// type was loaded (or requested) in eager-loading or not.
	loadedTypes [2]bool
}

// ActionsOrErr returns the Actions value or an error if the edge
// was not loaded in eager-loading.
func (e RuleEdges) ActionsOrErr() ([]*RuleAction, error) {
	if e.loadedTypes[0] {
		return e.Actions, nil
	}
	return nil, &NotLoadedError{edge: "actions"}
}

// IndexOrErr returns the Index value or an error if the edge
// was not loaded in eager-loading, or loaded but was not found.
func (e RuleEdges) IndexOrErr() (*Index, error) {
	if e.loadedTypes[1] {
		if e.Index == nil {
			// The edge index was loaded in eager-loading,
			// but was not found.
			return nil, &NotFoundError{label: index.Label}
		}
		return e.Index, nil
	}
	return nil, &NotLoadedError{edge: "index"}
}

// scanValues returns the types for scanning values from sql.Rows.
func (*Rule) scanValues(columns []string) ([]interface{}, error) {
	values := make([]interface{}, len(columns))
	for i := range columns {
		switch columns[i] {
		case rule.FieldID, rule.FieldIndexID:
			values[i] = new(pulid.ID)
		case rule.FieldEnabled, rule.FieldInverse:
			values[i] = new(sql.NullBool)
		case rule.FieldOrder:
			values[i] = new(sql.NullInt64)
		case rule.FieldDescription, rule.FieldMatch:
			values[i] = new(sql.NullString)
		default:
			return nil, fmt.Errorf("unexpected column %q for type Rule", columns[i])
		}
	}
	return values, nil
}

// assignValues assigns the values that were returned from sql.Rows (after scanning)
// to the Rule fields.
func (r *Rule) assignValues(columns []string, values []interface{}) error {
	if m, n := len(values), len(columns); m < n {
		return fmt.Errorf("mismatch number of scan values: %d != %d", m, n)
	}
	for i := range columns {
		switch columns[i] {
		case rule.FieldID:
			if value, ok := values[i].(*pulid.ID); !ok {
				return fmt.Errorf("unexpected type %T for field id", values[i])
			} else if value != nil {
				r.ID = *value
			}
		case rule.FieldIndexID:
			if value, ok := values[i].(*pulid.ID); !ok {
				return fmt.Errorf("unexpected type %T for field index_id", values[i])
			} else if value != nil {
				r.IndexID = *value
			}
		case rule.FieldDescription:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field description", values[i])
			} else if value.Valid {
				r.Description = value.String
			}
		case rule.FieldMatch:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field match", values[i])
			} else if value.Valid {
				r.Match = value.String
			}
		case rule.FieldEnabled:
			if value, ok := values[i].(*sql.NullBool); !ok {
				return fmt.Errorf("unexpected type %T for field enabled", values[i])
			} else if value.Valid {
				r.Enabled = value.Bool
			}
		case rule.FieldInverse:
			if value, ok := values[i].(*sql.NullBool); !ok {
				return fmt.Errorf("unexpected type %T for field inverse", values[i])
			} else if value.Valid {
				r.Inverse = value.Bool
			}
		case rule.FieldOrder:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for field order", values[i])
			} else if value.Valid {
				r.Order = int(value.Int64)
			}
		}
	}
	return nil
}

// QueryActions queries the "actions" edge of the Rule entity.
func (r *Rule) QueryActions() *RuleActionQuery {
	return (&RuleClient{config: r.config}).QueryActions(r)
}

// QueryIndex queries the "index" edge of the Rule entity.
func (r *Rule) QueryIndex() *IndexQuery {
	return (&RuleClient{config: r.config}).QueryIndex(r)
}

// Update returns a builder for updating this Rule.
// Note that you need to call Rule.Unwrap() before calling this method if this Rule
// was returned from a transaction, and the transaction was committed or rolled back.
func (r *Rule) Update() *RuleUpdateOne {
	return (&RuleClient{config: r.config}).UpdateOne(r)
}

// Unwrap unwraps the Rule entity that was returned from a transaction after it was closed,
// so that all future queries will be executed through the driver which created the transaction.
func (r *Rule) Unwrap() *Rule {
	tx, ok := r.config.driver.(*txDriver)
	if !ok {
		panic("ent: Rule is not a transactional entity")
	}
	r.config.driver = tx.drv
	return r
}

// String implements the fmt.Stringer.
func (r *Rule) String() string {
	var builder strings.Builder
	builder.WriteString("Rule(")
	builder.WriteString(fmt.Sprintf("id=%v", r.ID))
	builder.WriteString(", index_id=")
	builder.WriteString(fmt.Sprintf("%v", r.IndexID))
	builder.WriteString(", description=")
	builder.WriteString(r.Description)
	builder.WriteString(", match=")
	builder.WriteString(r.Match)
	builder.WriteString(", enabled=")
	builder.WriteString(fmt.Sprintf("%v", r.Enabled))
	builder.WriteString(", inverse=")
	builder.WriteString(fmt.Sprintf("%v", r.Inverse))
	builder.WriteString(", order=")
	builder.WriteString(fmt.Sprintf("%v", r.Order))
	builder.WriteByte(')')
	return builder.String()
}

// Rules is a parsable slice of Rule.
type Rules []*Rule

func (r Rules) config(cfg config) {
	for _i := range r {
		r[_i].config = cfg
	}
}
