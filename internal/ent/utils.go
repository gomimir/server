package ent

import (
	"github.com/rs/zerolog/log"
)

func RollbackHelper(tx *Tx, err error) error {
	log.Debug().Msg("Rolling back the transaction")
	if err := tx.Rollback(); err != nil {
		log.Warn().Err(err).Msg("Failed to rollback transaction")
	}

	return err
}
