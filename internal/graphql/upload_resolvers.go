package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"time"

	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/apperror"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/auth/permissions"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/filelocation"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/graphql/model"
	"gitlab.com/gomimir/server/internal/requests/archiveload"
	"gitlab.com/gomimir/server/internal/secret"
)

func (r *mutationResolver) Upload(ctx context.Context, files []*model.FileUpload, index pulid.ID, allFilesAreSingleAsset *bool, uploadMore *bool) (*model.UploadResult, error) {
	// Check permissions
	err := permissions.CheckAssetCollectionPermissions(ctx, index, auth.OP_Upload)
	if err != nil {
		return nil, err
	}

	uploadFileLocation, err := indexFileLocation(ctx, index, filelocation.TypeUpload)
	if err != nil {
		if ent.IsNotFound(err) {
			return nil, fmt.Errorf("no file location for uploaded files configured for index %v", index)
		} else {
			return nil, err
		}
	}

	now := time.Now()

	req := uploadReq{
		UploadRepo:   uploadFileLocation.RepositoryID,
		UploadPrefix: uploadPrefix(uploadFileLocation.Prefix, now, r.Rand),

		Index:                  index,
		AllFilesAreSingleAsset: allFilesAreSingleAsset,
	}

	indexReq, err := uploadAssetFiles(ctx, now, req, files)
	if err != nil {
		return nil, err
	}

	result := &model.UploadResult{
		Ok: true,
	}

	if indexReq != nil || (uploadMore != nil && *uploadMore) {
		opts := mimir.QueueJobOpts{
			Name: "processUploadedFiles",
			Enqueue: []mimir.EnqueueTaskOpts{
				{
					Queue:      "main",
					Task:       "index",
					Parameters: indexReq,
				},
			},
		}

		if uploadMore != nil && *uploadMore {
			opts.Enqueue = append(opts.Enqueue, mimir.EnqueueTaskOpts{
				Queue: "wait",
				Task:  "waitForAllFiles",
			})
		}

		queueRes, err := r.TaskManager.QueueJob(ctx, opts)
		if err != nil {
			return nil, err
		}

		jobID := queueRes.Job.ID()
		result.JobID = &jobID

		if uploadMore != nil && *uploadMore {
			req.JobID = queueRes.Job.ID()
			req.WaitTaskID = queueRes.Tasks[1].ID()

			secretKeeper, err := secret.KeeperForContext(ctx)
			if err != nil {
				return nil, err
			}

			session, err := secretKeeper.Encrypt(req)
			if err != nil {
				log.Error().Err(err).Msg("Unable to create upload session")
			} else {
				result.UploadSession = &session
			}
		}
	}

	return result, nil
}

func (r *mutationResolver) UploadMore(ctx context.Context, files []*model.FileUpload, session string, finished bool) (*model.MutationResult, error) {
	var req uploadReq

	secretKeeper, err := secret.KeeperForContext(ctx)
	if err != nil {
		return nil, err
	}

	err = secretKeeper.Decrypt(session, &req)
	if err != nil {
		log.Error().Err(err).Msg("Malformed session upload key")
		return nil, apperror.New(apperror.InvalidUploadSession, "malformed session key")
	}

	// Check permissions
	err = permissions.CheckAssetCollectionPermissions(ctx, pulid.ID(req.Index), auth.OP_Upload)
	if err != nil {
		return nil, err
	}

	indexReq, err := uploadAssetFiles(ctx, time.Now(), req, files)
	if err != nil {
		return nil, err
	}

	if indexReq != nil {
		_, err := r.TaskManager.QueueTask(ctx, mimir.QueueTaskOpts{
			JobID: req.JobID,
			Enqueue: []mimir.EnqueueTaskOpts{
				{
					Queue:      "main",
					Task:       "index",
					Parameters: indexReq,
				},
			},
		})

		if err != nil {
			return nil, err
		}
	}

	if finished {
		err := r.TaskManager.CancelTask(context.Background(), "wait", req.WaitTaskID)
		if err != nil {
			return nil, err
		}
	}

	return &model.MutationResult{
		Ok: true,
	}, nil
}

func (r *mutationResolver) UploadArchiveFromURL(ctx context.Context, url string, index pulid.ID) (*model.AsyncMutationResult, error) {
	// Check permissions
	err := permissions.CheckAssetCollectionPermissions(ctx, index, auth.OP_Upload)
	if err != nil {
		return nil, err
	}

	uploadFileLocation, err := indexFileLocation(ctx, index, filelocation.TypeUpload)
	if err != nil {
		return nil, err
	}

	queueRes, err := r.TaskManager.QueueJob(ctx, mimir.QueueJobOpts{
		Name: "uploadArchiveFromURL",
		Enqueue: []mimir.EnqueueTaskOpts{
			{
				Queue: "main",
				Task:  "uploadArchiveFromURL",
				Parameters: archiveload.Request{
					ArchiveURL:   url,
					RepositoryID: uploadFileLocation.RepositoryID,
					Prefix:       uploadPrefix(uploadFileLocation.Prefix, time.Now(), r.Rand),
					IndexID:      index,
				},
			},
		},
	})

	if err != nil {
		return nil, err
	}

	return &model.AsyncMutationResult{
		Ok:    true,
		JobID: queueRes.Job.ID(),
	}, nil
}
