type IndexStats {
  "Number of assets within the index"
  count: Int!
  files: AssetFileStats!
}

type AssetFileStats {
  "Number of files"
  count: Int!
  fileSize: NumericStats!
  sidecars: SidecarStats!
}

type SidecarStats {
  "Number of sidecar files"
  count: Int!
  fileSize: NumericStats!
}

type NumericStats {
  sum: Int!
}

enum AssetType {
  DOCUMENT
  PHOTO
  UNKNOWN
}

type Index {
  id: ID!
  name: String!
  assetType: AssetType!
  rules: [IndexRule!]!
  fileLocations: [FileLocation!]!
  tagKinds: [TagKind!]!
  allPermissions: [IndexPermissionsAssignment!]!
  permissions: IndexPermissions!
  stats: IndexStats!
}

input IndexInput {
  name: String
  assetType: AssetType
  rules: [IndexRuleInput!]
  fileLocations: [FileLocationInput!]
  tagKinds: [TagKindInput!]
  permissions: [IndexPermissionsAssignmentInput!]
}

input CrawlRequest {
  "Index ID"
  index: ID!
  "File sets to crawl. If omitted all file locations registered for the index will be crawled."
  fileSets: [FileSetInput!]
  """
  Ignore previous results of processors with given name to force them to run again.
  Use '*' to ignore all processor results.
  """
  reprocess: [String!]
}

type IndexMutationResult {
  ok: Boolean!
  index: Index
}

input FileSetInput {
  "ID of the repository where to look for files"
  repository: ID!

  """
  Optional prefix. All file paths needs to start with it in order to be matched.
  Use strings ending with / in order to match everything within a directory.
  """
  prefix: String
}

extend type Query {
  index(id: ID!): Index!
  indices: [Index!]!
}

extend type Mutation {
  indexCreate(index: IndexInput): IndexMutationResult!
  indexUpdate(id: ID!, index: IndexInput!): IndexMutationResult!
  indexDelete(id: ID!): MutationResult!

  "Crawl file repositories to index new files"
  crawl(reqs: [CrawlRequest!]!): AsyncMutationResult!
}
