package graphql_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/documents"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/testdata"
	"gitlab.com/gomimir/server/internal/graphql/gqlreq"
	"gitlab.com/gomimir/server/internal/graphql/gqltest"
)

func TestAssetDetail(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var index *ent.Index
	var bob *ent.User
	var alice *ent.User
	var admin *ent.User
	tester.InitData(
		testdata.UserBob().Ref(&bob),
		testdata.UserAlice().Ref(&alice),
		testdata.UserAdmin().Ref(&admin),
		testdata.IndexDocuments().Ref(&index),
	)

	assetRef := mimir.AssetRefForID(string(index.ID), "JvYrKPvp28zA3kj3Ah7HnxKHZRocCi7w2aApdA")

	tester.Indexer.EnsureAsset(assetRef, func(asset documents.IndexedAsset) documents.IndexedAsset {
		asset.Key = "some_key"
		return asset
	})

	// 2. Retrieving index detail
	payload, err := gqlreq.AssetDetail(assetRef.String())
	if !assert.NoError(t, err) {
		return
	}

	for _, user := range []*ent.User{bob, alice, admin} {
		t.Run(fmt.Sprintf("As%v", user.DisplayName), func(t *testing.T) {
			tester.RequestSnapshotT(t, payload, gqltest.AsUser(user))
		})
	}
}

func TestAssetListing(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var index *ent.Index
	var bob *ent.User
	var alice *ent.User
	var admin *ent.User
	tester.InitData(
		testdata.UserBob().Ref(&bob),
		testdata.UserAlice().Ref(&alice),
		testdata.UserAdmin().Ref(&admin),
		testdata.IndexDocuments().Ref(&index),
	)

	// 2. Retrieving index detail
	payload, err := gqlreq.AssetListing(gqlreq.AssetListingVars{Index: []string{string(index.ID)}})
	if !assert.NoError(t, err) {
		return
	}

	for _, user := range []*ent.User{bob, alice, admin} {
		t.Run(fmt.Sprintf("As%v", user.DisplayName), func(t *testing.T) {
			tester.RequestSnapshotT(t, payload, gqltest.AsUser(user))
		})
	}
}
