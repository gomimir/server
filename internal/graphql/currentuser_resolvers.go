package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/auth/permissions"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/graphql/generated"
	"gitlab.com/gomimir/server/internal/graphql/model"
)

func (r *currentUserResolver) User(ctx context.Context, obj *model.CurrentUser) (*ent.User, error) {
	entClient := ent.FromContext(ctx)
	session, err := auth.SessionForContext(ctx)
	if err != nil {
		return nil, err
	}

	if session.UserID() != "" {
		return entClient.User.Get(ctx, session.UserID())
	}

	return nil, nil
}

func (r *currentUserResolver) CollectionPermissions(ctx context.Context, obj *model.CurrentUser) ([]*model.ResourceOperations, error) {
	perms, err := permissions.GlobalPermissions(ctx)
	if err != nil {
		return nil, err
	}

	var resourcePerms []*model.ResourceOperations
	allPerms := perms.All()

	for _, resource := range auth.AllResourceTypes {
		// Note: not a top level resource
		if resource == auth.RT_Asset {
			continue
		}

		if allPerms[resource] != nil {
			resourcePerms = append(resourcePerms, &model.ResourceOperations{
				Resource:          resource,
				AllowedOperations: allPerms[resource].AllowedCollectionOperations.SortedArray(),
			})
		}
	}

	return resourcePerms, nil
}

func (r *queryResolver) CurrentUser(ctx context.Context) (*model.CurrentUser, error) {
	session, err := auth.SessionForContext(ctx)
	if err != nil {
		return nil, err
	}

	return &model.CurrentUser{
		Authenticated: session.IsAuthenticated(),
		WillLogoutAt:  session.ExpiresIn(),
	}, nil
}

// CurrentUser returns generated.CurrentUserResolver implementation.
func (r *Resolver) CurrentUser() generated.CurrentUserResolver { return &currentUserResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type currentUserResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
