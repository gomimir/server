package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/gomimir/server/internal/graphql/model"
)

func (r *queryResolver) ServerInfo(ctx context.Context) (*model.ServerInfo, error) {
	return &model.ServerInfo{
		Version: &r.AppVersionInfo,
		Auth:    r.AuthProvider.Type(),
	}, nil
}
