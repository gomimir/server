package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"io"
	"math"
	"strings"
	"time"

	"github.com/99designs/gqlgen/graphql"
	"github.com/icza/gox/gox"
	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/apperror"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/auth/permissions"
	"gitlab.com/gomimir/server/internal/config"
	"gitlab.com/gomimir/server/internal/crawler"
	"gitlab.com/gomimir/server/internal/documents"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/filelocation"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/filedeletequeue"
	"gitlab.com/gomimir/server/internal/graphql/generated"
	"gitlab.com/gomimir/server/internal/graphql/model"
	"gitlab.com/gomimir/server/internal/indexer"
	"gitlab.com/gomimir/server/internal/requests/archivecreate"
	"gitlab.com/gomimir/server/internal/utils"
)

func (r *assetResolver) Files(ctx context.Context, obj *documents.IndexedAsset) ([]*documents.AssetFile, error) {
	res := make([]*documents.AssetFile, len(obj.Files))
	for i := range obj.Files {
		res[i] = &obj.Files[i]
	}

	return res, nil
}

func (r *assetResolver) Tags(ctx context.Context, obj *documents.IndexedAsset) ([]*documents.TagAssignment, error) {
	res := make([]*documents.TagAssignment, len(obj.Tags))
	for i, tag := range obj.Tags {
		t := tag
		res[i] = &t
	}

	return res, nil
}

func (r *assetResolver) Properties(ctx context.Context, obj *documents.IndexedAsset) ([]model.Property, error) {
	return model.MarshalProperties(obj.Properties)
}

func (r *assetResolver) MainFile(ctx context.Context, obj *documents.IndexedAsset) (*documents.AssetFile, error) {
	return obj.Files.Main(), nil
}

func (r *assetResolver) Thumbnail(ctx context.Context, obj *documents.IndexedAsset, width *int, height *int, acceptableFormats []model.ThumbnailFormat) (*documents.Thumbnail, error) {
	formats, err := convertThumbnailFormats(acceptableFormats)
	if err != nil {
		return nil, err
	}

	return obj.Thumbnail(width, height, formats), nil
}

func (r *assetResolver) AllowedOperations(ctx context.Context, obj *documents.IndexedAsset) ([]auth.Operation, error) {
	indexPerms, err := permissions.IndexPermissions(ctx, pulid.ID(obj.AssetRef.IndexName()))
	if err != nil {
		return nil, err
	}

	return indexPerms.AllowedAssetOperations.SortedArray(), nil
}

func (r *assetListingResolver) Facets(ctx context.Context, obj *indexer.AssetListing) (*model.AssetListingFacets, error) {
	return &model.AssetListingFacets{FacetResults: obj.FacetResults}, nil
}

func (r *assetListingResolver) Stats(ctx context.Context, obj *indexer.AssetListing) (*model.AssetListingStats, error) {
	return &model.AssetListingStats{
		Files: &model.FileStats{
			PropertyStats: obj.Stats.Files.PropertyStats,
		},
	}, nil
}

func (r *mutationResolver) UpdateAsset(ctx context.Context, id pulid.ID, update model.AssetUpdate) (*model.AssetMutationResult, error) {
	ref := mimir.AssetRefFromString(string(id))
	if !ref.IsValid() {
		return nil, apperror.New(apperror.InvalidID, "invalid asset ID: %v", ref.String())
	}

	err := permissions.CheckAssetPermissions(ctx, pulid.ID(strings.ToUpper(ref.IndexName())), auth.OP_Update)
	if err != nil {
		return nil, err
	}

	updater, err := buildUpdater(update)
	if err != nil {
		return nil, err
	}

	asset, err := r.Indexer.UpdateAsset(ref, updater)
	if err != nil {
		return nil, err
	}

	return &model.AssetMutationResult{
		Ok:    true,
		Asset: asset,
	}, nil
}

func (r *mutationResolver) UpdateAssetByQuery(ctx context.Context, index pulid.ID, query *indexer.AssetFilter, update model.AssetUpdate) (*model.MutationByQueryResult, error) {
	err := permissions.CheckAssetPermissions(ctx, index, auth.OP_Update)
	if err != nil {
		return nil, err
	}

	updater, err := buildUpdater(update)
	if err != nil {
		return nil, err
	}

	summary, err := r.Indexer.UpdateAssetsByQuery([]string{strings.ToLower(string(index))}, query, updater)

	if err != nil {
		return nil, err
	}

	return &model.MutationByQueryResult{
		Ok:         summary.NumFailed == 0,
		NumMatched: summary.NumMatched,
		NumChanged: summary.NumChanged,
		NumFailed:  summary.NumFailed,
	}, nil
}

func (r *mutationResolver) DeleteAsset(ctx context.Context, id mimir.AssetRef, deleteFiles *bool) (*model.MutationResult, error) {
	err := permissions.CheckAssetPermissions(ctx, pulid.ID(strings.ToUpper(id.IndexName())), auth.OP_Delete)
	if err != nil {
		return nil, err
	}

	if deleteFiles != nil && *deleteFiles {
		asset, err := r.Indexer.GetAsset(id)
		if err != nil {
			if apperror.MatchesCode(err, apperror.NotFound) {
				return &model.MutationResult{
					Ok: true,
				}, nil
			} else {
				return nil, err
			}
		}

		fdq := filedeletequeue.NewFileDeleteQueue()

		err = queueAssetFilesForDeletion(ctx, fdq, asset)
		if err != nil {
			return nil, err
		}

		_, err = executeFDQAndLog(ctx, fdq)
		if err != nil {
			return nil, err
		}
	}

	err = r.Indexer.DeleteAsset(id)
	if err != nil {
		return nil, err
	}

	return &model.MutationResult{
		Ok: true,
	}, nil
}

func (r *mutationResolver) DeleteAssetByQuery(ctx context.Context, index pulid.ID, query *indexer.AssetFilter, deleteFiles *bool) (*model.MutationResult, error) {
	err := permissions.CheckAssetPermissions(ctx, index, auth.OP_Delete)
	if err != nil {
		return nil, err
	}

	if deleteFiles != nil && *deleteFiles {
		start := time.Now()
		var assetRefs []mimir.AssetRef

		assetC := r.Indexer.ScrollAssets(indexer.ScrollAssetsOptions{
			IndexNames: []string{strings.ToLower(string(index))},
			Filter:     query,
		})

		fdq := filedeletequeue.NewFileDeleteQueue()

		for item := range assetC {
			if item.Error != nil {
				if item.Error != io.EOF {
					return nil, item.Error
				}
			} else {
				assetRefs = append(assetRefs, item.Asset.AssetRef)
				err = queueAssetFilesForDeletion(ctx, fdq, item.Asset)
				if err != nil {
					return nil, err
				}
			}
		}

		log.Info().Int("num_assets", len(assetRefs)).Str("scroll_took", time.Since(start).String()).Msg("Asset scroll finished")

		_, err = executeFDQAndLog(ctx, fdq)
		if err != nil {
			return nil, err
		}

		err = r.Indexer.DeleteAssetsByQuery([]string{strings.ToLower(string(index))}, &indexer.AssetFilter{
			ID: &indexer.IDFilter{
				In: assetRefs,
			},
		})

		if err != nil {
			return nil, err
		}
	} else {
		err = r.Indexer.DeleteAssetsByQuery([]string{strings.ToLower(string(index))}, query)
		if err != nil {
			return nil, err
		}
	}

	return &model.MutationResult{
		Ok: true,
	}, nil
}

func (r *mutationResolver) ProcessAsset(ctx context.Context, id mimir.AssetRef, reprocess []string) (*model.MaybeAsyncMutationResult, error) {
	err := permissions.CheckAssetPermissions(ctx, pulid.ID(strings.ToUpper(id.IndexName())), auth.OP_Process)
	if err != nil {
		return nil, err
	}

	asset, err := r.Indexer.GetAsset(id)
	if err != nil {
		return nil, err
	}

	rules, err := config.IndexRules(ctx, pulid.ID(id.IndexName()))
	if err != nil {
		return nil, err
	}

	indexReq := crawler.IndexRequest{
		Reprocess: reprocess,
	}

	for _, file := range asset.Files {
		sublog := log.With().Str("filename", file.FileRef.Filename).Str("repository", file.FileRef.Repository).Logger()

		evaluatedRules, err := crawler.ResolveFileRules(file.FileRef, rules)
		if err != nil {
			sublog.Error().Err(err).Msg("Unable resolve file rules")
			return nil, err
		}

		if !evaluatedRules.Ignore {
			indexReq.Files = append(indexReq.Files, crawler.FileIndexRequest{
				IndexName:      id.IndexName(),
				FileRef:        file.FileRef,
				EvaluatedRules: *evaluatedRules,
			})
		}
	}

	if len(indexReq.Files) > 0 {
		queueRes, err := r.TaskManager.QueueJob(context.Background(), mimir.QueueJobOpts{
			Name: "reindexAssetFiles",
			Enqueue: []mimir.EnqueueTaskOpts{
				{
					Queue:      "main",
					Task:       "index",
					Parameters: indexReq,
				},
			},
		})

		jobID := queueRes.Job.ID()

		if err != nil {
			return nil, err
		}

		return &model.MaybeAsyncMutationResult{
			Ok:    true,
			JobID: &jobID,
		}, nil
	}

	return &model.MaybeAsyncMutationResult{
		Ok: true,
	}, nil
}

func (r *mutationResolver) ArchiveAssets(ctx context.Context, index pulid.ID, query *indexer.AssetFilter, files *model.ArchiveFiles) (*model.ArchiveAssetsResult, error) {
	tempFileLocation, err := indexFileLocation(ctx, index, filelocation.TypeTemporary)
	if err != nil {
		if ent.IsNotFound(err) {
			return nil, fmt.Errorf("no file location for temporary files configured for index %v", index)
		} else {
			return nil, err
		}
	}

	// Name of the resulting archive
	now := time.Now()
	tempFilename := fmt.Sprintf("%v.zip", uploadPrefix(tempFileLocation.Prefix, now, r.Rand))

	// Check permissions
	err = permissions.CheckAssetCollectionPermissions(ctx, pulid.ID(index), auth.OP_List)
	if err != nil {
		return nil, err
	}

	err = permissions.CheckAssetPermissions(ctx, pulid.ID(index), auth.OP_Read)
	if err != nil {
		return nil, err
	}

	// File filter
	fileFilter := model.ArchiveFilesAllOriginals
	if files != nil {
		fileFilter = *files
	}

	start := time.Now()
	assetC := r.Indexer.ScrollAssets(indexer.ScrollAssetsOptions{
		IndexNames: []string{strings.ToLower(string(index))},
		Filter:     query,
	})

	var reqs []archivecreate.FileRequest
	var totalSize int64 = 0

	for item := range assetC {
		if item.Error != nil {
			if item.Error != io.EOF {
				return nil, item.Error
			}
		} else {
			// Export all original asset files (no sidecars)
			if fileFilter == model.ArchiveFilesAllOriginals {
				for _, assetFile := range item.Asset.Files {
					reqs = append(reqs, archivecreate.FileRequest{
						SourceRef:      assetFile.FileRef,
						TargetFilename: utils.HashFilename(fmt.Sprintf("%v/%v", assetFile.Repository, assetFile.Filename)),
					})

					totalSize += assetFile.FileInfo.Size
				}
			} else

			// Export only single cover full-res thumbnail for each asset
			if fileFilter == model.ArchiveFilesCoverThumbnails {
				thumb := item.Asset.Thumbnail(gox.NewInt(math.MaxInt64), gox.NewInt(math.MaxInt64), []mimir.ThumbnailFileFormat{mimir.TF_JPEG, mimir.TF_WebP})
				if thumb != nil {
					reqs = append(reqs, archivecreate.FileRequest{
						SourceRef:      thumb.File().FileRef,
						TargetFilename: utils.HashFilename(fmt.Sprintf("%v/%v", thumb.File().Repository, thumb.File().Filename)),
					})

					totalSize += thumb.File().FileInfo.Size
				}
			}
		}
	}

	if len(reqs) > 0 {
		log.Info().Int("num_files", len(reqs)).Int64("total_size", totalSize).Str("scroll_took", time.Since(start).String()).Msg("Queueing archive job")

		queueRes, err := r.TaskManager.QueueJob(ctx, mimir.QueueJobOpts{
			Name: "archiveAssets",
			Enqueue: []mimir.EnqueueTaskOpts{
				{
					Queue: "main",
					Task:  "createArchive",
					Parameters: archivecreate.Request{
						DestRef: mimir.FileRef{
							Repository: string(tempFileLocation.RepositoryID),
							Filename:   tempFilename,
						},
						Files: reqs,
					},
				},
			},
		})

		if err != nil {
			return nil, err
		}

		url, err := utils.FileUrl(string(tempFileLocation.RepositoryID), tempFilename, now.UTC().UnixNano(), r.LinkSignatureKey)
		if err != nil {
			return nil, err
		}

		return &model.ArchiveAssetsResult{
			Ok: true,
			Archive: &model.ArchiveJob{
				JobID: queueRes.Job.ID(),
				URL:   url,
			},
		}, nil
	}

	return &model.ArchiveAssetsResult{
		Ok: true,
	}, nil
}

func (r *queryResolver) Asset(ctx context.Context, id pulid.ID) (*documents.IndexedAsset, error) {
	ref := mimir.AssetRefFromString(string(id))
	if !ref.IsValid() {
		return nil, apperror.New(apperror.InvalidID, "invalid asset ID: %v", ref.String())
	}

	err := permissions.CheckAssetPermissions(ctx, pulid.ID(ref.IndexName()), auth.OP_Read)
	if err != nil {
		return nil, err
	}

	return r.Indexer.GetAsset(ref)
}

func (r *queryResolver) Assets(ctx context.Context, index []pulid.ID, skip *int, size *int, query *indexer.AssetFilter, sortBy []*indexer.SortRequest) (*indexer.AssetListing, error) {
	if len(index) == 0 {
		return &indexer.AssetListing{
			Items:        nil,
			TotalCount:   0,
			FacetResults: &indexer.FacetResults{},
		}, nil
	}

	indexNames := make([]string, len(index))
	for i, indexID := range index {
		err := permissions.CheckAssetCollectionPermissions(ctx, indexID, auth.OP_List)
		if err != nil {
			return nil, err
		}

		err = permissions.CheckAssetPermissions(ctx, indexID, auth.OP_Read)
		if err != nil {
			return nil, err
		}

		indexNames[i] = strings.ToLower(string(indexID))
	}

	opts := indexer.AssetListingOptions{
		IndexNames: indexNames,
		Skip:       skip,
		Size:       size,
		Filter:     query,
		SortBy:     sortBy,
	}

	// Determine if we should fetch some aggregations
	operationCtx := graphql.GetOperationContext(ctx)
	fields := graphql.CollectFieldsCtx(ctx, nil)
	for _, f := range fields {
		if f.Name == "facets" {
			facetFields := graphql.CollectFields(operationCtx, f.Selections, nil)
			opts.Facets.PropertyFacets = collectPropertyFacets(operationCtx, facetFields, r.TimeZoneOffsetStr)

			for _, f := range facetFields {
				if f.Name == "tags" {
					facet := indexer.TagFacet{
						BaseFacet: indexer.FacetWithID(f.Alias),
					}

					args := f.ArgumentMap(operationCtx.Variables)
					if kind, ok := args["kind"]; ok {
						if kindStr, ok := kind.(string); ok {
							facet.Kind = kindStr
						}
					}

					opts.Facets.TagFacets = append(opts.Facets.TagFacets, facet)

				} else if f.Name == "tagKinds" {
					facet := indexer.TagKindFacet{
						BaseFacet: indexer.FacetWithID(f.Alias),
					}

					opts.Facets.TagKindFacets = append(opts.Facets.TagKindFacets, facet)

				} else if f.Name == "files" {
					fileFields := graphql.CollectFields(operationCtx, f.Selections, nil)

					fileFacet := indexer.FileFacet{
						BaseFacet:      indexer.FacetWithID(f.Alias),
						PropertyFacets: collectPropertyFacets(operationCtx, fileFields, r.TimeZoneOffsetStr),
					}

					for _, f := range fileFields {
						if f.Name == "propertyIntrospection" {
							facet := indexer.PropertyIntrospectionFacet{
								BaseFacet: indexer.FacetWithID(f.Alias),
							}

							fileFacet.PropertyIntrospectionFacets = append(fileFacet.PropertyIntrospectionFacets, facet)
						}
					}

					opts.Facets.FileFacets = append(opts.Facets.FileFacets, fileFacet)

				} else if f.Name == "propertyIntrospection" {
					facet := indexer.PropertyIntrospectionFacet{
						BaseFacet: indexer.FacetWithID(f.Alias),
					}

					opts.Facets.PropertyIntrospectionFacets = append(opts.Facets.PropertyIntrospectionFacets, facet)
				}
			}
		} else if f.Name == "stats" {
			statsFields := graphql.CollectFields(operationCtx, f.Selections, nil)
			for _, f := range statsFields {
				if f.Name == "files" {
					fileStatsFields := graphql.CollectFields(operationCtx, f.Selections, nil)

					if opts.Stats.Files == nil {
						opts.Stats.Files = &indexer.AssetFileStatsRequest{
							MainFilesOnly: true,
							PropertyStats: make(map[string]*indexer.PropertyStatsRequest),
						}
					}

					propStats := collectPropertyStats(operationCtx, fileStatsFields)
					for alias, propStats := range propStats {
						opts.Stats.Files.PropertyStats[alias] = propStats
					}
				}
			}
		}
	}

	return r.Indexer.ListAssets(opts)
}

// Asset returns generated.AssetResolver implementation.
func (r *Resolver) Asset() generated.AssetResolver { return &assetResolver{r} }

// AssetListing returns generated.AssetListingResolver implementation.
func (r *Resolver) AssetListing() generated.AssetListingResolver { return &assetListingResolver{r} }

type assetResolver struct{ *Resolver }
type assetListingResolver struct{ *Resolver }
