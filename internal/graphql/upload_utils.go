package graphql

import (
	"context"
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/apperror"
	"gitlab.com/gomimir/server/internal/config"
	"gitlab.com/gomimir/server/internal/crawler"
	"gitlab.com/gomimir/server/internal/dataloader"
	"gitlab.com/gomimir/server/internal/documents"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/graphql/model"
	"gitlab.com/gomimir/server/internal/utils"
)

type uploadReq struct {
	UploadRepo   pulid.ID `json:"repo"`
	UploadPrefix string   `json:"prefix"`

	Index                  pulid.ID `json:"index"`
	AllFilesAreSingleAsset *bool    `json:"allFilesAreSingleAsset,omitempty"`

	JobID      string `json:"jobId"`
	WaitTaskID string `json:"taskId"`
}

func uploadPrefix(prefix string, t time.Time, r *rand.Rand) string {
	normalizedPrefix := utils.EnsureNoTrailingSlash(prefix)
	return fmt.Sprintf("%v/%v_%x", normalizedPrefix, t.UTC().Format("2006-01-02_15.04.05.999999999"), r.Uint32())
}

func uploadAssetFiles(ctx context.Context, now time.Time, req uploadReq, files []*model.FileUpload) (*crawler.IndexRequest, error) {
	implicitRules, err := config.IndexRules(ctx, req.Index)
	if err != nil {
		return nil, err
	}

	repo, err := dataloader.RepositoryByID(ctx, req.UploadRepo)
	if err != nil {
		return nil, err
	}

	// Create index rules
	indexRules := make([]config.Rule, len(implicitRules))
	copy(indexRules, implicitRules)

	if req.AllFilesAreSingleAsset != nil && *req.AllFilesAreSingleAsset {
		indexRules = append(indexRules, config.Rule{Actions: []config.RuleAction{
			{Key: &req.UploadPrefix},
		}})
	}

	indexReq := crawler.IndexRequest{}

	for _, fileInput := range files {
		filename := utils.SanitizeRelPath(fileInput.File.Filename)
		if filename == "" {
			return nil, apperror.New(apperror.InvalidUploadFilename, "invalid filename: %v", fileInput.File.Filename)
		}

		fileRef := mimir.FileRef{
			Repository: string(req.UploadRepo),
			Filename:   utils.JoinPath(req.UploadPrefix, filename),
		}

		sublog := log.With().Str("filename", fileRef.Filename).Str("repository", fileRef.Repository).Logger()

		err := repo.UploadFile(ctx, fileRef.Filename, fileInput.File.Size, fileInput.File.File)
		if err != nil {
			sublog.Error().Err(err).Msg("Upload failed")
			return nil, err
		}

		sublog.Info().Str("filename", filename).Msg("File uploaded")

		evaluatedRules, err := crawler.ResolveFileRules(fileRef, indexRules)
		if err != nil {
			sublog.Error().Err(err).Msg("Unable resolve file rules")
			return nil, err
		}

		if !evaluatedRules.Ignore {
			if len(fileInput.Properties) > 0 {
				for _, prop := range fileInput.Properties {
					evaluatedRules.Properties[prop.Name] = prop.Value
				}
			}

			fileIndexReq := crawler.FileIndexRequest{
				IndexName: strings.ToLower(string(req.Index)),
				FileRef:   fileRef,
				FileInfo: &documents.FileInfo{
					LastModified: now,
					Size:         fileInput.File.Size,
				},
				EvaluatedRules: *evaluatedRules,
			}

			indexReq.Files = append(indexReq.Files, fileIndexReq)
		}
	}

	if len(indexReq.Files) > 0 {
		return &indexReq, nil
	}

	return nil, nil
}
