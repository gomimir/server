package graphql

import (
	"context"

	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/filelocation"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/graphql/model"
)

func applyIndexChanges(mutation *ent.IndexMutation, input *model.IndexInput) {
	if input != nil {
		if input.Name != nil {
			mutation.SetName(*input.Name)
		}

		if input.AssetType != nil {
			mutation.SetAssetType(*input.AssetType)
		}
	}
}

func createRules(ctx context.Context, tx *ent.Tx, indexID pulid.ID, actions []*model.IndexRuleInput) error {
	ruleCreate := make([]*ent.RuleCreate, len(actions))

	for i, input := range actions {
		ruleCreate[i] = tx.Rule.Create().SetIndexID(indexID).SetOrder(i + 1)
		applyRuleChanges(ruleCreate[i].Mutation(), input)
	}

	rules, err := tx.Rule.CreateBulk(ruleCreate...).Save(ctx)
	if err != nil {
		return err
	}

	var actionCreate []*ent.RuleActionCreate
	for i, rule := range rules {
		actions := actions[i].Actions
		if len(actions) > 0 {
			actionCreate = append(actionCreate, createActions(tx, rule.ID, actions)...)
		}
	}

	if len(actionCreate) > 0 {
		return tx.RuleAction.CreateBulk(actionCreate...).Exec(ctx)
	}

	return nil
}

func createFileLocations(ctx context.Context, tx *ent.Tx, indexID pulid.ID, locations []*model.FileLocationInput) error {
	batch := make([]*ent.FileLocationCreate, len(locations))

	for i, input := range locations {
		batch[i] = tx.FileLocation.Create().
			SetIndexID(indexID).
			SetType(input.Type).
			SetRepositoryID(input.RepositoryID).
			SetPrefix(input.Prefix)
	}

	_, err := tx.FileLocation.CreateBulk(batch...).Save(ctx)
	return err
}

func createTagKinds(ctx context.Context, tx *ent.Tx, indexID pulid.ID, tagKinds []*model.TagKindInput) error {
	batch := make([]*ent.TagKindCreate, len(tagKinds))

	for i, input := range tagKinds {
		batch[i] = tx.TagKind.Create().
			SetIndexID(indexID).
			SetName(input.Name).
			SetNillableDisplayName(input.DisplayName).
			SetNillableDisplayNamePlural(input.DisplayNamePlural).
			SetNillableUserAssignable(input.UserAssignable)
	}

	_, err := tx.TagKind.CreateBulk(batch...).Save(ctx)
	return err
}

func createIndexPermissions(ctx context.Context, tx *ent.Tx, indexID pulid.ID, permissions []*model.IndexPermissionsAssignmentInput) error {
	batch := make([]*ent.IndexPermissionCreate, len(permissions))

	for i, input := range permissions {
		batch[i] = tx.IndexPermission.Create().
			SetIndexID(indexID).
			SetSubjectID(input.SubjectID).
			SetIndexOperations(input.Permissions.IndexOperations).
			SetAssetOperations(input.Permissions.AssetOperations).
			SetAssetCollectionOperations(input.Permissions.AssetCollectionOperations)
	}

	_, err := tx.IndexPermission.CreateBulk(batch...).Save(ctx)
	return err
}

func indexFileLocation(ctx context.Context, index pulid.ID, locationType filelocation.Type) (*ent.FileLocation, error) {
	entClient := ent.FromContext(ctx)
	return entClient.FileLocation.Query().
		Where(filelocation.IndexID(pulid.ID(index)), filelocation.TypeEQ(locationType)).
		First(ctx)
}
