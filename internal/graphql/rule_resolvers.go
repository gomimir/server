package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/auth/permissions"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/predicate"
	entrule "gitlab.com/gomimir/server/internal/ent/rule"
	"gitlab.com/gomimir/server/internal/ent/ruleaction"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/graphql/model"
)

func (r *mutationResolver) RuleCreate(ctx context.Context, indexID pulid.ID, rule *model.IndexRuleInput) (*model.RuleMutationResult, error) {
	entClient := ent.FromContext(ctx)
	err := permissions.CheckIndexPermissions(ctx, indexID, auth.OP_Update)
	if err != nil {
		return nil, err
	}

	tx, err := entClient.Tx(ctx)
	if err != nil {
		return nil, err
	}

	maxOrder, err := maxRuleOrder(ctx, tx, indexID)
	if err != nil {
		return nil, err
	}

	ruleCreate := tx.Rule.Create().SetIndexID(indexID).SetOrder(maxOrder + 1)
	applyRuleChanges(ruleCreate.Mutation(), rule)

	ruleEnt, err := ruleCreate.Save(ctx)
	if err != nil {
		return nil, ent.RollbackHelper(tx, err)
	}

	if len(rule.Actions) > 0 {
		actionCreates := createActions(tx, ruleEnt.ID, rule.Actions)
		err = tx.RuleAction.CreateBulk(actionCreates...).Exec(ctx)
		if err != nil {
			return nil, ent.RollbackHelper(tx, err)
		}
	}

	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	ruleEnt = ruleEnt.Unwrap()

	return &model.RuleMutationResult{
		Ok:   true,
		Rule: ruleEnt,
	}, nil
}

func (r *mutationResolver) RuleUpdate(ctx context.Context, id pulid.ID, rule model.IndexRuleInput) (*model.RuleMutationResult, error) {
	entClient := ent.FromContext(ctx)

	err := checkRulePermission(ctx, id)
	if err != nil {
		return nil, err
	}

	tx, err := entClient.Tx(ctx)
	if err != nil {
		return nil, err
	}

	ruleUpdate := tx.Rule.UpdateOneID(id)
	applyRuleChanges(ruleUpdate.Mutation(), &rule)

	ruleEnt, err := ruleUpdate.Save(ctx)
	if err != nil {
		return nil, ent.RollbackHelper(tx, err)
	}

	if rule.Actions != nil {
		_, err = tx.RuleAction.Delete().Where(ruleaction.RuleID(id)).Exec(ctx)
		if err != nil {
			return nil, ent.RollbackHelper(tx, err)
		}

		if len(rule.Actions) > 0 {
			actionCreates := createActions(tx, ruleEnt.ID, rule.Actions)
			err = tx.RuleAction.CreateBulk(actionCreates...).Exec(ctx)
			if err != nil {
				return nil, ent.RollbackHelper(tx, err)
			}
		}
	}

	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	ruleEnt = ruleEnt.Unwrap()

	return &model.RuleMutationResult{
		Ok:   true,
		Rule: ruleEnt,
	}, nil
}

func (r *mutationResolver) RuleDelete(ctx context.Context, id pulid.ID) (*model.MutationResult, error) {
	entClient := ent.FromContext(ctx)
	err := checkRulePermission(ctx, id)
	if err != nil {
		return nil, err
	}

	err = entClient.Rule.DeleteOneID(id).Exec(ctx)
	if err != nil {
		return nil, err
	}

	return &model.MutationResult{
		Ok: true,
	}, nil
}

func (r *mutationResolver) RuleMoveBefore(ctx context.Context, id pulid.ID, before *pulid.ID) (*model.MutationResult, error) {
	entClient := ent.FromContext(ctx)

	tx, err := entClient.Tx(ctx)
	if err != nil {
		return nil, err
	}

	ruleEnt, err := tx.Rule.Get(ctx, id)
	if err != nil {
		return nil, ent.RollbackHelper(tx, err)
	}

	err = permissions.CheckIndexPermissions(ent.NewContext(ctx, tx.Client()), ruleEnt.IndexID, auth.OP_Update)
	if err != nil {
		return nil, err
	}

	var pilotEnt *ent.Rule
	if before != nil {
		pilotEnt, err = tx.Rule.Get(ctx, *before)
		if err != nil {
			return nil, ent.RollbackHelper(tx, err)
		}

		if pilotEnt.IndexID != ruleEnt.IndexID {
			return nil, ent.RollbackHelper(tx, fmt.Errorf("invalid pilot entity, cannot move across parents"))
		}
	}

	// Making space
	if pilotEnt != nil && pilotEnt.Order < ruleEnt.Order {
		err = tx.Rule.Update().AddOrder(1).Where(entrule.IndexID(ruleEnt.IndexID), entrule.OrderGTE(pilotEnt.Order), entrule.OrderLT(ruleEnt.Order)).Exec(ctx)
		if err != nil {
			return nil, ent.RollbackHelper(tx, err)
		}
	}

	// Moving the item in the right place
	if pilotEnt != nil {
		order := pilotEnt.Order
		if ruleEnt.Order < pilotEnt.Order {
			order = order - 1
		}

		err = tx.Rule.UpdateOneID(ruleEnt.ID).SetOrder(order).Exec(ctx)
		if err != nil {
			return nil, ent.RollbackHelper(tx, err)
		}
	} else {
		maxOrder, err := maxRuleOrder(ctx, tx, ruleEnt.IndexID)
		if err != nil {
			return nil, err
		}

		err = tx.Rule.UpdateOneID(ruleEnt.ID).SetOrder(maxOrder + 1).Exec(ctx)
		if err != nil {
			return nil, ent.RollbackHelper(tx, err)
		}
	}

	// Adjusting the the items after the item I just moved
	where := []predicate.Rule{entrule.IndexID(ruleEnt.IndexID), entrule.OrderGT(ruleEnt.Order)}
	if pilotEnt != nil {
		where = append(where, entrule.OrderLT(pilotEnt.Order), entrule.IDNEQ(ruleEnt.ID))
	}

	err = tx.Rule.Update().AddOrder(-1).Where(where...).Exec(ctx)
	if err != nil {
		return nil, ent.RollbackHelper(tx, err)
	}

	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	return &model.MutationResult{
		Ok: true,
	}, nil
}
