package graphql_test

import (
	"testing"

	"github.com/icza/gox/gox"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/rule"
	"gitlab.com/gomimir/server/internal/ent/testdata"
	"gitlab.com/gomimir/server/internal/graphql/gqlreq"
	"gitlab.com/gomimir/server/internal/graphql/gqltest"
	"gitlab.com/gomimir/server/internal/graphql/model"
)

func TestRuleCreate(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var index *ent.Index
	var bob *ent.User
	var alice *ent.User
	tester.InitData(
		testdata.UserBob().Ref(&bob),
		testdata.UserAlice().Ref(&alice),
		testdata.Index("testindex").
			SetPermissions(
				testdata.IndexPermission().SetSubject(testdata.UserBob()).SetIndexOperations(auth.OP_Update),
			).
			Ref(&index),
	)

	// 2. Create
	payload, err := gqlreq.RuleCreate(gqlreq.RuleCreateVars{
		IndexID: string(index.ID),
		Rule: &model.IndexRuleInput{
			Description: gox.NewString("Test rule"),
			Actions: []*model.IndexActionInput{
				{
					Description: gox.NewString("Test action"),
				},
			},
		},
	})

	if !assert.NoError(t, err) {
		return
	}

	t.Run("AsAlice", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload)
	})

	t.Run("AsBob", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(bob))

		// 3. Check
		exists, err := tester.Client.Rule.Query().Where(rule.IndexID(index.ID)).Exist(tester.Context)
		if !assert.NoError(t, err) {
			return
		}

		assert.True(t, exists)
	})
}

func TestRuleUpdate(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var rule *ent.Rule
	var bob *ent.User
	var alice *ent.User
	tester.InitData(
		testdata.UserBob().Ref(&bob),
		testdata.UserAlice().Ref(&alice),
		testdata.Index("testindex").
			SetPermissions(
				testdata.IndexPermission().SetSubject(testdata.UserBob()).SetIndexOperations(auth.OP_Update),
			).
			SetRules(
				testdata.Rule().Ref(&rule).SetActions(
					testdata.RuleAction(),
				),
			),
	)

	// 2. Update
	payload, err := gqlreq.RuleUpdate(gqlreq.RuleUpdateVars{
		ID: string(rule.ID),
		Rule: model.IndexRuleInput{
			Description: gox.NewString("Test rule"),
			Actions: []*model.IndexActionInput{
				{
					Description: gox.NewString("Test action"),
				},
			},
		},
	})

	if !assert.NoError(t, err) {
		return
	}

	t.Run("AsAlice", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload)
	})

	t.Run("AsBob", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(bob))
	})
}

func TestRuleDelete(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var rule *ent.Rule
	var bob *ent.User
	var alice *ent.User
	tester.InitData(
		testdata.UserBob().Ref(&bob),
		testdata.UserAlice().Ref(&alice),
		testdata.Index("testindex").
			SetPermissions(
				testdata.IndexPermission().SetSubject(testdata.UserBob()).SetIndexOperations(auth.OP_Update),
			).
			SetRules(
				testdata.Rule().Ref(&rule).SetActions(
					testdata.RuleAction(),
				),
			),
	)

	// 2. Delete
	payload, err := gqlreq.RuleDelete(string(rule.ID))

	if !assert.NoError(t, err) {
		return
	}

	t.Run("AsAlice", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload)
	})

	t.Run("AsBob", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(bob))

		// 3. Check no record exists anymore
		numRules, err := tester.Client.Rule.Query().Count(tester.Context)
		if !assert.NoError(t, err) {
			return
		}

		assert.Equal(t, 0, numRules)

		// 4. Check delete cascaded to actions
		numActions, err := tester.Client.RuleAction.Query().Count(tester.Context)
		if !assert.NoError(t, err) {
			return
		}

		assert.Equal(t, 0, numActions)
	})
}

func TestRuleMoveBefore(t *testing.T) {
	t.Run("AsAlice", func(t *testing.T) {
		tester := gqltest.NewTester(t)
		defer tester.Close()

		var alice *ent.User
		var rule1 *ent.Rule
		var rule2 *ent.Rule
		tester.InitData(
			testdata.UserAlice().Ref(&alice),
			testdata.Index("testindex").
				SetPermissions(
					testdata.IndexPermission().SetSubject(testdata.UserBob()).SetIndexOperations(auth.OP_Update),
				).
				SetRules(testdata.Rule().Ref(&rule1), testdata.Rule().Ref(&rule2)),
		)

		payload, err := gqlreq.RuleMoveBefore(gqlreq.RuleMoveBeforeVars{
			ID:       string(rule2.ID),
			BeforeID: gox.NewString(string(rule1.ID)),
		})

		if !assert.NoError(t, err) {
			return
		}

		tester.RequestSnapshot(payload, gqltest.AsUser(alice))
	})

	t.Run("AsBob", func(t *testing.T) {
		t.Run("ToTheEnd", func(t *testing.T) {
			// Move C -> last position
			res, err := testOrderChange(t, 5, 2, nil)
			if !assert.NoError(t, err) {
				return
			}

			assert.Equal(t, []string{"A", "B", "D", "E", "C"}, res)
		})

		t.Run("ToTheStart", func(t *testing.T) {
			// Move C -> first position
			res, err := testOrderChange(t, 5, 2, gox.NewInt(0))
			if !assert.NoError(t, err) {
				return
			}

			assert.Equal(t, []string{"C", "A", "B", "D", "E"}, res)
		})

		t.Run("ToTheLeft", func(t *testing.T) {
			// Move D -> before B
			res, err := testOrderChange(t, 6, 3, gox.NewInt(1))
			if !assert.NoError(t, err) {
				return
			}

			assert.Equal(t, []string{"A", "D", "B", "C", "E", "F"}, res)
		})

		t.Run("ToTheRight", func(t *testing.T) {
			// Move B -> before E
			res, err := testOrderChange(t, 6, 1, gox.NewInt(4))
			if !assert.NoError(t, err) {
				return
			}

			assert.Equal(t, []string{"A", "C", "D", "B", "E", "F"}, res)
		})

		t.Run("LastToTheMiddle", func(t *testing.T) {
			// Move F -> before D
			res, err := testOrderChange(t, 6, 5, gox.NewInt(3))
			if !assert.NoError(t, err) {
				return
			}

			assert.Equal(t, []string{"A", "B", "C", "F", "D", "E"}, res)
		})
	})
}

func testOrderChange(t *testing.T, n int, index int, moveBeforeIndex *int) ([]string, error) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	rules := make([]*ent.Rule, n)
	ruleReqs := make([]*testdata.TestRuleRequest, n)
	for i := 0; i < n; i++ {
		r := rune('A' + i)

		ruleReqs[i] = testdata.Rule().SetByCallback(func(rc *ent.RuleCreate) {
			rc.SetDescription(string(r))
		}).Ref(&rules[i])
	}

	var bob *ent.User
	tester.InitData(
		testdata.UserBob().Ref(&bob),
		testdata.Index("testindex").
			SetPermissions(
				testdata.IndexPermission().SetSubject(testdata.UserBob()).SetIndexOperations(auth.OP_Update),
			).
			SetRules(ruleReqs...),
	)

	vars := gqlreq.RuleMoveBeforeVars{
		ID: string(rules[index].ID),
	}

	if moveBeforeIndex != nil {
		vars.BeforeID = gox.NewString(string(rules[*moveBeforeIndex].ID))
	}

	// 2. Move
	payload, err := gqlreq.RuleMoveBefore(vars)
	if err != nil {
		return nil, err
	}

	tester.Request(payload, gqltest.AssertNoErrors(), gqltest.AsUser(bob))

	actualRules, err := tester.Client.Rule.Query().Where(rule.IndexID(rules[0].IndexID)).Order(ent.Asc(rule.FieldOrder)).All(tester.Context)
	if err != nil {
		return nil, err
	}

	res := make([]string, len(actualRules))
	for i, rule := range actualRules {
		res[i] = rule.Description
		assert.Equal(t, i+1, rule.Order, "Order for %v should be %v", rule.Description, i+1)
	}

	return res, nil
}
