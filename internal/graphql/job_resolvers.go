package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/auth/permissions"
	"gitlab.com/gomimir/server/internal/graphql/generated"
	"gitlab.com/gomimir/server/internal/graphql/model"
)

func (r *jobResolver) TaskSummary(ctx context.Context, obj mimir.JobInfo) ([]mimir.TaskProgress, error) {
	ts := obj.TaskSummary()
	if ts != nil {
		return ts, nil
	}

	state, err := r.JobMonitor.GetJobState(ctx, obj.ID())
	if err != nil {
		return nil, err
	}

	if state == nil {
		return nil, nil
	}

	return state.TaskProgress(), nil
}

func (r *queryResolver) Jobs(ctx context.Context) ([]mimir.JobInfo, error) {
	err := permissions.CheckCollectionPermissions(ctx, auth.RT_Job, auth.OP_List)
	if err != nil {
		return nil, err
	}

	err = permissions.CheckItemPermissions(ctx, auth.RT_Job, auth.OP_Read)
	if err != nil {
		return nil, err
	}

	return r.TaskManager.GetJobs(ctx, mimir.GetJobsOpts{Count: 20})
}

func (r *subscriptionResolver) JobEvents(ctx context.Context) (<-chan model.JobEvent, error) {
	err := permissions.CheckCollectionPermissions(ctx, auth.RT_Job, auth.OP_List)
	if err != nil {
		return nil, err
	}

	err = permissions.CheckItemPermissions(ctx, auth.RT_Job, auth.OP_Read)
	if err != nil {
		return nil, err
	}

	evtC := make(chan model.JobEvent)
	go func() {
		for evt := range r.JobMonitor.Subscribe(ctx) {
			if evt.Error() != nil {
				continue
			}

			// fmt.Printf("Job event(%v): %v\n", evt.Job().ID(), evt.State())

			// Note: we model it using different structures / common interface
			// to allow client to fetch different selections for individual event types
			// so that we can keep the amount of transmitted data low.
			if evt.State().Finished() {
				evtC <- &model.FinishedJobEvent{
					Job: evt.Job(),
				}
			} else if evt.PrevState() == nil {
				evtC <- &model.NewJobEvent{
					Job: evt.Job(),
				}
			} else {
				evtC <- &model.ProgressJobEvent{
					Job: evt.Job(),
				}
			}
		}

		close(evtC)
	}()

	return evtC, nil
}

// Job returns generated.JobResolver implementation.
func (r *Resolver) Job() generated.JobResolver { return &jobResolver{r} }

// Subscription returns generated.SubscriptionResolver implementation.
func (r *Resolver) Subscription() generated.SubscriptionResolver { return &subscriptionResolver{r} }

type jobResolver struct{ *Resolver }
type subscriptionResolver struct{ *Resolver }
