package graphql

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/99designs/gqlgen/graphql"
	"github.com/icza/gox/gox"
	"github.com/mitchellh/mapstructure"
	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/documents"
	"gitlab.com/gomimir/server/internal/filedeletequeue"
	"gitlab.com/gomimir/server/internal/graphql/model"
	"gitlab.com/gomimir/server/internal/indexer"
)

func convertThumbnailFormats(formats []model.ThumbnailFormat) ([]mimir.ThumbnailFileFormat, error) {
	if len(formats) > 0 {
		out := make([]mimir.ThumbnailFileFormat, len(formats))
		for i, format := range formats {
			out[i] = mimir.ThumbnailFileFormat(strings.ToLower(string(format)))
			if !out[i].IsValid() {
				return nil, fmt.Errorf("invalid thumbnail format: %v", format)
			}
		}

		return out, nil
	}

	return nil, nil
}

func collectPropertyFacets(operationCtx *graphql.OperationContext, fields []graphql.CollectedField, timeZoneOffsetStr string) []indexer.PropertyFacet {
	var result []indexer.PropertyFacet

	for _, filesChildField := range fields {
		if filesChildField.Name == "property" {
			args := filesChildField.ArgumentMap(operationCtx.Variables)
			if name, ok := args["name"]; ok {
				if nameStr, ok := name.(string); ok {
					propertyFacet := indexer.PropertyFacet{
						BaseFacet:    indexer.FacetWithID(filesChildField.Alias),
						PropertyName: nameStr,
					}

					for _, propertyChildField := range graphql.CollectFields(operationCtx, filesChildField.Selections, nil) {
						switch propertyChildField.Name {
						case "textValues":
							propertyFacet.Children = append(propertyFacet.Children, indexer.TextValueFacet{BaseFacet: indexer.FacetWithID(propertyChildField.Alias)})
						case "timeRanges":
							args := propertyChildField.ArgumentMap(operationCtx.Variables)

							tzOffset := timeZoneOffsetStr
							if val, ok := args["tzOffset"]; ok {
								if str, ok := val.(string); ok {
									tzOffset = str
								}
							}

							ranges := []indexer.TimeRange{
								{Name: "TODAY", FromExpr: gox.NewString("now/d")},
								{Name: "YESTERDAY", FromExpr: gox.NewString("now/d-1d"), ToExpr: gox.NewString("now/d")},
								{Name: "LAST_7_DAYS", FromExpr: gox.NewString("now/d-7d")},
								{Name: "LAST_30_DAYS", FromExpr: gox.NewString("now/d-30d")},
								{Name: "THIS_YEAR", FromExpr: gox.NewString("now/y")},
								{Name: "LAST_YEAR", FromExpr: gox.NewString("now/y-1y"), ToExpr: gox.NewString("now/y")},
							}

							if val, ok := args["ranges"]; ok {
								if tr, ok := val.([]interface{}); ok {
									ranges = nil
									for _, timeRange := range tr {
										if tr, ok := timeRange.(map[string]interface{}); ok {
											var decoded indexer.TimeRange
											if mapstructure.Decode(tr, &decoded) == nil {
												ranges = append(ranges, decoded)
											}
										}
									}
								}
							}

							propertyFacet.Children = append(propertyFacet.Children, indexer.TimeRangeFacet{
								BaseFacet:         indexer.FacetWithID(propertyChildField.Alias),
								Ranges:            ranges,
								TimeZoneOffsetStr: tzOffset,
							})
						}
					}

					result = append(result, propertyFacet)
				}
			}
		}
	}

	return result
}

func buildUpdater(update model.AssetUpdate) (func(asset documents.IndexedAsset) documents.IndexedAsset, error) {
	if len(update.SetProperties) > 0 {
		if err := validatePropertyNames(update.SetProperties); err != nil {
			return nil, err
		}
	}

	return func(asset documents.IndexedAsset) documents.IndexedAsset {
		for _, tag := range update.AddTags {
			asset.Tags.AssignTag(documents.Tag{Kind: tag.Kind, Name: tag.Name}, documents.UserTagAssignment{
				UserID:    "SYSTEM",
				CreatedAt: time.Now(),
			})
		}

		for _, tag := range update.RemoveTags {
			asset.Tags.RemoveTag(documents.Tag{Kind: tag.Kind, Name: tag.Name})
		}

		for _, prop := range update.SetProperties {
			asset.Properties.Set(prop.Name, prop.Value)
		}

		for _, propName := range update.UnsetProperties {
			asset.Properties.Unset(propName)
		}

		for _, fileUpdate := range update.UpdateFiles {
			for i := range asset.Files {
				if fileUpdate.Selector != nil {
					if fileUpdate.Selector.FileRef != nil {
						if fileUpdate.Selector.FileRef.Filename != nil && *fileUpdate.Selector.FileRef.Filename != asset.Files[i].Filename {
							continue
						}

						if fileUpdate.Selector.FileRef.Repository != nil && *fileUpdate.Selector.FileRef.Repository != asset.Files[i].Repository {
							continue
						}
					}
				}

				for _, prop := range fileUpdate.SetProperties {
					asset.Files[i].Properties.Set(prop.Name, prop.Value)
				}

				for _, propName := range fileUpdate.UnsetProperties {
					asset.Files[i].Properties.Unset(propName)
				}
			}
		}

		return asset
	}, nil
}

func collectPropertyStats(operationCtx *graphql.OperationContext, fields []graphql.CollectedField) map[string]*indexer.PropertyStatsRequest {
	res := make(map[string]*indexer.PropertyStatsRequest)

	for _, f := range fields {
		if f.Name == "property" {
			args := f.ArgumentMap(operationCtx.Variables)
			if name, ok := args["name"]; ok {
				if nameStr, ok := name.(string); ok {
					res[f.Alias] = &indexer.PropertyStatsRequest{
						Name: nameStr,
					}

					for _, propStatsField := range graphql.CollectFields(operationCtx, f.Selections, nil) {
						if propStatsField.Name == "timeValue" {
							res[f.Alias].TimeValue = &indexer.TimeValueStatsRequest{}
							for _, timeValueField := range graphql.CollectFields(operationCtx, propStatsField.Selections, nil) {
								if timeValueField.Name == "min" {
									res[f.Alias].TimeValue.Min = true
								}
								if timeValueField.Name == "max" {
									res[f.Alias].TimeValue.Max = true
								}
							}
						}
					}
				}
			}
		}
	}

	return res
}

// validatePropertyNames - validates that property names are in correct format
func validatePropertyNames(properties []*model.PropertyInput) error {
	for idx, prop := range properties {
		if prop.Name == "" {
			return fmt.Errorf("invalid property name at %v: cannot be empty", idx)
		} else if strings.Contains(prop.Name, ".") {
			return fmt.Errorf("invalid property name at %v: cannot contain dots", idx)
		}
	}

	return nil
}

func queueAssetFilesForDeletion(ctx context.Context, fdq *filedeletequeue.FileDeleteQueue, asset *documents.IndexedAsset) error {
	for _, file := range asset.Files {
		for _, sidecar := range file.Sidecars {
			err := fdq.Enqueue(ctx, asset.AssetRef, sidecar.File().FileRef)
			if err != nil {
				return err
			}
		}

		err := fdq.Enqueue(ctx, asset.AssetRef, file.FileRef)
		if err != nil {
			return err
		}
	}

	return nil
}

func executeFDQAndLog(ctx context.Context, fdq *filedeletequeue.FileDeleteQueue) (*filedeletequeue.FileDeleteStats, error) {
	stats, err := fdq.Execute(ctx)
	if err != nil {
		if stats != nil {
			log.Info().
				Int("num_deleted", stats.Deleted).
				Int("num_skipped", stats.Skipped).
				Int("num_remaining", stats.Queued-stats.Deleted).
				Str("took", stats.Took.String()).
				Err(err).
				Msg("File cleanup failed")
		} else {
			log.Info().
				Err(err).
				Msg("File cleanup failed")
		}

		return stats, err
	}

	log.Info().Int("num_deleted", stats.Deleted).Int("num_skipped", stats.Skipped).Str("took", stats.Took.String()).Msg("Finished file cleanup")
	return stats, err
}
