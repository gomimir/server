package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/graphql/generated"
	"gitlab.com/gomimir/server/internal/graphql/model"
)

func (r *indexActionResolver) Config(ctx context.Context, obj *ent.RuleAction) (model.ActionConfig, error) {
	if obj.Config == nil || obj.Config.IsEmpty() {
		return nil, nil
	}

	if obj.Config.Ignore != nil {
		return &model.IgnoreActionConfig{
			Action: model.ActionTypeIgnore,
			Ignore: *obj.Config.Ignore,
		}, nil
	}

	if obj.Config.SetKey != nil {
		return &model.SetKeyActionConfig{
			Action: model.ActionTypeSetKey,
			Key:    *obj.Config.SetKey,
		}, nil
	}

	if obj.Config.SetParams != nil {
		return &model.SetParamsActionConfig{
			Action: model.ActionTypeSetParams,
			Params: obj.Config.SetParams,
		}, nil
	}

	if obj.Config.AddTag != nil {
		return &model.AddTagActionConfig{
			Action: model.ActionTypeAddTag,
			Tag:    obj.Config.AddTag,
		}, nil
	}

	if obj.Config.Process != nil {
		return &model.ProcessActionConfig{
			Action:  model.ActionTypeProcess,
			Request: obj.Config.Process.Request,
			Queue:   obj.Config.Process.Queue,
			Params:  obj.Config.Process.Parameters,
		}, nil
	}

	return &model.UnknownActionConfig{
		Action: model.ActionTypeUnknown,
	}, nil
}

func (r *mutationResolver) ActionCreate(ctx context.Context, ruleID pulid.ID, action *model.IndexActionInput) (*model.ActionMutationResult, error) {
	entClient := ent.FromContext(ctx)
	err := checkRulePermission(ctx, ruleID)
	if err != nil {
		return nil, err
	}

	create := entClient.RuleAction.Create().SetRuleID(ruleID)
	applyActionChanges(create.Mutation(), action)

	entity, err := create.Save(ctx)
	if err != nil {
		return nil, err
	}

	return &model.ActionMutationResult{
		Ok:     true,
		Action: entity,
	}, nil
}

func (r *mutationResolver) ActionUpdate(ctx context.Context, id pulid.ID, action model.IndexActionInput) (*model.ActionMutationResult, error) {
	entClient := ent.FromContext(ctx)
	err := checkActionPermission(ctx, id)
	if err != nil {
		return nil, err
	}

	update := entClient.RuleAction.UpdateOneID(id)
	applyActionChanges(update.Mutation(), &action)

	entity, err := update.Save(ctx)
	if err != nil {
		return nil, err
	}

	return &model.ActionMutationResult{
		Ok:     true,
		Action: entity,
	}, nil
}

func (r *mutationResolver) ActionDelete(ctx context.Context, id pulid.ID) (*model.MutationResult, error) {
	entClient := ent.FromContext(ctx)
	err := checkActionPermission(ctx, id)
	if err != nil {
		return nil, err
	}

	err = entClient.RuleAction.DeleteOneID(id).Exec(ctx)
	if err != nil {
		return nil, err
	}

	return &model.MutationResult{
		Ok: true,
	}, nil
}

// IndexAction returns generated.IndexActionResolver implementation.
func (r *Resolver) IndexAction() generated.IndexActionResolver { return &indexActionResolver{r} }

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

type indexActionResolver struct{ *Resolver }
type mutationResolver struct{ *Resolver }
