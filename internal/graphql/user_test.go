package graphql_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/testdata"
	"gitlab.com/gomimir/server/internal/graphql/gqlreq"
	"gitlab.com/gomimir/server/internal/graphql/gqltest"
)

func TestUserDetail(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	var bob *ent.User
	tester.InitData(testdata.UserBob().Ref(&bob))

	payload, err := gqlreq.UserDetail(string(bob.ID))
	if !assert.NoError(t, err) {
		return
	}

	t.Run("AsGuest", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload)
	})

	t.Run("AsBob", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(bob))
	})
}

func TestUserListing(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	var bob *ent.User
	tester.InitData(
		testdata.UserBob().Ref(&bob),
		testdata.UserAlice(),
	)

	payload, err := gqlreq.UserListing()
	if !assert.NoError(t, err) {
		return
	}

	t.Run("AsGuest", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload)
	})

	t.Run("AsBob", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(bob))
	})
}
