package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"strings"

	"gitlab.com/gomimir/server/internal/documents"
	"gitlab.com/gomimir/server/internal/graphql/generated"
	"gitlab.com/gomimir/server/internal/graphql/model"
	"gitlab.com/gomimir/server/internal/utils"
)

func (r *assetFileResolver) URL(ctx context.Context, obj *documents.AssetFile) (string, error) {
	return utils.FileUrl(obj.Repository, obj.Filename, obj.FileInfo.LastModified.UTC().UnixNano(), r.LinkSignatureKey)
}

func (r *assetFileResolver) Sidecars(ctx context.Context, obj *documents.AssetFile) ([]documents.Sidecar, error) {
	return obj.Sidecars, nil
}

func (r *assetFileResolver) Thumbnail(ctx context.Context, obj *documents.AssetFile, width *int, height *int, acceptableFormats []model.ThumbnailFormat) (*documents.Thumbnail, error) {
	formats, err := convertThumbnailFormats(acceptableFormats)
	if err != nil {
		return nil, err
	}

	return obj.Thumbnail(width, height, formats), nil
}

func (r *assetFileResolver) Properties(ctx context.Context, obj *documents.AssetFile) ([]model.Property, error) {
	return model.MarshalProperties(obj.Properties)
}

func (r *fileResolver) URL(ctx context.Context, obj *documents.File) (string, error) {
	return utils.FileUrl(obj.Repository, obj.Filename, obj.FileInfo.LastModified.UTC().UnixNano(), r.LinkSignatureKey)
}

func (r *thumbnailResolver) Format(ctx context.Context, obj *documents.Thumbnail) (model.ThumbnailFormat, error) {
	return model.ThumbnailFormat(strings.ToUpper(string(obj.Format))), nil
}

func (r *videoResolver) Format(ctx context.Context, obj *documents.Video) (model.VideoFormat, error) {
	return model.VideoFormat(strings.ToUpper(string(obj.Format))), nil
}

// AssetFile returns generated.AssetFileResolver implementation.
func (r *Resolver) AssetFile() generated.AssetFileResolver { return &assetFileResolver{r} }

// File returns generated.FileResolver implementation.
func (r *Resolver) File() generated.FileResolver { return &fileResolver{r} }

// Thumbnail returns generated.ThumbnailResolver implementation.
func (r *Resolver) Thumbnail() generated.ThumbnailResolver { return &thumbnailResolver{r} }

// Video returns generated.VideoResolver implementation.
func (r *Resolver) Video() generated.VideoResolver { return &videoResolver{r} }

type assetFileResolver struct{ *Resolver }
type fileResolver struct{ *Resolver }
type thumbnailResolver struct{ *Resolver }
type videoResolver struct{ *Resolver }
