package graphql

import (
	"context"

	"gitlab.com/gomimir/server/internal/ent"
	entrule "gitlab.com/gomimir/server/internal/ent/rule"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/graphql/model"
)

func applyRuleChanges(mutation *ent.RuleMutation, input *model.IndexRuleInput) {
	if input != nil {
		if input.Description != nil {
			mutation.SetDescription(*input.Description)
		}

		if input.Match != nil {
			mutation.SetMatch(*input.Match)
		}

		if input.Inverse != nil {
			mutation.SetInverse(*input.Inverse)
		}

		if input.Enabled != nil {
			mutation.SetEnabled(*input.Enabled)
		}
	}
}

func createActions(tx *ent.Tx, ruleID pulid.ID, actions []*model.IndexActionInput) []*ent.RuleActionCreate {
	actionCreate := make([]*ent.RuleActionCreate, len(actions))
	for i, actionInput := range actions {
		actionCreate[i] = tx.RuleAction.Create().SetRuleID(ruleID)
		applyActionChanges(actionCreate[i].Mutation(), actionInput)
	}

	return actionCreate
}

func maxRuleOrder(ctx context.Context, tx *ent.Tx, indexID pulid.ID) (int, error) {
	var v []struct {
		IndexID string `json:"index_id"`
		Max     int    `json:"max"`
	}

	err := tx.Rule.Query().Where(entrule.IndexID(indexID)).GroupBy(entrule.FieldIndexID).Aggregate(ent.Max(entrule.FieldOrder)).Scan(ctx, &v)
	if err != nil {
		return 0, err
	}

	if len(v) > 0 {
		return v[0].Max, nil
	}

	return 0, nil
}
