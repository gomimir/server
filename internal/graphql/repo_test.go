package graphql_test

import (
	"testing"

	"github.com/icza/gox/gox"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/testdata"
	"gitlab.com/gomimir/server/internal/graphql/gqlreq"
	"gitlab.com/gomimir/server/internal/graphql/gqltest"
	"gitlab.com/gomimir/server/internal/graphql/model"
)

func TestRepositoryDetail(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var repo *ent.Repository
	var bob *ent.User
	tester.InitData(
		testdata.RepositoryMinio().Ref(&repo),
		testdata.UserBob().Ref(&bob),
	)

	// 2. Retrieving repo detail
	payload, err := gqlreq.RepositoryDetail(string(repo.ID))
	if !assert.NoError(t, err) {
		return
	}

	t.Run("AsGuest", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload)
	})

	t.Run("AsBob", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(bob))
	})
}

func TestRepositoryListing(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var bob *ent.User
	tester.InitData(
		testdata.RepositoryMinio(),
		testdata.UserBob().Ref(&bob),
	)

	// 2. Retrieving index detail
	payload, err := gqlreq.RepositoryListing()
	if !assert.NoError(t, err) {
		return
	}

	t.Run("AsGuest", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload)
	})

	t.Run("AsBob", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(bob))
	})
}

func TestRepositoryCreate(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var bob *ent.User
	var admin *ent.User
	tester.InitData(
		testdata.UserBob().Ref(&bob),
		testdata.UserAdmin().Ref(&admin),
	)

	// 2. Create
	payload, err := gqlreq.RepositoryCreate(gqlreq.RepositoryCreateVars{
		Repository: &model.RepositoryInput{
			Name: gox.NewString("minio"),
			S3: &model.S3RepositoryInput{
				Endpoint:  gox.NewString("127.0.0.1:9000"),
				AccessKey: gox.NewString("minio"),
				SecretKey: gox.NewString("minio123"),
			},
		},
	})

	if !assert.NoError(t, err) {
		return
	}

	t.Run("AsGuest", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload)
	})

	t.Run("AsBob", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(bob))
	})

	t.Run("AsAdmin", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(admin))

		// 3. Check
		exists, err := tester.Client.Repository.Query().Exist(tester.Context)
		if !assert.NoError(t, err) {
			return
		}

		assert.True(t, exists)
	})
}

func TestRepositoryUpdate(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var repo *ent.Repository
	var bob *ent.User
	var admin *ent.User
	tester.InitData(
		testdata.UserBob().Ref(&bob),
		testdata.UserAdmin().Ref(&admin),
		testdata.RepositoryMinio().Ref(&repo),
	)

	// 2. Update
	payload, err := gqlreq.RepositoryUpdate(gqlreq.RepositoryUpdateVars{
		ID: string(repo.ID),
		Repository: model.RepositoryInput{
			S3: &model.S3RepositoryInput{
				Endpoint: gox.NewString("127.0.0.1:9001"),
			},
		},
	})

	if !assert.NoError(t, err) {
		return
	}

	t.Run("AsGuest", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload)
	})

	t.Run("AsBob", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(bob))
	})

	t.Run("AsAdmin", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(admin))

		// 3. Check
		actual, err := tester.Client.Repository.Get(tester.Context, repo.ID)
		if !assert.NoError(t, err) {
			return
		}

		assert.Equal(t, "127.0.0.1:9001", actual.Config.S3.Endpoint)
		assert.Equal(t, repo.Config.S3.AccessKey, actual.Config.S3.AccessKey)
	})
}

func TestRepositoryDelete(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var repo *ent.Repository
	var bob *ent.User
	var admin *ent.User
	tester.InitData(
		testdata.UserBob().Ref(&bob),
		testdata.UserAdmin().Ref(&admin),
		testdata.RepositoryMinio().Ref(&repo),
	)

	// 2. Delete
	payload, err := gqlreq.RepositoryDelete(string(repo.ID))

	if !assert.NoError(t, err) {
		return
	}

	t.Run("AsGuest", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload)
	})

	t.Run("AsBob", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(bob))
	})

	t.Run("AsAdmin", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(admin))

		// 3. Check no record exists anymore
		numEntities, err := tester.Client.Repository.Query().Count(tester.Context)
		if !assert.NoError(t, err) {
			return
		}

		assert.Equal(t, 0, numEntities)
	})
}
