package graphql

import (
	"context"
	"errors"
	"math/rand"
	"net/http"
	"time"

	"github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/handler/extension"
	"github.com/99designs/gqlgen/graphql/handler/lru"
	"github.com/99designs/gqlgen/graphql/handler/transport"
	"github.com/gorilla/websocket"
	"github.com/justinas/alice"
	"github.com/rs/zerolog/log"
	"github.com/vektah/gqlparser/v2/gqlerror"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/apperror"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/auth/permissions"
	"gitlab.com/gomimir/server/internal/dataloader"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/graphql/generated"

	"gitlab.com/gomimir/server/internal/indexer"
)

type HTTPHandlerOpts struct {
	AppVersionInfo    mimir.VersionInfo
	Indexer           indexer.Indexer
	AuthProvider      auth.Provider
	TimeZoneOffsetStr string
	TaskManager       mimir.TaskManager
	JobMonitor        mimir.JobMonitor
	LinkSignatureKey  []byte
	Rand              *rand.Rand
}

func HTTPHandler(opts HTTPHandlerOpts) http.Handler {
	es := generated.NewExecutableSchema(generated.Config{
		Resolvers: &Resolver{
			AppVersionInfo:    opts.AppVersionInfo,
			Indexer:           opts.Indexer,
			AuthProvider:      opts.AuthProvider,
			TaskManager:       opts.TaskManager,
			JobMonitor:        opts.JobMonitor,
			TimeZoneOffsetStr: opts.TimeZoneOffsetStr,
			LinkSignatureKey:  opts.LinkSignatureKey,
			Rand:              opts.Rand,
		},
	})

	srv := handler.New(es)

	srv.AddTransport(&transport.Websocket{
		KeepAlivePingInterval: 10 * time.Second,
		Upgrader: websocket.Upgrader{
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
		},
		InitFunc: func(ctx context.Context, initPayload transport.InitPayload) (context.Context, error) {
			return opts.AuthProvider.WebsocketInit(ctx, initPayload)
		},
	})

	srv.AddTransport(transport.Options{})
	srv.AddTransport(transport.GET{})
	srv.AddTransport(transport.POST{})
	srv.AddTransport(transport.MultipartForm{
		// Max size of single upload request: 256 MB
		MaxUploadSize: 256 << 20,
		// Max size of single upload file to be kept in memory, anything larger will be buffered in temp. file
		MaxMemory: 64 << 20,
	})

	srv.SetQueryCache(lru.New(1000))

	srv.Use(extension.Introspection{})
	srv.Use(extension.AutomaticPersistedQuery{
		Cache: lru.New(100),
	})

	srv.AroundFields(PrometheusFieldIntercept)

	// Global checking of invalid auth. sessions
	// We do this here to fail early while still providing consistent experience with permissions checking
	srv.AroundFields(func(ctx context.Context, next graphql.Resolver) (res interface{}, err error) {
		session, err := auth.SessionForContext(ctx)
		if err != nil {
			return nil, err
		}

		// Checking top-level queries (children of Query, Mutation or Subscription)
		fc := graphql.GetFieldContext(ctx)
		if fc.Parent.Parent == nil {
			if session.State() == auth.Session_AuthInvalid {
				return nil, apperror.New(apperror.Unauthorized, "invalid auth. session")
			}
		}

		return next(ctx)
	})

	srv.SetErrorPresenter(func(ctx context.Context, e error) *gqlerror.Error {

		// Converting not found errors to proper code
		var notFoundErr *ent.NotFoundError
		if errors.As(e, &notFoundErr) {
			e = graphql.DefaultErrorPresenter(ctx, apperror.New(apperror.NotFound, notFoundErr.Error()))
		}

		// Wrap error in gqlerror.Error if not already
		err := graphql.DefaultErrorPresenter(ctx, e)

		if err.Extensions == nil {
			err.Extensions = make(map[string]interface{})
		}

		// Our own application errors => we can just pass them to the user
		var myErr apperror.Error
		if errors.As(e, &myErr) {
			err.Extensions["code"] = myErr.Code()
			l := log.Warn().Err(myErr).Str("errcode", string(myErr.Code()))

			if err.Path.String() != "" {
				l.Str("path", err.Path.String())
			}

			l.Msg("Caught error during GraphQL request")
			return err
		}

		// Other, unrecognizable errors => logging and masking the cause
		// We are ignore graphql errors (ie. validation errors), they are not useful to us.
		if err.Extensions["code"] != "GRAPHQL_VALIDATION_FAILED" {
			l := log.Error().Err(e)

			if err.Path.String() != "" {
				l.Str("path", err.Path.String())
			}

			l.Msg("Uncaught error during GraphQL request")

			err.Extensions["code"] = "UNKNOWN"
			err.Message = "unknown error occurred"
		}

		return err
	})

	return alice.New(
		opts.AuthProvider.Middleware,
		func(next http.Handler) http.Handler {
			return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				ctx := permissions.CacheContext(r.Context())
				ctx = dataloader.CacheContext(ctx)

				next.ServeHTTP(w, r.WithContext(ctx))
			})
		},
	).Then(srv)
}
