package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"errors"
	"fmt"

	"github.com/99designs/gqlgen/graphql"
	"github.com/rs/zerolog/log"
	"gitlab.com/gomimir/server/internal/graphql/generated"
	"gitlab.com/gomimir/server/internal/graphql/model"
	"gitlab.com/gomimir/server/internal/indexer"
)

func (r *assetListingFacetsResolver) Tags(ctx context.Context, obj *model.AssetListingFacets, kind string, size *int) ([]*indexer.TagBucket, error) {
	field := graphql.GetFieldContext(ctx)
	for _, tagFacetResult := range obj.FacetResults.TagFacetResults {
		if tagFacetResult.Facet.ID() == field.Field.Alias {
			return tagFacetResult.Buckets, nil
		}
	}

	log.Error().Msgf("Unable to resolve tag facet %v", field.Field.Alias)
	return nil, errors.New("internal server error")
}

func (r *assetListingFacetsResolver) TagKinds(ctx context.Context, obj *model.AssetListingFacets) ([]*indexer.TagKindBucket, error) {
	field := graphql.GetFieldContext(ctx)
	for _, facetResult := range obj.FacetResults.TagKindFacetResults {
		if facetResult.Facet.ID() == field.Field.Alias {
			return facetResult.Kinds, nil
		}
	}

	log.Error().Msgf("Unable to resolve tag kind facet %v", field.Field.Alias)
	return nil, errors.New("internal server error")
}

func (r *assetListingFacetsResolver) Property(ctx context.Context, obj *model.AssetListingFacets, name string) (*model.PropertyFacets, error) {
	field := graphql.GetFieldContext(ctx)
	for _, propFacetResult := range obj.FacetResults.PropertyFacetResults {
		if propFacetResult.Facet.ID() == field.Field.Alias {
			return &model.PropertyFacets{PropertyFacetResults: &propFacetResult}, nil
		}
	}

	log.Error().Msgf("Unable to resolve property facet results %v", field.Field.Alias)
	return nil, errors.New("internal server error")
}

func (r *assetListingFacetsResolver) PropertyIntrospection(ctx context.Context, obj *model.AssetListingFacets) ([]*indexer.PropertyIntrospection, error) {
	field := graphql.GetFieldContext(ctx)
	for _, facetResult := range obj.FacetResults.PropertyIntrospectionFacetResults {
		if facetResult.Facet.ID() == field.Field.Alias {
			return facetResult.Introspection, nil
		}
	}

	log.Error().Msgf("Unable to resolve property introspection results %v", field.Field.Alias)
	return nil, errors.New("internal server error")
}

func (r *assetListingFacetsResolver) Files(ctx context.Context, obj *model.AssetListingFacets) (*model.FileFacets, error) {
	field := graphql.GetFieldContext(ctx)
	for _, fileFacetResults := range obj.FacetResults.FileFacetResults {
		if fileFacetResults.Facet.ID() == field.Field.Alias {
			return &model.FileFacets{FileFacetResult: &fileFacetResults}, nil
		}
	}

	log.Error().Msgf("Unable to resolve file facet results %v", field.Field.Alias)
	fmt.Printf("%#v\n", obj.FacetResults)
	return nil, errors.New("internal server error")
}

func (r *fileFacetsResolver) Property(ctx context.Context, obj *model.FileFacets, name string) (*model.PropertyFacets, error) {
	field := graphql.GetFieldContext(ctx)
	for _, propFacetResult := range obj.FileFacetResult.PropertyFacetResults {
		if propFacetResult.Facet.ID() == field.Field.Alias {
			return &model.PropertyFacets{PropertyFacetResults: &propFacetResult}, nil
		}
	}

	log.Error().Msgf("Unable to resolve property facet results %v", field.Field.Alias)
	return nil, errors.New("internal server error")
}

func (r *fileFacetsResolver) PropertyIntrospection(ctx context.Context, obj *model.FileFacets) ([]*indexer.PropertyIntrospection, error) {
	field := graphql.GetFieldContext(ctx)
	for _, facetResult := range obj.FileFacetResult.PropertyIntrospectionFacetResults {
		if facetResult.Facet.ID() == field.Field.Alias {
			return facetResult.Introspection, nil
		}
	}

	log.Error().Msgf("Unable to resolve file property introspection results %v", field.Field.Alias)
	return nil, errors.New("internal server error")
}

func (r *propertyFacetsResolver) TextValues(ctx context.Context, obj *model.PropertyFacets) ([]*indexer.TextValueBucket, error) {
	field := graphql.GetFieldContext(ctx)
	for _, valueFacetResult := range obj.PropertyFacetResults.Children {
		if propFacetResult, ok := valueFacetResult.(indexer.TextValueFacetResult); ok {
			if propFacetResult.Facet.ID() == field.Field.Alias {
				return propFacetResult.Buckets, nil
			}
		}
	}

	log.Error().Msgf("Unable to resolve text property facet results %v", field.Field.Alias)
	return nil, errors.New("internal server error")
}

func (r *propertyFacetsResolver) TimeRanges(ctx context.Context, obj *model.PropertyFacets, ranges []*indexer.TimeRange, timezone *string) ([]*indexer.TimeRangeBucket, error) {
	field := graphql.GetFieldContext(ctx)
	for _, valueFacetResult := range obj.PropertyFacetResults.Children {
		if propFacetResult, ok := valueFacetResult.(indexer.TimeRangeFacetResult); ok {
			if propFacetResult.Facet.ID() == field.Field.Alias {
				return propFacetResult.Buckets, nil
			}
		}
	}

	log.Error().Msgf("Unable to resolve time property facet results %v", field.Field.Alias)
	return nil, errors.New("internal server error")
}

// AssetListingFacets returns generated.AssetListingFacetsResolver implementation.
func (r *Resolver) AssetListingFacets() generated.AssetListingFacetsResolver {
	return &assetListingFacetsResolver{r}
}

// FileFacets returns generated.FileFacetsResolver implementation.
func (r *Resolver) FileFacets() generated.FileFacetsResolver { return &fileFacetsResolver{r} }

// PropertyFacets returns generated.PropertyFacetsResolver implementation.
func (r *Resolver) PropertyFacets() generated.PropertyFacetsResolver {
	return &propertyFacetsResolver{r}
}

type assetListingFacetsResolver struct{ *Resolver }
type fileFacetsResolver struct{ *Resolver }
type propertyFacetsResolver struct{ *Resolver }
