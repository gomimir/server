package graphql_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/testdata"
	"gitlab.com/gomimir/server/internal/graphql/gqlreq"
	"gitlab.com/gomimir/server/internal/graphql/gqltest"
)

func TestCurrentUser(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	var admin *ent.User
	tester.InitData(
		testdata.UserAdmin().Ref(&admin),
	)

	payload, err := gqlreq.CurrentUser()
	require.NoError(t, err)

	t.Run("AsGuest", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload)
	})

	t.Run("AsAdmin", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(admin))
	})
}
