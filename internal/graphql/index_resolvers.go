package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"strings"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqljson"
	"github.com/icza/gox/gox"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/apperror"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/auth/permissions"
	"gitlab.com/gomimir/server/internal/crawler"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/filelocation"
	"gitlab.com/gomimir/server/internal/ent/index"
	"gitlab.com/gomimir/server/internal/ent/indexpermission"
	"gitlab.com/gomimir/server/internal/ent/rule"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/ent/tagkind"
	"gitlab.com/gomimir/server/internal/graphql/generated"
	"gitlab.com/gomimir/server/internal/graphql/model"
	"gitlab.com/gomimir/server/internal/indexer"
)

func (r *indexResolver) Rules(ctx context.Context, obj *ent.Index) ([]*ent.Rule, error) {
	entClient := ent.FromContext(ctx)
	return entClient.Rule.Query().Where(rule.IndexID(obj.ID)).Order(ent.Asc(rule.FieldOrder)).All(ctx)
}

func (r *indexResolver) AllPermissions(ctx context.Context, obj *ent.Index) ([]*model.IndexPermissionsAssignment, error) {
	perms, err := obj.Permissions(ctx)
	if err != nil {
		return nil, err
	}

	res := make([]*model.IndexPermissionsAssignment, len(perms))
	for i, indexPerms := range perms {
		res[i] = &model.IndexPermissionsAssignment{
			Subject: indexPerms.SubjectID,
			Permissions: &model.IndexPermissions{
				IndexOperations:           indexPerms.IndexOperations,
				AssetOperations:           indexPerms.AssetOperations,
				AssetCollectionOperations: indexPerms.AssetCollectionOperations,
			},
		}
	}

	return res, nil
}

func (r *indexResolver) Permissions(ctx context.Context, obj *ent.Index) (*model.IndexPermissions, error) {
	indexPerms, err := permissions.IndexPermissions(ctx, obj.ID)
	if err != nil {
		return nil, err
	}

	return &model.IndexPermissions{
		IndexOperations:           indexPerms.AllowedIndexOperations.SortedArray(),
		AssetOperations:           indexPerms.AllowedAssetOperations.SortedArray(),
		AssetCollectionOperations: indexPerms.AllowedAssetCollectionOperations.SortedArray(),
	}, nil
}

func (r *indexResolver) Stats(ctx context.Context, obj *ent.Index) (*model.IndexStats, error) {
	result, err := r.Indexer.ListAssets(indexer.AssetListingOptions{
		Size:       gox.NewInt(0),
		IndexNames: []string{strings.ToLower(string(obj.ID))},
		Stats: indexer.StatsRequest{
			ByIndex: &indexer.IndexStatsRequest{
				Files: &indexer.AssetFileStatsRequest{
					FileStatsRequest: indexer.FileStatsRequest{
						Count: &indexer.FileCountRequest{},
						Size:  &indexer.FileSizeRequest{},
					},
					Sidecars: &indexer.FileStatsRequest{
						Count: &indexer.FileCountRequest{},
						Size:  &indexer.FileSizeRequest{},
					},
				},
			},
		},
	})

	if err != nil {
		return nil, err
	}

	if len(result.Stats.ByIndex) > 0 {
		indexStats := result.Stats.ByIndex[0]

		return &model.IndexStats{
			Count: indexStats.Count,
			Files: indexStats.Files,
		}, nil
	} else {
		return &model.IndexStats{
			Count: 0,
			Files: &indexer.AssetFileStatsResult{
				FileStatsResult: indexer.FileStatsResult{
					Count: gox.NewInt(0),
					Size: &indexer.NumericStatsResult{
						Sum: gox.NewInt(0),
					},
				},
				Sidecars: &indexer.FileStatsResult{
					Count: gox.NewInt(0),
					Size: &indexer.NumericStatsResult{
						Sum: gox.NewInt(0),
					},
				},
			},
		}, nil
	}
}

func (r *mutationResolver) IndexCreate(ctx context.Context, index *model.IndexInput) (*model.IndexMutationResult, error) {
	entClient := ent.FromContext(ctx)
	err := permissions.CheckCollectionPermissions(ctx, auth.RT_Index, auth.OP_Create)
	if err != nil {
		return nil, err
	}

	tx, err := entClient.Tx(ctx)
	if err != nil {
		return nil, err
	}

	indexCreate := tx.Index.Create()
	applyIndexChanges(indexCreate.Mutation(), index)

	indexEnt, err := indexCreate.Save(ctx)
	if err != nil {
		return nil, ent.RollbackHelper(tx, err)
	}

	if len(index.Rules) > 0 {
		err = createRules(ctx, tx, indexEnt.ID, index.Rules)
		if err != nil {
			return nil, ent.RollbackHelper(tx, err)
		}
	}

	if len(index.FileLocations) > 0 {
		err = createFileLocations(ctx, tx, indexEnt.ID, index.FileLocations)
		if err != nil {
			return nil, ent.RollbackHelper(tx, err)
		}
	}

	if len(index.TagKinds) > 0 {
		err = createTagKinds(ctx, tx, indexEnt.ID, index.TagKinds)
		if err != nil {
			return nil, ent.RollbackHelper(tx, err)
		}
	}

	if len(index.Permissions) > 0 {
		err = createIndexPermissions(ctx, tx, indexEnt.ID, index.Permissions)
		if err != nil {
			return nil, ent.RollbackHelper(tx, err)
		}
	}

	err = r.Indexer.EnsureIndices([]string{strings.ToLower(string(indexEnt.ID))})
	if err != nil {
		return nil, ent.RollbackHelper(tx, err)
	}

	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	return &model.IndexMutationResult{
		Ok:    true,
		Index: indexEnt.Unwrap(),
	}, nil
}

func (r *mutationResolver) IndexUpdate(ctx context.Context, id pulid.ID, index model.IndexInput) (*model.IndexMutationResult, error) {
	entClient := ent.FromContext(ctx)
	err := permissions.CheckIndexPermissions(ctx, id, auth.OP_Update)
	if err != nil {
		return nil, err
	}

	tx, err := entClient.Tx(ctx)
	if err != nil {
		return nil, err
	}

	indexUpdate := tx.Index.UpdateOneID(id)
	applyIndexChanges(indexUpdate.Mutation(), &index)

	indexEnt, err := indexUpdate.Save(ctx)
	if err != nil {
		return nil, ent.RollbackHelper(tx, err)
	}

	if index.Rules != nil {
		_, err = tx.Rule.Delete().Where(rule.IndexID(indexEnt.ID)).Exec(ctx)
		if err != nil {
			return nil, ent.RollbackHelper(tx, err)
		}

		if len(index.Rules) > 0 {
			err = createRules(ctx, tx, indexEnt.ID, index.Rules)
			if err != nil {
				return nil, ent.RollbackHelper(tx, err)
			}
		}
	}

	if index.FileLocations != nil {
		_, err = tx.FileLocation.Delete().Where(filelocation.IndexID(indexEnt.ID)).Exec(ctx)
		if err != nil {
			return nil, ent.RollbackHelper(tx, err)
		}

		if len(index.FileLocations) > 0 {
			err = createFileLocations(ctx, tx, indexEnt.ID, index.FileLocations)
			if err != nil {
				return nil, ent.RollbackHelper(tx, err)
			}
		}
	}

	if index.TagKinds != nil {
		_, err = tx.TagKind.Delete().Where(tagkind.IndexID(indexEnt.ID)).Exec(ctx)
		if err != nil {
			return nil, ent.RollbackHelper(tx, err)
		}

		if len(index.TagKinds) > 0 {
			err = createTagKinds(ctx, tx, indexEnt.ID, index.TagKinds)
			if err != nil {
				return nil, ent.RollbackHelper(tx, err)
			}
		}
	}

	if index.Permissions != nil {
		_, err = tx.IndexPermission.Delete().Where(indexpermission.IndexID(indexEnt.ID)).Exec(ctx)
		if err != nil {
			return nil, ent.RollbackHelper(tx, err)
		}

		if len(index.Permissions) > 0 {
			err = createIndexPermissions(ctx, tx, indexEnt.ID, index.Permissions)
			if err != nil {
				return nil, ent.RollbackHelper(tx, err)
			}
		}
	}

	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	return &model.IndexMutationResult{
		Ok:    true,
		Index: indexEnt.Unwrap(),
	}, nil
}

func (r *mutationResolver) IndexDelete(ctx context.Context, id pulid.ID) (*model.MutationResult, error) {
	entClient := ent.FromContext(ctx)
	err := permissions.CheckIndexPermissions(ctx, id, auth.OP_Delete)
	if err != nil {
		return nil, err
	}

	err = entClient.Index.DeleteOneID(id).Exec(ctx)
	if err != nil {
		return nil, err
	}

	return &model.MutationResult{
		Ok: true,
	}, nil
}

func (r *mutationResolver) Crawl(ctx context.Context, reqs []*crawler.Request) (*model.AsyncMutationResult, error) {
	entClient := ent.FromContext(ctx)

	tasks := make([]mimir.EnqueueTaskOpts, len(reqs))
	for i, req := range reqs {
		err := permissions.CheckIndexPermissions(ctx, req.IndexID, auth.OP_Crawl)
		if err != nil {
			return nil, err
		}

		fileLocations, err := entClient.FileLocation.Query().Where(
			filelocation.IndexID(req.IndexID),
			filelocation.TypeIn(filelocation.TypeCrawl, filelocation.TypeUpload),
		).All(ctx)

		if err != nil {
			return nil, err
		}

		var allFileSets []*crawler.FileSet
		for _, fl := range fileLocations {
			allFileSets = append(allFileSets, &crawler.FileSet{
				RepositoryID: fl.RepositoryID,
				Prefix:       fl.Prefix,
			})
		}

		if req.FileSets == nil {
			req.FileSets = allFileSets
		} else {
			for i, fs := range req.FileSets {
				allowed := false
				for _, fs2 := range allFileSets {
					if fs2.Includes(*fs) {
						allowed = true
						break
					}
				}

				if !allowed {
					return nil, apperror.New(apperror.InvalidFileSet, "invalid file set #%v", i)
				}
			}
		}

		tasks[i] = mimir.EnqueueTaskOpts{
			Queue:      "main",
			Task:       "crawl",
			Parameters: req,
		}
	}

	queueRes, err := r.TaskManager.QueueJob(context.Background(), mimir.QueueJobOpts{
		Name:    "crawl",
		Enqueue: tasks,
	})

	if err != nil {
		return nil, err
	}

	return &model.AsyncMutationResult{
		Ok:    true,
		JobID: queueRes.Job.ID(),
	}, nil
}

func (r *queryResolver) Index(ctx context.Context, id pulid.ID) (*ent.Index, error) {
	entClient := ent.FromContext(ctx)
	err := permissions.CheckIndexPermissions(ctx, id, auth.OP_Read)
	if err != nil {
		return nil, err
	}

	return entClient.Index.Get(ctx, id)
}

func (r *queryResolver) Indices(ctx context.Context) ([]*ent.Index, error) {
	entClient := ent.FromContext(ctx)
	globalPerms, err := permissions.GlobalPermissions(ctx)
	if err != nil {
		return nil, err
	}

	globalPermsForIndices := globalPerms.For(auth.RT_Index)
	err = globalPermsForIndices.CheckCollectionPermissions(auth.OP_List)
	if err != nil {
		return nil, err
	}

	query := entClient.Index.Query()

	canReadAllIndices := globalPermsForIndices.AllowedItemOperations.Contains(auth.OP_Read)
	if !canReadAllIndices {
		session, err := auth.SessionForContext(ctx)
		if err != nil {
			return nil, err
		}

		query.Where(func(s *sql.Selector) {
			t := sql.Table(indexpermission.Table)
			s.Join(t).On(s.C(index.FieldID), t.C(indexpermission.FieldIndexID))
			s.Where(sql.And(
				sql.In(t.C(indexpermission.FieldSubjectID), session.Identities()...),
				sqljson.ValueContains(t.C(indexpermission.FieldIndexOperations), auth.OP_Read),
			))
		})
	}

	return query.All(ctx)
}

// Index returns generated.IndexResolver implementation.
func (r *Resolver) Index() generated.IndexResolver { return &indexResolver{r} }

type indexResolver struct{ *Resolver }
