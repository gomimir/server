package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"github.com/rs/zerolog/log"
	"gitlab.com/gomimir/server/internal/apperror"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/graphql/model"
	"gitlab.com/gomimir/server/internal/utils"
)

func (r *mutationResolver) RefreshAuth(ctx context.Context, refreshToken string) (*auth.OAuthTokens, error) {
	if provider, ok := r.AuthProvider.(*auth.OIDCProvider); ok {
		return provider.RefreshToken(ctx, refreshToken)
	} else {
		return nil, apperror.New(apperror.UnsupportedAuth, "Auth. refresh is not supported for provider %v", r.AuthProvider.Type())
	}
}

func (r *mutationResolver) OauthLogin(ctx context.Context, state *utils.AppState, callbackURL string) (*model.OAuthLogin, error) {
	if provider, ok := r.AuthProvider.(*auth.OIDCProvider); ok {
		if state == nil {
			state = &utils.AppState{Path: "/"}
		}

		stateStr, err := state.Serialize(r.LinkSignatureKey)
		if err != nil {
			return nil, err
		}

		return &model.OAuthLogin{
			URL: provider.GenerateAuthLink(stateStr, callbackURL),
		}, nil
	} else {
		return nil, apperror.New(apperror.UnsupportedAuth, "OAuth2 login is not supported for provider %v", r.AuthProvider.Type())
	}
}

func (r *mutationResolver) OauthExchange(ctx context.Context, code string, callbackURL string, serializedState string) (*model.OAuthExchange, error) {
	if provider, ok := r.AuthProvider.(*auth.OIDCProvider); ok {
		state, err := utils.AppStateFromString(serializedState, r.LinkSignatureKey)
		if err != nil {
			return nil, err
		}

		token, err := provider.Exchange(ctx, callbackURL, code)
		if err != nil {
			log.Warn().Err(err).Msg("Failed to exchange authentication code for a token")
			return nil, err
		}

		res := model.OAuthExchange{
			Token:        token.IDToken,
			RefreshToken: token.RefreshToken,
			State:        state,
		}

		return &res, nil
	} else {
		return nil, apperror.New(apperror.UnsupportedAuth, "OAuth2 exchange is not supported for provider %v", r.AuthProvider.Type())
	}
}
