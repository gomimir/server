package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/auth/permissions"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

func (r *queryResolver) User(ctx context.Context, id pulid.ID) (*ent.User, error) {
	entClient := ent.FromContext(ctx)
	err := permissions.CheckItemPermissions(ctx, auth.RT_User, auth.OP_Read)
	if err != nil {
		return nil, err
	}

	return entClient.User.Get(ctx, id)
}

func (r *queryResolver) Users(ctx context.Context) ([]*ent.User, error) {
	entClient := ent.FromContext(ctx)
	err := permissions.CheckCollectionPermissions(ctx, auth.RT_User, auth.OP_List)
	if err != nil {
		return nil, err
	}

	err = permissions.CheckItemPermissions(ctx, auth.RT_User, auth.OP_Read)
	if err != nil {
		return nil, err
	}

	return entClient.User.Query().All(ctx)
}
