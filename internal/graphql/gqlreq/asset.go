package gqlreq

import (
	_ "embed"
	"encoding/json"
)

//go:embed "asset.graphql"
var assetDocument string

func AssetDetail(id string) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "AssetDetail",
		Query:         assetDocument,
		Variables: map[string]interface{}{
			"id": id,
		},
	})
}

type AssetListingVars struct {
	Index []string `json:"index"`
}

func AssetListing(vars AssetListingVars) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "AssetListing",
		Query:         assetDocument,
		Variables:     vars,
	})
}
