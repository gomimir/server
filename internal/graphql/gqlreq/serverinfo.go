package gqlreq

import (
	_ "embed"
	"encoding/json"
)

//go:embed "serverinfo.graphql"
var serverInfoDocument string

func ServerInfo() ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "ServerInfo",
		Query:         serverInfoDocument,
	})
}
