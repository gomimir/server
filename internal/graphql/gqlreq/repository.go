package gqlreq

import (
	_ "embed"
	"encoding/json"

	"gitlab.com/gomimir/server/internal/graphql/model"
)

//go:embed "repository.graphql"
var repoDocument string

func RepositoryDetail(id string) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "RepositoryDetail",
		Query:         repoDocument,
		Variables: map[string]interface{}{
			"id": id,
		},
	})
}

func RepositoryListing() ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "RepositoryListing",
		Query:         repoDocument,
	})
}

type RepositoryCreateVars struct {
	Repository *model.RepositoryInput `json:"repository,omitempty"`
}

func RepositoryCreate(vars RepositoryCreateVars) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "RepositoryCreate",
		Query:         repoDocument,
		Variables:     vars,
	})
}

type RepositoryUpdateVars struct {
	ID         string                `json:"id"`
	Repository model.RepositoryInput `json:"repository"`
}

func RepositoryUpdate(vars RepositoryUpdateVars) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "RepositoryUpdate",
		Query:         repoDocument,
		Variables:     vars,
	})
}

func RepositoryDelete(id string) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "RepositoryDelete",
		Query:         repoDocument,
		Variables: map[string]interface{}{
			"id": id,
		},
	})
}
