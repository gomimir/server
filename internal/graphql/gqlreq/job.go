package gqlreq

import (
	_ "embed"
	"encoding/json"
)

//go:embed "job.graphql"
var jobDocument string

func JobListing() ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "JobListing",
		Query:         jobDocument,
	})
}
