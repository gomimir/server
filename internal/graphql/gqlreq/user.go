package gqlreq

import (
	_ "embed"
	"encoding/json"
)

//go:embed "user.graphql"
var userDocument string

func UserDetail(id string) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "UserDetail",
		Query:         userDocument,
		Variables: map[string]interface{}{
			"id": id,
		},
	})
}

func UserListing() ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "UserListing",
		Query:         userDocument,
	})
}
