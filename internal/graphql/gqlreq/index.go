package gqlreq

import (
	_ "embed"
	"encoding/json"

	"gitlab.com/gomimir/server/internal/graphql/model"
)

//go:embed "index.graphql"
var indexDocument string

func IndexDetail(id string) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "IndexDetail",
		Query:         indexDocument,
		Variables: map[string]interface{}{
			"id": id,
		},
	})
}

func IndexListing() ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "IndexListing",
		Query:         indexDocument,
	})
}

func IndexCreate(input *model.IndexInput) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "IndexCreate",
		Query:         indexDocument,
		Variables: map[string]interface{}{
			"index": input,
		},
	})
}

type IndexUpdateVars struct {
	ID    string           `json:"id"`
	Index model.IndexInput `json:"index"`
}

func IndexUpdate(vars IndexUpdateVars) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "IndexUpdate",
		Query:         indexDocument,
		Variables:     vars,
	})
}

func IndexDelete(id string) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "IndexDelete",
		Query:         indexDocument,
		Variables: map[string]interface{}{
			"id": id,
		},
	})
}

type RuleCreateVars struct {
	IndexID string                `json:"indexId"`
	Rule    *model.IndexRuleInput `json:"rule,omitempty"`
}

func RuleCreate(vars RuleCreateVars) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "RuleCreate",
		Query:         indexDocument,
		Variables:     vars,
	})
}

type RuleUpdateVars struct {
	ID   string               `json:"id"`
	Rule model.IndexRuleInput `json:"rule"`
}

func RuleUpdate(vars RuleUpdateVars) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "RuleUpdate",
		Query:         indexDocument,
		Variables:     vars,
	})
}

func RuleDelete(id string) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "RuleDelete",
		Query:         indexDocument,
		Variables: map[string]interface{}{
			"id": id,
		},
	})
}

type RuleMoveBeforeVars struct {
	ID       string  `json:"id"`
	BeforeID *string `json:"before,omitempty"`
}

func RuleMoveBefore(vars RuleMoveBeforeVars) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "RuleMoveBefore",
		Query:         indexDocument,
		Variables:     vars,
	})
}

type ActionCreateVars struct {
	RuleID string                  `json:"ruleId"`
	Action *model.IndexActionInput `json:"action,omitempty"`
}

func ActionCreate(vars ActionCreateVars) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "ActionCreate",
		Query:         indexDocument,
		Variables:     vars,
	})
}

type ActionUpdateVars struct {
	ID     string                 `json:"id"`
	Action model.IndexActionInput `json:"action"`
}

func ActionUpdate(vars ActionUpdateVars) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "ActionUpdate",
		Query:         indexDocument,
		Variables:     vars,
	})
}

func ActionDelete(id string) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "ActionDelete",
		Query:         indexDocument,
		Variables: map[string]interface{}{
			"id": id,
		},
	})
}
