package gqlreq

import (
	_ "embed"
	"encoding/json"
)

//go:embed "tag.graphql"
var tagDocument string

type TagListingVars struct {
	Index []string `json:"index"`
}

func TagListing(vars TagListingVars) ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "TagListing",
		Query:         tagDocument,
		Variables:     vars,
	})
}
