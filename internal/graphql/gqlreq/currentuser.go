package gqlreq

import (
	_ "embed"
	"encoding/json"
)

//go:embed "currentuser.graphql"
var currentUserDocument string

func CurrentUser() ([]byte, error) {
	return json.Marshal(graphQLPayload{
		OperationName: "CurrentUser",
		Query:         currentUserDocument,
	})
}
