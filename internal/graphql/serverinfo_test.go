package graphql_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/graphql/gqlreq"
	"gitlab.com/gomimir/server/internal/graphql/gqltest"
)

func TestServerInfo(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	payload, err := gqlreq.ServerInfo()
	if !assert.NoError(t, err) {
		return
	}

	tester.RequestSnapshotT(t, payload)
}
