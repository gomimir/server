package graphql_test

import (
	"fmt"
	"testing"

	"github.com/icza/gox/gox"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/filelocation"
	"gitlab.com/gomimir/server/internal/ent/testdata"
	"gitlab.com/gomimir/server/internal/graphql/gqlreq"
	"gitlab.com/gomimir/server/internal/graphql/gqltest"
	"gitlab.com/gomimir/server/internal/graphql/model"

	_ "modernc.org/sqlite"
)

func TestIndexDetail(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var index *ent.Index
	var bob *ent.User
	var alice *ent.User
	var admin *ent.User
	tester.InitData(
		testdata.UserBob().Ref(&bob),
		testdata.UserAlice().Ref(&alice),
		testdata.UserAdmin().Ref(&admin),
		testdata.IndexDocuments().Ref(&index),
	)

	// 2. Retrieving index detail
	payload, err := gqlreq.IndexDetail(string(index.ID))
	if !assert.NoError(t, err) {
		return
	}

	for _, user := range []*ent.User{bob, alice, admin} {
		t.Run(fmt.Sprintf("As%v", user.DisplayName), func(t *testing.T) {
			tester.RequestSnapshotT(t, payload, gqltest.AsUser(user))
		})
	}
}

func TestIndexListing(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var bob *ent.User
	var alice *ent.User
	var admin *ent.User
	tester.InitData(
		testdata.UserBob().Ref(&bob),
		testdata.UserAlice().Ref(&alice),
		testdata.UserAdmin().Ref(&admin),
		testdata.IndexPhotos(),
		testdata.IndexDocuments(),
	)

	// 2. Retrieving index detail
	payload, err := gqlreq.IndexListing()
	if !assert.NoError(t, err) {
		return
	}

	t.Run("AsGuest", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload)
	})

	for _, user := range []*ent.User{bob, alice, admin} {
		t.Run(fmt.Sprintf("As%v", user.DisplayName), func(t *testing.T) {
			tester.RequestSnapshotT(t, payload, gqltest.AsUser(user))
		})
	}
}

func TestIndexCreate(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var admin *ent.User
	tester.InitData(
		testdata.UserAdmin().Ref(&admin),
	)

	// 2. Create
	payload, err := gqlreq.IndexCreate(&model.IndexInput{
		Name: gox.NewString("testindex"),
		Rules: []*model.IndexRuleInput{
			{
				Description: gox.NewString("Test rule"),
				Actions: []*model.IndexActionInput{
					{
						Description: gox.NewString("Test action"),
					},
				},
			},
		},
	})

	if !assert.NoError(t, err) {
		return
	}

	// 3. Check
	t.Run("AsGuest", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload)
	})

	t.Run("AsAdmin", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(admin))

		exists, err := tester.Client.Index.Query().Exist(tester.Context)
		if !assert.NoError(t, err) {
			return
		}

		assert.True(t, exists)
	})
}

func TestIndexUpdate(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var index *ent.Index
	var repo *ent.Repository
	var userRole *ent.UserRole
	var bob *ent.User
	var alice *ent.User
	tester.InitData(
		testdata.RepositoryMinio().Ref(&repo),
		testdata.UserBob().Ref(&bob),
		testdata.UserAlice().Ref(&alice),
		testdata.UserRoleUser().Ref(&userRole),
		testdata.Index("testindex").Ref(&index).
			SetRules(testdata.Rule()).
			SetPermissions(
				testdata.IndexPermission().
					SetSubject(testdata.UserBob()).
					SetIndexOperations(auth.OP_Update),
			),
	)

	// 2. Update
	payload, err := gqlreq.IndexUpdate(gqlreq.IndexUpdateVars{
		ID: string(index.ID),
		Index: model.IndexInput{
			Rules: []*model.IndexRuleInput{
				{
					Description: gox.NewString("Test rule"),
					Actions: []*model.IndexActionInput{
						{
							Description: gox.NewString("Test action"),
						},
					},
				},
			},
			FileLocations: []*model.FileLocationInput{
				{
					Type:         filelocation.TypeCrawl,
					RepositoryID: repo.ID,
					Prefix:       "photos/",
				},
			},
			TagKinds: []*model.TagKindInput{
				{
					Name:           "custom",
					UserAssignable: gox.NewBool(false),
				},
			},
			Permissions: []*model.IndexPermissionsAssignmentInput{
				{
					SubjectID: userRole.ID,
					Permissions: &model.IndexPermissionsInput{
						IndexOperations: []auth.Operation{auth.OP_Read},
					},
				},
			},
		},
	})

	if !assert.NoError(t, err) {
		return
	}

	for _, user := range []*ent.User{bob, alice} {
		t.Run(fmt.Sprintf("As%v", user.DisplayName), func(t *testing.T) {
			tester.RequestSnapshotT(t, payload, gqltest.AsUser(user))
		})
	}
}

func TestIndexDelete(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var index *ent.Index
	var bob *ent.User
	var alice *ent.User
	tester.InitData(
		testdata.UserBob().Ref(&bob),
		testdata.UserAlice().Ref(&alice),
		testdata.IndexDocuments().Ref(&index),
	)

	// 2. Delete
	payload, err := gqlreq.IndexDelete(string(index.ID))
	if !assert.NoError(t, err) {
		return
	}

	// Forbidden as Alice
	t.Run("AsAlice", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(alice))
	})

	// Allowed as Bob
	t.Run("AsBob", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(bob))

		// 3. Check no record exists anymore
		exists, err := tester.Client.Index.Query().Exist(tester.Context)
		if !assert.NoError(t, err) || !assert.False(t, exists) {
			return
		}

		// 4. Check delete cascaded properly
		exists, err = tester.Client.Rule.Query().Exist(tester.Context)
		if !assert.NoError(t, err) || !assert.False(t, exists) {
			return
		}

		exists, err = tester.Client.RuleAction.Query().Exist(tester.Context)
		if !assert.NoError(t, err) || !assert.False(t, exists) {
			return
		}

		exists, err = tester.Client.FileLocation.Query().Exist(tester.Context)
		if !assert.NoError(t, err) || !assert.False(t, exists) {
			return
		}

		exists, err = tester.Client.TagKind.Query().Exist(tester.Context)
		if !assert.NoError(t, err) || !assert.False(t, exists) {
			return
		}

		exists, err = tester.Client.IndexPermission.Query().Exist(tester.Context)
		if !assert.NoError(t, err) || !assert.False(t, exists) {
			return
		}
	})
}
