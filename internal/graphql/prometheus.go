package graphql

import (
	"context"
	"fmt"
	"time"

	"github.com/99designs/gqlgen/graphql"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var timeToResolveField = promauto.NewHistogramVec(prometheus.HistogramOpts{
	Name:    "graphql_resolver_duration_ms",
	Help:    "The time taken to resolve a field by the graphql server",
	Buckets: prometheus.ExponentialBuckets(1, 2, 11),
}, []string{"status", "field"})

func init() {
	prometheus.Register(timeToResolveField)
}

func PrometheusFieldIntercept(ctx context.Context, next graphql.Resolver) (interface{}, error) {
	fc := graphql.GetFieldContext(ctx)

	since := time.Now()
	res, err := next(ctx)

	var status string
	if err == nil {
		status = "ok"
	} else {
		status = "err"
	}

	if fc.IsResolver {
		duration := float64(time.Since(since).Nanoseconds() / int64(time.Millisecond))

		timeToResolveField.WithLabelValues(
			status, fmt.Sprintf("%v.%v", fc.Object, fc.Field.Name),
		).Observe(duration)
	}

	return res, err
}
