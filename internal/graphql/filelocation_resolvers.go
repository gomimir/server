package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/graphql/generated"
	"gitlab.com/gomimir/server/internal/graphql/model"
)

func (r *fileLocationResolver) Repository(ctx context.Context, obj *ent.FileLocation) (model.Repository, error) {
	repo, err := obj.Repository(ctx)
	if err != nil {
		return nil, err
	}

	return mapRepository(repo), nil
}

// FileLocation returns generated.FileLocationResolver implementation.
func (r *Resolver) FileLocation() generated.FileLocationResolver { return &fileLocationResolver{r} }

type fileLocationResolver struct{ *Resolver }
