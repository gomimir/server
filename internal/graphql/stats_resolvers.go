package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"github.com/99designs/gqlgen/graphql"
	"gitlab.com/gomimir/server/internal/graphql/generated"
	"gitlab.com/gomimir/server/internal/graphql/model"
	"gitlab.com/gomimir/server/internal/indexer"
)

func (r *fileStatsResolver) Property(ctx context.Context, obj *model.FileStats, name string) (*indexer.PropertyStats, error) {
	field := graphql.GetFieldContext(ctx)
	return obj.PropertyStats[field.Field.Alias], nil
}

// FileStats returns generated.FileStatsResolver implementation.
func (r *Resolver) FileStats() generated.FileStatsResolver { return &fileStatsResolver{r} }

type fileStatsResolver struct{ *Resolver }
