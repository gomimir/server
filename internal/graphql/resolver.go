package graphql

import (
	"math/rand"

	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/graphql/generated"
	"gitlab.com/gomimir/server/internal/indexer"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.
type Resolver struct {
	AppVersionInfo    mimir.VersionInfo
	Indexer           indexer.Indexer
	TimeZoneOffsetStr string
	AuthProvider      auth.Provider
	TaskManager       mimir.TaskManager
	JobMonitor        mimir.JobMonitor
	LinkSignatureKey  []byte
	Rand              *rand.Rand
}

// Fix for gqlgen 0.16.0
func (r *Resolver) PropertyInput() generated.PropertyInputResolver {
	return nil
}
