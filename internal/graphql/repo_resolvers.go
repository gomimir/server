package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/auth/permissions"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/graphql/model"
)

func (r *mutationResolver) RepositoryCreate(ctx context.Context, repository *model.RepositoryInput) (*model.RepositoryMutationResult, error) {
	entClient := ent.FromContext(ctx)
	err := permissions.CheckCollectionPermissions(ctx, auth.RT_Repository, auth.OP_Create)
	if err != nil {
		return nil, err
	}

	create := entClient.Repository.Create()
	err = applyRepositoryChanges(ctx, create.Mutation(), repository)
	if err != nil {
		return nil, err
	}

	entity, err := create.Save(ctx)
	if err != nil {
		return nil, err
	}

	return &model.RepositoryMutationResult{
		Ok:         true,
		Repository: mapRepository(entity),
	}, nil
}

func (r *mutationResolver) RepositoryUpdate(ctx context.Context, id pulid.ID, repository model.RepositoryInput) (*model.RepositoryMutationResult, error) {
	entClient := ent.FromContext(ctx)
	err := permissions.CheckItemPermissions(ctx, auth.RT_Repository, auth.OP_Update)
	if err != nil {
		return nil, err
	}

	repo, err := entClient.Repository.Get(ctx, id)
	if err != nil {
		return nil, err
	}

	update := repo.Update()

	// Note: we need to prefetch the entity and set the config to be able to preserve the existing values
	update.SetConfig(repo.Config)

	err = applyRepositoryChanges(ctx, update.Mutation(), &repository)
	if err != nil {
		return nil, err
	}

	entity, err := update.Save(ctx)
	if err != nil {
		return nil, err
	}

	return &model.RepositoryMutationResult{
		Ok:         true,
		Repository: mapRepository(entity),
	}, nil
}

func (r *mutationResolver) RepositoryDelete(ctx context.Context, id pulid.ID) (*model.MutationResult, error) {
	entClient := ent.FromContext(ctx)
	err := permissions.CheckItemPermissions(ctx, auth.RT_Repository, auth.OP_Delete)
	if err != nil {
		return nil, err
	}

	err = entClient.Repository.DeleteOneID(id).Exec(ctx)
	if err != nil {
		return nil, err
	}

	return &model.MutationResult{
		Ok: true,
	}, nil
}

func (r *queryResolver) Repository(ctx context.Context, id pulid.ID) (model.Repository, error) {
	entClient := ent.FromContext(ctx)
	err := permissions.CheckItemPermissions(ctx, auth.RT_Repository, auth.OP_Read)
	if err != nil {
		return nil, err
	}

	repo, err := entClient.Repository.Get(ctx, id)
	if err != nil {
		return nil, err
	}

	return mapRepository(repo), nil
}

func (r *queryResolver) Repositories(ctx context.Context) ([]model.Repository, error) {
	entClient := ent.FromContext(ctx)

	err := permissions.CheckCollectionPermissions(ctx, auth.RT_Repository, auth.OP_List)
	if err != nil {
		return nil, err
	}

	err = permissions.CheckItemPermissions(ctx, auth.RT_Repository, auth.OP_Read)
	if err != nil {
		return nil, err
	}

	res, err := entClient.Repository.Query().All(ctx)
	if err != nil {
		return nil, err
	}

	repos := make([]model.Repository, len(res))
	for i, repo := range res {
		repos[i] = mapRepository(repo)
	}

	return repos, nil
}
