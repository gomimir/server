package model

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"reflect"
	"time"

	mimir "gitlab.com/gomimir/processor"
)

// PropertyInput - property pair wrapper with custom marshaling logic
type PropertyInput struct {
	mimir.PropertyPair
}

// MarshalGQL - mashaling is not implemented,
// PropertyInput is an input type therefore it is not serialized back to GraphQL.
// But the gqlgen still checking its presence in order to recognize the marsling interface.
func (pi PropertyInput) MarshalGQL(w io.Writer) {
	panic(errors.New("not implemented"))
}

// UnmarshalGQL - custom unmarshaling logic leveraging pair unserialization from mimir package
func (pi *PropertyInput) UnmarshalGQL(v interface{}) error {
	if v != nil {
		if data, ok := v.(map[string]interface{}); ok {
			err := mimir.UnserializePropertyPair(data, &pi.PropertyPair)
			if err != nil {
				return err
			}

			return nil
		}

		return fmt.Errorf("unable to unmarshal property value, expected object, got: %v", reflect.TypeOf(v))
	}

	return nil
}

func MarshalProperties(props mimir.Properties) ([]Property, error) {
	var result []Property
	for _, pair := range props.Pairs() {
		k := pair.Name
		v := pair.Value

		if v == nil {
			continue
		}

		switch v := v.(type) {
		case string:
			result = append(result, TextProperty{
				Name:      k,
				TextValue: v,
			})

		case float64:
			result = append(result, NumberProperty{
				Name:         k,
				NumericValue: v,
			})

		case json.Number:
			vf, err := v.Float64()
			if err != nil {
				return nil, fmt.Errorf("unable to convert JSON number %v into 64-bit float (key: %v)", v, k)
			}

			result = append(result, NumberProperty{
				Name:         k,
				NumericValue: vf,
			})

		case mimir.GPS:
			result = append(result, GPSProperty{
				Name:      k,
				Latitude:  v.Latitude,
				Longitude: v.Longitude,
			})

		case mimir.Fraction:
			result = append(result, FractionProperty{
				Name:        k,
				Numerator:   int(v.Numerator),
				Denominator: int(v.Denominator),
			})

		case time.Time:
			result = append(result, TimeProperty{
				Name:      k,
				TimeValue: v,
			})

		default:
			return nil, fmt.Errorf("unable to convert value %v into graphql type (key: %v)", reflect.TypeOf(v), k)
		}
	}

	return result, nil
}
