package model

import "gitlab.com/gomimir/server/internal/indexer"

type PropertyHolder interface {
	IsPropertyHolder()
}

type AssetListingFacets struct {
	FacetResults *indexer.FacetResults
}

func (AssetListingFacets) IsPropertyHolder() {}

type FileFacets struct {
	FileFacetResult *indexer.FileFacetResult
}

func (FileFacets) IsPropertyHolder() {}

type PropertyFacets struct {
	PropertyFacetResults *indexer.PropertyFacetResult
}
