package model

import "gitlab.com/gomimir/server/internal/indexer"

type AssetListingStats struct {
	Files *FileStats `json:"files"`
}

type FileStats struct {
	PropertyStats map[string]*indexer.PropertyStats
}
