package model

import (
	"fmt"
	"io"
	"reflect"
	"strconv"

	"github.com/99designs/gqlgen/graphql"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/apperror"
)

func MarshalAssetID(ar mimir.AssetRef) graphql.Marshaler {
	return graphql.WriterFunc(func(writer io.Writer) {
		io.WriteString(writer, strconv.Quote(ar.String()))
	})
}

func UnmarshalAssetID(v interface{}) (mimir.AssetRef, error) {
	if v != nil {
		if data, ok := v.(string); ok {
			assetRef := mimir.AssetRefFromString(data)
			if !assetRef.IsValid() {
				return assetRef, apperror.New(apperror.InvalidID, "invalid asset ID: %v", assetRef.String())
			}

			return assetRef, nil
		}

		return mimir.AssetRef{}, fmt.Errorf("unable to unmarshal asset id, expected string, got: %v", reflect.TypeOf(v))
	}

	return mimir.AssetRef{}, nil
}
