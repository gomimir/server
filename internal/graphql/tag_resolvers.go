package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"strings"

	"github.com/99designs/gqlgen/graphql"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/auth/permissions"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/indexer"
)

func (r *queryResolver) Tags(ctx context.Context, kind []string, index []pulid.ID, nameQuery *string, sort *indexer.TagSortRequest) ([]*indexer.TagListingEntry, error) {
	if len(index) == 0 {
		return nil, nil
	}

	indexNames := make([]string, len(index))
	for i, indexID := range index {
		err := permissions.CheckAssetCollectionPermissions(ctx, indexID, auth.OP_List)
		if err != nil {
			return nil, err
		}

		err = permissions.CheckAssetPermissions(ctx, indexID, auth.OP_Read)
		if err != nil {
			return nil, err
		}

		indexNames[i] = strings.ToLower(string(indexID))
	}

	opts := indexer.TagListingOptions{
		IndexNames: indexNames,
		Kind:       kind,
		NameQuery:  nameQuery,
		Sort:       sort,
	}

	fields := graphql.CollectFieldsCtx(ctx, nil)
	for _, f := range fields {
		if f.Name == "coverAsset" {
			opts.FetchCoverAsset = true
			break
		}
	}

	return r.Indexer.GetTags(opts)
}
