package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/graphql/generated"
	"gitlab.com/gomimir/server/internal/graphql/model"
)

func (r *indexPermissionsAssignmentResolver) Subject(ctx context.Context, obj *model.IndexPermissionsAssignment) (model.PermissionsSubject, error) {
	entClient := ent.FromContext(ctx)
	subj := obj.Subject.(pulid.ID)

	if strings.HasPrefix(string(subj), "UA") {
		return entClient.User.Get(ctx, subj)
	}

	if strings.HasPrefix(string(subj), "UR") {
		return entClient.UserRole.Get(ctx, subj)
	}

	return nil, fmt.Errorf("unable to resolve subject %v", subj)
}

// IndexPermissionsAssignment returns generated.IndexPermissionsAssignmentResolver implementation.
func (r *Resolver) IndexPermissionsAssignment() generated.IndexPermissionsAssignmentResolver {
	return &indexPermissionsAssignmentResolver{r}
}

type indexPermissionsAssignmentResolver struct{ *Resolver }
