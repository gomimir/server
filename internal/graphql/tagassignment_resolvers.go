package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/gomimir/server/internal/documents"
	"gitlab.com/gomimir/server/internal/graphql/generated"
)

func (r *tagAssignmentResolver) Reasons(ctx context.Context, obj *documents.TagAssignment) ([]documents.TagAssignmentReason, error) {
	return obj.Reasons, nil
}

// TagAssignment returns generated.TagAssignmentResolver implementation.
func (r *Resolver) TagAssignment() generated.TagAssignmentResolver { return &tagAssignmentResolver{r} }

type tagAssignmentResolver struct{ *Resolver }
