package gqltest

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http/httptest"
	"testing"

	"github.com/bradleyjkemp/cupaloy"
	"github.com/stretchr/testify/assert"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/auth/permissions"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/ent/testdata"
	"gitlab.com/gomimir/server/internal/graphql"
	"gitlab.com/gomimir/server/internal/idprovider"
	"gitlab.com/gomimir/server/internal/indexer"
	"gitlab.com/gomimir/server/internal/indexer/memoryindexer"
	"gitlab.com/gomimir/server/internal/secret"
	"gitlab.com/gomimir/server/internal/testutils"

	_ "modernc.org/sqlite"
)

type tester struct {
	T        *testing.T
	Context  context.Context
	Client   *ent.Client
	Indexer  indexer.Indexer
	TestData *testdata.TestDataBuilder
}

func NewTester(t *testing.T) *tester {
	ctx := idprovider.ProviderContext(context.Background(), idprovider.NewTestProvider())
	ctx = secret.KeeperContext(ctx, secret.NewTestKeeper())

	client := testutils.EntClient(t)
	ctx = ent.NewContext(ctx, client)

	indexer := memoryindexer.NewIndexer()

	return &tester{
		Context:  ctx,
		Client:   client,
		Indexer:  indexer,
		TestData: testdata.NewTestDataBuilder(ctx),
		T:        t,
	}
}

func (t *tester) InitData(reqs ...testdata.TestDataRequest) {
	t.TestData.Require(reqs...)
}

func (t *tester) Request(payload []byte, opts ...TestRequestOption) map[string]interface{} {
	return t.RequestT(t.T, payload, opts...)
}

func (t *tester) RequestT(childT *testing.T, payload []byte, opts ...TestRequestOption) map[string]interface{} {
	r := httptest.NewRequest("POST", "/graphql", bytes.NewReader(payload))
	r.Header.Add("Content-Type", "application/json")

	mergedOpts := &TestRequestOptions{}
	for _, opt := range opts {
		opt(mergedOpts)
	}

	ctx := t.Context
	var session *auth.Session
	if mergedOpts.user != nil {
		roles, err := mergedOpts.user.Roles(ctx)
		if err != nil {
			childT.Error(err)
			childT.FailNow()
		}

		roleIDs := make([]pulid.ID, len(roles))
		for i, role := range roles {
			roleIDs[i] = role.ID
		}

		session = auth.NewAuthenticatedSession(mergedOpts.user.ID, nil, roleIDs...)
	} else {
		var guestRole *ent.UserRole
		t.InitData(testdata.UserRoleGuest().Ref(&guestRole))

		session = auth.NewSession(auth.Session_AuthNotAttempted, guestRole.ID)
	}

	ctx = permissions.CacheContext(ctx)
	r = r.WithContext(ctx)

	w := httptest.NewRecorder()

	version, err := mimir.VersionInfoFromStr("0.0.1")
	if err != nil {
		panic(err)
	}

	graphql.HTTPHandler(graphql.HTTPHandlerOpts{
		Indexer:           t.Indexer,
		AppVersionInfo:    version,
		AuthProvider:      &testAuthProvider{session: session},
		TimeZoneOffsetStr: "+01:00",
		TaskManager:       &testTaskManager{},
		JobMonitor:        &testJobMonitor{},
		LinkSignatureKey:  []byte("abcdefghijklmnop"),
	}).ServeHTTP(w, r)

	if !assert.Equal(childT, 200, w.Code) {
		childT.Log(w.Body.String())
	}

	var data map[string]interface{}
	if err := json.Unmarshal(w.Body.Bytes(), &data); err != nil {
		childT.Error(err)
		childT.FailNow()
	}

	if mergedOpts.assertNoErrors {
		if errors, ok := data["errors"]; ok {
			if errors, ok := errors.([]interface{}); ok {
				if len(errors) > 0 {
					childT.Log(errors)
					childT.FailNow()
				}
			}
		}
	}

	return data
}

func (t *tester) RequestSnapshot(payload []byte, opts ...TestRequestOption) {
	t.RequestSnapshotT(t.T, payload, opts...)
}

func (t *tester) RequestSnapshotT(childT *testing.T, payload []byte, opts ...TestRequestOption) {
	data := t.RequestT(childT, payload, opts...)
	formatted, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		childT.Error(err)
		childT.FailNow()
	}

	snapshotter := cupaloy.New(cupaloy.SnapshotFileExtension(".json"))
	snapshotter.SnapshotT(childT, string(formatted))
}

func (t *tester) Close() {
	err := t.Client.Close()
	if err != nil {
		t.T.Error(err)
	}
}
