package gqltest

import (
	"gitlab.com/gomimir/server/internal/ent"
)

type TestRequestOptions struct {
	assertNoErrors bool
	user           *ent.User
}

type TestRequestOption func(opts *TestRequestOptions)

func AssertNoErrors() func(opts *TestRequestOptions) {
	return func(opts *TestRequestOptions) {
		opts.assertNoErrors = true
	}
}

func AsUser(user *ent.User) func(opts *TestRequestOptions) {
	return func(opts *TestRequestOptions) {
		opts.user = user
	}
}
