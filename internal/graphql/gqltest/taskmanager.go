package gqltest

import (
	"context"
	"fmt"

	mimir "gitlab.com/gomimir/processor"
)

// mimir.TaskManager
type testTaskManager struct{}

func (*testTaskManager) EnsureReady(ctx context.Context, opts mimir.RetryOpts) error {
	return nil
}

func (*testTaskManager) QueueJob(ctx context.Context, opts mimir.QueueJobOpts) (*mimir.QueueJobResult, error) {
	panic(fmt.Errorf("not implemented"))
}

func (*testTaskManager) QueueTask(ctx context.Context, opts mimir.QueueTaskOpts) ([]mimir.Task, error) {
	panic(fmt.Errorf("not implemented"))
}

func (*testTaskManager) CancelTask(ctx context.Context, queue string, taskID string) error {
	panic(fmt.Errorf("not implemented"))
}

func (*testTaskManager) GetJobs(ctx context.Context, opts mimir.GetJobsOpts) ([]mimir.JobInfo, error) {
	return nil, nil
}

func (*testTaskManager) MonitorJobs(ctx context.Context) (mimir.JobMonitor, error) {
	return &testJobMonitor{}, nil
}

func (*testTaskManager) ProcessQueue(ctx context.Context, opts mimir.ProcessQueueOpts) error {
	panic(fmt.Errorf("not implemented"))
}

func (*testTaskManager) ProcessTaskReports(ctx context.Context, opts mimir.ProcessTaskReportsOpts) error {
	panic(fmt.Errorf("not implemented"))
}

// mimir.JobMonitor
type testJobMonitor struct{}

func (*testJobMonitor) GetJobState(ctx context.Context, jobID string) (mimir.JobState, error) {
	panic(fmt.Errorf("not implemented"))
}

func (*testJobMonitor) Subscribe(ctx context.Context) <-chan mimir.JobMonitorEvent {
	return make(chan mimir.JobMonitorEvent)
}

func (*testJobMonitor) Done() <-chan error {
	return make(chan error)
}
