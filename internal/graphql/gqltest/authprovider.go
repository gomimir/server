package gqltest

import (
	"context"
	"net/http"

	"gitlab.com/gomimir/server/internal/auth"
)

type testAuthProvider struct {
	session *auth.Session
}

func (p *testAuthProvider) Type() auth.ProviderType {
	return auth.ProviderType("TEST")
}

func (p *testAuthProvider) WebsocketInit(ctx context.Context, initPayload map[string]interface{}) (context.Context, error) {
	return auth.SessionContext(ctx, p.session), nil
}

func (p *testAuthProvider) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := auth.SessionContext(r.Context(), p.session)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
