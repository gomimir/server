package graphql

import (
	"context"
	"fmt"

	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/auth/permissions"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/ent/schema/schematype"
	"gitlab.com/gomimir/server/internal/graphql/model"
	"gitlab.com/gomimir/server/internal/secret"
)

func mapRepository(repo *ent.Repository) model.Repository {
	if repo.Config != nil {
		if repo.Config.S3 != nil {
			return &model.S3Repository{
				ID:        repo.ID,
				Type:      model.RepositoryTypeS3,
				Name:      repo.Name,
				Endpoint:  repo.Config.S3.Endpoint,
				UseSsl:    repo.Config.S3.UseSSL,
				CaCert:    repo.Config.S3.CACert,
				AccessKey: repo.Config.S3.AccessKey,
			}
		}

		if repo.Config.Local != nil {
			return &model.LocalRepository{
				ID:   repo.ID,
				Type: model.RepositoryTypeLocal,
				Name: repo.Name,
				Path: repo.Config.Local.Path,
			}
		}
	}

	return &model.UnknownRepository{
		ID:   repo.ID,
		Type: model.RepositoryTypeUnknown,
		Name: repo.Name,
	}
}

func applyRepositoryChanges(ctx context.Context, mutation *ent.RepositoryMutation, input *model.RepositoryInput) error {
	if input != nil {
		if input.Name != nil {
			mutation.SetName(*input.Name)
		}

		if input.S3 != nil {
			cfg, _ := mutation.Config()
			if cfg == nil {
				cfg = &schematype.RepositoryConfig{}
			} else {
				cfg = cfg.Clone()
				cfg.Local = nil
			}

			if cfg.S3 == nil {
				cfg.S3 = &schematype.S3RepositoryConfig{}
			}

			if input.S3.Endpoint != nil {
				cfg.S3.Endpoint = *input.S3.Endpoint
			}

			if input.S3.UseSsl != nil {
				cfg.S3.UseSSL = *input.S3.UseSsl
			}

			if input.S3.CaCert != nil {
				cfg.S3.CACert = *input.S3.CaCert
			}

			if input.S3.AccessKey != nil {
				cfg.S3.AccessKey = *input.S3.AccessKey
			}

			if input.S3.SecretKey != nil {
				keeper, err := secret.KeeperForContext(ctx)
				if err != nil {
					return err
				}

				secret, err := keeper.EncryptString(*input.S3.SecretKey)
				if err != nil {
					return err
				}

				cfg.S3.SecretKey = secret
			}

			mutation.SetConfig(cfg)
		}

		if input.Local != nil {
			if input.S3 != nil {
				// TODO: this should be a specialized error, otherwise it is going to be masked to the end-user
				return fmt.Errorf("local and s3 repository configuration is mutually exclusive")
			}

			cfg, _ := mutation.Config()
			if cfg == nil {
				cfg = &schematype.RepositoryConfig{}
			} else {
				cfg = cfg.Clone()
				cfg.S3 = nil
			}

			if cfg.Local == nil {
				cfg.Local = &schematype.LocalRepositoryConfig{}
			}

			if input.Local.Path != nil {
				cfg.Local.Path = *input.Local.Path
			}

			mutation.SetConfig(cfg)
		}
	}

	return nil
}

func checkRulePermission(ctx context.Context, ruleID pulid.ID) error {
	entClient := ent.FromContext(ctx)

	ruleEnt, err := entClient.Rule.Get(ctx, ruleID)
	if err != nil {
		return err
	}

	err = permissions.CheckIndexPermissions(ctx, ruleEnt.IndexID, auth.OP_Update)
	if err != nil {
		return err
	}

	return nil
}
