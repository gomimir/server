package graphql_test

import (
	"testing"

	"github.com/icza/gox/gox"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/ruleaction"
	"gitlab.com/gomimir/server/internal/ent/schema/schematype"
	"gitlab.com/gomimir/server/internal/ent/testdata"
	"gitlab.com/gomimir/server/internal/graphql/gqlreq"
	"gitlab.com/gomimir/server/internal/graphql/gqltest"
	"gitlab.com/gomimir/server/internal/graphql/model"
)

func TestActionCreate(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var rule *ent.Rule
	var bob *ent.User
	var alice *ent.User
	tester.InitData(
		testdata.UserBob().Ref(&bob),
		testdata.UserAlice().Ref(&alice),
		testdata.Index("testindex").
			SetPermissions(
				testdata.IndexPermission().SetSubject(testdata.UserBob()).SetIndexOperations(auth.OP_Update),
			).
			SetRules(testdata.Rule().Ref(&rule)),
	)

	// 2. Create
	payload, err := gqlreq.ActionCreate(gqlreq.ActionCreateVars{
		RuleID: string(rule.ID),
		Action: &model.IndexActionInput{
			Description: gox.NewString("Test action"),
			Config: &schematype.ActionConfig{
				Process: &schematype.ProcessActionConfig{
					Request: "doSomething",
				},
			},
		},
	})

	if !assert.NoError(t, err) {
		return
	}

	t.Run("AsAlice", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload)
	})

	t.Run("AsBob", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(bob))

		// 3. Check
		exists, err := tester.Client.RuleAction.Query().Where(ruleaction.RuleID(rule.ID)).Exist(tester.Context)
		if !assert.NoError(t, err) {
			return
		}

		assert.True(t, exists)
	})
}

func TestActionUpdate(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var action *ent.RuleAction
	var bob *ent.User
	var alice *ent.User
	tester.InitData(
		testdata.UserBob().Ref(&bob),
		testdata.UserAlice().Ref(&alice),
		testdata.Index("testindex").
			SetPermissions(
				testdata.IndexPermission().SetSubject(testdata.UserBob()).SetIndexOperations(auth.OP_Update),
			).
			SetRules(testdata.Rule().SetActions(
				testdata.RuleAction().Ref(&action).SetByCallback(func(rac *ent.RuleActionCreate) {
					rac.SetConfig(&schematype.ActionConfig{SetKey: gox.NewString("foo")})
				}),
			)),
	)

	// 2. Update
	payload, err := gqlreq.ActionUpdate(gqlreq.ActionUpdateVars{
		ID: string(action.ID),
		Action: model.IndexActionInput{
			Description: gox.NewString("Test action"),
		},
	})

	if !assert.NoError(t, err) {
		return
	}

	t.Run("AsAlice", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload)
	})

	t.Run("AsBob", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(bob))

		// 3. Check
		actual, err := tester.Client.RuleAction.Get(tester.Context, action.ID)
		if !assert.NoError(t, err) {
			return
		}

		assert.Equal(t, "Test action", actual.Description)
	})
}

func TestActionDelete(t *testing.T) {
	tester := gqltest.NewTester(t)
	defer tester.Close()

	// 1. Initial setup
	var action *ent.RuleAction
	var bob *ent.User
	var alice *ent.User
	tester.InitData(
		testdata.UserBob().Ref(&bob),
		testdata.UserAlice().Ref(&alice),
		testdata.Index("testindex").
			SetPermissions(
				testdata.IndexPermission().SetSubject(testdata.UserBob()).SetIndexOperations(auth.OP_Update),
			).
			SetRules(testdata.Rule().SetActions(
				testdata.RuleAction().Ref(&action).SetByCallback(func(rac *ent.RuleActionCreate) {
					rac.SetConfig(&schematype.ActionConfig{SetKey: gox.NewString("foo")})
				}),
			)),
	)

	// 2. Delete
	payload, err := gqlreq.ActionDelete(string(action.ID))

	if !assert.NoError(t, err) {
		return
	}

	t.Run("AsAlice", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload)
	})

	t.Run("AsBob", func(t *testing.T) {
		tester.RequestSnapshotT(t, payload, gqltest.AsUser(bob))

		// 3. Check no record exists anymore
		numActions, err := tester.Client.RuleAction.Query().Count(tester.Context)
		if !assert.NoError(t, err) {
			return
		}

		assert.Equal(t, 0, numActions)
	})
}
