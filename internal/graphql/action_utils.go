package graphql

import (
	"context"

	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/auth/permissions"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/graphql/model"
)

func applyActionChanges(mutation *ent.RuleActionMutation, input *model.IndexActionInput) {
	if input != nil {
		if input.Description != nil {
			mutation.SetDescription(*input.Description)
		}

		if input.Enabled != nil {
			mutation.SetEnabled(*input.Enabled)
		}

		if input.Config != nil {
			mutation.SetConfig(input.Config)
		}
	}
}

func checkActionPermission(ctx context.Context, actionID pulid.ID) error {
	entClient := ent.FromContext(ctx)

	actionEnt, err := entClient.RuleAction.Get(ctx, actionID)
	if err != nil {
		return err
	}

	ruleEnt, err := entClient.Rule.Get(ctx, actionEnt.RuleID)
	if err != nil {
		return err
	}

	err = permissions.CheckIndexPermissions(ctx, ruleEnt.IndexID, auth.OP_Update)
	if err != nil {
		return err
	}

	return nil
}
