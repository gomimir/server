package graphql

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/auth/permissions"
	"gitlab.com/gomimir/server/internal/ent"
)

func (r *queryResolver) UserRoles(ctx context.Context) ([]*ent.UserRole, error) {
	entClient := ent.FromContext(ctx)
	err := permissions.CheckCollectionPermissions(ctx, auth.RT_UserRole, auth.OP_List)
	if err != nil {
		return nil, err
	}

	err = permissions.CheckItemPermissions(ctx, auth.RT_UserRole, auth.OP_Read)
	if err != nil {
		return nil, err
	}

	return entClient.UserRole.Query().All(ctx)
}
