package secret

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"

	"github.com/lestrrat-go/jwx/jwa"
	"github.com/lestrrat-go/jwx/jwe"
)

type aesKeeper struct {
	key []byte
}

func NewAESKeeperFromBase64Str(base64KeyStr string) (Keeper, error) {
	keyBytes, err := base64.StdEncoding.DecodeString(base64KeyStr)
	if err != nil {
		return nil, fmt.Errorf("malformed key: %v", err)
	}

	return NewAESKeeper(keyBytes)
}

func NewAESKeeperWithRandKey() (Keeper, error) {
	key := make([]byte, 16)
	_, err := rand.Read(key)
	if err != nil {
		return nil, err
	}

	return NewAESKeeper(key)
}

func NewAESKeeper(key []byte) (Keeper, error) {
	if len(key) < 16 {
		return nil, fmt.Errorf("invalid key size, expected at least 16 bytes, got %v", len(key))
	}

	if len(key)%8 != 0 {
		return nil, fmt.Errorf("invalid key size, must be divisible by 8, got %v", len(key))
	}

	return &aesKeeper{key}, nil
}

func (k *aesKeeper) EncryptString(plain string) (string, error) {
	return k.encryptBytes([]byte(plain))
}

func (k *aesKeeper) Encrypt(obj interface{}) (string, error) {
	plainBytes, err := json.Marshal(obj)
	if err != nil {
		return "", err
	}

	return k.encryptBytes(plainBytes)
}

func (k *aesKeeper) encryptBytes(plainBytes []byte) (string, error) {
	outBytes, err := jwe.Encrypt(plainBytes, jwa.A128KW, k.key, jwa.A128CBC_HS256, jwa.NoCompress)
	if err != nil {
		return "", err
	}

	return base64Enc.EncodeToString(outBytes), nil
}

func (k *aesKeeper) DecryptString(secret string) (string, error) {
	plainBytes, err := k.decryptBytes(secret)
	if err != nil {
		return "", err
	}

	return string(plainBytes), nil
}

func (k *aesKeeper) Decrypt(secret string, out interface{}) error {
	plainBytes, err := k.decryptBytes(secret)
	if err != nil {
		return err
	}

	return json.Unmarshal(plainBytes, out)
}

func (k *aesKeeper) decryptBytes(secret string) ([]byte, error) {
	encryptedBytes, err := base64Enc.DecodeString(secret)
	if err != nil {
		return nil, err
	}

	return jwe.Decrypt(encryptedBytes, jwa.A128KW, k.key)
}
