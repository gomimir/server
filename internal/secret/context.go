package secret

import (
	"context"
	"errors"
)

type contextKey struct {
	name string
}

var ctxKey = &contextKey{"secretKeeper"}

func KeeperContext(parent context.Context, keeper Keeper) context.Context {
	return context.WithValue(parent, ctxKey, keeper)
}

func KeeperForContext(ctx context.Context) (Keeper, error) {
	if keeper, ok := ctx.Value(ctxKey).(Keeper); ok {
		return keeper, nil
	}

	return nil, errors.New("unable to retrieve secret keeper from context")
}
