package secret

import (
	"encoding/base64"
	"encoding/json"
)

var base64Enc = base64.StdEncoding.WithPadding(base64.NoPadding)

type testKeeper struct{}

func NewTestKeeper() Keeper {
	return &testKeeper{}
}

func (k *testKeeper) EncryptString(plain string) (string, error) {
	return base64Enc.EncodeToString([]byte(plain)), nil
}

func (k *testKeeper) Encrypt(obj interface{}) (string, error) {
	plainBytes, err := json.Marshal(obj)
	if err != nil {
		return "", err
	}

	return base64Enc.EncodeToString(plainBytes), nil
}

func (k *testKeeper) DecryptString(secret string) (string, error) {
	bytes, err := base64Enc.DecodeString(secret)
	if err != nil {
		return "", err
	}

	return string(bytes), nil
}

func (k *testKeeper) Decrypt(secret string, out interface{}) error {
	bytes, err := base64Enc.DecodeString(secret)
	if err != nil {
		return err
	}

	return json.Unmarshal(bytes, out)
}
