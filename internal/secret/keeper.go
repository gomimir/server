package secret

type Keeper interface {
	Encrypt(obj interface{}) (string, error)
	EncryptString(plain string) (string, error)

	Decrypt(secret string, out interface{}) error
	DecryptString(secret string) (string, error)
}
