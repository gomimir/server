package apperror

// Error code enum
type Code string

const (

	// -----------------------------------
	// Auth.

	// Current session is not authenticated. You need to login first
	Unauthorized Code = "UNAUTHORIZED"

	// User of current session is forbidden to access the resource or perform an action
	Forbidden Code = "FORBIDDEN"

	// User is trying invoke action which is not supported by the configured auth. provider
	UnsupportedAuth Code = "UNSUPPORTED_AUTH"

	// -----------------------------------
	// Repository

	// Note: occurs for example in S3 repo if we are not able to parse bucket name out of it
	InvalidFilename Code = "INVALID_FILENAME"

	// Referenced file was not found in the repository
	FileNotFound Code = "FILE_NOT_FOUND"

	// -----------------------------------
	// Uploads

	// Invalid session argument for uploadMore
	InvalidUploadSession Code = "INVALID_UPLOAD_SESSION"

	// Whenever we receive upload request with malformed filenames (ie. empty)
	InvalidUploadFilename Code = "INVALID_UPLOAD_FILENAME"

	// -----------------------------------
	// Indexer

	// Unable to parse given asset id
	InvalidID Code = "INVALID_ID"

	// Referenced document was not found
	NotFound Code = "NOT_FOUND"

	// Given file set is not valid (ie. not subset of what is registered for the index)
	InvalidFileSet Code = "INVALID_FILE_SET"
)
