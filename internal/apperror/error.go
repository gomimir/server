package apperror

import (
	"errors"
	"fmt"

	grpcerrdetails "google.golang.org/genproto/googleapis/rpc/errdetails"
	grpccodes "google.golang.org/grpc/codes"
	grpcstatus "google.golang.org/grpc/status"
)

type Error interface {
	Code() Code
	Error() string
}

func New(code Code, message string, values ...interface{}) Error {
	if len(values) > 0 {
		message = fmt.Sprintf(message, values...)
	}

	return &indexerError{
		code:    code,
		message: message,
	}
}

func MatchesCode(err error, code Code) bool {
	var myErr Error
	if errors.As(err, &myErr) {
		if myErr.Code() == code {
			return true
		}
	}

	return false
}

type indexerError struct {
	code    Code
	message string
}

func (err *indexerError) Code() Code {
	return err.code
}

func (err *indexerError) Error() string {
	return err.message
}

func (err indexerError) GRPCStatus() *grpcstatus.Status {
	st := grpcstatus.New(trGRPCCode(err.code), err.message)

	ei := &grpcerrdetails.ErrorInfo{
		Domain: "gomimir.gitlab.io",
		Reason: string(err.code),
	}

	stWithDetails, stErr := st.WithDetails(ei)
	if stErr != nil {
		return st
	}

	return stWithDetails
}

func trGRPCCode(appcode Code) grpccodes.Code {
	switch appcode {
	case Unauthorized:
		return grpccodes.Unauthenticated
	case Forbidden:
		return grpccodes.PermissionDenied

	case InvalidUploadSession:
		return grpccodes.InvalidArgument
	case InvalidUploadFilename:
		return grpccodes.InvalidArgument

	case InvalidID:
		return grpccodes.InvalidArgument
	case InvalidFileSet:
		return grpccodes.InvalidArgument
	case InvalidFilename:
		return grpccodes.InvalidArgument

	case NotFound:
		return grpccodes.NotFound

	default:
		return grpccodes.Internal
	}
}
