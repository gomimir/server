package utils

import (
	"crypto/md5"
	"encoding/base64"
	"path/filepath"
	"strings"
)

// EnsureTrailingSlash - ensures that the path is ending with /
func EnsureTrailingSlash(path string) string {
	if strings.HasSuffix(path, "/") {
		return path
	} else {
		return path + "/"
	}
}

// EnsureNoTrailingSlash - ensures that the path is not ending with /
func EnsureNoTrailingSlash(path string) string {
	return strings.Trim(path, "/")
}

// SanitizeRelPath - Sanitizes path to prevent escaping the directory jail
// 1) cleans . and .. and repeated slashes with filepath.Clean
// 2) removes initial slash
// 3) removes any leading ..
func SanitizeRelPath(path string) string {
	if len(path) > 0 {
		return filepath.Clean("/" + path)[1:]
	}

	return path
}

// HashFilename - Replaces the whole path with a hash, keeps the file extension
func HashFilename(path string) string {
	ext := filepath.Ext(path)
	rest := path[:len(path)-len(ext)]
	hash := md5.Sum([]byte(rest))
	hashStr := base64.URLEncoding.WithPadding(base64.NoPadding).EncodeToString(hash[:])
	return hashStr + ext
}

// JoinPath - this is pretty much filepath.Join but with posix only paths
func JoinPath(tokens ...string) string {
	str := ""
	for _, token := range tokens {
		if len(str) == 0 {
			str = token
		} else {
			if str[len(str)-1] != '/' {
				str = str + "/"
			}

			str = str + token
		}
	}

	return str
}
