package utils

import "fmt"

type Diff struct {
	Path  []string
	Left  interface{}
	Right interface{}
}

type matchContext struct {
	path        []string
	differences *[]Diff
}

func (ctx *matchContext) Child(key string) *matchContext {
	child := &matchContext{
		differences: ctx.differences,
		path:        make([]string, len(ctx.path)+1),
	}

	if len(ctx.path) > 0 {
		copy(child.path, ctx.path)
	}

	child.path[len(child.path)-1] = key
	return child
}

func (ctx *matchContext) ReportDifference(a, b interface{}) {
	*ctx.differences = append(*ctx.differences, Diff{
		Path:  ctx.path,
		Left:  a,
		Right: b,
	})
}

func ObjMatch(a map[string]interface{}, b map[string]interface{}) []Diff {
	var diff []Diff
	ctx := &matchContext{differences: &diff}
	objMatch(a, b, ctx)
	return diff
}

func objMatch(a map[string]interface{}, b map[string]interface{}, ctx *matchContext) bool {
	for k, aV := range a {
		if bV, ok := b[k]; ok {
			if !match(aV, bV, ctx.Child(k)) {
				return false
			}
		} else {
			ctx.Child(k).ReportDifference(aV, nil)
			return false
		}
	}

	return true
}

func sliceMatch(a []interface{}, b []interface{}, ctx *matchContext) bool {
	if len(a) != len(b) {
		ctx.ReportDifference(a, b)
		return false
	}

	if len(a) == 0 {
		return true
	}

	for i, aV := range a {
		if !match(aV, b[i], ctx.Child(fmt.Sprintf("%v", i))) {
			return false
		}
	}

	return true
}

func match(aV interface{}, bV interface{}, ctx *matchContext) bool {
	if aV == nil && bV == nil {
		return true
	}

	if (aV == nil && bV != nil) || (bV == nil && aV != nil) {
		ctx.ReportDifference(aV, bV)
		return false
	}

	if aMap, ok := aV.(map[string]interface{}); ok {
		if bMap, ok := bV.(map[string]interface{}); ok {
			if !objMatch(aMap, bMap, ctx) {
				return false
			}
		} else {
			ctx.ReportDifference(aV, bV)
			return false
		}

	} else if aSlice, ok := aV.([]interface{}); ok {
		if bSlice, ok := bV.([]interface{}); ok {
			if !sliceMatch(aSlice, bSlice, ctx) {
				return false
			}
		} else {
			ctx.ReportDifference(aV, bV)
			return false
		}

	} else if aV != bV {
		ctx.ReportDifference(aV, bV)
		return false
	}

	return true
}
