package utils_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/utils"
)

func TestLinkSigning(t *testing.T) {

	repository := "photos"
	filename := "photos/someAlbum/image12.jpg"
	salt := []byte("abcdefghijklmnop")
	key := []byte("xyzuijkfheayeeedsa")

	signature, err := utils.SignLink(utils.SignLinkOpts{
		Data:       []interface{}{repository, filename, salt},
		StartIndex: 2,
		Key:        key,
	})

	if assert.NoError(t, err) {
		// spew.Dump(signature)

		data, err := utils.CheckLinkSignature(utils.VerifyLinkSignatureOpts{
			Data:      []interface{}{repository, filename},
			Signature: signature,
			Key:       key,
		})

		if assert.NoError(t, err) {
			assert.Equal(t, []interface{}{repository, filename, salt}, data)
		}

		_, err = utils.CheckLinkSignature(utils.VerifyLinkSignatureOpts{
			Data:      []interface{}{repository, filename},
			Signature: signature[:4] + "X" + signature[5:],
			Key:       key,
		})

		if assert.Error(t, err) {
			assert.EqualError(t, err, "invalid signature")
		}
	}
}
