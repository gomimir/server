package utils

import (
	"reflect"
	"strings"
)

// ToMap - converts data from an interface into map while honoring JSON renames
func ToMap(data interface{}) map[string]interface{} {
	if data != nil {
		result := make(map[string]interface{})

		dataValue := reflect.ValueOf(data)
		if dataValue.Type().Kind() == reflect.Ptr {
			dataValue = dataValue.Elem()
		}

		dataType := dataValue.Type()

		if dataType.Kind() == reflect.Struct {
			for i := 0; i < dataValue.NumField(); i++ {
				fieldType := dataType.Field(i)
				fieldValue := dataValue.Field(i)

				if fieldType.Type.Kind() == reflect.Ptr {
					if fieldValue.IsNil() {
						continue
					} else {
						fieldValue = fieldValue.Elem()
					}
				}

				key := fieldType.Name
				jsonTag := fieldType.Tag.Get("json")
				if jsonTag != "" {
					tokens := strings.Split(jsonTag, ",")
					if len(tokens) > 0 && tokens[0] != "" {
						key = tokens[0]
					}
				}

				result[key] = fieldValue.Interface()
			}
		} else {
			panic("Expected struct")
		}

		return result
	}

	return nil
}

// DeeplyGet retrieves data on given path
func DeeplyGet(data map[string]interface{}, keys ...string) interface{} {
	curr := data
	for idx, key := range keys {
		if value, ok := curr[key]; ok {
			if idx == len(keys)-1 {
				return value
			}

			if valueAsMap, ok := value.(map[string]interface{}); ok {
				curr = valueAsMap
				continue
			}
		}

		return nil
	}

	return data
}

func MergeMaps(maps ...map[string]interface{}) (result map[string]interface{}) {
	result = make(map[string]interface{})
	for _, m := range maps {
		for k, v := range m {
			result[k] = v
		}
	}
	return result
}
