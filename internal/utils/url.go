package utils

import (
	"fmt"
	"net/url"
	"strings"
)

const (
	signatureIndex_repository = iota
	signatureIndex_filename
	signatureIndex_lastModNanos

	signatureLength
)

func signFileLink(repository, filename string, lastModNanos int64, key []byte) (string, error) {
	opts := SignLinkOpts{
		Data:       make([]interface{}, signatureLength),
		StartIndex: 2,
		Key:        key,
	}

	opts.Data[signatureIndex_repository] = repository
	opts.Data[signatureIndex_filename] = filename
	opts.Data[signatureIndex_lastModNanos] = lastModNanos

	return SignLink(opts)
}

func CheckFileSignature(repository, filename, signature string, key []byte) error {
	opts := VerifyLinkSignatureOpts{
		Data:      make([]interface{}, signatureIndex_filename+1),
		Signature: signature,
		Key:       key,
	}

	opts.Data[signatureIndex_repository] = repository
	opts.Data[signatureIndex_filename] = filename

	_, err := CheckLinkSignature(opts)
	return err
}

func FileUrl(repo string, filename string, lastModNanos int64, key []byte) (string, error) {
	tokens := strings.Split(filename, "/")
	for i, token := range tokens {
		tokens[i] = url.PathEscape(token)
	}

	s, err := signFileLink(repo, filename, lastModNanos, key)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("/file/%v/%v?s=%v", url.PathEscape(repo), strings.Join(tokens, "/"), s), nil
}
