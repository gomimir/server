package utils

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"

	"github.com/vmihailenco/msgpack/v5"
)

var rawURLEncoding = base64.URLEncoding.WithPadding(base64.NoPadding)

type SignLinkOpts struct {
	Data       []interface{}
	StartIndex int
	Key        []byte
}

type VerifyLinkSignatureOpts struct {
	Data      []interface{}
	Signature string
	Key       []byte
}

func SignLink(opts SignLinkOpts) (string, error) {
	signatureBytes, err := msgpack.Marshal(opts.Data)
	if err != nil {
		return "", err
	}

	mac := hmac.New(sha256.New, opts.Key)
	mac.Write(signatureBytes)
	expectedMAC := mac.Sum(nil)

	payload := make([]interface{}, 1+len(opts.Data)-opts.StartIndex)
	payload[0] = expectedMAC
	copy(payload[1:], opts.Data[opts.StartIndex:])

	payloadBytes, err := msgpack.Marshal(payload)
	if err != nil {
		return "", err
	}

	return rawURLEncoding.EncodeToString(payloadBytes), nil
}

func CheckLinkSignature(opts VerifyLinkSignatureOpts) ([]interface{}, error) {
	payloadBytes, err := rawURLEncoding.DecodeString(opts.Signature)
	if err != nil {
		return nil, fmt.Errorf("malformed signature")
	}

	var payload []interface{}
	err = msgpack.Unmarshal(payloadBytes, &payload)
	if err != nil {
		return nil, fmt.Errorf("malformed signature")
	}

	if len(payload) > 1 {
		if actualMAC, ok := payload[0].([]byte); ok {
			toVerify := make([]interface{}, len(opts.Data)+len(payload)-1)
			copy(toVerify, opts.Data)
			copy(toVerify[len(opts.Data):], payload[1:])

			signatureBytes, err := msgpack.Marshal(toVerify)
			if err != nil {
				return nil, err
			}

			mac := hmac.New(sha256.New, opts.Key)
			mac.Write(signatureBytes)
			expectedMAC := mac.Sum(nil)

			if hmac.Equal(actualMAC, expectedMAC) {
				return toVerify, nil
			}

			return nil, fmt.Errorf("invalid signature")
		}
	}

	return nil, fmt.Errorf("malformed signature")
}
