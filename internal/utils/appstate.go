package utils

import (
	"fmt"
	"time"
)

// AppState is used for conserving application state between login redirects
type AppState struct {
	// Relative URL of the FE application
	Path string
}

func (s AppState) Serialize(key []byte) (string, error) {
	return SignLink(SignLinkOpts{
		Data: []interface{}{time.Now().Add(10 * time.Minute).UTC().Unix(), s.Path},
		Key:  key,
	})
}

func AppStateFromString(str string, key []byte) (*AppState, error) {
	data, err := CheckLinkSignature(VerifyLinkSignatureOpts{
		Signature: str,
		Key:       key,
	})

	if err != nil {
		return nil, err
	}

	if len(data) == 2 {
		if exp, ok := data[0].(int64); ok {
			if path, ok := data[1].(string); ok {
				if exp < time.Now().UTC().Unix() {
					return nil, fmt.Errorf("expired signature")
				}

				return &AppState{
					Path: path,
				}, nil
			}
		}
	}

	return nil, fmt.Errorf("malformed signature")
}
