package utils

import (
	"fmt"
	"regexp"
	"time"
)

type uploadRand interface {
	Uint32() uint32
}

var suffixRx = regexp.MustCompile(`^(.+?)(\.[^.]+)?$`)

type UploadFilenameGenerator struct {
	fullPrefix     string
	takenFilenames map[string]int
}

// NewUploadFilenameGenerator - creates generator for upload filenames
func NewUploadFilenameGenerator(prefix string, r uploadRand) *UploadFilenameGenerator {
	normalizedPrefix := EnsureNoTrailingSlash(prefix)
	fullPrefix := fmt.Sprintf("%v/%v_%x", normalizedPrefix, time.Now().UTC().Format("2006-01-02_15.04.05.999999999"), r.Uint32())

	return &UploadFilenameGenerator{
		fullPrefix:     fullPrefix,
		takenFilenames: make(map[string]int),
	}
}

func (g *UploadFilenameGenerator) Prefix() string {
	return g.fullPrefix
}

// Filename - returns full path to file with given filename, while handling filename conflicts (it will always be unique)
func (g *UploadFilenameGenerator) Filename(filename string) string {
	filename = SanitizeRelPath(filename)

	nextIndexToTry, alreadyTaken := g.takenFilenames[filename]
	if !alreadyTaken {
		g.takenFilenames[filename] = 1
		return g.fullPrefix + "/" + filename
	}

	filenamePrefix := filename
	filenameSuffix := ""
	m := suffixRx.FindStringSubmatch(filename)
	if len(m) == 3 {
		filenamePrefix = m[1]
		filenameSuffix = m[2]
	}

	for {
		altFilename := fmt.Sprintf("%v-%v%v", filenamePrefix, nextIndexToTry, filenameSuffix)
		if _, alreadyTaken = g.takenFilenames[altFilename]; alreadyTaken {
			nextIndexToTry++
		} else {
			g.takenFilenames[filename] = nextIndexToTry + 1
			g.takenFilenames[altFilename] = 1
			return g.fullPrefix + "/" + altFilename
		}
	}
}
