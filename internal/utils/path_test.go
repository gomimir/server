package utils_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/utils"
)

func TestSanitizeRelPath(t *testing.T) {
	assert.Equal(t, "a.txt", utils.SanitizeRelPath("../a.txt"))
	assert.Equal(t, "a.txt", utils.SanitizeRelPath("/a.txt"))
	assert.Equal(t, "a.txt", utils.SanitizeRelPath("/foo/../a.txt"))
	assert.Equal(t, "a.txt", utils.SanitizeRelPath("/foo/../../a.txt"))
	assert.Equal(t, "a.txt", utils.SanitizeRelPath("/foo/../../../a.txt"))
	assert.Equal(t, "", utils.SanitizeRelPath(".."))
	assert.Equal(t, "", utils.SanitizeRelPath("../.."))
	assert.Equal(t, "", utils.SanitizeRelPath("../../.."))
	assert.Equal(t, "", utils.SanitizeRelPath("../../../"))
	assert.Equal(t, "", utils.SanitizeRelPath("/"))
	assert.Equal(t, "", utils.SanitizeRelPath("."))
	assert.Equal(t, "", utils.SanitizeRelPath("./"))
	assert.Equal(t, "", utils.SanitizeRelPath("./."))
}

func TestHashFilename(t *testing.T) {
	assert.Equal(t, "DMF1ucDxtqgxw5niaXcmYQ.txt", utils.HashFilename("a.txt"))
	assert.Equal(t, "dR2etx0QEKgGvB8clwzqgw.txt", utils.HashFilename("docs/a.txt"))
	assert.Equal(t, "d4sXnizZaQDQSEDxZnPWXg.txt", utils.HashFilename("docs/a.b.c/test.txt"))
	assert.Equal(t, "kAFQmDzST7DWlj99KOF_cg", utils.HashFilename("abc"))
}
