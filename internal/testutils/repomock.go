package testutils

import (
	"context"
	"fmt"
	"io"
	"testing"

	"gitlab.com/gomimir/server/internal/dataloader"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/repository"
	"gitlab.com/gomimir/server/internal/secret"
)

type repoMock struct {
	getFileMock    func(ctx context.Context, filename string) (*repository.GetFile, error)
	deleteFileMock func(ctx context.Context, filename string) error
}

func (r *repoMock) EnsureTemp(ctx context.Context, prefix string) error {
	panic(fmt.Errorf("EnsureTemp: not mocked"))
}

func (r *repoMock) ListFiles(ctx context.Context, prefix string) (chan repository.Entry, error) {
	panic(fmt.Errorf("ListFiles: not mocked"))
}

func (r *repoMock) GetFile(ctx context.Context, filename string) (*repository.GetFile, error) {
	if r.getFileMock != nil {
		return r.getFileMock(ctx, filename)
	}

	panic(fmt.Errorf("GetFile: not mocked"))
}

func (r *repoMock) MockGetFile(mock func(ctx context.Context, filename string) (*repository.GetFile, error)) {
	r.getFileMock = mock
}

func (r *repoMock) UploadFile(ctx context.Context, filename string, fileSize int64, reader io.Reader) error {
	panic(fmt.Errorf("UploadFile: not mocked"))
}

func (r *repoMock) DeleteFile(ctx context.Context, filename string) error {
	if r.deleteFileMock != nil {
		return r.deleteFileMock(ctx, filename)
	}

	panic(fmt.Errorf("DeleteFile: not mocked"))
}

func (r *repoMock) MockDeleteFile(mock func(ctx context.Context, filename string) error) {
	r.deleteFileMock = mock
}

func MockRepository(t *testing.T, repositoryID pulid.ID) *repoMock {
	initializer := dataloader.RepositoryInitializer
	mock := &repoMock{}

	dataloader.RepositoryInitializer = func(repoEnt *ent.Repository, secretKeeper secret.Keeper) (repository.Repository, error) {
		if repoEnt.ID == repositoryID {
			return mock, nil
		}

		return initializer(repoEnt, secretKeeper)
	}

	t.Cleanup(func() {
		dataloader.RepositoryInitializer = initializer
	})

	return mock
}
