package testutils

import (
	"context"
	"database/sql"
	"fmt"
	"testing"

	entsql "entgo.io/ent/dialect/sql"
	"gitlab.com/gomimir/server/internal/ent"
	_ "gitlab.com/gomimir/server/internal/ent/runtime"
	_ "modernc.org/sqlite"
)

func EntClient(t *testing.T) *ent.Client {
	sqlDb, err := sql.Open("sqlite", fmt.Sprintf("file:%v?mode=memory&cache=shared&_pragma=foreign_keys(1)", t.Name()))
	if err != nil {
		panic(err)
	}

	sqlDb.SetMaxOpenConns(100)
	drv := entsql.OpenDB("sqlite3", sqlDb)
	client := ent.NewClient(ent.Driver(drv))

	err = client.Schema.Create(context.Background())
	if err != nil {
		panic(err)
	}

	return client
}
