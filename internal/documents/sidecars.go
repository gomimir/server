package documents

import (
	"encoding/json"
	"fmt"
)

type Sidecars []Sidecar

func (s *Sidecars) UnmarshalJSON(data []byte) error {
	var wrappers []sidecarJSONWrapper
	err := json.Unmarshal(data, &wrappers)
	if err != nil {
		return err
	}

	for _, wrapper := range wrappers {
		*s = append(*s, wrapper.Sidecar)
	}

	return nil
}

type sidecarJSONWrapper struct {
	Sidecar Sidecar
}

func (wrapper *sidecarJSONWrapper) UnmarshalJSON(data []byte) error {
	var partial struct {
		Kind string `json:"kind"`
	}

	err := json.Unmarshal(data, &partial)
	if err != nil {
		return err
	}

	if partial.Kind == "thumbnail" {
		var thumb Thumbnail
		err := json.Unmarshal(data, &thumb)
		if err != nil {
			return err
		}

		wrapper.Sidecar = thumb
		return nil
	} else if partial.Kind == "video" {
		var video Video
		err := json.Unmarshal(data, &video)
		if err != nil {
			return err
		}

		wrapper.Sidecar = video
		return nil
	}

	return fmt.Errorf("unexpected sidecar kind: %v", partial.Kind)
}
