package documents

import "encoding/json"

type TagAssignmentReasons []TagAssignmentReason

func (s *TagAssignmentReasons) UnmarshalJSON(data []byte) error {
	var wrappers []tarJSONWrapper
	err := json.Unmarshal(data, &wrappers)
	if err != nil {
		return err
	}

	for _, wrapper := range wrappers {
		*s = append(*s, wrapper.TagAssignmentReason)
	}

	return nil
}
