package documents

import (
	"encoding/json"

	mimir "gitlab.com/gomimir/processor"
)

// Video - video sidecar
type Video struct {
	mimir.VideoDescriptor
	SidecarFile File `json:"file"`
}

func (thumb Video) File() File {
	return thumb.SidecarFile
}

func (thumb Video) Match(another Sidecar) bool {
	if anotherThumb, ok := another.(Video); ok {
		return thumb.VideoDescriptor.Equal(anotherThumb.VideoDescriptor)
	}

	return false
}

func (thumb Video) Equal(another Sidecar) bool {
	if !thumb.Match(another) {
		return false
	}

	if !thumb.SidecarFile.Equal(another.File()) {
		return false
	}

	return true
}

func (thumb Video) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		Kind string `json:"kind"`
		mimir.VideoDescriptor
		File File `json:"file"`
	}{
		Kind:            "video",
		VideoDescriptor: thumb.VideoDescriptor,
		File:            thumb.SidecarFile,
	})
}
