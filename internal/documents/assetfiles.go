package documents

import (
	"encoding/json"
	"sort"

	mimir "gitlab.com/gomimir/processor"
)

// Helper struct for marshalling additional computed fields
type assetFileJSON struct {
	AssetFile
	IsMain bool `json:"main,omitempty"`
}

type AssetFiles []AssetFile

// Checks for deep equality of the asset files
func (af AssetFiles) Equal(another AssetFiles) bool {
	if len(af) != len(another) {
		return false
	}

	for i := range af {
		if !af[i].Equal(&another[i]) {
			return false
		}
	}

	return true
}

// Performs deep clone of the asset files
func (af AssetFiles) Clone() AssetFiles {
	clone := make([]AssetFile, len(af))
	for i, f := range af {
		clone[i] = f.Clone()
	}

	return clone
}

func (af AssetFiles) ShallowClone() AssetFiles {
	result := make([]AssetFile, len(af))
	copy(result, af)
	return result
}

// Finds index of a file with given ref, returns -1 if file was not found
func (af AssetFiles) IndexByRef(ref mimir.FileRef) int {
	for idx, file := range af {
		if file.MatchesRef(ref) {
			return idx
		}
	}

	return -1
}

// Finds file with given ref and returns ptr to it
func (af AssetFiles) FindByRef(ref mimir.FileRef) *AssetFile {
	idx := af.IndexByRef(ref)
	if idx > -1 {
		return &af[idx]
	}

	return nil
}

// Updates file with given ref by callback. Appends it first if it does not exist.
func (af *AssetFiles) EnsureByRef(ref mimir.FileRef, callback func(existing AssetFile) AssetFile) {
	if idx := af.IndexByRef(ref); idx > -1 {
		(*af)[idx] = callback((*af)[idx])
		return
	}

	newFile := AssetFile{
		File: File{
			FileRef: ref,
		},
	}

	newFile = callback(newFile)
	*af = append(*af, newFile)
}

// Returns new shallow copy of the asset file array with all the files sorted by priority
func (af AssetFiles) ByPriority() AssetFiles {
	result := af.ShallowClone()

	// Sort from the highest to lowest priority
	sort.Slice(result, func(i, j int) bool {
		leftPriority := result[i].Priority()
		rightPriority := result[j].Priority()

		return leftPriority > rightPriority
	})

	return result
}

// Returns index of a main file (file with the highest priority) or -1 if there are no files
func (af AssetFiles) MainIndex() int {
	if len(af) > 0 {
		mainFileIndex := 0
		mainFilePrio := 0.0

		for i, f := range af {
			prioValue := f.Priority()
			if prioValue > mainFilePrio {
				mainFilePrio = prioValue
				mainFileIndex = i
			}
		}

		return mainFileIndex
	}

	return -1
}

// Returns ptr to main file (file with the highest priority) or nil if there are no files
func (af AssetFiles) Main() *AssetFile {
	idx := af.MainIndex()
	if idx > -1 {
		return &af[idx]
	}

	return nil
}

// Marshals asset files to JSON using helper struct assetFileJSON to populated all computed fields
func (af AssetFiles) MarshalJSON() ([]byte, error) {
	res := make([]assetFileJSON, len(af))

	for i, f := range af {
		res[i] = assetFileJSON{
			AssetFile: f,
		}
	}

	idx := af.MainIndex()
	if idx > -1 {
		res[idx].IsMain = true
	}

	return json.Marshal(res)
}
