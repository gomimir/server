package documents

import "time"

// FileProcessing - processing records
type FileProcessing struct {
	Name        string               `json:"name"`
	JobID       string               `json:"jobId"`
	ProcessedAt time.Time            `json:"processedAt"`
	Took        *int                 `json:"took,omitempty"`
	ConfigHash  string               `json:"configHash,omitempty"`
	Error       *FileProcessingError `json:"error,omitempty"`
}

func (fp FileProcessing) Equal(another FileProcessing) bool {
	if fp.JobID != another.JobID {
		return false
	}

	if fp.Name != another.Name {
		return false
	}

	if fp.ProcessedAt.Equal(another.ProcessedAt) {
		return false
	}

	if fp.Took != nil && another.Took != nil {
		if *fp.Took != *another.Took {
			return false
		}
	} else if fp.Took != nil || another.Took != nil {
		return false
	}

	if fp.ConfigHash != another.ConfigHash {
		return false
	}

	if fp.Error != nil && another.Error != nil {
		if !fp.Error.Equal(*another.Error) {
			return false
		}
	} else if fp.Error != nil || another.Error != nil {
		return false
	}

	return true
}
