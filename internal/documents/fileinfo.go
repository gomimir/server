package documents

import "time"

// FileInfo - helper structure for holding additional information about file content
type FileInfo struct {
	// Date of the last modification
	LastModified time.Time `json:"lastModified"`

	// File size in bytes
	Size int64 `json:"size" example:"2451"`
}

func (fi FileInfo) Equal(another FileInfo) bool {
	return fi.LastModified.Equal(another.LastModified) && fi.Size == another.Size
}
