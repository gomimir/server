package documents

import (
	"encoding/json"

	mimir "gitlab.com/gomimir/processor"
)

type IndexedAsset struct {
	Asset
	AssetRef mimir.AssetRef
}

func (asset IndexedAsset) ID() string {
	return asset.AssetRef.String()
}

func IndexedAssetFromData(indexName, documentId string, rawData json.RawMessage) (*IndexedAsset, error) {
	asset := Asset{
		Files: AssetFiles{},
		Tags:  TagAssignments{},
	}

	err := json.Unmarshal(rawData, &asset)
	if err != nil {
		return nil, err
	}

	return &IndexedAsset{
		Asset:    asset,
		AssetRef: mimir.AssetRefForID(indexName, documentId),
	}, nil
}

// Asset - Document containing single asset, grouping multiple files
type Asset struct {
	// Unique asset identifier which is used to group the files together
	Key string `json:"key" example:"photos/somedir/somefile"`

	// List of files composing this asset
	Files AssetFiles `json:"files"`

	// List of assigned tags
	Tags TagAssignments `json:"tags,omitempty"`

	// Assigned properties
	Properties mimir.Properties `json:"properties,omitempty"`
}

// Equals - Returns true if the asset is exactly the same as another asset (deep equality check)
func (asset Asset) Equal(another *Asset) bool {
	if asset.Key != another.Key {
		return false
	}

	if !asset.Tags.Equal(another.Tags) {
		return false
	}

	if !asset.Properties.Equal(&another.Properties) {
		return false
	}

	if !asset.Files.Equal(another.Files) {
		return false
	}

	return true
}

// Clone - performs deep clone of the asset document
func (asset Asset) Clone() Asset {
	asset.Tags = asset.Tags.Clone()
	asset.Properties = *asset.Properties.Clone()
	asset.Files = asset.Files.Clone()

	return asset
}

// Thumbnail - returns best fitting thumbnail of the first file which has them
func (asset *Asset) Thumbnail(width *int, height *int, acceptableFormats []mimir.ThumbnailFileFormat) *Thumbnail {
	if len(asset.Files) > 0 {
		files := asset.Files.ByPriority()

		for _, file := range files {
			thumb := file.Thumbnail(width, height, acceptableFormats)
			if thumb != nil {
				return thumb
			}
		}
	}

	return nil
}
