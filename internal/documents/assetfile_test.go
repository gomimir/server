package documents_test

import (
	"testing"

	"github.com/icza/gox/gox"
	"github.com/stretchr/testify/assert"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/documents"
)

func TestAssetFileThumbnail(t *testing.T) {
	assetFile := documents.AssetFile{
		Sidecars: []documents.Sidecar{
			documents.Thumbnail{
				ThumbnailDescriptor: mimir.ThumbnailDescriptor{
					Width:  1200,
					Height: 800,
				},
				SidecarFile: documents.File{
					FileRef: mimir.FileRef{
						Repository: "minio",
						Filename:   "w800h800.jpg",
					},
				},
			},
			documents.Thumbnail{
				ThumbnailDescriptor: mimir.ThumbnailDescriptor{
					Width:  2400,
					Height: 1600,
				},
				SidecarFile: documents.File{
					FileRef: mimir.FileRef{
						Repository: "minio",
						Filename:   "w1600h1600.jpg",
					},
				},
			},
			documents.Thumbnail{
				ThumbnailDescriptor: mimir.ThumbnailDescriptor{
					Width:  4500,
					Height: 3000,
				},
				SidecarFile: documents.File{
					FileRef: mimir.FileRef{
						Repository: "minio",
						Filename:   "fullres.jpg",
					},
				},
			},
		},
	}

	assert.Equal(t, "w800h800.jpg", assetFile.Thumbnail(nil, nil, nil).File().Filename)
	assert.Equal(t, "w1600h1600.jpg", assetFile.Thumbnail(gox.NewInt(1600), gox.NewInt(1600), nil).File().Filename)
	assert.Equal(t, "w800h800.jpg", assetFile.Thumbnail(gox.NewInt(480), gox.NewInt(480), nil).File().Filename)
	assert.Equal(t, "w1600h1600.jpg", assetFile.Thumbnail(gox.NewInt(810), gox.NewInt(810), nil).File().Filename)
}
