package documents

type Sidecar interface {
	File() File
	Match(another Sidecar) bool
	Equal(another Sidecar) bool
}
