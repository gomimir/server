package documents_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/documents"
)

func TestAddTag(t *testing.T) {
	tags := documents.TagAssignments{}
	tags.AssignTag(documents.Tag{Kind: "album", Name: "foo"}, nil)
	tags.AssignTag(documents.Tag{Kind: "album", Name: "foo"}, nil)

	assert.Equal(t, 1, len(tags))
	assert.Equal(t, documents.TagAssignment{
		Tag: documents.Tag{
			Kind: "album",
			Name: "foo",
		},
		Key: "album:foo",
	}, tags[0])
}

func TestAddTagReason(t *testing.T) {
	tags := documents.TagAssignments{}
	tags.AssignTag(documents.Tag{Kind: "album", Name: "foo"}, documents.FilenameRuleTagAssignment{
		FileRef: mimir.FileRef{Repository: "photos", Filename: "foo.arw"},
	})
	tags.AssignTag(documents.Tag{Kind: "album", Name: "foo"}, documents.FilenameRuleTagAssignment{
		FileRef: mimir.FileRef{Repository: "photos", Filename: "foo.jpg"},
	})

	assert.Equal(t, 1, len(tags))
	assert.Equal(t, documents.TagAssignment{
		Tag: documents.Tag{
			Kind: "album",
			Name: "foo",
		},
		Key: "album:foo",
		Reasons: []documents.TagAssignmentReason{
			documents.FilenameRuleTagAssignment{
				FileRef: mimir.FileRef{Repository: "photos", Filename: "foo.arw"},
			},
			documents.FilenameRuleTagAssignment{
				FileRef: mimir.FileRef{Repository: "photos", Filename: "foo.jpg"},
			},
		},
	}, tags[0])
}

func TestRemoveTag(t *testing.T) {
	tags := documents.TagAssignments{
		{Tag: documents.Tag{Kind: "album", Name: "a"}},
		{Tag: documents.Tag{Kind: "album", Name: "b"}},
		{Tag: documents.Tag{Kind: "album", Name: "c"}},
	}

	tags.RemoveTag(documents.Tag{Kind: "album", Name: "b"})
	assert.Equal(t, documents.TagAssignments{
		{Tag: documents.Tag{Kind: "album", Name: "a"}},
		{Tag: documents.Tag{Kind: "album", Name: "c"}},
	}, tags)

	tags.RemoveTag(documents.Tag{Kind: "album", Name: "c"})
	assert.Equal(t, documents.TagAssignments{
		{Tag: documents.Tag{Kind: "album", Name: "a"}},
	}, tags)
}

func TestClone(t *testing.T) {
	reason := documents.UserTagAssignment{UserID: "SYSTEM"}

	tags := documents.TagAssignments{}
	tags.AssignTag(documents.Tag{Kind: "album", Name: "foo"}, reason)

	tags2 := tags.Clone()
	tags2.AssignTag(documents.Tag{Kind: "album", Name: "foo2"}, reason)

	assert.Equal(t, 1, len(tags))
	assert.Equal(t, documents.TagAssignment{
		Tag: documents.Tag{
			Kind: "album",
			Name: "foo",
		},
		Key:     "album:foo",
		Reasons: []documents.TagAssignmentReason{reason},
	}, tags[0])

	assert.Equal(t, 2, len(tags2))
	assert.Equal(t, documents.TagAssignment{
		Tag: documents.Tag{
			Kind: "album",
			Name: "foo",
		},
		Key:     "album:foo",
		Reasons: []documents.TagAssignmentReason{reason},
	}, tags2[0])
	assert.Equal(t, documents.TagAssignment{
		Tag: documents.Tag{
			Kind: "album",
			Name: "foo2",
		},
		Key:     "album:foo2",
		Reasons: []documents.TagAssignmentReason{reason},
	}, tags2[1])
}

func TestEqual(t *testing.T) {
	tags1 := documents.TagAssignments{}
	tags2 := documents.TagAssignments{}

	assert.True(t, tags1.Equal(tags2))

	tags1.AssignTag(documents.Tag{Kind: "album", Name: "foo"}, nil)

	assert.False(t, tags1.Equal(tags2))

	tags2.AssignTag(documents.Tag{Kind: "album", Name: "foo"}, nil)

	assert.True(t, tags1.Equal(tags2))
}
