package documents_test

import (
	"encoding/json"
	"testing"

	"github.com/bradleyjkemp/cupaloy/v2"
	"github.com/stretchr/testify/require"
	"gitlab.com/gomimir/server/internal/documents"

	_ "embed"
)

//go:embed "fixtures/asset.json"
var assetSource []byte

func TestAssetSerialization(t *testing.T) {
	var asset documents.Asset

	t.Run("Unmarshal", func(t *testing.T) {
		err := json.Unmarshal(assetSource, &asset)
		require.NoError(t, err)

		require.Equal(t, "2021-02 Green album/DSC03735", asset.Key)

		cupaloy.SnapshotT(t, asset)
	})

	t.Run("Marshal", func(t *testing.T) {
		bytes, err := json.Marshal(asset)
		require.NoError(t, err)

		var asset2 documents.Asset
		err = json.Unmarshal(bytes, &asset2)
		require.NoError(t, err)

		require.True(t, asset2.Equal(&asset))
	})
}
