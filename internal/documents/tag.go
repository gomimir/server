package documents

// Tag - Asset tag
type Tag struct {
	// Tag kind
	Kind string `json:"kind" example:"album"`

	// Name of the tag
	Name string `json:"name" example:"Vacation"`
}

func (tag Tag) Equal(another Tag) bool {
	return tag.Name == another.Name && tag.Kind == another.Kind
}

type TagAssignment struct {
	Tag

	// Holds unique identifier of the kind/name pair (tag).
	// It is purely computed property which we materialize for easier lookup.
	Key     string               `json:"key"`
	Reasons TagAssignmentReasons `json:"reasons"`

	// TODO: if tag was explicitly removed by the user, we keep this flag to prevent readding the tag on re-index
	// Disabled bool
}

func (assignment TagAssignment) Clone() TagAssignment {
	assignment.Reasons = append([]TagAssignmentReason{}, assignment.Reasons...)
	return assignment
}

func (assignment TagAssignment) WithReason(reason TagAssignmentReason) TagAssignment {
	if assignment.IndexOfReason(reason, true) == -1 {
		assignment.Reasons = append([]TagAssignmentReason{}, assignment.Reasons...)
		assignment.Reasons = append(assignment.Reasons, reason)
	}

	return assignment
}

func (assignment TagAssignment) HasReasonOfKind(reasonKind TagAssignmentReasonKind) bool {
	for _, reason := range assignment.Reasons {
		if reason.Kind() == reasonKind {
			return true
		}
	}

	return false
}

func (assignment TagAssignment) WithoutReasonsOfKind(reasonKind TagAssignmentReasonKind) TagAssignment {
	var filteredReasons []TagAssignmentReason
	for _, reason := range assignment.Reasons {
		if reason.Kind() != reasonKind {
			filteredReasons = append(filteredReasons, reason)
		}
	}

	if len(filteredReasons) != len(assignment.Reasons) {
		assignment.Reasons = filteredReasons
	}

	return assignment
}

func (assignment TagAssignment) IndexOfReason(reason TagAssignmentReason, ignoreCreatedAt bool) int {
	for i, needle := range assignment.Reasons {
		if needle.Equal(reason, ignoreCreatedAt) {
			return i
		}
	}

	return -1
}

func (assignment TagAssignment) Equal(another TagAssignment) bool {
	if !assignment.Tag.Equal(another.Tag) {
		return false
	}

	if len(assignment.Reasons) != len(another.Reasons) {
		return false
	}

	for _, reason := range assignment.Reasons {
		if another.IndexOfReason(reason, false) == -1 {
			return false
		}
	}

	return true
}
