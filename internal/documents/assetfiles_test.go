package documents_test

import (
	"encoding/json"
	"testing"
	"time"

	"github.com/bradleyjkemp/cupaloy"
	"github.com/stretchr/testify/require"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/documents"
)

type testDoc struct {
	Files documents.AssetFiles `json:"files"`
}

var testFiles = documents.AssetFiles{
	{
		File: documents.File{
			FileRef: mimir.FileRef{
				Repository: "testrepo",
				Filename:   "testfile.jpg",
			},
			FileInfo: documents.FileInfo{
				LastModified: time.Date(2022, 5, 18, 9, 54, 21, 0, time.FixedZone("CEST", 2*60*60)),
				Size:         4151,
			},
		},
		Properties: *mimir.PropertiesFromMap(map[string]interface{}{
			"priority": 1.0,
		}),
	},
	{
		File: documents.File{
			FileRef: mimir.FileRef{
				Repository: "testrepo",
				Filename:   "testfile2.jpg",
			},
			FileInfo: documents.FileInfo{
				LastModified: time.Date(2022, 5, 18, 10, 01, 11, 0, time.FixedZone("CEST", 2*60*60)),
				Size:         8411,
			},
		},
		Properties: *mimir.PropertiesFromMap(map[string]interface{}{
			"priority": 2.0,
		}),
	},
}

func TestAssetFilesClone(t *testing.T) {
	clonedTestFiles := testFiles.Clone()
	require.True(t, clonedTestFiles.Equal(testFiles))
	require.NotSame(t, testFiles, clonedTestFiles)

	clonedTestFiles[1].FileRef.Filename = "XXXX"
	require.False(t, clonedTestFiles.Equal(testFiles))
}

func TestAssetFilesShallowClone(t *testing.T) {
	clonedTestFiles := testFiles.ShallowClone()
	require.True(t, clonedTestFiles.Equal(testFiles))
	require.NotSame(t, testFiles, clonedTestFiles)
}

func TestAssetFilesMarshalJSON(t *testing.T) {
	doc := testDoc{
		Files: testFiles,
	}

	bytes, err := json.MarshalIndent(doc, "", "  ")
	require.NoError(t, err)

	snapshotter := cupaloy.New(cupaloy.SnapshotFileExtension(".json"))
	snapshotter.SnapshotT(t, string(bytes))

	var doc2 testDoc
	err = json.Unmarshal(bytes, &doc2)
	require.NoError(t, err)

	require.True(t, doc.Files.Equal(doc2.Files))
}

func TestAssetFilesMain(t *testing.T) {
	mainFile := testFiles.Main()
	require.Same(t, mainFile, &testFiles[1])
}

func TestAssetEnsureByRef(t *testing.T) {
	t.Run("Existing", func(t *testing.T) {
		af := testFiles.Clone()

		af.EnsureByRef(af[0].FileRef, func(file documents.AssetFile) documents.AssetFile {
			file.Filename = "XXX"
			return file
		})

		require.Equal(t, "XXX", af[0].Filename)
	})

	t.Run("NonExisting", func(t *testing.T) {
		af := testFiles.Clone()

		af.EnsureByRef(mimir.FileRef{
			Repository: "testrepo",
			Filename:   "XXX",
		}, func(file documents.AssetFile) documents.AssetFile {
			require.Equal(t, "XXX", file.Filename)
			file.FileInfo.Size = 1111
			return file
		})

		require.Len(t, af, 3)
		require.Equal(t, int64(1111), af[2].FileInfo.Size)
	})
}
