package documents

type FileProcessingError struct {
	Code    string `json:"code,omitempty"`
	Message string `json:"message"`
}

func (err FileProcessingError) Equal(another FileProcessingError) bool {
	return err.Code == another.Code && err.Message == another.Message
}
