package documents

import (
	mimir "gitlab.com/gomimir/processor"
)

// File - Document fragment containing store file info
type File struct {
	mimir.FileRef

	// Additional information about the contents of the file
	FileInfo FileInfo `json:"fileInfo"`
}

// MatchesRef - returns true if the reference points to this file
func (f *File) MatchesRef(ref mimir.FileRef) bool {
	return f.Filename == ref.Filename && f.Repository == ref.Repository
}

func (f File) Equal(another File) bool {
	if !another.MatchesRef(f.FileRef) {
		return false
	}

	if !f.FileInfo.Equal(another.FileInfo) {
		return false
	}

	return true
}
