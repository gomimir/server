package documents

import (
	"fmt"
)

type TagAssignments []TagAssignment

func (assignments TagAssignments) Clone() TagAssignments {
	var result TagAssignments
	for _, a := range assignments {
		result = append(result, a.Clone())
	}

	return result
}

func (assignments *TagAssignments) Merge(another TagAssignments) {
	for _, assignment := range another {
		if len(assignment.Reasons) > 0 {
			for _, reason := range assignment.Reasons {
				assignments.AssignTag(assignment.Tag, reason)
			}
		} else {
			assignments.AssignTag(assignment.Tag, nil)
		}
	}
}

func (assignments *TagAssignments) RemoveTag(tag Tag) {
	idx := assignments.IndexOf(tag)
	if idx > -1 {
		*assignments = append((*assignments)[:idx], (*assignments)[idx+1:]...)
	}
}

func (assignments *TagAssignments) AssignTag(tag Tag, reason TagAssignmentReason) {
	idx := assignments.IndexOf(tag)
	if idx == -1 {
		var reasons []TagAssignmentReason
		if reason != nil {
			reasons = []TagAssignmentReason{reason}
		}

		*assignments = append(*assignments, TagAssignment{Tag: tag, Key: fmt.Sprintf("%v:%v", tag.Kind, tag.Name), Reasons: reasons})
	} else if reason != nil {
		(*assignments)[idx] = (*assignments)[idx].WithReason(reason)
	}
}

func (assignments *TagAssignments) LiftAllTagReasonsOfKind(tag Tag, reasonKind TagAssignmentReasonKind) {
	idx := assignments.IndexOf(tag)
	if idx != -1 {
		(*assignments)[idx] = (*assignments)[idx].WithoutReasonsOfKind(reasonKind)
	}
}

func (assignments TagAssignments) IndexOf(tag Tag) int {
	for idx, assignment := range assignments {
		if assignment.Tag.Equal(tag) {
			return idx
		}
	}

	return -1
}

// Compares with another list of tags, ignores the order
func (assignments TagAssignments) Equal(another TagAssignments) bool {
	if len(assignments) != len(another) {
		return false
	}

	for _, assignment := range assignments {
		idx := another.IndexOf(assignment.Tag)
		if idx == -1 {
			return false
		}

		if !assignment.Equal(another[idx]) {
			return false
		}
	}

	return true
}
