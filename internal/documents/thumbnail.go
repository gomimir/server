package documents

import (
	"encoding/json"

	mimir "gitlab.com/gomimir/processor"
)

// Thumbnail - file thumbnail
type Thumbnail struct {
	mimir.ThumbnailDescriptor
	SidecarFile File `json:"file"`
}

func (thumb Thumbnail) File() File {
	return thumb.SidecarFile
}

func (thumb Thumbnail) Match(another Sidecar) bool {
	if anotherThumb, ok := another.(Thumbnail); ok {
		return thumb.ThumbnailDescriptor.Equal(anotherThumb.ThumbnailDescriptor)
	}

	return false
}

func (thumb Thumbnail) Equal(another Sidecar) bool {
	if !thumb.Match(another) {
		return false
	}

	if !thumb.SidecarFile.Equal(another.File()) {
		return false
	}

	return true
}

func (thumb Thumbnail) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		Kind string `json:"kind"`
		mimir.ThumbnailDescriptor
		File File `json:"file"`
	}{
		Kind:                "thumbnail",
		ThumbnailDescriptor: thumb.ThumbnailDescriptor,
		File:                thumb.SidecarFile,
	})
}
