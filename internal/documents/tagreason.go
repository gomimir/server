package documents

import (
	"encoding/json"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"

	mimir "gitlab.com/gomimir/processor"
)

type TagAssignmentReasonKind string

const (
	TagAssignmentReasonUser         TagAssignmentReasonKind = "user"
	TagAssignmentReasonFilenameRule TagAssignmentReasonKind = "filenameRule"
)

func (e TagAssignmentReasonKind) String() string {
	return string(e)
}

func (e TagAssignmentReasonKind) MarshalGQL(w io.Writer) {
	if e == TagAssignmentReasonFilenameRule {
		// TODO: some utility for SCREAMING_SNAKE_CASE would be helpful
		fmt.Fprint(w, strconv.Quote("FILENAME_RULE"))
	} else {
		fmt.Fprint(w, strconv.Quote(strings.ToUpper(e.String())))
	}
}

func (e *TagAssignmentReasonKind) UnmarshalGQL(v interface{}) error {
	str, ok := v.(string)
	if !ok {
		return fmt.Errorf("enums must be strings")
	}

	switch str {
	case "FILENAME_RULE":
		*e = TagAssignmentReasonFilenameRule
		return nil
	case "USER":
		*e = TagAssignmentReasonUser
		return nil
	}

	return fmt.Errorf("%s is not a valid TagAssignmentReasonKind", str)
}

// TagAssignmentReason - object describing the reason why the tag was assigned to an asset
type TagAssignmentReason interface {
	Kind() TagAssignmentReasonKind
	Equal(another TagAssignmentReason, ignoreCreatedAt bool) bool
}

// ---------------

// UserTagAssignment - object explaining that tag was assigned by the user
type UserTagAssignment struct {
	UserID    string    `json:"uid"`
	CreatedAt time.Time `json:"createdAt"`
}

func (reason UserTagAssignment) Kind() TagAssignmentReasonKind {
	return TagAssignmentReasonUser
}

func (reason UserTagAssignment) Equal(another TagAssignmentReason, ignoreCreatedAt bool) bool {
	return reason.Kind() == another.Kind() &&
		another.(UserTagAssignment).UserID == reason.UserID &&
		(ignoreCreatedAt || another.(UserTagAssignment).CreatedAt == reason.CreatedAt)
}

func (reason UserTagAssignment) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]interface{}{
		"kind":      reason.Kind(),
		"createdAt": reason.CreatedAt.Format(time.RFC3339),
		"uid":       reason.UserID,
	})
}

// ---------------

// FilenameRuleTagAssignment - object explaining that tag was assigned because rule over filename
type FilenameRuleTagAssignment struct {
	mimir.FileRef
	CreatedAt time.Time `json:"createdAt"`
}

func (reason FilenameRuleTagAssignment) Kind() TagAssignmentReasonKind {
	return TagAssignmentReasonFilenameRule
}

func (reason FilenameRuleTagAssignment) Equal(another TagAssignmentReason, ignoreCreatedAt bool) bool {
	return reason.Kind() == another.Kind() &&
		another.(FilenameRuleTagAssignment).FileRef.Filename == reason.Filename &&
		another.(FilenameRuleTagAssignment).FileRef.Repository == reason.Repository &&
		(ignoreCreatedAt || another.(FilenameRuleTagAssignment).CreatedAt == reason.CreatedAt)
}

func (reason FilenameRuleTagAssignment) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]interface{}{
		"kind":       reason.Kind(),
		"createdAt":  reason.CreatedAt.Format(time.RFC3339),
		"repository": reason.Repository,
		"filename":   reason.Filename,
	})
}

// ---------------

type tarJSONWrapper struct {
	TagAssignmentReason TagAssignmentReason
}

func (wrapper *tarJSONWrapper) UnmarshalJSON(data []byte) error {
	var partial struct {
		Kind string `json:"kind"`
	}

	err := json.Unmarshal(data, &partial)
	if err != nil {
		return err
	}

	if partial.Kind == TagAssignmentReasonUser.String() {
		var reason UserTagAssignment
		err := json.Unmarshal(data, &reason)
		if err != nil {
			return err
		}

		wrapper.TagAssignmentReason = reason
		return nil
	} else if partial.Kind == TagAssignmentReasonFilenameRule.String() {
		var reason FilenameRuleTagAssignment
		err := json.Unmarshal(data, &reason)
		if err != nil {
			return err
		}

		wrapper.TagAssignmentReason = reason
		return nil
	}

	return fmt.Errorf("unknown tag assignment reason kind: %v", partial.Kind)
}
