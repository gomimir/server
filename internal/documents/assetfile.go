package documents

import mimir "gitlab.com/gomimir/processor"

// AssetFile - enhanced file for assets (contains file processings, thumbnails, etc.)
type AssetFile struct {
	File

	// Processing results
	Processings []FileProcessing `json:"processings,omitempty"`

	// Sidecar files
	Sidecars Sidecars `json:"sidecars,omitempty"`

	// List of assigned properties
	Properties mimir.Properties `json:"properties,omitempty"`
}

func (f AssetFile) Priority() float64 {
	// FIXME: we shall introduce something like GetNumeric() on the Properties to help handling range of numeric types (ie. int, int64, ...)
	if prio := f.Properties.Get("priority"); prio != nil {
		if prioValue, ok := prio.(float64); ok {
			return prioValue
		}
	}

	return 0
}

func (f AssetFile) Equal(another *AssetFile) bool {
	if !f.File.Equal(another.File) {
		return false
	}

	if !f.Properties.Equal(&another.Properties) {
		return false
	}

	if len(f.Sidecars) != len(another.Sidecars) {
		return false
	}

	for i := range f.Sidecars {
		if !f.Sidecars[i].Equal(another.Sidecars[i]) {
			return false
		}
	}

	if len(f.Processings) != len(another.Processings) {
		return false
	}

	for i := range f.Processings {
		if f.Processings[i] != another.Processings[i] {
			return false
		}
	}

	return true
}

// Clone - performs deep clone of the asset document
func (f AssetFile) Clone() AssetFile {
	if len(f.Processings) != 0 {
		tmp := make([]FileProcessing, len(f.Processings))
		copy(tmp, f.Processings)
		f.Processings = tmp
	}

	if len(f.Sidecars) != 0 {
		tmp := make([]Sidecar, len(f.Sidecars))
		copy(tmp, f.Sidecars)
		f.Sidecars = tmp
	}

	f.Properties = *f.Properties.Clone()

	return f
}

// FindProcessing - finds file processing result by name
func (f *AssetFile) FindProcessing(name string) *FileProcessing {
	for i, fp := range f.Processings {
		if fp.Name == name {
			return &f.Processings[i]
		}
	}

	return nil
}

// EnsureProcessing - ensures processing exists within file and updates its values
func (f *AssetFile) EnsureProcessing(fp FileProcessing) {
	for idx, curr := range f.Processings {
		if curr.Name == fp.Name {
			f.Processings[idx] = fp
			return
		}
	}

	f.Processings = append(f.Processings, fp)
}

// Thumbnail - returns best fitting thumbnail
func (f *AssetFile) Thumbnail(width *int, height *int, acceptableFormats []mimir.ThumbnailFileFormat) *Thumbnail {
	if len(acceptableFormats) > 0 {
		for _, format := range acceptableFormats {
			thumb := f.thumbnailForFormat(width, height, format)
			if thumb != nil {
				return thumb
			}
		}

		return nil
	} else {
		return f.thumbnailForFormat(width, height, "")
	}
}

func (f *AssetFile) thumbnailForFormat(width *int, height *int, format mimir.ThumbnailFileFormat) *Thumbnail {
	var bestFit *Thumbnail
	var bestFitScore float64

	for _, sidecar := range f.Sidecars {
		if thumb, ok := sidecar.(Thumbnail); ok {
			if format == "" || thumb.Format == format {
				score := thumb.ScoreDimensionFit(width, height)

				// Bail early on exact match
				if score == 1 {
					return &thumb
				}

				if bestFit == nil || bestFitScore < score {
					bestFit = &thumb
					bestFitScore = score
				}
			}
		}
	}

	return bestFit
}
