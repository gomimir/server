package crawler

import (
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/documents"
)

type FileIndexRequest struct {
	mimir.FileRef `mapstructure:",squash"`

	IndexName string

	// Optional file info, present if the request originated from repository crawl
	// If not present, worker loads the data from repository itself.
	FileInfo *documents.FileInfo

	// Evaluated file rules
	EvaluatedRules ResolvedFileRules
}

func (req FileIndexRequest) AssetRef() mimir.AssetRef {
	return mimir.AssetRefForKey(req.IndexName, req.EvaluatedRules.AssetKey)
}

type IndexRequest struct {
	// Index requests of individual files
	Files []FileIndexRequest

	// List of processors which we should re-run, ignoring the previous results
	// '*' can be used as a wildcard match
	Reprocess []string
}

func (req IndexRequest) fileRequestsByAssetKey() map[string][]FileIndexRequest {
	result := make(map[string][]FileIndexRequest)

	for _, fileReq := range req.Files {
		key := fileReq.AssetRef().String()
		result[key] = append(result[key], fileReq)
	}

	return result
}

func (req IndexRequest) shouldReprocess(name string) bool {
	for _, reprocessName := range req.Reprocess {
		if reprocessName == name || reprocessName == "*" {
			return true
		}
	}

	return false
}
