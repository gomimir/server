package crawler

import (
	"fmt"
	"strconv"
	"time"

	"github.com/imdario/mergo"
	"github.com/mitchellh/hashstructure/v2"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/config"
	"gitlab.com/gomimir/server/internal/documents"
	"golang.org/x/text/unicode/norm"
)

// ResolvedProcessRequest - resolved prescript for file processors
type ResolvedProcessRequest struct {
	Name        string
	RequestKind string
	QueueName   string

	Config     map[string]interface{}
	ConfigHash string
}

func (rpr *ResolvedProcessRequest) mergeConfig(processConfig *config.ProcessAction) error {
	if processConfig.RequestKind != nil {
		rpr.RequestKind = *processConfig.RequestKind
	}

	if processConfig.QueueName != nil {
		rpr.QueueName = *processConfig.QueueName
	}

	if processConfig.Config != nil {
		err := mergo.Merge(&rpr.Config, processConfig.Config)
		if err != nil {
			return err
		}
	}

	return nil
}

// ResolvedFileRules - resolved descriptor of what we should with the file
type ResolvedFileRules struct {
	Ignore   bool
	AssetKey string

	Properties map[string]interface{}
	Tags       documents.TagAssignments

	ProcessRequests []*ResolvedProcessRequest
}

func (rfr *ResolvedFileRules) ensureProcessRequest(name string) *ResolvedProcessRequest {
	for _, pr := range rfr.ProcessRequests {
		if pr.Name == name {
			return pr
		}
	}

	pr := &ResolvedProcessRequest{
		Name:   name,
		Config: make(map[string]interface{}),
	}

	rfr.ProcessRequests = append(rfr.ProcessRequests, pr)
	return pr
}

func (rfr *ResolvedFileRules) mergeProperties(props map[string]interface{}) {
	for k, v := range props {
		rfr.Properties[k] = v
	}
}

// ResolveFileRules - resolve configured file rules for given filename
func ResolveFileRules(file mimir.FileRef, rules []config.Rule) (*ResolvedFileRules, error) {
	// We want to normalize the filename for pattern matching (and the name capture resolution)
	// Some filenames might be normalized in different way than we expect and it can cause trouble,
	// when comparing strings byte by byte. Ie. MacOS is using NFD and the rest NFC when storing filenames.
	// https://en.wikipedia.org/wiki/Filename#Unicode_interoperability
	normalizedFilename := norm.NFC.String(file.Filename)

	result := &ResolvedFileRules{
		AssetKey:   normalizedFilename,
		Properties: make(map[string]interface{}),
	}

	for _, rule := range rules {
		if !rule.MatchesRepository(file.Repository) {
			continue
		}

		namedCaptures := make(map[string]string)
		if rule.Pattern != nil {
			match := rule.Pattern.FindStringSubmatch(normalizedFilename)
			if len(match) > 0 {
				if rule.Inverse != nil && *rule.Inverse {
					continue
				}

				for i, name := range rule.Pattern.SubexpNames() {
					if i != 0 && name != "" {
						namedCaptures[name] = match[i]
					}
				}
			} else if rule.Inverse == nil || !*rule.Inverse {
				continue
			}
		}

		err := resolveActions(result, config.ResolveValue(rule.Actions, namedCaptures).([]config.RuleAction), file)
		if err != nil {
			return nil, err
		}
	}

	if err := validateAndSealResult(result); err != nil {
		return nil, err
	}

	return result, nil
}

func validateAndSealResult(resolved *ResolvedFileRules) error {
	for _, processReq := range resolved.ProcessRequests {
		if processReq.RequestKind == "" {
			return fmt.Errorf("unable to determine request for processing task %v", processReq.Name)
		}

		if processReq.QueueName == "" {
			return fmt.Errorf("unable to determine queue for processing task %v", processReq.Name)
		}

		hash, err := hashstructure.Hash(processReq.Config, hashstructure.FormatV2, nil)
		if err != nil {
			return err
		}

		processReq.ConfigHash = strconv.FormatUint(hash, 16)
	}

	return nil
}

func resolveActions(result *ResolvedFileRules, actions []config.RuleAction, file mimir.FileRef) error {
	t := time.Now()
	for _, action := range actions {
		if action.AddTag != nil {
			result.Tags.AssignTag(documents.Tag{Kind: action.AddTag.Kind, Name: action.AddTag.Name}, documents.FilenameRuleTagAssignment{
				FileRef:   file,
				CreatedAt: t,
			})
		}

		if action.Key != nil {
			result.AssetKey = *action.Key
		}

		if action.Ignore != nil {
			result.Ignore = *action.Ignore
		}

		if action.Process != nil {
			name := action.Process.GetName()
			if name != "" {
				err := result.ensureProcessRequest(name).mergeConfig(action.Process)
				if err != nil {
					return err
				}
			}
		}

		if action.Set != nil {
			result.mergeProperties(*action.Set)
		}
	}

	return nil
}
