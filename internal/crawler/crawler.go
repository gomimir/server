package crawler

import (
	"context"
	"strings"

	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/app"
	"gitlab.com/gomimir/server/internal/config"
	"gitlab.com/gomimir/server/internal/dataloader"
	"gitlab.com/gomimir/server/internal/documents"
)

// RegisterHandler - registers task handler
func RegisterHandler(appInstance *app.App) {
	appInstance.RegisterTaskHandler("crawl", func(req mimir.HandleRequest) error {
		if req.Error() != nil {
			return req.Error()
		}

		crawlReq := &Request{}
		if err := req.Task().Parameters().LoadTo(crawlReq); err != nil {
			return err
		}

		ctx := dataloader.CacheContext(appInstance.DefaultContext(context.TODO()))
		err := crawl(ctx, req.Job(), crawlReq, appInstance)
		if err != nil {
			return err
		}

		return nil
	})

	appInstance.RegisterTaskHandler("index", func(req mimir.HandleRequest) error {
		if req.Error() != nil {
			return req.Error()
		}

		indexReq := &IndexRequest{}
		err := req.Task().Parameters().LoadTo(indexReq)
		if err != nil {
			return err
		}

		err = Index(req.Job(), *indexReq, appInstance)
		if err != nil {
			return err
		}

		return nil
	})
}

func crawl(ctx context.Context, job mimir.ActiveJob, req *Request, appInstance *app.App) error {
	log.Info().Interface("fileSets", req.FileSets).Msg("Initiating crawl")

	var fileIndexRequests []FileIndexRequest

	for _, fileSet := range req.FileSets {
		repo, err := dataloader.RepositoryByIDCached(ctx, fileSet.RepositoryID)
		if err != nil {
			return err
		}

		sublog := log.With().Str("repository", string(fileSet.RepositoryID)).Logger()

		indexRules, err := config.IndexRules(ctx, req.IndexID)
		if err != nil {
			return err
		}

		var repoRules []config.Rule
		for _, rule := range indexRules {
			if len(rule.Repositories) > 0 {
				for _, requiredRepo := range rule.Repositories {
					if string(fileSet.RepositoryID) == requiredRepo {
						repoRules = append(repoRules, rule)
						break
					}
				}
			} else {
				repoRules = append(repoRules, rule)
			}
		}

		// FIXME: the prefix can be potentially broken, this is called from GraphQL
		// we have no control over if it is unicode normalized or not and how
		filesCh, err := repo.ListFiles(context.Background(), fileSet.Prefix)

		if err != nil {
			sublog.Error().Err(err).Msg("Unable to list files in repository")
		} else {

			for entry := range filesCh {
				fileRef := mimir.FileRef{
					Repository: string(fileSet.RepositoryID),
					Filename:   entry.GetFilename(),
				}

				sublog := sublog.With().Str("filename", fileRef.Filename).Logger()

				evaluatedRules, err := ResolveFileRules(fileRef, repoRules)
				if err != nil {
					sublog.Error().Err(err).Msg("Unable resolve file rules")
					return err
				}

				if evaluatedRules.Ignore {
					sublog.Info().Msg("Skipping file indexing, ignored by the rules")
					continue
				}

				fileIndexRequests = append(fileIndexRequests, FileIndexRequest{
					IndexName: strings.ToLower(string(req.IndexID)),
					FileRef:   fileRef,
					FileInfo: &documents.FileInfo{
						LastModified: entry.GetLastModified(),
						Size:         entry.GetSize(),
					},
					EvaluatedRules: *evaluatedRules,
				})

				if len(fileIndexRequests) >= 20 {
					err := processFiles(job, req, appInstance, &fileIndexRequests)
					if err != nil {
						return err
					}
				}
			}
		}

	}

	if len(fileIndexRequests) > 0 {
		err := processFiles(job, req, appInstance, &fileIndexRequests)
		if err != nil {
			return err
		}
	}

	return nil
}

func processFiles(job mimir.ActiveJob, crawlReq *Request, appInstance *app.App, fileIndexRequests *[]FileIndexRequest) error {
	indexReq := IndexRequest{
		Files:     *fileIndexRequests,
		Reprocess: crawlReq.Reprocess,
	}

	*fileIndexRequests = nil

	return Index(job, indexReq, appInstance)
}
