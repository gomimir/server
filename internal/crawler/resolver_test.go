package crawler_test

import (
	"regexp"
	"testing"

	"github.com/icza/gox/gox"
	"github.com/stretchr/testify/assert"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/config"
	"gitlab.com/gomimir/server/internal/crawler"
)

func sampleRules() []config.Rule {
	requestKind := "generateThumbnail"
	queueName := "image"

	return []config.Rule{
		{
			Pattern: regexp.MustCompile(`.*`),
			Actions: []config.RuleAction{
				{Process: &config.ProcessAction{
					RequestKind: &requestKind,
					QueueName:   &queueName,
					Config: &map[string]interface{}{
						"variants": map[string]interface{}{
							"s": map[string]interface{}{
								"fitSize": 800,
							},
						},
					},
				}},
			},
		},
		{
			Pattern: regexp.MustCompile(`\.jpg$`),
			Actions: []config.RuleAction{
				{Set: &map[string]interface{}{
					"priority": 1,
				}},
				{Process: &config.ProcessAction{
					RequestKind: &requestKind,
					Config: &map[string]interface{}{
						"variants": map[string]interface{}{
							"m": map[string]interface{}{
								"fitSize": 2000,
							},
						},
					},
				}},
			},
		},
	}
}

func TestResolveFileRules(t *testing.T) {

	resolved, err := crawler.ResolveFileRules(mimir.FileRef{
		Filename:   "photos/some_img.jpg",
		Repository: "minio",
	}, sampleRules())

	if assert.NoError(t, err) {
		assert.False(t, resolved.Ignore)
		assert.Equal(t, map[string]interface{}{
			"priority": 1,
		}, resolved.Properties)
		assert.Equal(t, map[string]interface{}{
			"variants": map[string]interface{}{
				"s": map[string]interface{}{
					"fitSize": 800,
				},
				"m": map[string]interface{}{
					"fitSize": 2000,
				},
			},
		}, resolved.ProcessRequests[0].Config)

		assert.Equal(t, "d4a100e287cee76f", resolved.ProcessRequests[0].ConfigHash)
	}
}

func TestResolveFileRulesWithNoPattern(t *testing.T) {
	rules := []config.Rule{{Actions: []config.RuleAction{{Ignore: gox.NewBool(true)}}}}

	resolved, err := crawler.ResolveFileRules(
		mimir.FileRef{
			Filename:   "photos/some_img.jpg",
			Repository: "minio",
		},
		rules,
	)

	if assert.NoError(t, err) {
		assert.True(t, resolved.Ignore)
	}
}
