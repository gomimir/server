package crawler

import (
	"context"
	"fmt"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/app"
	"gitlab.com/gomimir/server/internal/dataloader"
	"gitlab.com/gomimir/server/internal/documents"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

func Index(job mimir.ActiveJob, req IndexRequest, appInstance *app.App) error {
	var enqueueRequests []mimir.EnqueueTaskOpts
	indices := make(map[string]struct{})

	sublog := log.With().Str("job", job.Name()).Str("job_id", job.ID()).Logger()

	processErr := processFileReqs(job.ID(), req, appInstance, &enqueueRequests, indices, sublog)

	// Ensuring index is up-to-date before scheduling additional processing tasks
	if len(indices) > 0 {
		var indexNames []string
		for indexName := range indices {
			indexNames = append(indexNames, indexName)
		}

		start := time.Now()
		refreshErr := appInstance.Indexer.RefreshIndex(indexNames)
		timeSpent := time.Since(start)
		if refreshErr != nil {
			log.Error().Err(processErr).Msg("Failed to refresh indices")
			if processErr == nil {
				return refreshErr
			}
		}

		log.Trace().Str("took", fmt.Sprintf("%v", timeSpent)).Msg("Indices refreshed")
	}

	// Queue processing tasks
	for _, req := range enqueueRequests {
		_, queueErr := job.EnqueueTask(req)
		if queueErr != nil {
			log.Error().Err(processErr).Msg("Unable to queue task")
			if processErr == nil {
				return queueErr
			}

			break
		}
	}

	return processErr
}

func processFileReqs(jobID string, req IndexRequest, appInstance *app.App, enqueueRequests *[]mimir.EnqueueTaskOpts, indices map[string]struct{}, sublog zerolog.Logger) error {
	for _, assetFileReqs := range req.fileRequestsByAssetKey() {
		if len(assetFileReqs) > 0 {
			// Note: Asset key is build from ref, it is safe to assume that all items of assetFileReqs
			// have the same ref => we are picking the first index
			assetRef := assetFileReqs[0].AssetRef()

			_, err := appInstance.Indexer.EnsureAsset(assetRef, func(asset documents.IndexedAsset) documents.IndexedAsset {
				asset.Key = assetFileReqs[0].EvaluatedRules.AssetKey

				for _, fileReq := range assetFileReqs {
					sublog := sublog.With().Str("repository", fileReq.FileRef.Repository).Str("filename", fileReq.FileRef.Filename).Logger()
					sublog.Info().Msg("Indexing file")

					// Merge tags
					asset.Tags.Merge(fileReq.EvaluatedRules.Tags)

					// Index file
					asset.Files.EnsureByRef(fileReq.FileRef, func(assetFile documents.AssetFile) documents.AssetFile {

						fp := documents.FileProcessing{
							Name:        "fileindex",
							JobID:       jobID,
							ProcessedAt: time.Now(),
						}

						// Ensure file exists and load fresh file info (size, last modified date, ...)
						err := ensureFileInfo(&fileReq, appInstance)
						if err != nil {
							fp.Error = &documents.FileProcessingError{
								Code:    "UNKNOWN",
								Message: err.Error(),
							}
						} else {
							assetFile.FileInfo = *fileReq.FileInfo
							assetFile.Properties.MergeMap(fileReq.EvaluatedRules.Properties)

							if !assetFile.FileInfo.LastModified.IsZero() && assetFile.Properties.Get("date") == nil {
								assetFile.Properties.Set("date", assetFile.FileInfo.LastModified)
							}

							*enqueueRequests = append(
								*enqueueRequests,
								buildProcessingQueue(
									req, fileReq, assetFile, sublog,
								)...,
							)
						}

						assetFile.EnsureProcessing(fp)

						return assetFile
					})
				}

				return asset
			})

			if err != nil {
				return err
			} else {
				indices[assetRef.IndexName()] = struct{}{}
			}
		}
	}

	return nil
}

// ensureFileInfo - fills in file info for requests not originating from repository crawl
func ensureFileInfo(req *FileIndexRequest, appInstance *app.App) error {

	// Load file info if missing
	if req.FileInfo == nil {
		// TODO: replace with dataloader.RepositoryByIDCached
		repo, err := dataloader.RepositoryByID(ent.NewContext(context.TODO(), appInstance.EntClient), pulid.ID(req.Repository))
		if err != nil {
			return err
		}

		file, err := repo.GetFile(context.TODO(), req.Filename)
		if err != nil {
			return err
		}

		// We don't need file data
		file.Reader.Close()

		req.FileInfo = &documents.FileInfo{
			Size: file.Size,
		}

		if file.LastModified != nil {
			req.FileInfo.LastModified = *file.LastModified
		}
	}

	return nil
}

func buildProcessingQueue(req IndexRequest, fileReq FileIndexRequest, assetFile documents.AssetFile, sublog zerolog.Logger) []mimir.EnqueueTaskOpts {
	var enqueueRequests []mimir.EnqueueTaskOpts

	for _, processReq := range fileReq.EvaluatedRules.ProcessRequests {
		fp := assetFile.FindProcessing(processReq.Name)

		var reason string
		if req.shouldReprocess(processReq.Name) {
			reason = "FORCED"
		} else if fp == nil {
			reason = "HAS_NOT_BEEN_PROCESSED_YET"
		} else if fp.ConfigHash != processReq.ConfigHash {
			reason = "CONFIG_CHANGED"
		} else if fp.ProcessedAt.Before(fileReq.FileInfo.LastModified) {
			reason = "SOURCE_FILE_CHANGED"
		} else if fp.Error != nil {
			reason = "PREVIOUSLY_FAILED"
		}

		if reason != "" {
			scheduleLog := sublog.Info().Str("request", processReq.Name).Str("reason", reason)
			scheduleLog.Msg("Scheduling for processing")

			params := map[string]interface{}{
				"asset": fileReq.AssetRef(),
				"file":  fileReq.FileRef,
			}

			if len(processReq.Config) > 0 {
				params["config"] = processReq.Config
			}

			enqueueRequests = append(enqueueRequests, mimir.EnqueueTaskOpts{
				Queue:      processReq.QueueName,
				Task:       processReq.Name,
				Parameters: params,
			})
		} else {
			sublog.Debug().Str("request", processReq.Name).Msg("Processing skipped, nothing to update")
		}
	}

	return enqueueRequests
}
