package crawler

import (
	"strings"

	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

// Request - Crawling request
type Request struct {
	IndexID   pulid.ID   `json:"index"`
	FileSets  []*FileSet `json:"fileSets"`
	Reprocess []string   `json:"reprocess,omitempty"`
}

// FileSet - Configuration about where to put/get files
type FileSet struct {
	RepositoryID pulid.ID `mapstructure:"repository" json:"repository"`
	Prefix       string   `mapstructure:"prefix" json:"prefix"`
}

func (fs FileSet) Includes(another FileSet) bool {
	if fs.RepositoryID != another.RepositoryID {
		return false
	}

	return strings.HasPrefix(another.Prefix, fs.Prefix)
}
