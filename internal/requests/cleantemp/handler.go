package cleantemp

import (
	"context"
	"time"

	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/app"
	"gitlab.com/gomimir/server/internal/dataloader"
	"gitlab.com/gomimir/server/internal/ent/filelocation"
	"gitlab.com/gomimir/server/internal/repository/local"
)

type tempLocation struct {
	Repository *local.Repository
	Prefix     string
}

type deleteFile struct {
	Repository *local.Repository
	Filename   string
}

// RegisterHandler - registers task handler
func RegisterHandler(appInstance *app.App) {
	appInstance.RegisterTaskHandler("cleanTemp", func(req mimir.HandleRequest) error {
		if req.Error() != nil {
			return req.Error()
		}

		return handler(req.Job(), appInstance)
	})
}

func handler(job mimir.ActiveJob, appInstance *app.App) error {
	log.Info().Msg("Cleaning temporary directories")

	ctx := appInstance.DefaultContext(context.TODO())
	ctx = dataloader.CacheContext(ctx)

	fileLocations, err := appInstance.EntClient.FileLocation.Query().Where(filelocation.TypeEQ(filelocation.TypeTemporary)).All(ctx)
	if err != nil {
		return err
	}

	var tempLocations []tempLocation
	for _, fileLocation := range fileLocations {
		repo, err := dataloader.RepositoryByIDCached(ctx, fileLocation.RepositoryID)
		if err != nil {
			return err
		}

		if localRepo, ok := repo.(*local.Repository); ok {
			tempLocations = append(tempLocations, tempLocation{
				Repository: localRepo,
				Prefix:     fileLocation.Prefix,
			})
		}
	}

	now := time.Now()
	var deleteFiles []deleteFile
	for _, tempLocation := range tempLocations {
		entries, err := tempLocation.Repository.ListFiles(ctx, tempLocation.Prefix)
		if err != nil {
			return err
		}

		for entry := range entries {
			if now.Sub(entry.GetLastModified()) > 24*time.Hour {
				deleteFiles = append(deleteFiles, deleteFile{
					Repository: tempLocation.Repository,
					Filename:   entry.GetFilename(),
				})
			}
		}
	}

	if len(deleteFiles) > 0 {
		for _, fileToDelete := range deleteFiles {
			err := fileToDelete.Repository.DeleteFile(ctx, fileToDelete.Filename)
			if err != nil {
				return err
			}
		}

		log.Info().Int("num_files", len(deleteFiles)).Msg("Temporary files cleaned up")
	} else {
		log.Info().Msg("No temporary files to cleanup")
	}

	return nil
}
