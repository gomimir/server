package archiveload

import "gitlab.com/gomimir/server/internal/ent/schema/pulid"

// Request - file indexing request
type Request struct {
	ArchiveURL string `json:"archiveUrl"`

	RepositoryID pulid.ID `json:"repository" mapstructure:"repository"`
	Prefix       string   `json:"prefix"`

	IndexID pulid.ID `json:"index" mapstructure:"index"`
}
