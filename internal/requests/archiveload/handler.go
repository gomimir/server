package archiveload

import (
	"archive/tar"
	"compress/gzip"
	"context"
	"io"
	"net/http"
	"strings"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/app"
	"gitlab.com/gomimir/server/internal/config"
	"gitlab.com/gomimir/server/internal/crawler"
	"gitlab.com/gomimir/server/internal/dataloader"
	"gitlab.com/gomimir/server/internal/documents"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/repository"
	"gitlab.com/gomimir/server/internal/utils"
)

// RegisterHandler - registers task handler
func RegisterHandler(appInstance *app.App) {
	appInstance.RegisterTaskHandler("uploadArchiveFromURL", func(req mimir.HandleRequest) error {
		if req.Error() != nil {
			return req.Error()
		}

		archiveLoadReq := &Request{}
		if err := req.Task().Parameters().LoadTo(archiveLoadReq); err != nil {
			return err
		}

		return handler(req.Job(), archiveLoadReq, appInstance)
	})
}

func handler(job mimir.ActiveJob, req *Request, appInstance *app.App) error {
	ctx := appInstance.DefaultContext(context.TODO())

	repo, err := dataloader.RepositoryByID(ctx, req.RepositoryID)
	if err != nil {
		return err
	}

	rules, err := config.IndexRules(ctx, req.IndexID)
	if err != nil {
		return err
	}

	sublog := log.With().Str("archive_url", req.ArchiveURL).Logger()

	res, err := http.Get(req.ArchiveURL)
	if err != nil {
		sublog.Error().Err(err).Msg("Unable to download file")
		return err
	}

	defer res.Body.Close()

	return UploadArchive(UploadArchiveOpts{
		IndexName:    strings.ToLower(string(req.IndexID)),
		Source:       res.Body,
		RepositoryID: req.RepositoryID,
		Repository:   repo,
		Prefix:       req.Prefix,
		Job:          job,
		IndexRules:   rules,
		Log:          sublog,
		App:          appInstance,
	})
}

type UploadArchiveOpts struct {
	Source io.Reader

	RepositoryID pulid.ID
	Repository   repository.Repository
	Prefix       string

	Job mimir.ActiveJob

	IndexName  string
	IndexRules []config.Rule

	App *app.App
	Log zerolog.Logger
}

func UploadArchive(opts UploadArchiveOpts) error {
	gzipReader, err := gzip.NewReader(opts.Source)
	if err != nil {
		opts.Log.Error().Err(err).Msg("Unable to uncompress GZIP stream")
		return err
	}

	defer gzipReader.Close()

	indexReq := crawler.IndexRequest{}

	tarReader := tar.NewReader(gzipReader)
	for {
		header, err := tarReader.Next()
		if err != nil {
			if err == io.EOF {
				break
			} else {
				opts.Log.Error().Err(err).Msg("Unable to read tar archive")
				return err
			}
		}

		if header.Typeflag == tar.TypeReg {
			filename := utils.JoinPath(opts.Prefix, utils.SanitizeRelPath(header.Name))
			err := opts.Repository.UploadFile(context.TODO(), filename, header.Size, tarReader)
			if err != nil {
				opts.Log.Error().Err(err).Str("filename", header.Name).Msg("Unable to upload archived file")
				return err
			}

			fileRef := mimir.FileRef{
				Repository: string(opts.RepositoryID),
				Filename:   filename,
			}

			sublog := opts.Log.With().Str("filename", fileRef.Filename).Str("repository", fileRef.Repository).Logger()

			evaluatedRules, err := crawler.ResolveFileRules(fileRef, opts.IndexRules)
			if err != nil {
				sublog.Error().Err(err).Msg("Unable resolve file rules")
				return err
			}

			if !evaluatedRules.Ignore {
				fileIndexReq := crawler.FileIndexRequest{
					IndexName: opts.IndexName,
					FileRef:   fileRef,
					FileInfo: &documents.FileInfo{
						LastModified: header.ModTime,
						Size:         header.Size,
					},
					EvaluatedRules: *evaluatedRules,
				}

				indexReq.Files = append(indexReq.Files, fileIndexReq)
			}
		}
	}

	if len(indexReq.Files) > 0 {
		return crawler.Index(opts.Job, indexReq, opts.App)
	}

	return nil
}
