package archivecreate

import mimir "gitlab.com/gomimir/processor"

type FileRequest struct {
	SourceRef      mimir.FileRef `json:"source"`
	TargetFilename string        `json:"targetFilename"`
}

type Request struct {
	DestRef mimir.FileRef `json:"destination"`
	Files   []FileRequest
}
