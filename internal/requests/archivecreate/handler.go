package archivecreate

import (
	"archive/zip"
	"context"
	"io"
	"regexp"
	"time"

	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/app"
	"gitlab.com/gomimir/server/internal/dataloader"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

// RegisterHandler - registers task handler
func RegisterHandler(appInstance *app.App) {
	appInstance.RegisterTaskHandler("createArchive", func(req mimir.HandleRequest) error {
		if req.Error() != nil {
			return req.Error()
		}

		handlerReq := &Request{}
		if err := req.Task().Parameters().LoadTo(handlerReq); err != nil {
			return err
		}

		start := time.Now()
		err := handler(req.Job(), handlerReq, appInstance)
		if err == nil {
			log.Info().Str("took", time.Since(start).String()).Msg("Archive job finished")
		} else {
			log.Error().Str("took", time.Since(start).String()).Err(err).Msg("Archive job failed")
		}

		return err
	})
}

func handler(job mimir.ActiveJob, req *Request, appInstance *app.App) error {
	ctx := appInstance.DefaultContext(context.TODO())

	destRepo, err := dataloader.RepositoryByID(ctx, pulid.ID(req.DestRef.Repository))
	if err != nil {
		return err
	}

	if err != nil {
		return err
	}

	var bucketNameRx = regexp.MustCompile(`^(^[^/]+)\/?`)
	m := bucketNameRx.FindStringSubmatch(req.DestRef.Filename)
	if len(m) > 1 {
		err = destRepo.EnsureTemp(ctx, m[1])
		if err != nil {
			return err
		}
	}

	closers := make(map[io.Closer]struct{})
	defer func() {
		for closer := range closers {
			closer.Close()
		}
	}()

	archiveReader, archiveWriter := io.Pipe()

	waitC := make(chan error, 1)
	go func() {
		err := destRepo.UploadFile(context.Background(), req.DestRef.Filename, -1, archiveReader)
		archiveReader.Close()
		waitC <- err
	}()

	closers[archiveWriter] = struct{}{}

	start := time.Now()

	zipWriter := zip.NewWriter(archiveWriter)
	closers[zipWriter] = struct{}{}

	for _, fileReq := range req.Files {
		repo, err := dataloader.RepositoryByID(ctx, pulid.ID(fileReq.SourceRef.Repository))
		if err != nil {
			return err
		}

		file, err := repo.GetFile(context.Background(), fileReq.SourceRef.Filename)
		if err != nil {
			return err
		}

		closers[file.Reader] = struct{}{}

		fh := &zip.FileHeader{
			Name: fileReq.TargetFilename,
			// TODO: detect based on file extension and use zip.Deflate for non-image files
			Method: zip.Store,
		}

		fileWriter, err := zipWriter.CreateHeader(fh)
		if err != nil {
			return err
		}

		_, err = io.Copy(fileWriter, file.Reader)
		if err != nil {
			return err
		}

		err = file.Reader.Close()
		if err != nil {
			return err
		}

		delete(closers, file.Reader)
	}

	err = zipWriter.Close()
	if err != nil {
		return err
	}

	delete(closers, zipWriter)

	log.Debug().Str("took", time.Since(start).String()).Msg("Compression finished, waiting for upload to finish")

	err = archiveWriter.Close()
	if err != nil {
		return err
	}

	delete(closers, archiveWriter)

	return <-waitC
}
