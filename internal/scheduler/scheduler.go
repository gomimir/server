package scheduler

import (
	"context"
	"time"
)

type TaskSchedule struct {
	TaskName string
	NextTime func() *time.Time
}

type Scheduler interface {
	SetupSchedule(ctx context.Context, schedule ...TaskSchedule) error
	Run(ctx context.Context) error
}

func RepeatEvery(dur time.Duration) func() *time.Time {
	return func() *time.Time {
		t := time.Now().Add(dur)
		return &t
	}
}

func RepeatAfter(dur ...time.Duration) func() *time.Time {
	// Note: it is not thread-safe, but we do not expect multiple schedulers running at the same time within single process
	iter := 0
	return func() *time.Time {
		if len(dur) > iter {
			t := time.Now().Add(dur[iter])
			iter++
			return &t
		}

		return nil
	}
}
