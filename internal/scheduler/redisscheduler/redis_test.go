package redisscheduler_test

import (
	"context"

	"os"
	"testing"
	"time"

	"github.com/alicebob/miniredis/v2"
	"github.com/go-redis/redis/v8"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/require"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/scheduler"
	"gitlab.com/gomimir/server/internal/scheduler/redisscheduler"
)

type mockedTaskManager struct {
	queueOptsOut *[]mimir.QueueJobOpts
}

func testTaskManager(queueOptsOut *[]mimir.QueueJobOpts) redisscheduler.TaskManager {
	return &mockedTaskManager{
		queueOptsOut: queueOptsOut,
	}
}

func (tm *mockedTaskManager) QueueJob(ctx context.Context, opts mimir.QueueJobOpts) (*mimir.QueueJobResult, error) {
	*tm.queueOptsOut = append(*tm.queueOptsOut, opts)

	return nil, nil
}

func TestMain(m *testing.M) {
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.Stamp})
	os.Exit(m.Run())
}

func TestRedisScheduler(t *testing.T) {
	mr, err := miniredis.Run()
	if err != nil {
		panic(err)
	}

	client := redis.NewClient(&redis.Options{
		Addr: mr.Addr(),
	})

	var calledQueueJobs []mimir.QueueJobOpts
	tm := testTaskManager(&calledQueueJobs)

	sched := redisscheduler.NewScheduler(client, tm)

	err = sched.SetupSchedule(context.Background(), scheduler.TaskSchedule{
		TaskName: "test",
		NextTime: scheduler.RepeatAfter(0, 1*time.Second),
	})

	require.NoError(t, err)

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	err = sched.Run(ctx)
	require.ErrorIs(t, err, context.DeadlineExceeded)

	require.Equal(t, []mimir.QueueJobOpts{
		{
			Name:     "scheduled_test",
			Internal: true,
			Enqueue: []mimir.EnqueueTaskOpts{
				{
					Queue: "main",
					Task:  "test",
				},
			},
		},
		{
			Name:     "scheduled_test",
			Internal: true,
			Enqueue: []mimir.EnqueueTaskOpts{
				{
					Queue: "main",
					Task:  "test",
				},
			},
		},
	}, calledQueueJobs)
}
