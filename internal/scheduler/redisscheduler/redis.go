package redisscheduler

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/server/internal/scheduler"
)

const keyZSet = "schedule"
const keyChannel = "schedule_chan"

func keyTaskLock(task string) string { return fmt.Sprintf("scheduler_task_lock:%v", task) }

const taskTimeout = 10 * time.Second
const redisCmdTimeout = 3 * time.Second

type TaskManager interface {
	QueueJob(ctx context.Context, opts mimir.QueueJobOpts) (*mimir.QueueJobResult, error)
}

type RedisScheduler struct {
	schedule map[string]scheduler.TaskSchedule
	client   *redis.Client
	timer    *time.Timer
	tm       TaskManager
}

func NewScheduler(client *redis.Client, tm TaskManager) *RedisScheduler {
	return &RedisScheduler{
		schedule: make(map[string]scheduler.TaskSchedule),
		client:   client,
		tm:       tm,
	}
}

func (rs *RedisScheduler) SetupSchedule(ctx context.Context, schedule ...scheduler.TaskSchedule) error {

	var items []*redis.Z
	for _, taskSchedule := range schedule {
		rs.schedule[taskSchedule.TaskName] = taskSchedule

		items = append(items, &redis.Z{
			Member: taskSchedule.TaskName,
			Score:  float64(taskSchedule.NextTime().Unix()),
		})
	}

	cmdCtx, cancel := context.WithTimeout(ctx, redisCmdTimeout)
	defer cancel()

	var zAddRes *redis.IntCmd
	_, err := rs.client.TxPipelined(cmdCtx, func(p redis.Pipeliner) error {
		// Note: ideally it should be ZAddGT to update scheduled items in case we moved their scheduled execution closer
		// but there is no such method in the client library.
		zAddRes = p.ZAddNX(cmdCtx, keyZSet, items...)
		p.Publish(cmdCtx, keyChannel, "refresh")

		return nil
	})

	if err != nil {
		return err
	}

	if zAddRes.Val() > 0 {
		log.Info().Int64("num_updated_tasks", zAddRes.Val()).Msg("Schedule updated")
	} else {
		log.Info().Msg("Schedule up to date")
	}

	return nil
}

func (rs *RedisScheduler) Run(ctx context.Context) error {
	refreshC := make(chan struct{}, 1)
	var exitWg sync.WaitGroup
	var mainLoopErr error

	mainLoopCtx, cancelMainLoop := context.WithCancel(ctx)

	exitWg.Add(1)
	go func() {
		mainLoopErr = rs.mainLoop(mainLoopCtx, refreshC)
		log.Trace().Msg("Scheduler run loop stopped")
		exitWg.Done()
	}()

	cmdCtx, cancel := context.WithTimeout(ctx, redisCmdTimeout)
	defer cancel()

	sub := rs.client.Subscribe(cmdCtx, keyChannel)

	exitWg.Add(1)
	go func() {
		ctxDoneC := ctx.Done()

		defer func() {
			cancelMainLoop()
			log.Trace().Msg("Stopped listening for schedule changes")
			exitWg.Done()
		}()

		for {
			messageC := sub.Channel()
			select {
			case _, ok := <-messageC:
				if !ok {
					continue
				}

				refreshC <- struct{}{}
				if timer := rs.timer; timer != nil {
					if !timer.Stop() {
						<-timer.C
					}
				}

			case <-ctxDoneC:
				sub.Close()
				return
			}
		}
	}()

	exitWg.Wait()
	return mainLoopErr
}

func (rs *RedisScheduler) tryToProcessNext(ctx context.Context) error {
	cmdCtx, cancel := context.WithTimeout(ctx, redisCmdTimeout)
	defer cancel()

	res := rs.client.ZRangeByScoreWithScores(cmdCtx, keyZSet, &redis.ZRangeBy{
		Count: 1,
		Min:   "0",
		Max:   "inf",
	})

	items, err := res.Result()
	if err != nil {
		return err
	} else if len(items) == 1 {

		if taskName, ok := items[0].Member.(string); ok {
			nextRunScheduledAt := time.Unix(int64(items[0].Score), 0)
			now := time.Now()

			if nextRunScheduledAt.Before(now) {
				log.Debug().Str("task", taskName).Msg("Scheduled task is due for execution")

				// Set timer to try again in case task times out
				cooldown := taskTimeout + 1*time.Second
				rs.setTimer(cooldown)

				log.Trace().Str("task", taskName).Msg("Acquiring lock for task")
				locked, err := rs.lockTask(ctx, taskName, taskTimeout)
				if err != nil {
					return err
				}

				if locked {
					log.Debug().Str("task", taskName).Str("ttl", taskTimeout.String()).Msg("Task lock acquired")

					execCtx, execCancel := context.WithTimeout(ctx, taskTimeout)
					defer execCancel()

					rs.executeTask(execCtx, taskName)

					delCtx, delCancel := context.WithTimeout(ctx, redisCmdTimeout)
					defer delCancel()

					var err error
					var nextRunAt *time.Time
					if taskSchedule, ok := rs.schedule[taskName]; ok {
						nextRunAt = taskSchedule.NextTime()
					}

					if nextRunAt != nil {
						log.Info().Str("task", taskName).Time("next_run_at", *nextRunAt).Msg("Scheduling next run")
						err = rs.client.ZAdd(delCtx, keyZSet, &redis.Z{
							Member: taskName,
							Score:  float64(nextRunAt.Unix()),
						}).Err()
					} else {
						log.Info().Str("task", taskName).Msg("No schedule for the next run, removing")
						err = rs.client.ZRem(delCtx, keyZSet, taskName).Err()
					}

					if err != nil {
						return err
					}

					rs.unlockTask(ctx, taskName)
					if err != nil {
						return err
					}

					rs.setTimer(100 * time.Millisecond)
				} else {
					log.Debug().Str("task", taskName).Str("cooldown", cooldown.String()).Msg("Not able to acquire task lock. Some other process is processing it. Will try again after cooldown.")
				}

			} else {
				log.Debug().Str("task", taskName).Time("next_run_at", nextRunScheduledAt).Msg("Scheduler is sleeping until the next run")
				rs.setTimer(nextRunScheduledAt.Sub(now))
			}
		}
	}

	return nil
}

func (rs *RedisScheduler) executeTask(ctx context.Context, taskName string) {
	log.Info().Str("task", taskName).Msg("Enqueuing scheduled task for execution")
	rs.tm.QueueJob(ctx, mimir.QueueJobOpts{
		Name:     fmt.Sprintf("scheduled_%v", taskName),
		Internal: true,
		Enqueue: []mimir.EnqueueTaskOpts{
			{
				Queue: "main",
				Task:  taskName,
			},
		},
	})
}

// Note: we might want to replace this with something more robust
func (rs *RedisScheduler) lockTask(ctx context.Context, taskName string, ttl time.Duration) (bool, error) {
	cmdCtx, cancel := context.WithTimeout(ctx, 3*redisCmdTimeout)
	defer cancel()

	return rs.client.SetNX(cmdCtx, keyTaskLock(taskName), true, ttl).Result()
}

// Note: we might want to replace this with something more robust
func (rs *RedisScheduler) unlockTask(ctx context.Context, taskName string) error {
	cmdCtx, cancel := context.WithTimeout(ctx, 3*redisCmdTimeout)
	defer cancel()

	return rs.client.Del(cmdCtx, keyTaskLock(taskName)).Err()
}

func (rs *RedisScheduler) mainLoop(ctx context.Context, refreshC <-chan struct{}) error {
	defer func() {
		if timer := rs.timer; timer != nil {
			timer.Stop()
		}
	}()

	ctxDoneC := ctx.Done()

	for {
		err := rs.tryToProcessNext(ctx)
		if err != nil {
			return err
		}

		var timerC <-chan time.Time
		if timer := rs.timer; timer != nil {
			timerC = timer.C
		}

		select {
		case <-ctxDoneC:
			return ctx.Err()
		case <-timerC:
		case <-refreshC:
			log.Info().Msg("Refreshing scheduled items")
		}
	}
}

func (rs *RedisScheduler) setTimer(dur time.Duration) {
	if rs.timer == nil {
		rs.timer = time.NewTimer(dur)
	} else {
		rs.timer.Reset(dur)
	}
}
