package auth

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"
)

type ProviderType string

const (
	Provider_None  ProviderType = "none"
	Provider_Proxy ProviderType = "proxy"
	Provider_OIDC  ProviderType = "oidc"
)

func (pt ProviderType) MarshalGQL(w io.Writer) {
	fmt.Fprint(w, strconv.Quote(strings.ToUpper(string(pt))))
}

// UnmarshalGQL - unmarshaling is not implemented, we do not use it as an input type
// Gqlgen still checking its presence in order to recognize the marshaling interface.
func (pt *ProviderType) UnmarshalGQL(v interface{}) error {
	panic(errors.New("not implemented"))
}

type Provider interface {
	Type() ProviderType
	WebsocketInit(ctx context.Context, initPayload map[string]interface{}) (context.Context, error)
	Middleware(next http.Handler) http.Handler
}
