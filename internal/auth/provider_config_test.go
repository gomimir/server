package auth_test

import (
	"testing"

	"github.com/mitchellh/mapstructure"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/auth"
)

func TestProviderConfigHook(t *testing.T) {
	var decoded struct {
		Provider auth.ProviderConfig
	}

	d, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		DecodeHook: auth.ProviderConfigHook,
		Result:     &decoded,
	})

	if assert.NoError(t, err) {
		err := d.Decode(map[string]interface{}{
			"provider": map[string]interface{}{
				"type":        "proxy",
				"userHeader":  "X-Authenticated-User",
				"rolesHeader": "X-Authenticated-Roles",
			},
		})

		if assert.NoError(t, err) {
			assert.Equal(t, auth.ProxyConfig{
				UserHeader:  "X-Authenticated-User",
				RolesHeader: "X-Authenticated-Roles",
			}, decoded.Provider)
		}
	}
}
