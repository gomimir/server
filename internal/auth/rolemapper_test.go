package auth_test

import (
	"context"

	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

type testRoleMapper struct{}

func (*testRoleMapper) GuestRoleIDs(ctx context.Context) ([]pulid.ID, error) {
	return nil, nil
}

func (*testRoleMapper) AuthenticatedRoleIDs(ctx context.Context, additionalRoleNames ...string) ([]pulid.ID, error) {
	var mapped []pulid.ID
	for _, roleName := range additionalRoleNames {
		mapped = append(mapped, pulid.ID(roleName))
	}

	return mapped, nil
}
