package auth

import (
	"context"

	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

type RoleMapper interface {
	GuestRoleIDs(ctx context.Context) ([]pulid.ID, error)
	AuthenticatedRoleIDs(ctx context.Context, additionalRoleNames ...string) ([]pulid.ID, error)
}
