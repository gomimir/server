package auth

import (
	"context"

	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

type UserSyncRequest struct {
	// Username by which we do synchronize. Required.
	Username string

	// Role IDs to map to the user. Required at least one.
	RoleIDs []pulid.ID

	// Name to show in an application
	DisplayName *string
}

type UserSynchronizer interface {
	User(ctx context.Context, req UserSyncRequest) (pulid.ID, error)
}
