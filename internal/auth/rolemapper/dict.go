package rolemapper

import (
	"context"

	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/ent/userrole"
)

type DictionaryRoleMapperConfig struct {
	// Role names for any anonymous user
	GuestRoles []string `json:"guest"`
	// Role names for any authenticated user
	UserRoles []string `json:"authenticated"`
	// Additional name mapping for stuff coming providers
	AdditionalRoles map[string][]string `json:"additional"`
}

type DictionaryRoleMapper struct {
	config DictionaryRoleMapperConfig
}

func NewDictionaryRoleMapper(config DictionaryRoleMapperConfig) *DictionaryRoleMapper {
	return &DictionaryRoleMapper{config}
}

func (rm *DictionaryRoleMapper) GuestRoleIDs(ctx context.Context) ([]pulid.ID, error) {
	return resolveIDs(ctx, rm.config.GuestRoles)
}

func (rm *DictionaryRoleMapper) AuthenticatedRoleIDs(ctx context.Context, additionalRoleNames ...string) ([]pulid.ID, error) {
	roleNames := append([]string(nil), rm.config.UserRoles...)
	for _, roleName := range additionalRoleNames {
		if rm.config.AdditionalRoles != nil {
			if tr := rm.config.AdditionalRoles[roleName]; tr != nil {
				roleNames = append(roleNames, tr...)
				continue
			}
		}

		roleNames = append(roleNames, roleName)
	}

	return resolveIDs(ctx, roleNames)
}

func resolveIDs(ctx context.Context, roleNames []string) ([]pulid.ID, error) {
	entClient := ent.FromContext(ctx)
	roles, err := entClient.UserRole.Query().Where(userrole.NameIn(roleNames...)).All(ctx)
	if err != nil {
		return nil, err
	}

	roleIDs := make([]pulid.ID, len(roles))
	for i, role := range roles {
		roleIDs[i] = role.ID
	}

	return roleIDs, nil
}
