package auth

type Operation string

const (
	OP_List   Operation = "LIST"
	OP_Create Operation = "CREATE"
	OP_Read   Operation = "READ"
	OP_Update Operation = "UPDATE"
	OP_Delete Operation = "DELETE"

	// Index specific
	OP_Crawl  Operation = "CRAWL"
	OP_Upload Operation = "UPLOAD"

	// Asset specific
	OP_Process Operation = "PROCESS"
)
