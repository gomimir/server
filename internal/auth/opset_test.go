package auth_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/auth"
)

func TestOpsetSortedArray(t *testing.T) {
	opset := auth.OpSet(auth.OP_Upload, auth.OP_Crawl, auth.OP_Delete)
	assert.Equal(t, []auth.Operation{auth.OP_Crawl, auth.OP_Delete, auth.OP_Upload}, opset.SortedArray())
}

func TestOpsetString(t *testing.T) {
	opset := auth.OpSet(auth.OP_Upload, auth.OP_Crawl, auth.OP_Delete)
	assert.Equal(t, "CRAWL, DELETE, UPLOAD", opset.String())
}
