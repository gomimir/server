package usersync_test

import (
	"context"
	"testing"

	"github.com/icza/gox/gox"
	"github.com/stretchr/testify/require"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/auth/usersync"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/ent/testdata"
	"gitlab.com/gomimir/server/internal/ent/user"
	"gitlab.com/gomimir/server/internal/idprovider"
	"gitlab.com/gomimir/server/internal/testutils"
)

func TestUserSync(t *testing.T) {
	client := testutils.EntClient(t)
	defer client.Close()

	ctx := context.Background()
	ctx = ent.NewContext(ctx, client)
	ctx = idprovider.ProviderContext(ctx, idprovider.NewTestProvider())

	var adminRole *ent.UserRole
	var userRole *ent.UserRole
	databuilder := testdata.NewTestDataBuilder(ctx)
	databuilder.Require(testdata.UserRoleUser().Ref(&userRole), testdata.UserRoleAdmin().Ref(&adminRole), testdata.UserBob())

	t.Run("NewUser", func(t *testing.T) {
		sync := usersync.NewSynchronizer()
		uid, err := sync.User(ctx, auth.UserSyncRequest{
			Username:    "newuser",
			RoleIDs:     []pulid.ID{userRole.ID},
			DisplayName: gox.NewString("New user"),
		})

		require.NoError(t, err)

		user, err := client.User.Get(ctx, uid)
		require.NoError(t, err)
		require.Equal(t, "newuser", user.Username)

		roles, err := user.Roles(ctx)
		require.NoError(t, err)
		require.Len(t, roles, 1)
		require.Equal(t, userRole.ID, roles[0].ID)
	})

	t.Run("ExistingUser", func(t *testing.T) {
		sync := usersync.NewSynchronizer()
		uid, err := sync.User(ctx, auth.UserSyncRequest{
			Username:    "bob",
			RoleIDs:     []pulid.ID{adminRole.ID},
			DisplayName: gox.NewString("Bob Bobovic"),
		})

		require.NoError(t, err)

		userEnt, err := client.User.Get(ctx, uid)
		require.NoError(t, err)
		require.Equal(t, "bob", userEnt.Username)
		require.Equal(t, "Bob Bobovic", userEnt.DisplayName)

		roles, err := userEnt.Roles(ctx)
		require.NoError(t, err)
		require.Len(t, roles, 1)
		require.Equal(t, adminRole.ID, roles[0].ID)

		newUser, err := client.User.Query().Where(user.Username("newuser")).First(ctx)
		require.NoError(t, err)
		require.Equal(t, "New user", newUser.DisplayName)
	})
}
