package usersync

import (
	"context"

	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
	"gitlab.com/gomimir/server/internal/ent/user"
)

type Synchronizer struct{}

func NewSynchronizer() *Synchronizer {
	return &Synchronizer{}
}

func (*Synchronizer) User(ctx context.Context, req auth.UserSyncRequest) (pulid.ID, error) {
	entClient := ent.FromContext(ctx)

	tx, err := entClient.Tx(ctx)
	if err != nil {
		return "", err
	}

	userEnt, err := tx.User.Query().Where(user.Username(req.Username)).First(ctx)
	if err != nil {
		if ent.IsNotFound(err) {
			userEnt, err = tx.User.Create().
				SetUsername(req.Username).
				SetNillableDisplayName(req.DisplayName).
				AddRoleIDs(req.RoleIDs...).
				Save(ctx)

			if err != nil {
				return "", ent.RollbackHelper(tx, err)
			}

			err = tx.Commit()
			if err != nil {
				return "", err
			}

			return userEnt.ID, nil
		}

		return "", ent.RollbackHelper(tx, err)
	}

	err = tx.User.Update().
		Where(user.Username(req.Username)).
		ClearRoles().
		AddRoleIDs(req.RoleIDs...).
		SetNillableDisplayName(req.DisplayName).
		Exec(ctx)

	if err != nil {
		return "", ent.RollbackHelper(tx, err)
	}

	err = tx.Commit()
	if err != nil {
		return "", err
	}

	return userEnt.ID, nil
}
