package permissions

import (
	"context"

	"gitlab.com/gomimir/server/internal/apperror"
	"gitlab.com/gomimir/server/internal/auth"
)

type ResourcePermissions struct {
	resource                    auth.ResourceType
	AllowedCollectionOperations auth.OperationSet
	AllowedItemOperations       auth.OperationSet
}

func (perms *ResourcePermissions) CheckCollectionPermissions(op auth.Operation) error {
	if !perms.AllowedCollectionOperations.Contains(op) {
		return apperror.New(apperror.Forbidden, "not allowed to %v on resource type %v", op, perms.resource)
	}

	return nil
}

func (perms *ResourcePermissions) CheckItemPermissions(op auth.Operation) error {
	if !perms.AllowedItemOperations.Contains(op) {
		return apperror.New(apperror.Forbidden, "not allowed to %v on resource type %v", op, perms.resource)
	}

	return nil
}

type GlobalPermissionsAccessor struct {
	globalPermissions map[auth.ResourceType]*ResourcePermissions
}

func GlobalPermissions(ctx context.Context) (*GlobalPermissionsAccessor, error) {
	cache, err := cacheFromContext(ctx)
	if err != nil {
		return nil, err
	}

	globalPermissions, err := cache.ensureGlobalPermissions(ctx)
	if err != nil {
		return nil, err
	}

	return &GlobalPermissionsAccessor{globalPermissions}, nil
}

func (gp *GlobalPermissionsAccessor) All() map[auth.ResourceType]*ResourcePermissions {
	return gp.globalPermissions
}

func (gp *GlobalPermissionsAccessor) For(resource auth.ResourceType) *ResourcePermissions {
	resourcePermissions := gp.globalPermissions[resource]
	if resourcePermissions == nil {
		return &ResourcePermissions{
			resource: resource,
		}
	} else {
		return resourcePermissions
	}
}

func CheckCollectionPermissions(ctx context.Context, resource auth.ResourceType, op auth.Operation) error {
	perms, err := GlobalPermissions(ctx)
	if err != nil {
		return err
	}

	return perms.For(resource).CheckCollectionPermissions(op)
}

func CheckItemPermissions(ctx context.Context, resource auth.ResourceType, op auth.Operation) error {
	perms, err := GlobalPermissions(ctx)
	if err != nil {
		return err
	}

	return perms.For(resource).CheckItemPermissions(op)
}
