package permissions

import (
	"context"

	"github.com/graph-gophers/dataloader"
	"gitlab.com/gomimir/server/internal/apperror"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

type ResolvedIndexPermissions struct {
	AllowedIndexOperations           auth.OperationSet
	AllowedAssetOperations           auth.OperationSet
	AllowedAssetCollectionOperations auth.OperationSet
}

func IndexPermissions(ctx context.Context, indexID pulid.ID) (*ResolvedIndexPermissions, error) {
	cache, err := cacheFromContext(ctx)
	if err != nil {
		return nil, err
	}

	globalPerms, err := cache.ensureGlobalPermissions(ctx)
	if err != nil {
		return nil, err
	}

	thunk := cache.indexLoader.Load(ctx, dataloader.StringKey(string(indexID)))
	res, err := thunk()
	if err != nil {
		return nil, err
	}

	indexPerms := res.(*ResolvedIndexPermissions)

	result := &ResolvedIndexPermissions{
		AllowedIndexOperations:           make(auth.OperationSet),
		AllowedAssetOperations:           make(auth.OperationSet),
		AllowedAssetCollectionOperations: make(auth.OperationSet),
	}

	result.AllowedIndexOperations.Merge(indexPerms.AllowedIndexOperations)
	if resourcePermissions := globalPerms[auth.RT_Index]; resourcePermissions != nil {
		result.AllowedIndexOperations.Merge(resourcePermissions.AllowedItemOperations)
	}

	result.AllowedAssetOperations.Merge(indexPerms.AllowedAssetOperations)
	if resourcePermissions := globalPerms[auth.RT_Asset]; resourcePermissions != nil {
		result.AllowedAssetOperations.Merge(resourcePermissions.AllowedItemOperations)
	}

	result.AllowedAssetCollectionOperations.Merge(indexPerms.AllowedAssetCollectionOperations)
	if resourcePermissions := globalPerms[auth.RT_Asset]; resourcePermissions != nil {
		result.AllowedAssetCollectionOperations.Merge(resourcePermissions.AllowedCollectionOperations)
	}

	return result, nil
}

func CheckIndexPermissions(ctx context.Context, indexID pulid.ID, op auth.Operation) error {
	perms, err := IndexPermissions(ctx, indexID)
	if err != nil {
		return err
	}

	if !perms.AllowedIndexOperations.Contains(op) {
		return apperror.New(apperror.Forbidden, "not allowed to %v on index %v", op, indexID)
	}

	return nil
}

func CheckAssetCollectionPermissions(ctx context.Context, indexID pulid.ID, op auth.Operation) error {
	perms, err := IndexPermissions(ctx, indexID)
	if err != nil {
		return err
	}

	if !perms.AllowedAssetCollectionOperations.Contains(op) {
		return apperror.New(apperror.Forbidden, "not allowed to %v on assets in index %v", op, indexID)
	}

	return nil
}

func CheckAssetPermissions(ctx context.Context, indexID pulid.ID, op auth.Operation) error {
	perms, err := IndexPermissions(ctx, indexID)
	if err != nil {
		return err
	}

	if !perms.AllowedAssetOperations.Contains(op) {
		return apperror.New(apperror.Forbidden, "not allowed to %v on assets in index %v", op, indexID)
	}

	return nil
}
