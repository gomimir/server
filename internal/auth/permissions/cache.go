package permissions

import (
	"context"
	"sync"

	"entgo.io/ent/dialect/sql"
	"github.com/graph-gophers/dataloader"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/ent"
	"gitlab.com/gomimir/server/internal/ent/globalpermission"
	"gitlab.com/gomimir/server/internal/ent/indexpermission"
)

type PermissionsCache struct {
	globalPermissions         map[auth.ResourceType]*ResourcePermissions
	globalPermissionsErr      error
	globalPermissionsLoadOnce sync.Once

	indexLoader *dataloader.Loader
}

func newPermissionsCache() *PermissionsCache {
	return &PermissionsCache{
		indexLoader: dataloader.NewBatchedLoader(loadIndexPermissions),
	}
}

func (pc *PermissionsCache) ensureGlobalPermissions(ctx context.Context) (map[auth.ResourceType]*ResourcePermissions, error) {
	pc.globalPermissionsLoadOnce.Do(func() {
		pc.globalPermissions, pc.globalPermissionsErr = loadGlobalPermissions(ctx)
	})

	return pc.globalPermissions, pc.globalPermissionsErr
}

func loadGlobalPermissions(ctx context.Context) (map[auth.ResourceType]*ResourcePermissions, error) {
	session, err := auth.SessionForContext(ctx)
	if err != nil {
		return nil, err
	}

	globalPermissions := make(map[auth.ResourceType]*ResourcePermissions)
	if len(session.Identities()) > 0 {
		entClient := ent.FromContext(ctx)

		records, err := entClient.GlobalPermission.Query().Where(func(s *sql.Selector) {
			s.Where(
				sql.In(globalpermission.FieldSubjectID, session.Identities()...),
			)
		}).All(ctx)

		if err != nil {
			return nil, err
		}

		for _, record := range records {
			resourcePermissions := globalPermissions[record.Resource]
			if resourcePermissions == nil {
				resourcePermissions = &ResourcePermissions{
					resource:                    record.Resource,
					AllowedCollectionOperations: make(auth.OperationSet),
					AllowedItemOperations:       make(auth.OperationSet),
				}
				globalPermissions[record.Resource] = resourcePermissions
			}

			resourcePermissions.AllowedCollectionOperations.Add(record.CollectionOperations...)
			resourcePermissions.AllowedItemOperations.Add(record.ItemOperations...)
		}
	}

	return globalPermissions, nil
}

func loadIndexPermissions(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
	results := make([]*dataloader.Result, len(keys))

	session, err := auth.SessionForContext(ctx)
	if err != nil {
		for i := range keys {
			results[i] = &dataloader.Result{
				Error: err,
			}
		}

		return results
	}

	indexIDs := make([]interface{}, len(keys))
	byID := make(map[string]*ResolvedIndexPermissions)

	for i, key := range keys {
		indexIDs[i] = key.String()
		byID[key.String()] = &ResolvedIndexPermissions{
			AllowedIndexOperations:           make(auth.OperationSet),
			AllowedAssetOperations:           make(auth.OperationSet),
			AllowedAssetCollectionOperations: make(auth.OperationSet),
		}
		results[i] = &dataloader.Result{
			Data: byID[key.String()],
		}
	}

	if len(session.Identities()) > 0 {
		entClient := ent.FromContext(ctx)
		records, err := entClient.IndexPermission.Query().Where(func(s *sql.Selector) {
			s.Where(
				sql.And(
					sql.In(indexpermission.FieldIndexID, indexIDs...),
					sql.In(indexpermission.FieldSubjectID, session.Identities()...),
				),
			)
		}).All(ctx)

		if err != nil {
			for i := range keys {
				results[i] = &dataloader.Result{
					Error: err,
				}
			}

			return results
		}

		for _, record := range records {
			byID[string(record.IndexID)].AllowedIndexOperations.Add(record.IndexOperations...)
			byID[string(record.IndexID)].AllowedAssetOperations.Add(record.AssetOperations...)
			byID[string(record.IndexID)].AllowedAssetCollectionOperations.Add(record.AssetCollectionOperations...)
		}
	}

	return results
}
