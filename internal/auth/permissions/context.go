package permissions

import (
	"context"
	"errors"
)

type contextKey struct {
	name string
}

var ctxKey = &contextKey{"permissions"}

func CacheContext(parent context.Context) context.Context {
	return context.WithValue(parent, ctxKey, newPermissionsCache())
}

func cacheFromContext(ctx context.Context) (*PermissionsCache, error) {
	if session, ok := ctx.Value(ctxKey).(*PermissionsCache); ok {
		return session, nil
	}

	return nil, errors.New("unable to retrieve permissions cache from context")
}
