package auth

import (
	"context"
	"net/http"

	"github.com/rs/zerolog/log"
)

type DummyProvider struct {
	RoleMapper RoleMapper
}

func NewDummyProvider(roleMapper RoleMapper) (*DummyProvider, error) {
	return &DummyProvider{
		RoleMapper: roleMapper,
	}, nil
}

func (*DummyProvider) Type() ProviderType {
	return Provider_None
}

func (p *DummyProvider) WebsocketInit(ctx context.Context, initPayload map[string]interface{}) (context.Context, error) {
	// Nothing to do, it should already be there because of Middleware()
	return ctx, nil
}

func (p *DummyProvider) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		roleIDs, err := p.RoleMapper.GuestRoleIDs(r.Context())
		if err != nil {
			log.Error().Err(err).Msg("Auth middleware failed")
			w.WriteHeader(500)

			return
		}

		session := NewSession(Session_AuthNotAttempted, roleIDs...)

		ctx := SessionContext(r.Context(), session)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
