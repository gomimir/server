package auth

import (
	"fmt"
	"reflect"
	"time"

	"github.com/mitchellh/mapstructure"
)

type ProviderConfig interface {
	Type() ProviderType
}

func ProviderConfigHook(from reflect.Type, to reflect.Type, data interface{}) (interface{}, error) {
	if to == reflect.TypeOf((*ProviderConfig)(nil)).Elem() {
		var partial struct {
			Type   string                 `mapstructure:"type"`
			Config map[string]interface{} `mapstructure:",remain"`
		}

		err := mapstructure.Decode(data, &partial)
		if err != nil {
			return nil, err
		}

		switch partial.Type {
		case string(Provider_Proxy):
			var config ProxyConfig
			if err := decodeConfig(partial.Config, &config); err != nil {
				return nil, err
			}

			return config, nil

		case string(Provider_OIDC):
			var config OIDCConfig
			if err := decodeConfig(partial.Config, &config); err != nil {
				return nil, err
			}

			return config, nil

		default:
			return nil, fmt.Errorf("unknown authentication provider type %v", partial.Type)
		}
	}

	return data, nil
}

func decodeConfig(data interface{}, out interface{}) error {
	d, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		Result:      out,
		ErrorUnused: true,
		DecodeHook:  mapstructure.StringToTimeHookFunc(time.RFC3339),
	})

	if err != nil {
		return err
	}

	return d.Decode(data)
}
