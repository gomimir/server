package auth

import (
	"context"
	"errors"
)

type contextKey struct {
	name string
}

var sessionCtxKey = &contextKey{"auth"}

func SessionContext(parent context.Context, session *Session) context.Context {
	return context.WithValue(parent, sessionCtxKey, session)
}

func SessionForContext(ctx context.Context) (*Session, error) {
	if session, ok := ctx.Value(sessionCtxKey).(*Session); ok {
		return session, nil
	}

	return nil, errors.New("unable to retrieve auth. session from context")
}
