package auth_test

import (
	"context"

	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

type testUserSynchronizer struct{}

func (*testUserSynchronizer) User(ctx context.Context, req auth.UserSyncRequest) (pulid.ID, error) {
	return pulid.ID(req.Username), nil
}
