package auth

import (
	"sort"
)

type OperationSet map[Operation]struct{}

func OpSet(operations ...Operation) OperationSet {
	opset := OperationSet{}
	opset.Add(operations...)
	return opset
}

func (opset OperationSet) Add(operations ...Operation) {
	for _, op := range operations {
		opset[op] = struct{}{}
	}
}

func (opset OperationSet) Merge(another OperationSet) {
	for op := range another {
		opset[op] = struct{}{}
	}
}

func (opset OperationSet) Contains(op Operation) bool {
	_, ok := opset[op]
	return ok
}

func (opset OperationSet) SortedArray() []Operation {
	var opArr []Operation
	for op := range opset {
		opArr = append(opArr, op)
	}

	sort.SliceStable(opArr, func(i, j int) bool { return opArr[i] < opArr[j] })
	return opArr
}

func (opset OperationSet) String() string {
	if len(opset) > 0 {
		str := ""
		for _, op := range opset.SortedArray() {
			str += ", " + string(op)
		}

		return str[2:]
	}

	return ""
}
