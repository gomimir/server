package auth

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/coreos/go-oidc/v3/oidc"
	"github.com/rs/zerolog/log"
	"golang.org/x/oauth2"
)

type OIDCConfig struct {
	URL string

	ClientID     string
	ClientSecret string

	SkipTokenIssuerCheck bool
	Now                  func() time.Time
}

func (OIDCConfig) Type() ProviderType {
	return Provider_OIDC
}

type claims struct {
	ScopesSupported []string `json:"scopes_supported"`
	ClaimsSupported []string `json:"claims_supported"`
}

type OAuthTokens struct {
	IDToken    string
	ValidUntil time.Time

	RefreshToken *string
}

type OIDCProvider struct {
	Config           OIDCConfig
	UserSynchronizer UserSynchronizer
	RoleMapper       RoleMapper

	oidcProvider *oidc.Provider
	claims       claims
	verifier     *oidc.IDTokenVerifier
}

func NewOIDCProvider(config OIDCConfig, userSynchronizer UserSynchronizer, roleMapper RoleMapper) (*OIDCProvider, error) {
	provider := &OIDCProvider{
		Config:           config,
		UserSynchronizer: userSynchronizer,
		RoleMapper:       roleMapper,
	}

	var err error
	provider.oidcProvider, err = oidc.NewProvider(context.Background(), config.URL)
	if err != nil {
		return nil, err
	}

	if err := provider.oidcProvider.Claims(&provider.claims); err != nil {
		return nil, err
	}

	provider.verifier = provider.oidcProvider.Verifier(&oidc.Config{
		ClientID:        config.ClientID,
		SkipIssuerCheck: config.SkipTokenIssuerCheck,
		Now:             config.Now,
	})

	return provider, nil
}

func (*OIDCProvider) Type() ProviderType {
	return Provider_OIDC
}

func (p *OIDCProvider) GenerateAuthLink(state string, redirectURL string) string {
	config := p.oauthConfigForRedirectURL(redirectURL)
	return config.AuthCodeURL(state)
}

func (p *OIDCProvider) Exchange(ctx context.Context, redirectURL string, code string) (*OAuthTokens, error) {
	config := p.oauthConfigForRedirectURL(redirectURL)
	token, err := config.Exchange(ctx, code)
	if err != nil {
		return nil, err
	}

	return parseToken(token)
}

func (p *OIDCProvider) RefreshToken(ctx context.Context, refreshToken string) (*OAuthTokens, error) {
	conf := p.oauthConfigForRedirectURL("")
	token, err := conf.TokenSource(ctx, &oauth2.Token{
		RefreshToken: refreshToken,
	}).Token()

	if err != nil {
		return nil, err
	}

	return parseToken(token)
}

func (p *OIDCProvider) WebsocketInit(ctx context.Context, initPayload map[string]interface{}) (context.Context, error) {
	timeoutCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	if authToken, ok := initPayload[WebsocketAuthTokenKey].(string); ok {
		session, err := p.sessionForToken(timeoutCtx, authToken)
		if err != nil {
			return nil, err
		}

		return SessionContext(ctx, session), nil
	}

	roleIDs, err := p.RoleMapper.GuestRoleIDs(timeoutCtx)
	if err != nil {
		return nil, err
	}

	session := NewSession(Session_AuthNotAttempted, roleIDs...)
	return SessionContext(ctx, session), nil
}

func (p *OIDCProvider) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var session *Session

		timeoutCtx, cancel := context.WithTimeout(r.Context(), 5*time.Second)
		defer cancel()

		auth := r.Header.Get("Authorization")
		if auth != "" {
			if strings.HasPrefix(auth, "Bearer ") {
				var err error

				session, err = p.sessionForToken(timeoutCtx, auth[7:])
				if err != nil {
					handleMiddlewareError(w, err)
					return
				}
			} else {
				log.Warn().Msg("Expected Bearer token")

				roleIDs, err := p.RoleMapper.GuestRoleIDs(timeoutCtx)
				if err != nil {
					handleMiddlewareError(w, err)
					return
				}

				session = NewSession(Session_AuthInvalid, roleIDs...)
			}
		} else {
			roleIDs, err := p.RoleMapper.GuestRoleIDs(timeoutCtx)
			if err != nil {
				handleMiddlewareError(w, err)
				return
			}

			session = NewSession(Session_AuthNotAttempted, roleIDs...)
		}

		ctx := SessionContext(r.Context(), session)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (p *OIDCProvider) oauthConfigForRedirectURL(redirectURL string) oauth2.Config {
	scopes := []string{oidc.ScopeOpenID}
	for _, scope := range p.claims.ScopesSupported {
		switch scope {
		case "profile":
			scopes = append(scopes, scope)
		case "groups":
			scopes = append(scopes, scope)
		}
	}

	return oauth2.Config{
		ClientID:     p.Config.ClientID,
		ClientSecret: p.Config.ClientSecret,
		Endpoint:     p.oidcProvider.Endpoint(),
		RedirectURL:  redirectURL,
		Scopes:       scopes,
	}
}

func (p *OIDCProvider) sessionForToken(ctx context.Context, token string) (*Session, error) {
	idToken, err := p.verifier.Verify(ctx, token)
	if err != nil {
		log.Debug().Err(err).Msg("Unable to verify token")

		roleIDs, err := p.RoleMapper.GuestRoleIDs(ctx)
		if err != nil {
			return nil, err
		}

		return NewSession(Session_AuthInvalid, roleIDs...), nil
	}

	var claims struct {
		Name              string   `json:"name"`
		PreferredUsername string   `json:"preferred_username"`
		Groups            []string `json:"groups"`
	}

	err = idToken.Claims(&claims)
	if err != nil {
		log.Error().Err(err).Msg("Unable to parse token claims")
		return nil, err
	}

	roleIDs, err := p.RoleMapper.AuthenticatedRoleIDs(ctx, claims.Groups...)
	if err != nil {
		return nil, err
	}

	userSyncReq := UserSyncRequest{
		Username: claims.PreferredUsername,
		RoleIDs:  roleIDs,
	}

	if claims.Name != "" {
		userSyncReq.DisplayName = &claims.Name
	}

	userID, err := p.UserSynchronizer.User(ctx, userSyncReq)

	if err != nil {
		return nil, err
	}

	session := NewAuthenticatedSession(userID, &idToken.Expiry, roleIDs...)

	return session, nil
}

func parseToken(token *oauth2.Token) (*OAuthTokens, error) {
	if idToken, ok := token.Extra("id_token").(string); ok {
		tokens := OAuthTokens{
			IDToken:    idToken,
			ValidUntil: token.Expiry,
		}

		if token.RefreshToken != "" {
			tokens.RefreshToken = &token.RefreshToken
		}

		return &tokens, nil
	}

	return nil, fmt.Errorf("unable to read id token")
}
