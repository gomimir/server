package auth

import (
	"context"
	"net/http"
	"strings"

	"github.com/rs/zerolog/log"
)

type ProxyConfig struct {
	UserHeader  string `mapstructure:"userHeader"`
	RolesHeader string `mapstructure:"rolesHeader"`
}

func (ProxyConfig) Type() ProviderType {
	return Provider_Proxy
}

type ProxyProvider struct {
	Config           ProxyConfig
	UserSynchronizer UserSynchronizer
	RoleMapper       RoleMapper
}

func NewProxyProvider(config ProxyConfig, userSynchronizer UserSynchronizer, roleMapper RoleMapper) (*ProxyProvider, error) {
	return &ProxyProvider{
		Config:           config,
		UserSynchronizer: userSynchronizer,
		RoleMapper:       roleMapper,
	}, nil
}

func (*ProxyProvider) Type() ProviderType {
	return Provider_Proxy
}

func (p *ProxyProvider) WebsocketInit(ctx context.Context, initPayload map[string]interface{}) (context.Context, error) {
	// Nothing to do, it should already be there because of Middleware()
	// Reverse proxy should be adding headers to everything, even web sockets
	return ctx, nil
}

func (p *ProxyProvider) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var session *Session
		if p.Config.UserHeader != "" {
			username := r.Header.Get(p.Config.UserHeader)
			if username != "" {

				var additionalRoleNames []string
				if p.Config.RolesHeader != "" {
					roles := r.Header.Get(p.Config.RolesHeader)
					if roles != "" {
						additionalRoleNames = strings.Split(roles, ",")
					}
				}

				roleIDs, err := p.RoleMapper.AuthenticatedRoleIDs(r.Context(), additionalRoleNames...)
				if err != nil {
					handleMiddlewareError(w, err)
					return
				}

				userID, err := p.UserSynchronizer.User(r.Context(), UserSyncRequest{
					Username: username,
					RoleIDs:  roleIDs,
				})

				if err != nil {
					handleMiddlewareError(w, err)
					return
				}

				session = NewAuthenticatedSession(userID, nil, roleIDs...)
			}
		}

		if session == nil {
			roleIDs, err := p.RoleMapper.GuestRoleIDs(r.Context())
			if err != nil {
				handleMiddlewareError(w, err)
				return
			}

			session = NewSession(Session_AuthNotAttempted, roleIDs...)
		}

		ctx := SessionContext(r.Context(), session)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func handleMiddlewareError(w http.ResponseWriter, err error) {
	log.Error().Err(err).Msg("Auth middleware failed")
	w.WriteHeader(500)
}
