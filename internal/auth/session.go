package auth

import (
	"time"

	"gitlab.com/gomimir/server/internal/ent/schema/pulid"
)

type SessionState string

const (
	Session_AuthNotAttempted SessionState = "AUTHENTICATION_NOT_ATTEMPTED"
	Session_AuthInvalid      SessionState = "AUTHENTICATION_INVALID"
	Session_Authenticated    SessionState = "AUTHENTICATED"
)

type Session struct {
	state     SessionState
	userID    pulid.ID
	roleIDs   []pulid.ID
	expiresIn *time.Time
}

type SessionOpts struct {
	RoleIDs []pulid.ID
}

type AuthenticatedSessionOpts struct {
	UserID    pulid.ID
	ExpiresIn *time.Time
}

func NewSession(state SessionState, roleIDs ...pulid.ID) *Session {
	return &Session{
		state:   state,
		roleIDs: roleIDs,
	}
}

func NewAuthenticatedSession(userID pulid.ID, expiresIn *time.Time, roleIDs ...pulid.ID) *Session {
	return &Session{
		state:     Session_Authenticated,
		userID:    userID,
		roleIDs:   roleIDs,
		expiresIn: expiresIn,
	}
}

func (s *Session) UserID() pulid.ID {
	return s.userID
}

func (s *Session) RoleIDs() []pulid.ID {
	return s.roleIDs
}

func (s *Session) Identities() []interface{} {
	var identitiesAsInterfaces []interface{}

	if s.userID != "" {
		identitiesAsInterfaces = append(identitiesAsInterfaces, s.userID)
	}

	for _, identity := range s.roleIDs {
		identitiesAsInterfaces = append(identitiesAsInterfaces, identity)
	}

	return identitiesAsInterfaces
}

func (s *Session) State() SessionState {
	return s.state
}

func (s *Session) IsAuthenticated() bool {
	return s.state == Session_Authenticated
}

func (s *Session) ExpiresIn() *time.Time {
	return s.expiresIn
}
