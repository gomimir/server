package auth

type ResourceType string

const (
	RT_Index      ResourceType = "INDEX"
	RT_Asset      ResourceType = "ASSET"
	RT_Repository ResourceType = "REPOSITORY"
	RT_Job        ResourceType = "JOB"
	RT_User       ResourceType = "USER"
	RT_UserRole   ResourceType = "USER_ROLE"
)

var AllResourceTypes = []ResourceType{
	RT_Index,
	RT_Asset,
	RT_Repository,
	RT_Job,
	RT_User,
	RT_UserRole,
}
