package auth_test

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/server/internal/auth"
	"gitlab.com/gomimir/server/internal/ent/schema/pulid"

	_ "embed"
)

//go:embed "fixtures/oidc.json"
var oidConfig string

//go:embed "fixtures/jwks.json"
var jwks []byte

func TestOIDCProvider_ValidToken(t *testing.T) {
	ts := testServer()
	defer ts.Close()

	provider, err := auth.NewOIDCProvider(auth.OIDCConfig{
		URL:                  ts.URL,
		ClientID:             "myapp",
		SkipTokenIssuerCheck: true,
		Now: func() time.Time {
			return time.Date(2022, 1, 23, 19, 52, 11, 0, time.UTC)
		},
	}, &testUserSynchronizer{}, &testRoleMapper{})

	if !assert.NoError(t, err) {
		return
	}

	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/", nil)
	r.Header.Add("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjEyMjRjNCIsInR5cCI6IkpXVCJ9.eyJhdF9oYXNoIjoiUjlFc2ZPZlJNVXlFNUpRYThHaGQ1ZyIsImF1ZCI6WyJteWFwcCJdLCJhdXRoX3RpbWUiOjE2NDI5NjM4MzMsImV4cCI6MTY0Mjk2ODExMSwiZ3JvdXBzIjpbImFkbWlucyIsImRldiJdLCJpYXQiOjE2NDI5NjQ1MTEsImlzcyI6Imh0dHBzOi8vYXV0aGVsaWEuZXhhbXBsZS5jb20iLCJqdGkiOiIyYTZkYWRlZS0xZDQ2LTRkYzItYmI1MS1lMWRhYjRkNjg1NzciLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhdXRoZWxpYSIsInJhdCI6MTY0Mjk2NDQ5Miwic3ViIjoiYXV0aGVsaWEifQ.NXdMZZMZFG3YQ4WjqcgR2OnDG259z1JYXLCaFTCU4Aialv9Mvb5G2c6OEF2dEeUsuUA-PWBVfWOuDALIcEpwAOCP1_waHCDJZTAdecpW8n7XrEE9a4BULGI6IeErWJ9a1IQhCJox-fxfRweRbItUYvY6tJnpoOf8Y4RN9zPS9ijUEwVzY4S4y2wm5JatjSmcgql52HAskUKJ4HUq6SbfFzaWPrNkk72MOFBDTWlav5j46jS_VlZpqe0dFtYWTg_vVQTdeQl6yxile9LHKDb4szxaz3oRic6NW-gelZZTQpnssBhmwOXR47IqIzBIO_6-PaVSJsczOs_izp1-WmCpFg")

	provider.Middleware(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, err := auth.SessionForContext(r.Context())
		if assert.NoError(t, err) {
			assert.True(t, session.IsAuthenticated())
			assert.Equal(t, pulid.ID("authelia"), session.UserID())
			assert.Equal(t, []pulid.ID{"admins", "dev"}, session.RoleIDs())
			w.WriteHeader(200)
			return
		}

		w.WriteHeader(500)
	})).ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
}

func TestOIDCProvider_ExpiredToken(t *testing.T) {
	ts := testServer()
	defer ts.Close()

	provider, err := auth.NewOIDCProvider(auth.OIDCConfig{
		URL:                  ts.URL,
		ClientID:             "myapp",
		SkipTokenIssuerCheck: true,
		Now: func() time.Time {
			return time.Date(2022, 1, 30, 23, 52, 11, 0, time.UTC)
		},
	}, &testUserSynchronizer{}, &testRoleMapper{})

	if !assert.NoError(t, err) {
		return
	}

	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/", nil)
	r.Header.Add("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjEyMjRjNCIsInR5cCI6IkpXVCJ9.eyJhdF9oYXNoIjoiUjlFc2ZPZlJNVXlFNUpRYThHaGQ1ZyIsImF1ZCI6WyJteWFwcCJdLCJhdXRoX3RpbWUiOjE2NDI5NjM4MzMsImV4cCI6MTY0Mjk2ODExMSwiZ3JvdXBzIjpbImFkbWlucyIsImRldiJdLCJpYXQiOjE2NDI5NjQ1MTEsImlzcyI6Imh0dHBzOi8vYXV0aGVsaWEuZXhhbXBsZS5jb20iLCJqdGkiOiIyYTZkYWRlZS0xZDQ2LTRkYzItYmI1MS1lMWRhYjRkNjg1NzciLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhdXRoZWxpYSIsInJhdCI6MTY0Mjk2NDQ5Miwic3ViIjoiYXV0aGVsaWEifQ.NXdMZZMZFG3YQ4WjqcgR2OnDG259z1JYXLCaFTCU4Aialv9Mvb5G2c6OEF2dEeUsuUA-PWBVfWOuDALIcEpwAOCP1_waHCDJZTAdecpW8n7XrEE9a4BULGI6IeErWJ9a1IQhCJox-fxfRweRbItUYvY6tJnpoOf8Y4RN9zPS9ijUEwVzY4S4y2wm5JatjSmcgql52HAskUKJ4HUq6SbfFzaWPrNkk72MOFBDTWlav5j46jS_VlZpqe0dFtYWTg_vVQTdeQl6yxile9LHKDb4szxaz3oRic6NW-gelZZTQpnssBhmwOXR47IqIzBIO_6-PaVSJsczOs_izp1-WmCpFg")

	provider.Middleware(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, err := auth.SessionForContext(r.Context())
		if assert.NoError(t, err) {
			assert.False(t, session.IsAuthenticated())
			assert.Equal(t, pulid.ID(""), session.UserID())
			assert.Equal(t, []pulid.ID(nil), session.RoleIDs())
			w.WriteHeader(200)
			return
		}

		w.WriteHeader(500)
	})).ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
}

func TestOIDCProvider_NoToken(t *testing.T) {
	ts := testServer()
	defer ts.Close()

	provider, err := auth.NewOIDCProvider(auth.OIDCConfig{
		URL:                  ts.URL,
		ClientID:             "myapp",
		SkipTokenIssuerCheck: true,
		Now: func() time.Time {
			return time.Date(2022, 1, 30, 23, 52, 11, 0, time.UTC)
		},
	}, &testUserSynchronizer{}, &testRoleMapper{})

	if !assert.NoError(t, err) {
		return
	}

	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/", nil)

	provider.Middleware(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, err := auth.SessionForContext(r.Context())
		if assert.NoError(t, err) {
			assert.False(t, session.IsAuthenticated())
			assert.Equal(t, pulid.ID(""), session.UserID())
			assert.Equal(t, []pulid.ID(nil), session.RoleIDs())
			w.WriteHeader(200)
			return
		}

		w.WriteHeader(500)
	})).ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
}

func testServer() *httptest.Server {
	var ts *httptest.Server
	ts = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/.well-known/openid-configuration" {
			_, err := w.Write([]byte(strings.ReplaceAll(oidConfig, "https://authelia.example.com", ts.URL)))
			if err != nil {
				panic(err)
			}

			return
		}

		if r.URL.Path == "/api/oidc/jwks" {
			_, err := w.Write(jwks)
			if err != nil {
				panic(err)
			}

			return
		}

		w.WriteHeader(404)
	}))

	return ts
}
