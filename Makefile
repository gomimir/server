TAG_COMMIT := $(shell git rev-list --abbrev-commit --tags --max-count=1)
TAG := $(shell git describe --abbrev=0 --tags ${TAG_COMMIT} 2>/dev/null || true)
COMMIT := $(shell git rev-parse --short HEAD)
DATE := $(shell git log -1 --format=%cd --date=format:"%Y%m%d")
VERSION := $(TAG:v%=%)

ifeq ($(VERSION),)
    VERSION := 0.0.1-next-$(COMMIT)-$(DATE)
else
	ifneq ($(COMMIT), $(TAG_COMMIT))
			VERSION := $(VERSION)-next-$(COMMIT)-$(DATE)
	endif
endif
ifneq ($(shell git status --porcelain),)
    VERSION := $(VERSION)-dirty
endif

PLATFORMS := linux/amd64 linux/arm64
FLAGS := -ldflags "-X main.version=$(VERSION)"
SERVER_INPUT := cmd/server/server.go

temp = $(subst /, ,$@)
os = $(word 1, $(temp))
arch = $(word 2, $(temp))

version:
	@echo Version: $(VERSION)

test:
	go test $(FLAGS) ./...

coverage:
	go test $(FLAGS) ./... -coverprofile=coverage.out
	go tool cover -html=coverage.out

build:
	CGO_ENABLED=0 go build $(FLAGS) -o bin/mimir-server $(SERVER_INPUT)

docker:
	docker build . --tag registry.gitlab.com/gomimir/server:v$(VERSION) --build-arg VERSION=$(VERSION)

run:
	go run $(FLAGS) $(SERVER_INPUT) serve

gqlgen:
	go generate ./internal/graphql

entgen:
	go generate ./internal/ent

gqlschema:
	find internal/graphql -name '*.graphqls' -print0 | xargs -0 -I file bash -c 'cat file; echo' > schema.graphql

release: $(PLATFORMS)

$(PLATFORMS):
	CGO_ENABLED=0 GOOS=$(os) GOARCH=$(arch) go build $(FLAGS) -o 'bin/release/$(@)/mimir-server' $(SERVER_INPUT)

.PHONY: version test build docker run release gqlgen $(PLATFORMS)