FROM golang:1.16-buster AS build

ADD go.mod /app/
ADD go.sum /app/

WORKDIR /app
RUN go mod download

ADD cmd /app/cmd
ADD internal /app/internal

ARG VERSION
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -ldflags "-X main.version=$VERSION" -o ./bin/mimir-server ./cmd/server/server.go

FROM gcr.io/distroless/static
COPY --from=build /app/bin/mimir-server /mimir-server
CMD ["/mimir-server", "serve"]