# Mimir server

Mimir server is the core of the platform. It takes care of file indexing and distributing tasks across processors. It provides GraphQL API to its clients.

For more information about how to use it see [documentation](https://gomimir.gitlab.io).

## Development

Server requires several dependencies in order to run. You can start them all by provided docker-compose.yml file.

```bash
docker-compose up -d
```

Once the services are up and running, you can start the app by:

```bash
make run
```

Before you push, you should check your work is passing all the tests:

```bash
make test
# or to update test snapshots
UPDATE_SNAPSHOTS=true make test
```

You can make a docker image to test your work in deployed environment:

```bash
make docker
```

# Ent

## Initialize new entity 

```bash
go run entgo.io/ent/cmd/ent init --target internal/ent/schema EntityName
```