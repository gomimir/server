// +build tools

package tools

import (
	_ "entgo.io/ent/cmd/ent"
	_ "github.com/99designs/gqlgen/cmd"
	_ "github.com/davecgh/go-spew/spew"
)
